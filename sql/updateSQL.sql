-- UPDATE DATE 09-26-2013 --
ALTER TABLE `candidate_invitations` ADD COLUMN `is_approved` ENUM('Y','N') NOT NULL AFTER `employer_user_id`;
ALTER TABLE `candidate_invitations` CHANGE COLUMN `is_approved` `is_viewed` ENUM('Y','N') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'N';
ALTER TABLE `patsada_jobs`.`login_credentials` MODIFY COLUMN `is_candidate` ENUM('Y','N') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'N',
MODIFY COLUMN `is_employer` ENUM('Y','N') CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'N';
ALTER TABLE `candidate` DROP COLUMN `middle_name`;

-- UPDATE DATE 09-26-2013 2nd--
ALTER TABLE `patsada_jobs`.`candidate` MODIFY COLUMN `avatar` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci,
 MODIFY COLUMN `birthdate` DATE DEFAULT NULL,
 MODIFY COLUMN `gender` ENUM('Male','Female') CHARACTER SET latin1 COLLATE latin1_swedish_ci,
 MODIFY COLUMN `city` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `province` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `country` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;
 
 -- UPDATE DATE 09-27-2013 1st--
 ALTER TABLE `employer_user` MODIFY COLUMN `title` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `first_name` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `last_name` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 DROP COLUMN `middle_name`,
 MODIFY COLUMN `address` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `city` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `province` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `zip_code` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `country` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
 MODIFY COLUMN `position` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;
  -- UPDATE DATE 09-27-2013 2nd--
 ALTER TABLE `employer_user` MODIFY COLUMN `is_manager` ENUM('Y','N') NOT NULL DEFAULT 'Y',
 ADD COLUMN `is_confirmed` ENUM('Y','N') NOT NULL DEFAULT 'N' AFTER `position`;
 
   -- UPDATE DATE 09-28-2013 1st--
 ALTER TABLE `candidate` DROP COLUMN `gender`,
 ADD COLUMN `facebook_url` VARCHAR(200) AFTER `country`,
 ADD COLUMN `twitter_url` VARCHAR(200) AFTER `facebook_url`,
 ADD COLUMN `g_plus_url` VARCHAR(200) AFTER `twitter_url`,
 ADD COLUMN `zip_code` VARCHAR(45) AFTER `g_plus_url`;
    -- UPDATE DATE 09-28-2013 2nd--
 CREATE TABLE  `candidate_objective` (
  `candidate_objective_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `candidate_id` int(10) unsigned NOT NULL,
  `objective` varchar(45) NOT NULL,
  PRIMARY KEY (`candidate_objective_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

   -- UPDATE DATE 09-30-2013 1st--
ALTER TABLE `candidate_objective` MODIFY COLUMN `objective` LONGTEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;

 -- UPDATE DATE 10-1-2013 1st--
ALTER TABLE `candidate` ADD COLUMN `category_name` VARCHAR(200) AFTER `zip_code`;

 -- UPDATE DATE 10-3-2013 1st--
ALTER TABLE `employer` ADD COLUMN `video_url` VARCHAR(200) NOT NULL AFTER `company_logo`;
ALTER TABLE `employer` MODIFY COLUMN `video_url` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;

 
