-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2015 at 03:18 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `patsada_jobs`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidate`
--

CREATE TABLE IF NOT EXISTS `candidate` (
`candidate_id` int(10) unsigned NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `telephone` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `resume_id` int(10) unsigned DEFAULT NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `is_active` enum('Y','N') DEFAULT NULL,
  `is_confirmed` enum('Y','N') DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `skype_name` varchar(100) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `province` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `facebook_url` varchar(200) DEFAULT NULL,
  `twitter_url` varchar(200) DEFAULT NULL,
  `g_plus_url` varchar(200) DEFAULT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  `category_name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidate`
--

INSERT INTO `candidate` (`candidate_id`, `first_name`, `last_name`, `address`, `telephone`, `mobile`, `resume_id`, `avatar`, `birthdate`, `email`, `is_active`, `is_confirmed`, `title`, `skype_name`, `city`, `province`, `country`, `facebook_url`, `twitter_url`, `g_plus_url`, `zip_code`, `category_name`) VALUES
(1, 'Andrelin', 'Pareno', '', NULL, NULL, NULL, NULL, NULL, 'andrelinable@gmail.com', 'Y', 'N', 'Ms.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '', '', '', NULL, NULL, NULL, NULL, NULL, 'jhai@gmail.com', 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Maiden Flor', 'Caumban', '', '8561234', '09182454356', NULL, NULL, '1994-08-08', 'maiden@gmail.com', 'Y', 'Y', 'Ms.', NULL, 'Cagayan de Oro City', 'Misamis Oriental', 'Philippines', NULL, NULL, NULL, '9000', 'Computer/IT');

-- --------------------------------------------------------

--
-- Table structure for table `candidate_education`
--

CREATE TABLE IF NOT EXISTS `candidate_education` (
`candidate_education_id` int(11) NOT NULL,
  `candidate_id` varchar(255) NOT NULL,
  `school` varchar(254) NOT NULL,
  `address` varchar(254) NOT NULL,
  `degree` varchar(250) NOT NULL,
  `education_attainment` varchar(250) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `candidate_experience`
--

CREATE TABLE IF NOT EXISTS `candidate_experience` (
`candidate_experience_id` int(11) NOT NULL,
  `candidate_id` varchar(255) NOT NULL,
  `position` varchar(254) NOT NULL,
  `company_name` varchar(254) NOT NULL,
  `company_address` varchar(254) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `role` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `candidate_invitations`
--

CREATE TABLE IF NOT EXISTS `candidate_invitations` (
`candidate_invitations_id` int(10) unsigned NOT NULL,
  `candidate_id` int(10) unsigned NOT NULL,
  `employer_id` int(10) unsigned NOT NULL,
  `invitaion_content` longtext NOT NULL,
  `created_datetime` datetime NOT NULL,
  `job_post_id` int(10) unsigned NOT NULL,
  `employer_user_id` int(10) unsigned NOT NULL,
  `is_viewed` enum('Y','N') DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `candidate_objective`
--

CREATE TABLE IF NOT EXISTS `candidate_objective` (
`candidate_objective_id` int(10) unsigned NOT NULL,
  `candidate_id` int(10) unsigned NOT NULL,
  `objective` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `candidate_recommendation`
--

CREATE TABLE IF NOT EXISTS `candidate_recommendation` (
`candidate_recommendation_id` int(10) unsigned NOT NULL,
  `candidate_id` varchar(45) DEFAULT '0',
  `title` varchar(45) DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `is_accepted` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_datetime` datetime NOT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `employer_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`category_id` int(10) unsigned NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `created_datetime` datetime NOT NULL,
  `is_parent` enum('Y','N') NOT NULL DEFAULT 'N',
  `category_parent_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `created_by`, `created_datetime`, `is_parent`, `category_parent_id`) VALUES
(1, 'Accounting/Finance', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(2, 'Admin/HR', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(3, 'Computer/IT', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(4, 'Enineering', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(5, 'Sales/Marketing', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(6, 'Manufacturing', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(7, 'Arts/Media/Comm', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(8, 'Building/Contruction', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(9, 'Service', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(10, 'Hotel/Restaurant', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(11, 'Sciences', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(12, 'Healthcare', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(13, 'Others', 'system', '2013-08-06 00:00:00', 'Y', NULL),
(14, 'General/Cost Acct', 'system', '2013-08-06 00:00:00', 'N', 1),
(15, 'Banking/Financial', 'system', '2013-08-06 00:00:00', 'N', 1),
(16, 'Audit & Taxation', 'system', '2013-08-06 00:00:00', 'N', 1),
(17, 'Finance/Investment', 'system', '2013-08-06 00:00:00', 'N', 1),
(18, 'Clerical/Administrative', 'system', '2013-08-06 00:00:00', 'N', 2),
(19, 'Human Resources', 'system', '2013-08-06 00:00:00', 'N', 2),
(20, 'Secretarial', 'system', '2013-08-06 00:00:00', 'N', 2),
(21, 'Top Management', 'system', '2013-08-06 00:00:00', 'N', 2),
(22, 'Software', 'system', '2013-08-06 00:00:00', 'N', 3),
(23, 'Hardware', 'system', '2013-08-06 00:00:00', 'N', 3),
(24, 'Secretarial', 'system', '2013-08-06 00:00:00', 'N', 3),
(25, 'Mechanical/Automotive', 'system', '2013-08-06 00:00:00', 'N', 4),
(26, 'Other Eng', 'system', '2013-08-06 00:00:00', 'N', 4),
(27, 'Electrical Eng', 'system', '2013-08-06 00:00:00', 'N', 4),
(28, 'Environmental', 'system', '2013-08-06 00:00:00', 'N', 4),
(29, 'Oil/Gas Eng', 'system', '2013-08-06 00:00:00', 'N', 4),
(30, 'Industrial Eng', 'system', '2013-08-06 00:00:00', 'N', 4),
(31, 'Chemical Eng', 'system', '2013-08-06 00:00:00', 'N', 4),
(32, 'Sales - Financial Services', 'system', '2013-08-06 00:00:00', 'N', 5),
(33, 'Marketing', 'system', '2013-08-06 00:00:00', 'N', 5),
(34, 'Retail Sales', 'system', '2013-08-06 00:00:00', 'N', 5),
(35, 'Telesales/Telemarketing', 'system', '2013-08-06 00:00:00', 'N', 5),
(36, 'Sales - Corporate', 'system', '2013-08-06 00:00:00', 'N', 5),
(37, 'Sales - Eng/Tech/IT', 'system', '2013-08-06 00:00:00', 'N', 5),
(38, 'Merchandising', 'system', '2013-08-06 00:00:00', 'N', 5),
(39, 'Manufacturing', 'system', '2013-08-06 00:00:00', 'N', 6),
(40, 'Maintenance', 'system', '2013-08-06 00:00:00', 'N', 6),
(41, 'Quality Assurance', 'system', '2013-08-06 00:00:00', 'N', 6),
(42, 'Purchasing', 'system', '2013-08-06 00:00:00', 'N', 6),
(43, 'Process Design & Control', 'system', '2013-08-06 00:00:00', 'N', 6),
(44, 'Arts/Creative Design', 'system', '2013-08-06 00:00:00', 'N', 7),
(45, 'Advertising', 'system', '2013-08-06 00:00:00', 'N', 7),
(46, 'Public Relations', 'system', '2013-08-06 00:00:00', 'N', 7),
(47, 'Entertainment', 'system', '2013-08-06 00:00:00', 'N', 7),
(48, 'Civil Eng/Construction', 'system', '2013-08-06 00:00:00', 'N', 8),
(49, 'Architect/Interior', 'system', '2013-08-06 00:00:00', 'N', 8),
(50, 'Property/Real Estate', 'system', '2013-08-06 00:00:00', 'N', 8),
(51, 'Quantity Survey', 'system', '2013-08-06 00:00:00', 'N', 8),
(52, 'Customer Service', 'system', '2013-08-06 00:00:00', 'N', 9),
(53, 'Tech & Helpdesk Support', 'system', '2013-08-06 00:00:00', 'N', 9),
(54, 'Personal Care', 'system', '2013-08-06 00:00:00', 'N', 9),
(55, 'Logistics/Supply Chain', 'system', '2013-08-06 00:00:00', 'N', 9),
(56, 'Security/Armed Forces', 'system', '2013-08-06 00:00:00', 'N', 9),
(57, 'Law/Legal Services', 'system', '2013-08-06 00:00:00', 'N', 9),
(58, 'Social Services', 'system', '2013-08-06 00:00:00', 'N', 9),
(59, 'F & B ', 'system', '2013-08-06 00:00:00', 'N', 10),
(60, 'Hotel/Tourism', 'system', '2013-08-06 00:00:00', 'N', 10),
(61, 'Food Tech/Nutritionist', 'system', '2013-08-06 00:00:00', 'N', 11),
(62, 'Agriculture & Dev.', 'system', '2013-08-06 00:00:00', 'N', 11),
(63, 'Chemistry', 'system', '2013-08-06 00:00:00', 'N', 11),
(64, 'Aviation', 'system', '2013-08-06 00:00:00', 'N', 11),
(65, 'Actuarial/Statistics', 'system', '2013-08-06 00:00:00', 'N', 11),
(66, 'Science & Tech', 'system', '2013-08-06 00:00:00', 'N', 11),
(67, 'Geology', 'system', '2013-08-06 00:00:00', 'N', 11),
(68, 'Biotechnology', 'system', '2013-08-06 00:00:00', 'N', 11),
(69, 'Nurse/Medical Support', 'system', '2013-08-06 00:00:00', 'N', 12),
(70, 'Doctor/Diagnosis', 'system', '2013-08-06 00:00:00', 'N', 12),
(71, 'Pharmacy', 'system', '2013-08-06 00:00:00', 'N', 12),
(72, 'General Work', 'system', '2013-08-06 00:00:00', 'N', 13),
(73, 'Others', 'system', '2013-08-06 00:00:00', 'N', 13),
(74, 'Journalist/Editors', 'system', '2013-08-06 00:00:00', 'N', 13),
(75, 'Publishing', 'system', '2013-08-06 00:00:00', 'N', 13);

-- --------------------------------------------------------

--
-- Table structure for table `employer`
--

CREATE TABLE IF NOT EXISTS `employer` (
`employer_id` int(10) unsigned NOT NULL,
  `authentication_code` varchar(20) NOT NULL,
  `company_code` varchar(20) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_description` text NOT NULL,
  `company_address` varchar(200) NOT NULL,
  `company_city` varchar(100) NOT NULL,
  `company_province` varchar(100) NOT NULL,
  `company_zip_code` varchar(10) NOT NULL,
  `company_country` varchar(100) NOT NULL,
  `phone_no` varchar(45) NOT NULL,
  `fax_no` varchar(45) NOT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `is_confirmed` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_datetime` datetime NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `company_logo` varchar(200) NOT NULL,
  `video_url` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employer`
--

INSERT INTO `employer` (`employer_id`, `authentication_code`, `company_code`, `company_name`, `company_description`, `company_address`, `company_city`, `company_province`, `company_zip_code`, `company_country`, `phone_no`, `fax_no`, `is_active`, `is_confirmed`, `created_datetime`, `url`, `company_logo`, `video_url`) VALUES
(13, '6kDjUmoE3Z', 'YYYS1DBE1', 'XYZ', '', '', '', '', '', '', '', '', 'Y', 'N', '0000-00-00 00:00:00', NULL, '', NULL),
(14, 'y1SIANh66k', 'BBCFOX9VX', 'ABC', '', '', '', '', '', '', '', '', 'Y', 'N', '0000-00-00 00:00:00', NULL, '', NULL),
(15, 'gnNg95TF0q', 'aabLB829N', 'abcdefg', '', '', '', '', '', '', '', '', 'Y', 'N', '0000-00-00 00:00:00', NULL, '', NULL),
(16, '', 'eeeAHQ34D', 'wewe', '', '', '', '', '', '', '', '', 'Y', 'N', '0000-00-00 00:00:00', NULL, '', NULL),
(17, '7NSW8YmePK', 'cop1CTGMD', 'andrelinable company', '', '', '', '', '', '', '', '', 'Y', 'Y', '0000-00-00 00:00:00', NULL, '', NULL),
(18, 'bTCAjwWzMq', 'XWX8C3A2T', 'WX', '', '', '', '', '', '', '', '', 'Y', 'N', '0000-00-00 00:00:00', NULL, '', NULL),
(19, 'ClKAyfKhuZ', 'dadAM3SBD', 'abcd', '', '', '', '', '', '', '', '', 'Y', 'N', '0000-00-00 00:00:00', NULL, '', NULL),
(20, 'DAc8HSq7vn', 'PsoFB58Z0', 'JobsPH', '', '', '', '', '', '', '', '', 'Y', 'N', '0000-00-00 00:00:00', NULL, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employer_user`
--

CREATE TABLE IF NOT EXISTS `employer_user` (
`employer_user_id` int(10) unsigned NOT NULL,
  `employer_id` int(10) unsigned NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `created_datetime` datetime NOT NULL,
  `email` varchar(100) NOT NULL,
  `skype_name` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `province` varchar(45) DEFAULT NULL,
  `zip_code` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `is_manager` enum('Y','N') NOT NULL DEFAULT 'Y',
  `position` varchar(100) DEFAULT NULL,
  `is_confirmed` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employer_user`
--

INSERT INTO `employer_user` (`employer_user_id`, `employer_id`, `title`, `first_name`, `last_name`, `is_active`, `created_datetime`, `email`, `skype_name`, `address`, `city`, `province`, `zip_code`, `country`, `is_manager`, `position`, `is_confirmed`) VALUES
(14, 12, NULL, NULL, NULL, 'Y', '2015-01-15 11:50:40', 'andrelinable@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y'),
(16, 13, NULL, NULL, NULL, 'Y', '2015-01-16 01:15:02', 'meusck1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y'),
(17, 15, NULL, NULL, NULL, 'Y', '2015-01-16 01:17:02', 'asfk@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y'),
(18, 16, NULL, NULL, NULL, 'Y', '2015-01-19 10:07:18', 'etk@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'N'),
(19, 17, NULL, NULL, NULL, 'Y', '2015-01-19 11:02:27', 'andrelin123@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y'),
(20, 18, NULL, NULL, NULL, 'Y', '2015-01-20 11:10:18', 'meusck@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y'),
(21, 19, NULL, NULL, NULL, 'Y', '2015-01-23 01:37:04', 'abcd123@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y'),
(22, 20, NULL, NULL, NULL, 'Y', '2015-01-29 13:30:27', 'meusck12@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `job_post`
--

CREATE TABLE IF NOT EXISTS `job_post` (
`job_post_id` int(10) unsigned NOT NULL,
  `jobtype` varchar(100) NOT NULL,
  `category_id` varchar(254) NOT NULL,
  `position` varchar(254) NOT NULL,
  `location` varchar(254) NOT NULL,
  `experience` varchar(100) NOT NULL,
  `job_content` longtext NOT NULL,
  `created_date_time` datetime NOT NULL,
  `salary_type` enum('R','N','T','A') NOT NULL,
  `salary_from` decimal(20,2) NOT NULL,
  `salary_to` decimal(20,2) NOT NULL,
  `employer_id` int(10) unsigned NOT NULL,
  `expiration_datetime` datetime DEFAULT NULL,
  `is_paid` enum('Y','N') NOT NULL,
  `is_urgent` enum('Y','N') NOT NULL,
  `is_active` enum('Y','N') NOT NULL,
  `is_confirm` enum('Y','N') NOT NULL,
  `is_premium` enum('Y','N') NOT NULL,
  `request_deletion` enum('N','Y') DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_post`
--

INSERT INTO `job_post` (`job_post_id`, `jobtype`, `category_id`, `position`, `location`, `experience`, `job_content`, `created_date_time`, `salary_type`, `salary_from`, `salary_to`, `employer_id`, `expiration_datetime`, `is_paid`, `is_urgent`, `is_active`, `is_confirm`, `is_premium`, `request_deletion`) VALUES
(8, 'Full-Time', '6', 'Secretary', 'CDO', '1', 'Blah blah blah', '2015-01-15 03:38:14', 'R', '10000.00', '20000.00', 13, '2015-02-13 03:14:38', 'N', 'Y', 'N', 'Y', 'N', 'Y'),
(9, 'Part-Time', '6', 'Factory Worker', 'Cebu', '0.5', 'Must have pleasing personality', '2015-01-15 05:04:07', 'T', '10000.00', '15000.00', 13, '2015-02-13 05:07:04', 'N', 'Y', 'N', 'Y', 'N', 'N'),
(10, 'Fulltime', '19', ' Janitor', 'Cagayan', '1 to 2yrs', '<p>Clean my room</p>', '2015-01-19 04:10:24', 'R', '0.00', '0.00', 12, '2015-02-17 04:10:24', 'N', 'N', 'Y', 'Y', 'N', ''),
(11, 'Part-Time', '7', 'VJ', 'CDO', '2', 'Your time to shine', '2015-01-20 04:05:18', 'R', '15000.00', '18500.00', 18, '2015-02-18 04:18:05', 'N', 'Y', 'N', 'Y', 'N', 'N'),
(12, 'Part-Time', '7', 'VJ', 'CDO', '2', 'Your time to shine', '2015-01-20 05:18:10', 'R', '15000.00', '18500.00', 18, '2015-02-18 05:10:18', 'N', 'Y', 'N', 'N', 'N', 'N'),
(13, 'Contract', '37', ' Developer', 'Cagayan', '1 to 3yrs', '<p>The quick brown fox jumped over the lazy dog.</p>', '2015-01-22 06:37:57', 'R', '0.00', '0.00', 19, '2015-02-20 06:37:57', 'N', 'N', 'Y', 'Y', 'N', ''),
(14, 'Part-Time', '7', 'Cameraman', 'Ceb', '2', 'Your time to shine', '2015-01-23 07:54:36', 'R', '15000.00', '18500.00', 18, '2015-02-21 07:36:54', 'Y', 'Y', 'Y', 'Y', 'Y', NULL),
(15, 'Parttime', '24', 'Front-End Developer', 'Cagayan', '1 to 5yrs', '<p>sjgklsjlg</p>', '2015-01-29 06:31:15', 'R', '0.00', '0.00', 20, '2015-02-27 06:31:15', 'N', 'N', 'Y', 'Y', 'N', '');

-- --------------------------------------------------------

--
-- Table structure for table `job_post_event`
--

CREATE TABLE IF NOT EXISTS `job_post_event` (
`job_post_event_id` int(10) unsigned NOT NULL,
  `job_post_id` int(10) unsigned NOT NULL,
  `cover_letter` text NOT NULL,
  `applied_datetime` datetime NOT NULL,
  `resume_text` longtext NOT NULL,
  `candidate_id` int(10) unsigned NOT NULL DEFAULT '0',
  `resume_filename` varchar(200) DEFAULT NULL,
  `title` varchar(45) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `middle_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `skype_name` varchar(100) NOT NULL,
  `is_viewed` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_accepted` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_declined` enum('Y','N') NOT NULL DEFAULT 'N',
  `contact_no` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_post_event`
--

INSERT INTO `job_post_event` (`job_post_event_id`, `job_post_id`, `cover_letter`, `applied_datetime`, `resume_text`, `candidate_id`, `resume_filename`, `title`, `first_name`, `last_name`, `middle_name`, `email`, `skype_name`, `is_viewed`, `is_accepted`, `is_declined`, `contact_no`) VALUES
(1, 7, 'Dear Sir/Madam,<br/><br/>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  IT Specialists job advertisement in patsada.com on Jan 15 2015.<br/><br/>\r\n\r\n[ Add description of your achievements and contact information here ]<br/><br/>\r\n\r\nThank you very much.<br/><br/>\r\n\r\nSincerely yours,<br/><br/>\r\n\r\n[ your name ]\r\n                                    ', '2015-01-15 06:03:44', '', 0, '529aa61eccc82bbc4a0865f685273182.docx', 'Mr', 'tqwt', 'qtqwt', '', 'qtqwt@tqtqwt.com', '', 'N', 'N', 'N', 'qwtqwt'),
(2, 7, 'Dear Sir/Madam,<br/><br/>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  IT Specialists job advertisement in patsada.com on Jan 15 2015.<br/><br/>\r\n\r\n[ Add description of your achievements and contact information here ]<br/><br/>\r\n\r\nThank you very much.<br/><br/>\r\n\r\nSincerely yours,<br/><br/>\r\n\r\n[ your name ]\r\n                                    ', '2015-01-15 06:19:44', '', 0, '8d5f05deb20c4a5101c31574a4c88043.docx', 'Mr', 'TEST12', 'TEST12', '', 'TEST12@gmail.com', '', 'N', 'N', 'N', 't124124'),
(3, 7, 'Dear Sir/Madam,<br/><br/>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  IT Specialists job advertisement in patsada.com on Jan 15 2015.<br/><br/>\r\n\r\n[ Add description of your achievements and contact information here ]<br/><br/>\r\n\r\nThank you very much.<br/><br/>\r\n\r\nSincerely yours,<br/><br/>\r\n\r\n[ your name ]\r\n                                    ', '2015-01-15 06:24:51', '', 0, 'd3d7c2f5a8af27970525f87e667e4e5e.docx', 'Mr', 'TEST123123', 'tEST12', '', 'tQ12414W@gmail.com', '', 'N', 'N', 'N', '124124124'),
(4, 7, '<p>Dear Sir/Madam,<br /><br />Hello and good day! This letter is in response to your IT Specialists job advertisement in patsada.com on Jan 15 2015.<br /><br />[ Add description of your achievements and contact information here ]<br /><br />Thank you very much.<br /><br />Sincerely yours,<br /><br />[ your name ]</p><div style="display:none;" id="__if72ru4sdfsdfrkjahiuyi_once">&nbsp;</div><div style="display:none;" id="__if72ru4sdfsdfruh7fewui_once">&nbsp;</div><div style="display:none;" id="__hggasdgjhsagd_once">&nbsp;</div>', '2015-01-15 06:24:51', '<div style="display:none;" id="__if72ru4sdfsdfrkjahiuyi_once">&nbsp;</div><div style="display:none;" id="__if72ru4sdfsdfruh7fewui_once">&nbsp;</div><div style="display:none;" id="__hggasdgjhsagd_once">&nbsp;</div>', 0, 'f89831a5f3fddec494f3ae4db7f942f7.docx', 'Mr', 'TEST123123', 'tEST12', '', 'tQ124144W@gmail.com', '', 'N', 'N', 'N', '124124124'),
(5, 6, 'Dear Sir/Madam,<br/><br/>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  ADASD job advertisement in patsada.com on Jan 14 2015.<br/><br/>\r\n\r\n[ Add description of your achievements and contact information here ]<br/><br/>\r\n\r\nThank you very much.<br/><br/>\r\n\r\nSincerely yours,<br/><br/>\r\n\r\n[ your name ]\r\n                                    ', '2015-01-15 06:55:17', '', 0, 'd6250c1bcf318737e1d83928edc1c105.docx', 'Mr', 'rqwr', 'qwrqwr', '', 'qwr@gmail.com', '', 'N', 'N', 'N', 't124124'),
(6, 6, 'Dear Sir/Madam,<br/><br/>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  ADASD job advertisement in patsada.com on Jan 14 2015.<br/><br/>\r\n\r\n[ Add description of your achievements and contact information here ]<br/><br/>\r\n\r\nThank you very much.<br/><br/>\r\n\r\nSincerely yours,<br/><br/>\r\n\r\n[ your name ]\r\n                                    ', '2015-01-15 07:15:49', '', 0, '7576183ed8e7471d6454a67952cd4c79.docx', 'Mr', 'twe123', 'twe123', '', 'twe123@gmail.com', '', 'N', 'N', 'N', '124124'),
(8, 6, 'Dear Sir/Madam,<br/><br/>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  ADASD job advertisement in patsada.com on Jan 14 2015.<br/><br/>\r\n\r\n[ Add description of your achievements and contact information here ]<br/><br/>\r\n\r\nThank you very much.<br/><br/>\r\n\r\nSincerely yours,<br/><br/>\r\n\r\n[ your name ]\r\n                                    ', '2015-01-15 07:24:27', '', 0, 'f7675e12cb113100a4fc3ecb56ed8135.docx', 'Mr', 'tqtwtqwt', 'tqwt', '', 'qwtsd124a@112asdqwe24.com', '', 'N', 'N', 'N', 'qrqwrr'),
(9, 6, '<p>Dear Sir/Madam,<br /><br />Hello and good day! This letter is in response to your ADASD job advertisement in patsada.com on Jan 14 2015.<br /><br />[ Add description of your achievements and contact information here ]<br /><br />Thank you very much.<br /><br />Sincerely yours,<br /><br />[ your name ]</p><div style="display:none;" id="__if72ru4sdfsdfrkjahiuyi_once">&nbsp;</div><div style="display:none;" id="__if72ru4sdfsdfruh7fewui_once">&nbsp;</div><div style="display:none;" id="__hggasdgjhsagd_once">&nbsp;</div>', '2015-01-15 07:28:04', '<div style="display:none;" id="__if72ru4sdfsdfrkjahiuyi_once">&nbsp;</div><div style="display:none;" id="__if72ru4sdfsdfruh7fewui_once">&nbsp;</div><div style="display:none;" id="__hggasdgjhsagd_once">&nbsp;</div>', 0, 'b9f810283fbac6369d1254df2336b9ab.docx', 'Mr', 'test12', 'tEST12', '', 'test1231212431231231231@gmail.com', '', 'N', 'N', 'N', '12412412'),
(10, 6, '<p>Dear Sir/Madam,<br /><br />Hello and good day! This letter is in response to your ADASD job advertisement in patsada.com on Jan 14 2015.<br /><br />[ Add description of your achievements and contact information here ]<br /><br />Thank you very much.<br /><br />Sincerely yours,<br /><br />[ your name ]</p><div style="display:none;" id="__if72ru4sdfsdfrkjahiuyi_once">&nbsp;</div><div style="display:none;" id="__if72ru4sdfsdfruh7fewui_once">&nbsp;</div><div style="display:none;" id="__hggasdgjhsagd_once">&nbsp;</div>', '2015-01-15 07:46:31', '<div style="display:none;" id="__if72ru4sdfsdfrkjahiuyi_once">&nbsp;</div><div style="display:none;" id="__if72ru4sdfsdfruh7fewui_once">&nbsp;</div><div style="display:none;" id="__hggasdgjhsagd_once">&nbsp;</div>', 0, 'cdcbfcf44969bdf430be9a5ec9bae341.docx', 'Mr', 'test1224', 'test12', '', 'test1241241@gmail.com', '', 'N', 'N', 'N', '124142'),
(14, 6, 'Dear Sir/Madam,<br/><br/>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  ADASD job advertisement in patsada.com on Jan 14 2015.<br/><br/>\r\n\r\n[ Add description of your achievements and contact information here ]<br/><br/>\r\n\r\nThank you very much.<br/><br/>\r\n\r\nSincerely yours,<br/><br/>\r\n\r\n[ your name ]\r\n                                    ', '2015-01-15 08:19:46', '', 0, NULL, 'Mr', 'test1224', 'test1224', '', 'tQ12414asdasd4W@gmail.com', '', 'N', 'N', 'N', '14124'),
(15, 6, '<p>Dear Sir/Madam,<br /><br />Hello and good day! This letter is in response to your ADASD job advertisement in patsada.com on Jan 14 2015.<br /><br />[ Add description of your achievements and contact information here ]<br /><br />Thank you very much.<br /><br />Sincerely yours,<br /><br />[ your name ]</p><div style="display:none;" id="__if72ru4sdfsdfrkjahiuyi_once">&nbsp;</div><div style="display:none;" id="__if72ru4sdfsdfruh7fewui_once">&nbsp;</div><div style="display:none;" id="__hggasdgjhsagd_once">&nbsp;</div>', '2015-01-15 08:39:13', '<div style="display:none;" id="__if72ru4sdfsdfrkjahiuyi_once">&nbsp;</div><div style="display:none;" id="__if72ru4sdfsdfruh7fewui_once">&nbsp;</div><div style="display:none;" id="__hggasdgjhsagd_once">&nbsp;</div>', 0, NULL, 'Mr', 'dasd124r', 'eqwrt', '', 'dasd@dfadasd.com', '', 'N', 'N', 'N', '124124214124'),
(17, 6, 'Dear Sir/Madam,<br/><br/>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  ADASD job advertisement in patsada.com on Jan 14 2015.<br/><br/>\r\n\r\n[ Add description of your achievements and contact information here ]<br/><br/>\r\n\r\nThank you very much.<br/><br/>\r\n\r\nSincerely yours,<br/><br/>\r\n\r\n[ your name ]\r\n                                    ', '2015-01-15 08:41:46', '', 0, NULL, 'Mr', 'tqwtqw', 'tqwtqwtqt', '', 'qwtqdasdwt12@gmail.com', '', 'N', 'N', 'N', '14125125'),
(20, 6, 'Dear Sir/Madam,<br/><br/>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  ADASD job advertisement in patsada.com on Jan 14 2015.<br/><br/>\r\n\r\n[ Add description of your achievements and contact information here ]<br/><br/>\r\n\r\nThank you very much.<br/><br/>\r\n\r\nSincerely yours,<br/><br/>\r\n\r\n[ your name ]\r\n                                    ', '2015-01-15 08:45:34', '', 0, NULL, 'Mr', 'adasd142124', 'adsasdasd', '', 'asdasd124124@gmail.com', '', 'N', 'N', 'N', '151515'),
(21, 8, 'Dear Sir/Madam,<br/><br/>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your Secretary job advertisement in patsada.com on Jan 15 2015.<br/><br/>\r\n\r\n[ Add description of your achievements and contact information here ]<br/><br/>\r\n\r\nThank you very much.<br/><br/>\r\n\r\nSincerely yours,<br/><br/>\r\n\r\n[ your name ]\r\n                                    ', '2015-01-15 03:42:22', '', 0, NULL, 'Ms', 'Alaiza Geene', 'Maandig', '', 'meusck@gmail.com', '', 'N', 'N', 'N', '09985450029'),
(22, 10, 'Dear Sir/Madam,<br><br>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  Janitor job advertisement in patsada.com on Jan 19 2015.<br><br>\r\n\r\n[ Add description of your achievements and contact information here ]<br><br>\r\n\r\nThank you very much.<br><br>\r\n\r\nSincerely yours,<br><br>\r\n\r\n[ Jhai Maandig ]', '2015-01-19 04:13:20', '', 2, NULL, '', '', '', '', '', '', 'N', 'N', 'N', ''),
(23, 11, 'Dear Sir/Madam,<br><br>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your VJ job advertisement in patsada.com on Jan 20 2015.<br><br>\r\n\r\n[ Add description of your achievements and contact information here ]<br><br>\r\n\r\nThank you very much.<br><br>\r\n\r\nSincerely yours,<br><br>\r\n\r\n[ Maiden Flor Caumban ]', '2015-01-20 04:24:23', '', 3, NULL, '', '', '', '', '', '', 'N', 'N', 'N', ''),
(24, 13, 'Dear Sir/Madam,<br><br>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  Developer job advertisement in patsada.com on Jan 22 2015.<br><br>\r\n\r\n[ Add description of your achievements and contact information here ]<br><br>\r\n\r\nThank you very much.<br><br>\r\n\r\nSincerely yours,<br><br>\r\n\r\n[ your name ]', '2015-01-23 08:12:54', '', 0, '6db3806b79e8edce2d5a4162182fd0d8.docx', 'Ms', 'Jean', 'Parreno', '', 'itsmepotskie@gmail.com', '', 'Y', 'N', 'N', '0924389512'),
(25, 13, 'Dear Sir/Madam,<br><br>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your  Developer job advertisement in patsada.com on Jan 22 2015.<br><br>\r\n\r\n[ Add description of your achievements and contact information here ]<br><br>\r\n\r\nThank you very much.<br><br>\r\n\r\nSincerely yours,<br><br>\r\n\r\n[ your name ]', '2015-01-23 08:18:16', '', 3, NULL, '', '', '', '', '', '', 'Y', 'N', 'N', ''),
(26, 0, '', '0000-00-00 00:00:00', '', 0, NULL, '', '', '', '', '', '', 'N', 'N', 'N', ''),
(27, 8, 'Dear Sir/Madam,<br><br>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your Secretary job advertisement in patsada.com on Jan 15 2015.<br><br>\r\n\r\n[ Add description of your achievements and contact information here ]<br><br>\r\n\r\nThank you very much.<br><br>\r\n\r\nSincerely yours,<br><br>\r\n\r\n[ your name ]', '2015-01-28 07:19:28', 'skfjslgjslagjsl;gs', 0, NULL, 'Ms', 'Jhaiwho', 'Maandig', '', '123@gmail.com', '', 'N', 'N', 'N', '0924389512'),
(28, 15, 'Dear Sir/Madam,<br><br>\r\n\r\nHello and good day!\r\n\r\nThis letter is in response to your Front-End Developer job advertisement in patsada.com on Jan 29 2015.<br><br>\r\n\r\n[ Add description of your achievements and contact information here ]<br><br>\r\n\r\nThank you very much.<br><br>\r\n\r\nSincerely yours,<br><br>\r\n\r\n[ your name ]', '2015-01-29 07:14:21', 'egewwegwhweh', 0, NULL, 'Ms', 'Jhai', 'Maandig', '', 'meusck21@gmail.com', '', 'Y', 'N', 'N', '09123456641');

-- --------------------------------------------------------

--
-- Table structure for table `login_credentials`
--

CREATE TABLE IF NOT EXISTS `login_credentials` (
`login_credentials_id` int(10) unsigned NOT NULL,
  `is_candidate` enum('Y','N') DEFAULT 'N',
  `is_employer` enum('Y','N') DEFAULT 'N',
  `candidate_id` int(10) unsigned NOT NULL,
  `employer_user_id` int(10) unsigned NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `password_r` varchar(254) NOT NULL,
  `created_datetime` datetime NOT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y',
  `is_confirmed` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_admin` enum('Y','N') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_credentials`
--

INSERT INTO `login_credentials` (`login_credentials_id`, `is_candidate`, `is_employer`, `candidate_id`, `employer_user_id`, `user_name`, `password`, `password_r`, `created_datetime`, `is_active`, `is_confirmed`, `is_admin`) VALUES
(2, 'N', 'N', 0, 0, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '', '2015-01-06 03:20:20', 'Y', 'Y', 'Y'),
(14, 'N', 'Y', 0, 11, 'TEST12231', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'cXdlcnR5', '2015-01-14 20:40:56', 'Y', 'Y', 'N'),
(15, 'Y', 'N', 1, 0, 'Andrelin', 'e10adc3949ba59abbe56e057f20f883e', '', '2015-01-15 11:50:01', 'N', 'Y', 'N'),
(16, 'N', 'Y', 0, 14, 'TravelRez', 'e10adc3949ba59abbe56e057f20f883e', 'MTIzNDU2', '2015-01-15 11:50:40', 'Y', 'Y', 'N'),
(17, 'N', 'Y', 0, 13, 'XYZ', 'e10adc3949ba59abbe56e057f20f883e', '', '0000-00-00 00:00:00', 'Y', 'Y', 'N'),
(18, 'N', 'Y', 0, 16, 'ABC', 'e10adc3949ba59abbe56e057f20f883e', 'MTIzNDU2', '2015-01-16 01:15:02', 'Y', 'Y', 'N'),
(19, 'N', 'Y', 0, 17, 'abcdefg', 'e10adc3949ba59abbe56e057f20f883e', 'MTIzNDU2', '2015-01-16 01:17:02', 'Y', 'Y', 'N'),
(20, 'N', 'Y', 0, 18, 'wgwe', 'e10adc3949ba59abbe56e057f20f883e', 'MTIzNDU2', '2015-01-19 10:07:18', 'Y', 'N', 'N'),
(21, 'N', 'Y', 0, 19, 'andrelin', 'e10adc3949ba59abbe56e057f20f883e', 'MTIzNDU2', '2015-01-19 11:02:27', 'Y', 'Y', 'N'),
(22, 'Y', 'Y', 2, 13, 'jhaiwho', 'e10adc3949ba59abbe56e057f20f883e', '', '2015-01-19 11:08:00', 'Y', 'Y', 'N'),
(23, 'N', 'Y', 0, 20, 'wx', 'e10adc3949ba59abbe56e057f20f883e', 'MTIzNDU2', '2015-01-20 11:10:18', 'Y', 'Y', 'N'),
(24, 'Y', 'N', 3, 0, 'neneng', 'e10adc3949ba59abbe56e057f20f883e', '', '2015-01-20 11:23:35', 'Y', 'Y', 'N'),
(25, 'N', 'Y', 0, 0, 'tvz', 'e10adc3949ba59abbe56e057f20f883e', '', '0000-00-00 00:00:00', 'Y', 'Y', 'N'),
(26, 'N', 'Y', 0, 21, 'abcd', 'e10adc3949ba59abbe56e057f20f883e', 'MTIzNDU2', '2015-01-23 01:37:04', 'Y', 'Y', 'N'),
(27, 'N', 'Y', 0, 22, 'jhaiwho1', 'e10adc3949ba59abbe56e057f20f883e', 'MTIzNDU2', '2015-01-29 13:30:27', 'Y', 'Y', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`post_id` int(11) NOT NULL,
  `comp_code` varchar(50) NOT NULL,
  `pdate` varchar(50) NOT NULL,
  `post_type` varchar(50) NOT NULL,
  `job_type` varchar(50) NOT NULL,
  `position` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `category` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `yrs_exp` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `comp_code`, `pdate`, `post_type`, `job_type`, `position`, `description`, `category`, `location`, `yrs_exp`, `action`) VALUES
(4, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full Time\r', ' IT Developer\r', ' To maintain website\r', ' IT\r', ' CDO\r', ' 1-5\r', ''),
(5, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full Time\r', ' IT Developer\r', ' To maintain website\r', ' IT\r', ' CDO\r', ' 1-5\r', ''),
(6, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', ' Cebu\r', ' 1-3\r', ''),
(7, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', '', ' 1-3\r', ''),
(8, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(9, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(10, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full Time\r', ' IT Developer\r', ' To maintain website\r', ' IT\r', ' CDO\r', ' 1-5\r', ''),
(11, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', ' Cebu\r', ' 1-3\r', ''),
(12, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', '', ' 1-3\r', ''),
(13, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(14, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(15, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full Time\r', ' IT Developer\r', ' To maintain website\r', ' IT\r', ' CDO\r', ' 1-5\r', ''),
(16, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', ' Cebu\r', ' 1-3\r', ''),
(17, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', '', ' 1-3\r', ''),
(18, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(19, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(20, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full Time\r', ' IT Developer\r', ' To maintain website\r', ' IT\r', ' CDO\r', ' 1-5\r', '');

-- --------------------------------------------------------

--
-- Table structure for table `resume`
--

CREATE TABLE IF NOT EXISTS `resume` (
`resume_id` int(10) unsigned NOT NULL,
  `resume_filename` varchar(200) NOT NULL,
  `candidate_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
`skills_id` int(10) unsigned NOT NULL,
  `candidate_id` int(11) NOT NULL,
  `name` varchar(254) NOT NULL,
  `year` tinyint(2) NOT NULL,
  `proficiency` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidate`
--
ALTER TABLE `candidate`
 ADD PRIMARY KEY (`candidate_id`);

--
-- Indexes for table `candidate_education`
--
ALTER TABLE `candidate_education`
 ADD PRIMARY KEY (`candidate_education_id`);

--
-- Indexes for table `candidate_experience`
--
ALTER TABLE `candidate_experience`
 ADD PRIMARY KEY (`candidate_experience_id`) USING BTREE;

--
-- Indexes for table `candidate_invitations`
--
ALTER TABLE `candidate_invitations`
 ADD PRIMARY KEY (`candidate_invitations_id`) USING BTREE;

--
-- Indexes for table `candidate_objective`
--
ALTER TABLE `candidate_objective`
 ADD PRIMARY KEY (`candidate_objective_id`);

--
-- Indexes for table `candidate_recommendation`
--
ALTER TABLE `candidate_recommendation`
 ADD PRIMARY KEY (`candidate_recommendation_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `employer`
--
ALTER TABLE `employer`
 ADD PRIMARY KEY (`employer_id`);

--
-- Indexes for table `employer_user`
--
ALTER TABLE `employer_user`
 ADD PRIMARY KEY (`employer_user_id`), ADD KEY `fk_employer_user` (`employer_id`);

--
-- Indexes for table `job_post`
--
ALTER TABLE `job_post`
 ADD PRIMARY KEY (`job_post_id`);

--
-- Indexes for table `job_post_event`
--
ALTER TABLE `job_post_event`
 ADD PRIMARY KEY (`job_post_event_id`);

--
-- Indexes for table `login_credentials`
--
ALTER TABLE `login_credentials`
 ADD PRIMARY KEY (`login_credentials_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `resume`
--
ALTER TABLE `resume`
 ADD PRIMARY KEY (`resume_id`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
 ADD PRIMARY KEY (`skills_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidate`
--
ALTER TABLE `candidate`
MODIFY `candidate_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `candidate_education`
--
ALTER TABLE `candidate_education`
MODIFY `candidate_education_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `candidate_experience`
--
ALTER TABLE `candidate_experience`
MODIFY `candidate_experience_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `candidate_invitations`
--
ALTER TABLE `candidate_invitations`
MODIFY `candidate_invitations_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `candidate_objective`
--
ALTER TABLE `candidate_objective`
MODIFY `candidate_objective_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `candidate_recommendation`
--
ALTER TABLE `candidate_recommendation`
MODIFY `candidate_recommendation_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `employer`
--
ALTER TABLE `employer`
MODIFY `employer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `employer_user`
--
ALTER TABLE `employer_user`
MODIFY `employer_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `job_post`
--
ALTER TABLE `job_post`
MODIFY `job_post_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `job_post_event`
--
ALTER TABLE `job_post_event`
MODIFY `job_post_event_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `login_credentials`
--
ALTER TABLE `login_credentials`
MODIFY `login_credentials_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `resume`
--
ALTER TABLE `resume`
MODIFY `resume_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
MODIFY `skills_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
