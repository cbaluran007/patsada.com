$(document).ready(function(){

    var editor2= CKEDITOR.replace( 'job_content',
        {
            uiColor: '#dddddd',
            toolbar:
                [
                    ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList'],
                    ['UIColor']
                ]

        });


    $('#ful').click(function(){

        $('#jbtype').val('Fulltime');

    });
    $('#partym').click(function(){

        $('#jbtype').val('Parttime');

    });
    $('#fre').click(function(){

        $('#jbtype').val('Contract');

    });
    $('#urg').click(function(){

        $('#is_urgent').val('Y');

    });
    $('#non_urg').click(function(){

        $('#is_urgent').val('N');

    });


        $("#form_post").validate({
            rules:{

                position:{
                    required:true
                },
                category_id:{
                    required:true
                },
                location:{
                    required:true
                },
                experience_from:{
                    required:{
                        depends:function(elements){
                            if($('#experience_to').val()==''){
                                return true;
                            }
                        }
                    }
                },
                experience_to:{
                    required:{
                        depends:function(elements){
                            if($('#experience_from').val()==''){
                                return true;
                            }
                        }
                    }
                }

            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },submitHandler: function (form) {
                alert("Congrat");
                form.submit();
            }
        });




});