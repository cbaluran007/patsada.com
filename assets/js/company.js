$(document).ready(function() {
    $('[data-toggle=offcanvas]').click(function() {
        $('.row-offcanvas').toggleClass('active');
    });



    $("[rel='tooltip']").tooltip();

    $('#hover-img .thumbnail').hover(

        function () {
            $(this).find('.caption').fadeIn(250);
        },

        function () {
            $(this).find('.caption').fadeOut(250);
        });

    $('#next').click(function(){
        $('#form-emp').toggleClass('hide fade');
        $('#form-emp_d').toggleClass('show fade in');
    });
    $('#back').click(function(){
        $('#next').click();
    });


    $('#userfile').uploadify({
        'auto':true,
        'swf':  baseURL+'assets/js/uploadify_31/uploadify.swf',
        'uploader': baseURL+'employer/do_upload/'+$('#employer_id').val(),
        'cancelImg':baseURL+'assets/js/uploadify_31/uploadify-cancel.png',
        'fileTypeExts':'*.jpg;*.jpeg;*.gif;*.png',
        'fileTypeDesc':'Image Files (.jpg,.bmp,.png,.tif)',
        'fileSizeLimit':'90kb',
        'fileDesc'  :'Please select Gif,JPG,PNG IMAGES',
        'fileObjName':'userfile',
        'buttonText':'Select Photo',
        'buttonClass' : 'btn btn-upload-resume',
        'multi':false,
        'width': 200,
        'simUploadLimit' : 1,
        'removeCompleted':true,
        'onUploadSuccess' : function(file, data, response) {
            var ret = jQuery.parseJSON(data);
            var uId = ret.file_name;
            alert(uId);
            //$('#mypic').src('<?php echo base_url();?>assets/images/job_keeper/"'+uId);
            document.getElementById('img_upload').src='<?php echo base_url();?>assets/images/employer_pic/'+uId;
        }
    });


});