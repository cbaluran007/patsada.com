$(document).ready(function(){






    $("#registration_candidate").submit(function(e) {
        e.preventDefault();
    }).validate({
        rules:{
            user_name:{
                required:true,
                remote: {
                     url: baseURL + "ajax/get_validate_user_name_duplication/Y/N",
                     type: "post",
                     data: {
                     user_name: function(){ return $("#user_name").val(); }
                     }
                 }

            },
            email:{
                required:true,
                email: true,
				remote: {
                     url: baseURL + "ajax/get_validate_email_duplication/candidate",
                     type: "post",
                     data: {
                     email: function(){ return $("#email").val(); }
					}
                }
            },
            password:{
                required:true,
                minlength: 6
            }
        },
        messages: {
            email: {
                required: 'Email address is required',
                email: 'Please enter a valid email address',
                remote: 'Email already used.'
            },
            user_name: {
                required: 'Username is requeired',
                remote: 'Username already used.'
            }
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form){
            $.ajax({
                url:  baseURL+"ajax/candidate_registration/",
                type: "post",
                data: $('#registration_candidate').serialize(),
                dataType: 'json',
                success: function(data){
                    if(data['success']){
                        $('#alerts').removeClass('alert alert-danger');
                        $('#alerts').addClass('alert alert-success');
                        $('#alerts').html(data['message']);

                        window.setTimeout(function(){
                               window.location=baseURL+data['type'];
                        },2000);

					}else{
                        $('#alerts').removeClass('alert alert-success');
                        $('#alerts').addClass('alert alert-danger');
                        $('#alerts').html(data['message']);
                       


                    }
                },
                error: function(jq,status,message){
                    alert("failed");
                }
            });

        }
    });

    $("#form_forgot").submit(function(e) {
        e.preventDefault();
    }).validate({
        rules:{

            email:{
                required:true
            }
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form){
            $.ajax({
                url:  baseURL+"ajax/forgot_password/",
                type: "post",
                data: $('#form_forgot').serialize(),
                dataType: 'json',
                success: function(data){
                    if(data['success']){
                        $('#alerts').html('');
                        $('#alerts2').removeClass('alert alert-danger');
                        $('#alerts2').addClass('alert alert-success');
                        $('#alerts2').html(data['message']);
					}else{
                        $('#alerts2').removeClass('alert alert-success');
                        $('#alerts2').addClass('alert alert-danger');
                        $('#alerts2').html(data['message']);

                    }
                },
                error: function(jq,status,message){
                    alert("failed");
                }
            });

        }
    });
	
	$("#registration_employer").submit(function(e) {
        e.preventDefault();
    }).validate({
        rules:{
			ecompany_name:{
                required:true,
                remote: {
                     url: baseURL + "ajax/get_validate_company_duplication",
                     type: "post",
                     data: {
                     ecompany_name: function(){ return $("#ecompany_name").val(); }
                     }
                 }

            },
            euser_name:{
                required:true,
                remote: {
                     url: baseURL + "ajax/get_validate_user_name_duplication/N/Y",
                     type: "post",
                     data: {
                     euser_name: function(){ return $("#euser_name").val(); }
                     }
                 }

            },
            eemail:{
                required:true,
                email: true,
				remote: {
                     url: baseURL + "ajax/get_validate_email_duplication/employer",
                     type: "post",
                     data: {
                     eemail: function(){ return $("#eemail").val(); }
					}
                }
            },
            epassword:{
                required:true,
                minlength: 6
            }
        },
        messages: {
            eemail: {
                required: 'Email address is required',
                email: 'Please enter a valid email address',
                remote: 'Email already used.'
            },
            euser_name: {
                required: 'Username is requeired',
                remote: 'Username already used.'
            },
			ecompany_name: {
                required: 'Company Name is requeired',
                remote: 'This Company Name is already exist in the system. Please ask the company user account manager to add you as one of the user. THANK YOU!'
            }
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form){
            $.ajax({
                url:  baseURL+"ajax/employer_registration/",
                type: "post",
                data: $('#registration_employer').serialize(),
                dataType: 'json',
                success: function(data){
                    if(data['success']){
                        $('#alerts_emp').removeClass('alert alert-danger');
                        $('#alerts_emp').addClass('alert alert-success');
                        $('#alerts_emp').html(data['message']);
						
                        window.setTimeout(function(){
                               window.location=baseURL+data['type'];
                        },2000);

					}else{
                        $('#alerts_emp').removeClass('alert alert-success');
                        $('#alerts_emp').addClass('alert alert-danger');
                        $('#alerts_emp').html(data['message']);
                      



                    }
                },
                error: function(jq,status,message){
                    alert("failed");
                }
            });

        }
    });
	
	
	
	
	
	
    $('#login_credintials').submit(function(e) {
        e.preventDefault();
    }).validate({
                rules:{
                    user_name:{
                        required:true
                    },
                    password:{
                        required:true
                    }
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                }  ,
                submitHandler: function(form){
                $.ajax({
                        url:  baseURL+"ajax/login_credintials/",
                        type: "post",
                        data: $('#login_credintials').serialize(),
                        dataType: 'json',
                        success: function(data){
                            if(data['status']){
                                if(data['data']['type']=='candidate'){
                                    window.location=baseURL+'candidate/';
                                }else{
                                    window.location=baseURL+'employer/';

                                }

                            }else{
                                $('#log_alerts').removeClass('alert alert-success');
                                $('#log_alerts').addClass('alert alert-danger');
                                $('#log_alerts').html(data['data']);

                            }
                        },
                        error: function(jq,status,message){
                            $('#log_alerts').removeClass('alert alert-success');
                            $('#log_alerts').addClass('alert alert-danger');
                            $('#log_alerts').html(data['data']);
                        }
                    });

                }
    });





    



});