$(document).ready(function(){

    $('.tooltips_this').tooltip();

    $('.delete').click(function(e){
        if(confirm("Are you sure you to this applicants?"))
        {
            var selectedID = [];
            $('.checkbox:checked').each (function () {
                selectedID.push(this.id);


            });

            $.ajax({
                type:"POST",
                url:baseURL + "ajax/remove_applicants/",
                data:{'selectedID':selectedID},
                success:function(data){

                        jQuery('.checkbox:checked').parents("tr").fadeOut(500);

                }
            });

        }
        else
        {
            e.preventDefault();
        }
    });
    $("#add_user").submit(function(e) {
        e.preventDefault();
    }).validate({
            rules:{
                user_name:{
                    required:true,
                    remote: {
                        url: baseURL + "ajax/get_validate_user_name_duplication/N/Y",
                        type: "post",
                        data: {
                            user_name: function(){ return $("#e_user_name").val(); }
                        }
                    }

                },
                email:{
                    required:true,
                    email: true,
                    remote: {
                        url: baseURL + "ajax/get_validate_email_user_duplication/employer",
                        type: "post",
                        data: {
                            email: function(){ return $("#e_user_mail").val(); }
                        }
                    }
                },
                password:{
                    required:true,
                    minlength: 6
                }
            },
            messages: {
                email: {
                    required: 'Email address is required',
                    email: 'Please enter a valid email address',
                    remote: 'Email already used.'
                },
                user_name: {
                    required: 'Username is requeired',
                    remote: 'Username already used.'
                }
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            }
        });







        $('.accept').click(function(){

                var id = $(this).attr('id');
                var form_id="#form_accept_"+id;

                    $(form_id).ajaxForm({ // initialize the plugin

                        success: function (data) {

                                $('#is_accepted').val('N');
                            $("i",this).toggleClass("icon-thumbs-up icon-thumbs-down");


                        }
                    }).submit();
        });

        $('.decline').click(function(){
                var id = $(this).attr('id');
                var form_id="#form_decline_"+id;

                    $(form_id).ajaxForm({ // initialize the plugin

                        success: function (data) {

                                $('#is_accepted').val('N');
                            $("i",this).toggleClass("icon-thumbs-down icon-thumbs-up");



                        }
                    }).submit();
        });


        $('.remove_applicants').on('click',function(){


            var count = $('.checkbox:checked').length;
            if(count > 0){
                if(confirm("Are you sure you to this applicants?"))
                {
                    $('#remove_all').submit();
                }

            }else{

                }

        });

    $('.job_tbody tr').each(function(i,e)
    {
        $(e).children('td:not(:last)').click(function()
        {
            //here we are working on a td element, that's why we need
            //to refer to its parent (tr) in order to find the <a> element
            var href = $(this).closest("tr").find("a").attr("href");
            if(href)
            {
                window.location = href;
            }
        });
    });



    $('#openBtn').click(function(){
        var job_post_id = $('#job_post_id').val();

            $.ajax({
                    type:'POST',
                    url:'http://localhost/patsada.com/ajax/update_viewedapplicant',// controler function update
                    data:{'job_post_id':job_post_id},
                    dataType:'json',
                    success:function(data){
                        console.log(data);
                    },error:function(data){
                            console.log(data);
                    }
            });
        

        var frameSrc = baseURL+"employer/jobs_applicants/"+job_post_id;

            $('#iframe_applicants').attr("src",frameSrc);

    });
});


function acc_decc(id,action_type){
    if(action_type == 'accept'){
        $.ajax({
            type:"POST",
            url:baseURL+"ajax/accept_applicants/",
            data: {job_post_event_id: id},
            success: function (data) {
            var  modal_name='#app_'+id;
              $(modal_name).find('#stats').html('<p class="text-success"> - Accepted </p>');
               $('#rec_stats_'+id).html('Accepted');
                $('#rec_stats_'+id).attr('class','text-success');

            }
        });
    }else{
        $.ajax({
            type:"POST",
            url:baseURL+"ajax/decline_applicants/",
            data: {job_post_event_id: id},
            success: function (data) {
                var  modal_name='#app_'+id;
                $(modal_name).find('#stats').html('<p class="text-success">  - Declined </p>');
                $('#rec_stats_'+id).html('Declined');
                $('#rec_stats_'+id).attr('class','text-danger');


            }
        });
    }

}

function remove_applicants(id){
    if(confirm("Are you sure you to this applicants?"))
    {



        $.ajax({
            type:"POST",
            url:baseURL + "ajax/remove_applicants/",
            data:{tbl_name:"job_post_event", job_post_event_id: id},
            success:function(data){
                  location.reload(true);
            }
        });

    }
    else
    {
        e.preventDefault();
    }
}