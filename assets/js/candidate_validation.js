$(document).ready(function(){
    $('#modal-form-submit').on('click', function(e){
        // We don't want this to act as a link so cancel the link action
        e.preventDefault();
        // Find form and submit it
        $('#candidate_validation').submit();
    });

        $('#candidate_validation').validate({
            rules:{
                tittle:{
                    required:true
                },
                first_name:{
                    required:true
                },
                middle_name:{
                    required:true
                },
               last_name:{
                    required:true
                },
                expertise:{
                    required:true
                },
                category:{
                    required:true
                },
                date:{
                    required:true
                },
                natinality:{
                    required:true
                },
                tel:{
                    required:true
                },
                mbile:{
                    required:true
                },
                address:{
                    required:true
                }
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            }
        });
});