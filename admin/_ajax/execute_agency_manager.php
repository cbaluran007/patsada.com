<?php
session_start();
require_once('../_classes/_loader.php');


$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "submitAgency":{
			$mysql = new Database; 
			$tools = new Tools; 
			
			
			
			
			$agencyname   = $_POST['agencyname'];
			$code = $tools->generateCode($agencyname); 
			$internal_notes   = $_POST['internal_notes'];
			$is_agency_auto_invoice =0;
			if($_POST['is_agency_auto_invoice']=="true"){
				$is_agency_auto_invoice =1;
			}
			$agency_type  = $_POST['agency_type_combo_id'];
			$agency_markup   = $_POST['agency_markup'];
			$agency_comm   = $_POST['agency_comm'];
			$agency_disc   = $_POST['agency_disc'];
			$currency_agency   = $_POST['currency_agency'];
			
			$country   = $_POST['country'];
			$state   = $_POST['state'];
			$address   = $_POST['addressAgency'];
			$city   = $_POST['cityAgency'];
			$cardname   = $_POST['cardname'];
			$bcode = $tools->generateCode($cardname);
			$position   = $_POST['position'];
			$department   = $_POST['department'];
			$email_agency   = $_POST['email_agency'];
			$url_agency   = $_POST['url_agency'];
			$fax_num_agency   = $_POST['fax_num_agency'];
			$phone_num_agency   = $_POST['phone_num_agency'];
			$id   = $_POST['agency-id'];
			
			if($agencyname == ""){
				$response = array ( "status" => false);
				print json_encode($response);
			
				$mysql->conn_close();
				exit;
			}
			
			
						
			
			if($id==0){
				$mysql->begin();
			
				$sql1 = "INSERT INTO agency (
				`agency_name`,
				`agency_code`,
				`created_datetime`,
				`created_by`,
				`agency_type`,
				`is_autoinvoice`,
				`markup`,
				`agency_commision`,
				`agency_discount`,
				`currency`
				)
				VALUES(
				'$agencyname',
				'$code',
				NOW(),
				'".$_SESSION['USERNAME']."',
				'$agency_type',
				'$is_agency_auto_invoice',
				'$agency_markup',
				'$agency_comm',
				'$agency_disc',
				'$currency_agency'
				);";
				
				$res_id = $mysql->execute_query($sql1, true);
			
			
				$sql2 = "INSERT INTO business_card (
				`card_name`,
				`card_code`,
				`position`,
				`department`,
				`internal_notes`,
				`country`,
				`state`,
				`city`,
				`address`,
				`email`,
				`phone`,
				`fax`,
				`web_url`,
				`id_agency`
				)
				VALUES(
				'$cardname',
				'$bcode',
				'$position',
				'$department',
				'$internal_notes',
				'$country',
				'$state',
				'$city',
				'$address',
				'$email_agency',
				'$phone_num_agency',
				'$fax_num_agency',
				'$url_agency',
				$res_id
				);";
				
				
					
				$res = $mysql->execute_query($sql2, false);
			
				if( $res_id > 0 && $res){
					$mysql->commit();
					$response = array ( "status" => true);
				}else{
					$mysql->rollback();
					$response = array ( "status" => false);
				}
			
			
			}else{
				$mysql->begin();
			
				$sql1 = "UPDATE agency SET 
				`agency_name` = '$agencyname',
				`agency_code` = '$code',
				`agency_type` = '$agency_type',
				`is_autoinvoice` = '$is_agency_auto_invoice',
				`markup` = $agency_markup,
				`agency_commision` = $agency_comm,
				`agency_discount` = $agency_disc,
				`currency` = '$currency_agency',
				`updated_datetime` = NOW(),
				`updated_by` = '".$_SESSION['USERNAME']."'
				WHERE
				`id_agency` = $id;
				";
			
			$res1 = $mysql->execute_query($sql1, false);
			
			$sql2 = "UPDATE business_card SET 
				`card_name` ='$cardname',
				`card_code`='$bcode',
				`position`='$position',
				`department`='$department',
				`internal_notes`='$internal_notes',
				`country`='$country',
				`state`='$state',
				`city`='$city',
				`address`='$address',
				`email`='$email_agency',
				`phone`='$phone_num_agency',
				`fax`='$fax_num_agency',
				`web_url`='$url_agency'
				WHERE
				`id_agency` = $id;
				";
				
				
			$res2 = $mysql->execute_query($sql2, false);
			
			
			
				if($res1 && $res2){
					$mysql->commit();
					$response = array ( "status" => true);
				}else{
					$mysql->rollback();
					$response = array ( "status" => false);
				}
				
				
			}
			
			print json_encode($response);
			
			$mysql->conn_close();
  	   		exit;
			
  	  	break; 
		}
		case "view":{
			
			$start = 0;
			if(isset($_GET["start"])){
				$start = $_GET["start"];
			}
			$limit = 15;
			if(isset($_GET["limit"])){
				$limit = $_GET["limit"];
			}
			$filter = "";
			$filter2 = "";
			if(isset($_GET["query"])){
				$filter = " where agency_name like '%".$_GET["query"]."%' ";
				$filter2 = " where agency_name like '%".$_GET["query"]."%' ";
			}
			
			
			$mysql = new Database; 
			
			
			
			$sql = " 
			SELECT
			a.id_agency,
			a.agency_name,
			a.agency_code,
			a.is_active,
			a.currency,
			a.created_by,
			a.created_datetime,
			a.is_autoinvoice,
			a.markup,
			a.agency_commision,
			a.agency_discount,
			a.is_confirmed,
			a.agency_type,
			b.card_name,
			b.card_code,
			`b`.`position`,
			b.department,
			b.internal_notes,
			b.country,
			b.city,
			b.state,
			b.address,
			b.email,
			b.phone,
			b.fax,
			b.web_url
			FROM agency as a
			left join business_card as b on a.id_agency = b.id_agency
			$filter
			LIMIT $start, $limit 
			
			; ";
			$results = $mysql->select_execute_query($sql);	
			
			$sql2 = "SELECT count(*) as total
			from agency 	
			$filter2
			; ";
			$results2 = $mysql->select_execute_query($sql2);	
			
			$response = array ( "success" => true, "total" => $results2[0]["total"], "viewAgency" => $results);
  	  		
  	  		print json_encode($response);
  	  		exit;
  	  	break; 
		}
		case "activate":{
			$mysql = new Database; 
			$id = $_POST['agency_id'];
		
			$sql2 = "UPDATE agency  SET
			`is_active` =1
			WHERE
				`id_agency` = $id;
			";
			$res2 = $mysql->execute_query($sql2,false);
			
			$response = array ( "success" => true);
			print json_encode($response);
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$mysql = new Database; 
			$id = $_POST['agency_id'];
		
			$sql2 = "UPDATE agency  SET
			`is_active` =0
			WHERE
				`id_agency` = $id;
			";
			
			$res2 = $mysql->execute_query($sql2,false);
			
			$response = array ( "success" => true);
			print json_encode($response);
  	   		exit;
  	  	break;  
		}
		
		case "confirm":{
			$mysql = new Database; 
			$id = $_POST['agency_id'];
		
			$sql2 = "UPDATE agency  SET
			`is_confirmed` =1
			WHERE
				`id_agency` = $id;
			";
			
			$res2 = $mysql->execute_query($sql2,false);
			
			$response = array ( "success" => true);
			print json_encode($response);
  	   		exit;
  	  	break;  
		}
		case "unconfirm":{
			$mysql = new Database; 
			$id = $_POST['agency_id'];
		
			$sql2 = "UPDATE agency  SET
			`is_confirmed` =0
			WHERE
				`id_agency` = $id;
			";
			$res2 = $mysql->execute_query($sql2,false);
			
			$response = array ( "success" => true);
			print json_encode($response);
  	   		exit;
  	  	break;  
		}
		
		
		
		/* ENDLOGIN*/
	}
?>