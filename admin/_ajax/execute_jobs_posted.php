<?php
session_start();

require_once('../_classes/_loader.php');



$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];

switch ($action) {
    /* LOGIN*/
    case "view":{

        $mysql = new Database;
        $filter = '';
         if(isset($_GET['query'])){
            if(!empty($_GET['query'])){
                $query = $_GET['query']; 
                   $filter = "WHERE jp.position LIKE '%$query%'";
 
            }
        }

        $sql = "SELECT jp.job_post_id, jp.jobtype, jp.category_id,  jp.position,  jp.location,  jp.experience, jp.job_content,  jp.created_date_time, jp.employer_id, jp.is_paid, jp.is_active,jp.is_confirm, jp.is_premium,jp.request_deletion,  emp.company_name as employer_name, cat.name as category_name  FROM job_post as jp  LEFT JOIN employer as emp on emp.employer_id = jp.employer_id  LEFT JOIN category as cat on cat.category_id = jp.category_id $filter  ORDER BY  jp.created_date_time DESC; ";
        $results = $mysql->select_execute_query($sql);


        $response = array ( "success" => true, "total" => count($results), "view_job_post" => $results);

        print json_encode($response);
        exit;
        break;
    }
    case "activate":{
        $mysql = new Database;
        $id = $_POST['jobposted_id'];

        $sql2 = "UPDATE job_post  SET
			`is_active` ='Y'
			WHERE
				`job_post_id` = $id;
			";
        $res2 = $mysql->execute_query($sql2,false);

        $response = array ( "success" => true);
        print json_encode($response);
        exit;
        break;
    }
    
    case "perDelete":{
        $mysql = new Database;
        $id = $_POST['jobposted_id'];

        $response = array ( "success" => true);

        $sql3 = "SELECT e.employer_id, e.company_name, eu.email FROM employer as e
        LEFT JOIN  employer_user as eu 
        ON e.employer_id = eu.employer_id
        LEFT JOIN job_post as jp
        ON eu.employer_id = jp.employer_id
        WHERE job_post_id = $id";

        $res3 = $mysql->select_execute_query($sql3);

        print_r($res3);

        foreach ($res3 as $key) {
            $to = $key['email'];
            $user = $key['company_name'];
        }

        mail_delete_jobpost($id, $to, $user);

        $sql2 = "Delete from job_post where
                `job_post_id` = $id;
            ";
        $res2 = $mysql->execute_query($sql2,false);

        $response = array ( "success" => true);
        print json_encode($response);
        exit;
        break;
    }

    case "tempDelete":{
        $mysql = new Database;
        $id = $_POST['jobposted_id'];

        $response = array ( "success" => true);
        
        $sql3 = "SELECT e.employer_id, e.company_name, eu.email FROM employer as e
        LEFT JOIN  employer_user as eu 
        ON e.employer_id = eu.employer_id
        LEFT JOIN job_post as jp
        ON eu.employer_id = jp.employer_id
        WHERE job_post_id = $id";

        $res3 = $mysql->select_execute_query($sql3);

        print_r($res3);

        foreach ($res3 as $key) {
            $to = $key['email'];
            $user = $key['company_name'];
        }

        mail_delete_jobpost($id, $to, $user);

        $sql2 = "Update job_post set flagged_deleted = 'Y' where
                `job_post_id` = $id;
            ";
        $res2 = $mysql->execute_query($sql2,false);

        $response = array ( "success" => true);
        print json_encode($response);
        exit;
        break;
    }
    
    case "deactivate":{
        $mysql = new Database;
        $id = $_POST['jobposted_id'];

        $sql2 = "UPDATE job_post  SET
			`is_active` ='N'
			WHERE
				`job_post_id` = $id;
			";
        $res2 = $mysql->execute_query($sql2,false);

        $response = array ( "success" => true);
        print json_encode($response);
        exit;
        break;
    }
    case "activateConfirm":{
        $mysql = new Database;
        $id = $_POST['jobposted_id'];

        $sql2 = "UPDATE job_post  SET
			`is_confirm` ='Y'
			WHERE
				`job_post_id` = $id;
			";
        $res2 = $mysql->execute_query($sql2,false);

        $sql3 = "SELECT e.employer_id, e.company_name, eu.email FROM employer as e
        LEFT JOIN  employer_user as eu 
        ON e.employer_id = eu.employer_id
        LEFT JOIN job_post as jp
        ON eu.employer_id = jp.employer_id
        WHERE job_post_id = $id";

        $res3 = $mysql->select_execute_query($sql3);

        print_r($res3);

        foreach ($res3 as $key) {
            $to = $key['email'];
            $user = $key['company_name'];
        }

        $sql4 = "SELECT * FROM job_post WHERE job_post_id = $id";
        $res4 = $mysql->select_execute_query($sql4);

        print_r($res4);

        foreach ($res4 as $rows) {
            $category_id =  $rows['category_id'];
        }
        
        $sql5 = "SELECT name FROM category WHERE category_id = $category_id";
        $res5 = $mysql->select_execute_query($sql5);

        $msg = "";

        foreach ($res4 as $rows) {
            if($rows['is_urgent'] == "Y"){
                $postype = "Urgent";
            }else{
                $postype = "Non-Urgent";
            }

            $msg .="Post Type: ".$postype."</br>";
            $msg .="Job Type: ".$rows['jobtype']."</br>";
            $msg .="Position: ".$rows['position']."</br>"; 
            $msg .="Description: ".$rows['job_content']."</br>";
            $msg .="Years of Experience: ".$rows['experience']."</br>";
            $msg .="Category: ".$category_id."</br>";
            $msg .="Location: ".$rows['location']."</br>";
            $msg .="Salary Type: ".$rows['salary_type']."</br>";
            $msg .="Salary From: ".$rows['salary_from']."</br>";
            $msg .="Salary To: ".$rows['salary_to']."</br>";

        }

        mail_success_jobpost($msg, $id, $to, $user);

        $response = array ( "success" => true);
        print json_encode($response);


        exit;
        break;
    }
    case "deactivateConfirm":{
        $mysql = new Database;
        $id = $_POST['jobposted_id'];

        $sql2 = "UPDATE job_post  SET
			`is_confirm` ='N'
			WHERE
				`job_post_id` = $id;
			";
        $res2 = $mysql->execute_query($sql2,false);

        $response = array ( "success" => true);
        print json_encode($response);
        exit;
        break;
    }
    /* ENDLOGIN*/
}

function mail_success_jobpost($data, $jobpost_id, $to, $user){

            if($_SERVER['SERVER_NAME'] =='localhost'){
            $base_url   = 'http://localhost/patsada.com/';
            }else{
            $base_url   = 'http://patsada.com/';
            }


                 $msqHeader = '<table align="center" cellspacing="0" border="0" cellpadding="0" width="700" bgcolor="#FFFFFF" style="width:700px;background-color:#fff;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
        <tbody>
        <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;">
        <a href="'.$base_url.'" target="_blank" style="text-decoration:none;">
        <img src="'.$base_url.'assets/img/patsada_logo.PNG" style="height:30px;width:150px;padding-top:3px;margin-left:20px;" alt="PATSADA.com"><br>
        <span style="color:#fff;margin-left: 21px;font-size:12px;">Jobs, Right at Your Finger Tips</span>
        </a>

        </td>
        </tr>';

                $msqBody = '
        <tr>
            <td style="padding-top:34px;padding-left:39px;padding-right:39px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd">
                <h2 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:30px;color:#262626;font-weight:normal;margin-top:0;margin-bottom:13px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;letter-spacing:0">
                </h2>

            
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">                 
                    Dear '.$user.',
                    <br><br>
                    Congratulations! Your job post has successfully been posted on the site.
                    <br>
                    The Job number is <b>'.$jobpost_id.'</b> with the following data:
                     <br><br>

                     '.$data.'

                    <br><br>
                    The job number will be used as reference for your future transactions (e.g. getting applicants, editing job post, and deleting job post using the E-Mail Feature) 
                    with the job post therefore it must be noted at all times. Thank you.
                    <br><br>
                    
                </h3>

                    <br>'
                    ;

                $msqFooter = '
        <tr>
            <td style="color:#797c80;font-size:12px;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;padding-top:23px;padding-left:39px;padding-right:13px;padding-bottom:23px;text-align:left">
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                About PATSADA.com
                </h3>
                <p style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:13px;padding-right:0;padding-left:0;line-height:20px">
                The word PaTsada is a local word from Cagayan de Oro City.<br>
                        Hence <b>PaTsada.com</b> is an online Job Search Site for kagay-anons. <br>
                        We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                        <b>PATSADA.com</b>... Jobs, Right at Your Fingertips

                </p>

            </td>
        </tr>


        <tr>
    </tr>
    <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;text-align: right;font-size: 10px;">
            &copy; Copyright 2013. PATSADA.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359
        </td>
        </tr>
    </tbody>
    </table>';

                require '../PHPMailer-master/PHPMailerAutoload.php';

                echo $to;
                echo $user; 

                $msgWhole = $msqHeader.$msqBody.$msqFooter; 
                $subject = '[PaTSADA] Job Post Success';
                //echo "../_documents/".$results[0]['filename'];

                $mail = new PHPMailer;

                $mail->AddReplyTo("noreply@patsada.com","PaTSADA");

                $mail->SetFrom("noreply@patsada.com","PaTSADA");
                $mail->AddAddress($to, $user);
                $mail->Subject   = $subject;
                $mail->MsgHTML($msgWhole);
                //$mail->AddAttachment("../_documents/".$results[0]['filename']);

                if($_SERVER['SERVER_NAME'] =='localhost'){
                    $file = '../mailHtml.html';

                    // Write the contents back to the file
                    file_put_contents($file, $msgWhole);

                }else{
                    if($mail->Send()){
                        $response = array ( "success" => true);
                    }else{
                        echo "Mailer Error: " . $mail->ErrorInfo;
                        $response = array ( "failure" => true);
                    }
                }
}


function mail_delete_jobpost($jobpost_id, $to, $user){

            if($_SERVER['SERVER_NAME'] =='localhost'){
            $base_url   = 'http://localhost/patsada.com/';
            }else{
            $base_url   = 'http://patsada.com/';
            }



                 $msqHeader = '<table align="center" cellspacing="0" border="0" cellpadding="0" width="700" bgcolor="#FFFFFF" style="width:700px;background-color:#fff;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
        <tbody>
        <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;">
        <a href="'.$base_url.'" target="_blank" style="text-decoration:none;">
        <img src="'.$base_url.'assets/img/patsada_logo.PNG" style="height:30px;width:150px;padding-top:3px;margin-left:20px;" alt="PATSADA.com"><br>
        <span style="color:#fff;margin-left: 21px;font-size:12px;">Jobs, Right at Your Finger Tips</span>
        </a>

        </td>
        </tr>';

                $msqBody = '
        <tr>
            <td style="padding-top:34px;padding-left:39px;padding-right:39px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd">
                <h2 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:30px;color:#262626;font-weight:normal;margin-top:0;margin-bottom:13px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;letter-spacing:0">
                </h2>

            
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">                 
                    Dear '.$user.',
                    <br><br>
                    Job number is '.$jobpost_id.' has successfully been deleted and will no longer be published in the site. Thank you.
                    <br><br>
                    
                </h3>

                    <br>'
                    ;

                $msqFooter = '
        <tr>
            <td style="color:#797c80;font-size:12px;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;padding-top:23px;padding-left:39px;padding-right:13px;padding-bottom:23px;text-align:left">
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                About PATSADA.com
                </h3>
                <p style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:13px;padding-right:0;padding-left:0;line-height:20px">
                The word PaTsada is a local word from Cagayan de Oro City.<br>
                        Hence <b>PaTsada.com</b> is an online Job Search Site for kagay-anons. <br>
                        We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                        <b>PATSADA.com</b>... Jobs, Right at Your Fingertips

                </p>

            </td>
        </tr>


        <tr>
    </tr>
    <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;text-align: right;font-size: 10px;">
            &copy; Copyright 2013. PATSADA.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359
        </td>
        </tr>
    </tbody>
    </table>';

                require '../PHPMailer-master/PHPMailerAutoload.php';

                echo $to;
                echo $user; 

                $msgWhole = $msqHeader.$msqBody.$msqFooter; 
                $subject = '[PaTSADA] Job Delete Success';
                //echo "../_documents/".$results[0]['filename'];

                $mail = new PHPMailer;

                $mail->AddReplyTo("noreply@patsada.com","PaTSADA");

                $mail->SetFrom("noreply@patsada.com","PaTSADA");
                $mail->AddAddress($to, $user);
                $mail->Subject   = $subject;
                $mail->MsgHTML($msgWhole);
                //$mail->AddAttachment("../_documents/".$results[0]['filename']);

                if($_SERVER['SERVER_NAME'] =='localhost'){
                    $file = '../mailHtml.html';

                    // Write the contents back to the file
                    file_put_contents($file, $msgWhole);

                }else{
                    if($mail->Send()){
                        $response = array ( "success" => true);
                    }else{
                        echo "Mailer Error: " . $mail->ErrorInfo;
                        $response = array ( "failure" => true);
                    }
                }
}

?>