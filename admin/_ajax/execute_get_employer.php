<?php
session_start();
require_once('../_classes/_loader.php');


$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];

switch ($action) {
    /* LOGIN*/
    case "view":{

        $mysql = new Database;
        $filter = '';
        if(isset($_GET['query'])){
            if($_GET['query'] !=''){
                $query = $_GET['query'];
                   $filter = "AND e.company_name LIKE '$query%'";
            }
        }
        $sql = "SELECT e.employer_id,e.company_name,eu.email,eu.employer_user_id,l.login_credentials_id,l.password_r,l.user_name,l.created_datetime,e.is_active,e.is_confirmed FROM `employer` as e left join employer_user as eu on eu.employer_id = e.employer_id left join login_credentials as l on l.employer_user_id = eu.employer_user_id  WHERE eu.is_manager <> 'N' $filter";
        $results = $mysql->select_execute_query($sql); 
        $arrays = array();    
        foreach ($results as $row) {
                
               $pass_r =  base64_decode($row['password_r']); 

               $row['password_r'] = $pass_r;
               array_push($arrays, $row);
        }

        $response = array ( "success" => true, "total" => count($results), "view_employer_post" => $arrays);

        print json_encode($response);
        exit;
        break;
    }
    case "activate":{
        $mysql = new Database;
        $id = $_POST['employer_id'];

        $sql2 = "UPDATE employer  SET
			`is_active` ='Y'
			WHERE
				`employer_id` = $id;
			";
        $res2 = $mysql->execute_query($sql2,false);

        $response = array ( "success" => true);
        print json_encode($response);
        exit;
        break;
    }
    case "deactivate":{
        $mysql = new Database;
        $id = $_POST['employer_id'];

        $sql2 = "UPDATE employer  SET
			`is_active` ='N'
			WHERE
				`employer_id` = $id;
			";
        $res2 = $mysql->execute_query($sql2,false);

        $response = array ( "success" => true);
        print json_encode($response);
        exit;
        break;
    }
    case "activateConfirm":{
        $mysql = new Database;
        $id = $_POST['employer_id'];

        $sql2 = "UPDATE employer  SET
			`is_confirmed` ='Y'
			WHERE
				`employer_id` = $id;
			";
            echo $sql2;
        $res2 = $mysql->execute_query($sql2,false);

        $response = array ( "success" => true);
        print json_encode($response);
        exit;
        break;
    }
    case "perDelete":{
         $mysql = new Database;
        $employer_id = $_POST['employer_id'];
        $employer_user_id = $_POST['emp_user_id'];
        $login_credentials_id = $_POST['log_cred_id'];

        echo $employer_user_id;
        echo $login_credentials_id;

        $sql2 = "Delete from employer where
                `employer_id` = $employer_id;
            ";
        $res2 = $mysql->execute_query($sql2,false);

        $sql3 = "Delete from employer_user where
                `employer_user_id` = $employer_user_id and `employer_id` = $employer_id;
            ";

            echo $sql3;
        $res3 = $mysql->execute_query($sql3,false);

        $sql4 = "Delete from login_credentials where
                `login_credentials_id` = $login_credentials_id and `employer_user_id` = $employer_user_id;
            ";
        $res4 = $mysql->execute_query($sql4,false);

        echo $sql4;

        $response = array ( "success" => true);
        print json_encode($response);
        exit;
        break;
    }
    case "deactivateConfirm":{ 
        $mysql = new Database;
        $id = $_POST['employer_id'];

        $sql2 = "UPDATE employer  SET
			`is_confirmed` ='N'
			WHERE
				`employer_id` = $id;
			";
        $res2 = $mysql->execute_query($sql2,false);

        $response = array ( "success" => true);
        print json_encode($response);
        exit;
        break;
    }
    case "newEmployer":{
        $mysql = new Database;




        $company_name = $_POST['company_name'];
        $username = $_POST['username'];

        if(isset($_POST['password'])){
            $password = $_POST['password'];
        }
        if(isset($_POST['password_r'])){
            $password = $_POST['password_r'];
        }
        $email = $_POST['email'];
        $employer_id = $_POST['employer_id'];
        $employer_user_id = $_POST['employer_user_id'];
        $login_credentials_id = $_POST['login_credentials_id'];

        $authentication_code = generateAuthenticationCode(20);
        $company_code = generateCompanyCode($company_name,6);


     


     
        if($_POST['employer_id'] == 0){    //IF employer_id = 0 
                                          //new employer

            $query1 = "SELECT * from employer where company_name = '".$company_name."'";
            $results1 = $mysql->select_execute_query($query1); 
            
            $query2 = "SELECT * from employer_user where email = '".$email."'";
            $results2 = $mysql->select_execute_query($query2); 

            $query3 = "SELECT * from login_credentials where user_name = '".$username."'";
            $results3 = $mysql->select_execute_query($query3); 

             $resStr = "";
            if(count($results1) > 0 || count($results2) > 0 || count($results3) > 0){
                   
                    $result = array();
                    if (count($results1) > 0) $resStr .= " Company Name: ".$company_name;
                    if (count($results2) > 0) $resStr .= "  | Email: ".$email;
                    if (count($results3) > 0) $resStr .= " | Username: ".$username; 

                     $resStr .= " has been taken already!"; 
                    $resT = json_encode($result);


                    print json_encode(array("success" =>false,"message"=> $resStr));
                    exit;
                    break;



             }else{
            $datetime = date('Y-m-d h:i:s');
            $sql2 = "INSERT INTO employer (authentication_code,company_code,company_name,is_active,is_confirmed,created_datetime) 
        VALUES 
        ('".$authentication_code."',
        '".$company_code."',
        '".$company_name."',
        'N',
        'N',
        '".$datetime."'
            )";  
        $employer_id = $mysql->execute_query($sql2,true);

        //employer_user table
        $sql3 = "INSERT INTO employer_user (employer_id,email,is_active,is_manager,is_confirmed)
         VALUES 
         ('".$employer_id."',
          '".$email."',
          'N',
          'Y',
          'N'
          )"; 
    

        
        $employer_user_id = $mysql->execute_query($sql3,true);

        //login credentials table
        $sql4 = "INSERT INTO login_credentials 
        (is_candidate,is_employer,candidate_id,employer_user_id,user_name,password,password_r,created_datetime,is_active,is_confirmed,is_admin) 
        VALUES 
            ('N',
            'Y',
             0,
            '".$employer_user_id."',
            '".$username."',
            '".md5($password)."',
            '".base64_encode($password)."',
            '".$datetime."',
            'N',
            'N',
            'N')"; 

        $res2 = $mysql->execute_query($sql4,false); 
    }// end for else sa if count();


    }else{
        //do update
        //IF employer_id > 0 
        // update employer
        //Put code for update here

        $password_md5 = md5($password);
        $password_base64 = base64_encode($password);


        // Employer table update
         $sql_update1 ="UPDATE employer SET company_name='".$company_name."' WHERE employer_id=".$employer_id ;  
        $res1 = $mysql->execute_query($sql_update1,false);

          // Employer_user table update
         $sql_update2 ="UPDATE employer_user SET email='".$email."' WHERE employer_id=".$employer_id." and employer_user_id=".$employer_user_id ;  
         $res2 = $mysql->execute_query($sql_update2,false);
         

          // Login_credentials table update
         $sql_update3 ="UPDATE login_credentials SET user_name='".$username."',password='".md5($password)."',password_r='".base64_encode($password)."' WHERE login_credentials_id=".$login_credentials_id." and employer_user_id=".$employer_user_id ;  
         $res3 = $mysql->execute_query($sql_update3,false);



    }
       

        $response = array ( "success" => true);
        print json_encode($response);
        exit;
        break;
    }
    /* ENDLOGIN*/
}


function generateAuthenticationCode($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
  }


 function generateCompanyCode($agencyName, $length = 6) { // to generate a company code
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    $result = preg_replace("/[^a-zA-Z0-9]+/", "", $agencyName); 
     $characters = $result;
    $randomString2 = '';
    for ($i = 0; $i < 3; $i++) {
        $randomString2 .= $characters[rand(0, strlen($characters) - 1)];
    }
    return  $randomString2.$randomString;




  }


?>