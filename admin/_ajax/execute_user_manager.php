<?php
session_start();
require_once('../_classes/_loader.php');


$action = (isset($_POST['action'])) ? $_POST['action'] : $_GET['action'];
	
	switch ($action) {
		/* LOGIN*/
		case "submitCreateSysUser":{
			$mysql = new Database; 
			$tools = new Tools; 
			
			$aliasname   = $_POST['aliasname'];
			$code = $tools->generateCode($aliasname); 
			$username   = $_POST['username'];
			$pass   = $_POST['pass'];
			$repass   = $_POST['repass'];
			$id = $_POST['user_system_id'];
			$role = $_POST['user_role_id'];
			
			if($username == "" || $pass =="" || $aliasname =="" || $pass == "" || $pass != $repass){
				$response = array ( "status" => false);
				print json_encode($response);
			
				$mysql->conn_close();
				exit;
			}
			
			
			$user_sys_man_title_combo_name   = $_POST['user_title'];
			$firstname   = $_POST['firstname'];
			$lastname   = $_POST['lastname'];
			$email = $_POST['email'];
			$phone_num   = $_POST['phone_num'];
			$mobile_num   = $_POST['mobile_num'];
			$country = $_POST['country'];
			$state = $_POST['state'];
			$address = $_POST['address'];
			
			
			if($id==0){
				$mysql->begin();
			
				$sql = "INSERT INTO account (
				`account_name`,
				`account_code`,
				`username`,
				`password`,
				`created_datetime`,
				`created_by`,
				`id_agency`,
				`is_agency_manager`,
				`role`
				)
				VALUES(
				'$aliasname',
				'$code',
				'$username',
				'".md5($pass)."',
				NOW(),
				'".$_SESSION['USERNAME']."',
				1,
				1,
				'$role'
				);";
			
			$idtemp = $mysql->execute_query($sql, true);
			
			$sql2 = "INSERT INTO account_info (
			`id_account`,
			`first_name`,
			`last_name`,
			`title`,
			`email`,
			`country`,
			`state`,
			`address`,
			`phone_number`,
			`mobile_number`
			)
			VALUES(
			'$idtemp',
			'$firstname',
			'$lastname',
			'$user_sys_man_title_combo_name',
			'$email',
			'$country',
			'$state',
			'$address',
			'$phone_num',
			'$mobile_num'
			);";
			
			$res = $mysql->execute_query($sql2,false);
			
				if($idtemp > 0 && $res){
					$mysql->commit();
					$response = array ( "status" => true);
				}else{
					$mysql->rollback();
					$response = array ( "status" => false);
				}
			
			
			}else{
				$mysql->begin();
			
				$sql3 = "UPDATE account SET 
				`account_name` = '$aliasname',
				`account_code` = '$code',
				`username` = '$username',
				`password` = '".md5($pass)."',
				`updated_datetime` = NOW(),
				`updated_by` = '".$_SESSION['USERNAME']."'
				WHERE
				`id_account` = $id;
				";
			
			$res1 = $mysql->execute_query($sql3, false);
			
			
			$sql2 = "UPDATE account_info  SET
			`first_name` ='$firstname',
			`last_name` = '$lastname',
			`title` = '$user_sys_man_title_combo_name',
			`email` = '$email',
			`country` = '$country',
			`state` = '$state',
			`address` = '$address',
			`phone_number` = '$phone_num',
			`mobile_number` = '$mobile_num'
			WHERE
				`id_account` = $id;
			";
			$res2 = $mysql->execute_query($sql2,false);
			
				if($res2  && $res1){
					$mysql->commit();
					$response = array ( "status" => true);
				}else{
					$mysql->rollback();
					$response = array ( "status" => false);
				}
				
				
			}
			
			print json_encode($response);
			
			$mysql->conn_close();
  	   		exit;
			
  	  	break; 
		}
		case "viewUser":{
			
			$start = 0;
			if(isset($_GET["start"])){
				$start = $_GET["start"];
			}
			$limit = 15;
			if(isset($_GET["limit"])){
				$limit = $_GET["limit"];
			}
			
			$filter = "";
			$filter2 = "";
			if(isset($_GET["query"])){
				$filter = " and a.account_name like '%".$_GET["query"]."%' ";
				$filter2 = " and account_name like '%".$_GET["query"]."%' ";
			}
			
			$role = $_GET["role"];
			
  	  		$mysql = new Database; 
			
			$sql = "SELECT 
			a.id_account, 
			a.account_code,
			a.account_name,
			a.username,
			a.is_active,
			a.is_confirmed,
			a.created_datetime,
			a.updated_datetime,
			a.created_by,
			a.role,
			ai.first_name,
			ai.last_name,
			ai.title,
			ai.email,
			ai.country,
			ai.state,
			ai.address,
			ai.phone_number,
			ai.mobile_number 
			from account as a 
			left join account_info as ai on ai.id_account = a.id_account
			where a.role ='$role' 
			$filter
			ORDER BY a.created_datetime DESC
			LIMIT $start, $limit 
			
			; ";
			$results = $mysql->select_execute_query($sql);	
			
			$sql2 = "SELECT count(*) as total
			from account 
			where role ='$role' 
			$filter2		
			; ";
			$results2 = $mysql->select_execute_query($sql2);	
			
			$response = array ( "success" => true, "total" => $results2[0]["total"], "viewUser" => $results);
  	  		
  	  		print json_encode($response);
  	  		exit;
  	  	break; 
		}
		case "activate":{
			$mysql = new Database; 
			$id = $_POST['user_id'];
		
			$sql2 = "UPDATE account  SET
			`is_active` =1
			WHERE
				`id_account` = $id;
			";
			$res2 = $mysql->execute_query($sql2,false);
			
			$response = array ( "success" => true);
			print json_encode($response);
  	   		exit;
  	  	break;  
		}
		case "deactivate":{
			$mysql = new Database; 
			$id = $_POST['user_id'];
		
			$sql2 = "UPDATE account  SET
			`is_active` =0
			WHERE
				`id_account` = $id;
			";
			$res2 = $mysql->execute_query($sql2,false);
			
			$response = array ( "success" => true);
			print json_encode($response);
  	   		exit;
  	  	break;  
		}
		case "activateConfirm":{
			$mysql = new Database; 
			$id = $_POST['user_id'];
		
			$sql2 = "UPDATE account  SET
			`is_confirmed` =1
			WHERE
				`id_account` = $id;
			";
			$res2 = $mysql->execute_query($sql2,false);
			
			$response = array ( "success" => true);
			print json_encode($response);
  	   		exit;
  	  	break;  
		}
		case "deactivateConfirm":{
			$mysql = new Database; 
			$id = $_POST['user_id'];
		
			$sql2 = "UPDATE account  SET
			`is_confirmed` =0
			WHERE
				`id_account` = $id;
			";
			$res2 = $mysql->execute_query($sql2,false);
			
			$response = array ( "success" => true);
			print json_encode($response);
  	   		exit;
  	  	break;  
		}
		/* ENDLOGIN*/
	}
?>