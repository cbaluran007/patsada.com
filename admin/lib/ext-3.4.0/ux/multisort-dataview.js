/*!
 * Ext JS Library 3.4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */
Ext.onReady(function() {
   var store = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "../_ajax/execute_get_booking.php?action=getResultSession",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewResultsSession",
		            id: "code",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "id", mapping: "id"},
						{name: "image", mapping: "image"},
						{name: "code", mapping: "code"}
            			
			
			
        			]
					}),
					sortInfo:{field: 'code', direction: "DESC"}
				});
			
	var dataview = new Ext.DataView({
        store: store,
        tpl  : new Ext.XTemplate(
            '<ul>',
                '<tpl for=".">',
                    '<li class="phone">',
                        '<img width="64" height="64" src="{image}" />',
                        '<strong>{id}</strong>',
                        '<span>{code}</span>',
                    '</li>',
                '</tpl>',
            '</ul>'
        ),
        
        plugins : [
            new Ext.ux.DataViewTransition({
                duration  : 550,
                idProperty: 'id'
            })
        ],
        id: 'phones',
        
        itemSelector: 'li.phone',
        overClass   : 'phone-hover',
        singleSelect: true,
        multiSelect : true,
        autoScroll  : true
    });
	
	var tbar = new Ext.Toolbar({
        items  : ['Sort on these fields:', ''],
        plugins: [new Ext.ux.ToolbarReorderer()],
        
        listeners: {
            scope    : this,
            reordered: function(button) {
                changeSortDirection(button, false);
            }
        }
    });
    
    new Ext.Panel({
        title: 'Animated DataView',
        layout: 'fit',
        items : dataview,
        height: 615,
        width : 800,
        tbar  : tbar,
        renderTo: 'docbody'
    });
    
    tbar.add(createSorterButton({
        text: 'Megapixels',
        sortData: {
            field: 'code',
            direction: 'DESC'
        }
    }));
    
    tbar.add(createSorterButton({
        text: 'Price',
        sortData: {
            field: 'code',
            direction: 'DESC'
        }
    }));
    
    //perform an initial sort
    doSort();
    
    //The following functions are used to get the sorting data from the toolbar and apply it to the store
    
    /**
     * Tells the store to sort itself according to our sort data
     */
    function doSort() {
        store.sort(getSorters(), "ASC");
    };
    
    /**
     * Callback handler used when a sorter button is clicked or reordered
     * @param {Ext.Button} button The button that was clicked
     * @param {Boolean} changeDirection True to change direction (default). Set to false for reorder
     * operations as we wish to preserve ordering there
     */
    function changeSortDirection(button, changeDirection) {
        var sortData = button.sortData,
            iconCls  = button.iconCls;
        
        if (sortData != undefined) {
            if (changeDirection !== false) {
                button.sortData.direction = button.sortData.direction.toggle("ASC", "DESC");
                button.setIconClass(iconCls.toggle("sort-asc", "sort-desc"));
            }
            
            store.clearFilter();
            doSort();
        }
    };
    
    /**
     * Returns an array of sortData from the sorter buttons
     * @return {Array} Ordered sort data from each of the sorter buttons
     */
    function getSorters() {
        var sorters = [];
        
        Ext.each(tbar.findByType('button'), function(button) {
            sorters.push(button.sortData);
        }, this);
        
        return sorters;
    }
    
    /**
     * Convenience function for creating Toolbar Buttons that are tied to sorters
     * @param {Object} config Optional config object
     * @return {Ext.Button} The new Button object
     */
    function createSorterButton(config) {
        config = config || {};
              
        Ext.applyIf(config, {
            listeners: {
                click: function(button, e) {
                    changeSortDirection(button, true);                    
                }
            },
            iconCls: 'sort-' + config.sortData.direction.toLowerCase(),
            reorderable: true
        });
        
        return new Ext.Button(config);
    };
	store.load();
	
});