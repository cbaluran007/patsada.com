<?php

require_once('_config.php');


class Database{
	
	public static $conn; 
	public function __construct(){
		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DBNAME);
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}
		
		self::$conn = $mysqli;
		
	}
	
	public function select_execute_query($query){
	
		
		if($result = self::$conn->query($query)){
			$data= array();
			while($row = $result->fetch_assoc()){
				$data[] = $row;
			}
			
			return $data;
									
		}else{
			echo 'error on'.$query;
			echo '<script type="text/javascript">alert("Error executing database query \n no result query")</script>';
		}
	}
	
	
	public function execute_query($query,$withId){
			
		if($res = self::$conn->query($query)){
			if($withId){
				return self::$conn->insert_id;	
			}else{
				return true;
			}
		}else{
			return false;
		}
		
	}	
	public function conn_close(){
			
		self::$conn->close();
		
	}
	public function begin(){
			
		self::$conn->autocommit(FALSE);
		
	}
	public function commit(){
			
		self::$conn->commit();
		
	}
	public function rollback(){
			
		self::$conn->rollback();
		
	}
	
}
?>