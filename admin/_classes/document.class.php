<?php

require_once('database.class.php');
require_once('invoice.class.php');
require_once('item.class.php');
require_once('agency.class.php');
require_once('booking.class.php');
require_once('account.class.php');
require_once('receipt.class.php');
require_once('product.class.php');
require_once('voucher.class.php');
require_once('supplier.class.php');
require_once('itinerary.class.php');
require_once('item_sub.class.php');
include("../_includes/pdfGen/mpdf.php");

class Document{
	public static $mysql; 
	public function __construct(){
		$mysql_conn = new Database; 
		
		
		self::$mysql = $mysql_conn;
		
	}
	
	public function generateDocs($content,$title,$fileName,$code){
	$styleTitle= "panel-title";
	if($title == 'PREPAID VOUCHER' ){
		$styleTitle= "panel-title2";
	}
	$html = '
		<html>

		<head>
		</head>
		<body>
		<div class="container">
			<div class="row1">
				<div  class="logo left"> <img src="../images/logo.png" alt="" height="50px"></div>
				 <div class="'.$styleTitle.' right" >'.$title.'</div>
			 </div>
			 '.$content.'
		 

		</div>
		</body>
		</html>

		';
		


	$mpdf=new mPDF('c','A4', 11, 'DejaVuSansCondensed', 5, 5, 5, 15,10,10);
	$mpdf->SetHTMLFooter('<div align="center"><span style="font-size:12px;">Eastern Eurotours &copy; All Rights Reserved 2013</span></div>');
	$mpdf->SetHTMLFooter('<div align="center"><span style="font-size:12px;">Eastern Eurotours &copy; All Rights Reserved 2013</span></div>','E');
	$stylesheet = file_get_contents('../css/style_pdf.css');
	$mpdf->WriteHTML($stylesheet,1); 
	$mpdf->WriteHTML(utf8_encode($html));



	 
	//$mpdf->Output();
	$mpdf->Output('../_documents/'.$fileName);
	
	$doc_id = 0;
	$mysql = new Database; 
	$sql = "INSERT INTO document (
				`text`,
				 created_datetime,
				`created_by`,
				`filename`,
				`code`
				)
				VALUES(
				'$html',
				 NOW(),
				'".$_SESSION['USERNAME']."',
				'$fileName',
				'$code'
				);";
	$doc_id = $mysql->execute_query($sql, true);
	
		
	return $doc_id;
		
	}
	public function Invoice($invoiceID,$agencyID,$bookingID){
	$invoice = new Invoice($invoiceID); 
	$invoiceDetails = $invoice->getInvoice();
	$invoiceItems = $invoice->getItemsInvoice();
	
	$agency = new Agency($agencyID); 
	$agencyDetails = $agency->getAgency();
	$BusinessCardDetails = $agency->getBusinessCard();
	$booking = new Booking($bookingID); 
	$bookingDetails = $booking->getBooking();
	$account = new Account($bookingDetails[0]['id_account']); 
	$res_account = $account->getAccount();
	
	
	$start_datetime = new DateTime($bookingDetails[0]["start_datetime"]);
	$end_datetime = new DateTime($bookingDetails[0]["end_datetime"]);
	$created_datetime = new DateTime($invoiceDetails[0]["created_datetime"]);
	$invoice_date = new DateTime($invoiceDetails[0]["invoice_date"]);
	
	
	
	$content = '
		<div class="row2">
		<div class="panel-heading">
            D E T A I L S
        </div>
			<div class="info1">
				<table class="tableInfo1 left">
							<tbody>
								<tr><td class="subhead">Name:</td>
									<td class="data">'.$agencyDetails[0]["agency_name"].'</td>
									<td class="subhead">Creation Date:</td>
									<td class="data">'.$start_datetime->format('d, M Y').'</td>
								</tr>
								<tr><td class="subhead">Address:</td>
									<td class="data">'.$BusinessCardDetails[0]['address'].', '.$BusinessCardDetails[0]['city'].', '.$BusinessCardDetails[0]['country'].'</td>
									<td class="subhead">Departure Date:</td>
									<td class="data">'.$end_datetime->format('d, M Y').'</td>
								</tr>
								<tr><td class="subhead">Business Phone:</td>
									<td class="data">'.$BusinessCardDetails[0]['phone'].'</td>
									<td class="subhead">Consultant:</td>
									<td class="data">'.$res_account[0]['account_name'].'</td>
								</tr>
								<tr><td class="subhead">Business Fax:</td>
									<td class="data">'.$BusinessCardDetails[0]['fax'].'</td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
				 </table>
			</div>	
			
			<div class="info2">
				<table class="tableInfo1 right">
					<tr><td class="subhead">Invoice Reference:</td><td class="subhead">'.$invoiceDetails[0]["ref"].'</td></tr>
					<tr><td class="subhead">&nbsp;</td><td>&nbsp;</td></tr>
					<tr><td class="subhead">Document Printed:</td><td>'.$created_datetime->format('d, M Y').'</td></tr>
					<tr><td class="subhead">Invoice Date:</td><td>'.$invoice_date->format('d, M Y').'</td></tr>
					
				</table>
			</div>
	 </div>
	 <div class="bodyrow">
	 
	 
	 <table class="table-primary" >
                <thead>
                    <tr class="table-primary-head">
                        <th >Description</th>
                        <th   > Gross amt  </th>
                        <th > Commission amt </th>
                        <th  > Cost amt </th>
                    </tr>
                </thead>
        <tbody>';
	$TgrossAmt = 0;
	$TnettAmt = 0;
	$TcommAmt = 0;
    foreach($invoiceItems as $invoiceItem){   
	
		$item = new Item($invoiceItem['id_item']); 
		$itemName = $item->getProductName();
		$lead = $item->getItemsLeadPax();
		$itemNameSub = $item->getItemsSub();
		$grossAmt = $invoiceItem['gross_amount'];	
		$nettAmt = $invoiceItem['nett_amount'];	
		$commAmt = $invoiceItem['commision_amount'];	
		$TgrossAmt += $grossAmt;
		$TnettAmt += $nettAmt;
		$TcommAmt +=$commAmt;
	  $content .= '
			<tr >
				<td class="subhead" >
				I#'.$invoiceItem['id_item'].'-'.$itemName.'<br>';	
				foreach($itemNameSub as $itemSub){   			
					$content .= '<span class="data">'.$itemSub["item_name"].'</span><br>';  
				}
				$content .= '<br><span class="data">'.$lead[0]['title'].' '.$lead[0]['first_name'].' '.$lead[0]['last_name'].' (Lead Guest)</span>' ;
		  $content .= ' </td>
				<td class="dataC"  ><nobr>'.$grossAmt.' AUD</nobr></td>
				<td class="dataC"  ><nobr>'.$commAmt.' AUD</nobr></td>
				<td class="dataC"  ><nobr>'.$nettAmt.' AUD</nobr></td>
				
			</tr>
		';
	}
	$content .= '
        </tbody>
           <tfoot>

				<tr class="table-primary-footer">
				<td  class="subdata">Invoice Total</td>
                
                <td class="subdata small"><nobr>'.$TgrossAmt.' AUD</nobr></td>
                <td class="subdata small"><nobr>'.$commAmt.' AUD</nobr></td>
                <td class="subdata small"><nobr>'.$TnettAmt.' AUD</nobr></td>
            </tr>
            </tfoot>
        </table>
	 
	 
	 </div>';
	 
	 
	 $docID = self::generateDocs($content,'I N V O I C E','invoice-'.$invoiceID.'.pdf','INV|'.$bookingID.'-'.$invoiceID);
	 
	 $mysql = new Database; 
	 $mysql->execute_query("UPDATE invoice set id_document =  $docID where id_invoice = $invoiceID;", false);
	 
	 return $docID;
	 
	 
	 }
	 
	 public function Receipt($receiptID,$bookingID){
		$receipt = new Receipt($receiptID); 
		$receiptDetails = $receipt->getReceipt();
		$receiptItems = $receipt->getItemsReceipt();
		
		$booking = new Booking($bookingID);
		$bookingDetails = $booking->getBooking();
		$account = new Account($bookingDetails[0]['id_account']); 
		$res_account = $account->getAccount();
		$agency = new Agency($booking->getAgencyID());
		$agencyDetails = $agency->getAgency();
		$BusinessCardDetails = $agency->getBusinessCard();
		

		
		$content = '
			<div class="row2">
			<div class="panel-heading">
				D E T A I L S
			</div>
				<div class="info1">
					<table class="tableInfo1 left">
								<tbody>
									<tr><td class="subhead">Name:</td>
										<td class="data">'.$agencyDetails[0]["agency_name"].'</td>
										<td class="subhead">Creation Date:</td>
										<td class="data">'.$bookingDetails[0]["start_datetime"].'</td>
									</tr>
									<tr><td class="subhead">Address:</td>
										<td class="data">'.$BusinessCardDetails[0]['address'].', '.$BusinessCardDetails[0]['city'].', '.$BusinessCardDetails[0]['country'].'</td>
										<td class="subhead">Departure Date:</td>
										<td class="data">'.$bookingDetails[0]["end_datetime"].'</td>
									</tr>
									<tr><td class="subhead">Business Phone:</td>
										<td class="data">'.$BusinessCardDetails[0]['phone'].'</td>
										<td class="subhead">Consultant:</td>
										<td class="data">'.$res_account[0]['account_name'].'</td>
									</tr>
									<tr><td class="subhead">Business Fax:</td>
										<td class="data">'.$BusinessCardDetails[0]['fax'].'</td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
					 </table>
				</div>	
				
				<div class="info2">
					<table class="tableInfo1 right">
						<tr><td class="subhead">Receipt Reference:</td><td class="subhead">'.$receiptDetails[0]["id_booking"].'-'.$receiptDetails[0]["id_receipt"].'</td></tr>
						<tr><td class="subhead">Payment Reference:</td><td class="subhead">'.$receiptDetails[0]["payment_reference"].'</td></tr>
						<tr><td class="subhead">&nbsp;</td><td>&nbsp;</td></tr>
						<tr><td class="subhead">Document Printed:</td><td>'.$receiptDetails[0]["created_datetime"].'</td></tr>
						<tr><td class="subhead">Receipt Date:</td><td>'.$receiptDetails[0]["created_datetime"].'</td></tr>
						
					</table>
				</div>
		 </div>
		 <div class="bodyrow">
		 
		 
		 <table class="table-primary" >
					<thead>
						<tr class="table-primary-head">
							<th >Description</th>
							<th   > Gross amt  </th>
							<th > Commission amt </th>
							<th  > Cost amt </th>
						</tr>
					</thead>
			<tbody>';
		$TgrossAmt = 0;
		$TnettAmt = 0;
		$TcommAmt = 0;
		foreach($receiptItems as $receiptItem){   
		
		$item = new Item($receiptItem['id_item']); 
		$itemName = $item->getProductName();
		$lead = $item->getItemsLeadPax();
		$itemNameSub = $item->getItemsSub();
		$grossAmt = $receiptItem['gross_amount'];	
		$nettAmt = $receiptItem['nett_amount'];	
		$commAmt = $receiptItem['commision_amount'];	
		$TgrossAmt += $grossAmt;
		$TnettAmt += $nettAmt;
		$TcommAmt +=$commAmt;
	  $content .= '
			<tr >
				<td class="subhead" >
				I#'.$receiptItem['id_item'].'-'.$itemName.'<br>';	
				foreach($itemNameSub as $itemSub){   			
					$content .= '<span class="data">'.$itemSub["item_name"].'</span><br>';  
				}
				$content .= '<br><span class="data">'.$lead[0]['title'].' '.$lead[0]['first_name'].' '.$lead[0]['last_name'].' (Lead Guest)</span>' ;
		  $content .= ' </td>
				<td class="dataC"  ><nobr>'.$grossAmt.' AUD</nobr></td>
				<td class="dataC"  ><nobr>'.$commAmt.' AUD</nobr></td>
				<td class="dataC"  ><nobr>'.$nettAmt.' AUD</nobr></td>
				
			</tr>
		';
		}
		$content .= '
			</tbody>
			   <tfoot>

					<tr class="table-primary-footer">
					<td  class="subdata">Total Amount</td>
					
					<td class="subdata small"><nobr>'.$TgrossAmt.' AUD</nobr></td>
					<td class="subdata small"><nobr>'.$commAmt.' AUD</nobr></td>
					<td class="subdata small"><nobr>'.$TnettAmt.' AUD</nobr></td>
				</tr>
				</tfoot>
			</table>
		 
		 
		 </div>';
		 
		 
		 $docID = self::generateDocs($content,'R E C E I P T','receipt-'.$receiptID.'.pdf','RCPT|'.$bookingID.'-'.$receiptID);
		 
		 $mysql = new Database; 
		 $mysql->execute_query("UPDATE receipt set id_document =  $docID where id_receipt = $receiptID;", false);
		 
		 return $docID;
	 
	 
	 }
	 
	 
	 public function voucher($voucherId,$bookingID){
	$voucher = new Voucher($voucherId); 
	$voucherDetails = $voucher->getVoucher();
	$voucherItems = $voucher->getItemsVoucher();
	//print_r($voucherItems);
	$booking = new Booking($bookingID); 
	
	$agency = new Agency($booking->getAgencyID()); 
	$agencyDetails = $agency->getAgency();
	$BusinessCardDetails = $agency->getBusinessCard();
	
	$bookingDetails = $booking->getBooking();
	$account = new Account($bookingDetails[0]['id_account']); 
	$res_account = $account->getAccount();
	
	$content = '
	
		<div style="font-size:10px;padding-top:10px;width:100%;text-align: center;">
            Note* This voucher must be presented at the time of check in / registration.
        </div>
		
	 ';

	  foreach($voucherItems as $voucherItem){   
					//print_r( $voucherItem);
					$item = new Item($voucherItem['id_item']); 
					$itemDetail = $item->getItem();
					$itemName = $item->getProductName();
					$lead = $item->getItemsLeadPax();
					$itemNameSub = $item->getItemsSub();
					$paxs = $item->getItemsPax();
					$product = new Product(0); 
					$productDetails = $product->getProductByCode($item->getProductCode());
					$supplier = new Supplier($itemDetail[0]["id_supplier"]);
					$supplierDetails = $supplier->getSupplier();
					
					$content .= '<div class="bodyrow">';
					$content .= '
					 <table class="table-primary" >
						<thead>
							<tr class="table-primary-head">
								<th >VOUCHER TO</th>
								
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width="50%" class="data" valign="top">
									<span class="subhead">'.$itemName.'</span><br><br>';
									
									
						$content .=	'<span class="subhead">Address:</span><br>
									<span class="data">'.$productDetails[0]["product_address"].', '.$productDetails[0]["destination_city"].', '.$productDetails[0]["destination_country"].' '.$productDetails[0]["product_postal_code"].'</span><br>
									<span class="subhead">Phone:</span><br>
									<span class="data">'.$productDetails[0]["product_phone_number"].'</span><br>
								
						';
						$content .=	'</td>';
						
								
					$content .= '</td>
							</tr>';
					
							
						
					$content .= '	</tbody>
						
					</table>';
					
					$content .= '
					 <table class="table-primary" >
						<thead>
							<tr class="table-primary-head">
								<th colspan=2>CLIENTS INFO TO OUR CLIENTS</th>
								
							</tr>
						</thead>';
						
					$content .= '<tbody>
							<tr>
								<td width="50%" class="data" valign="top">';
								
								
								foreach($itemNameSub as $itemSub){   			
									$content .= '<span class="subhead">'.$itemSub["item_name"].'</span><br>';  
									$itemSubOjct = new ItemSub($itemSub['id_item_sub']);
									$paxArr = $itemSubOjct->getItemsSubPax();
									foreach($paxArr as $itemSubPax){
										$content .= '&bull; '.$itemSubPax['title'].' '.$itemSubPax['first_name'].' '.$itemSubPax['last_name'];
										if($itemSubPax['is_lead'] == 1){
											$content .= ' <span style="color:red;">(Lead Guest/Pax) </span><br>';
										}else{
											$content .= '<br>';
										}
									}
									$content .= '<br>';
								}
								
					$content .= '</td>
								<td width="50%" class="data" valign="top">';
								
								$content .= '<table class="tableInfo1 left" >
										<tbody >';
								$content .= '<tr >
												<td class="subhead" style="border:0;">Confirmation Reference:</td>
												<td class="data" style="border:0;">'.$itemDetail[0]["response_code"].'</td>
												
											</tr>';
								$content .= '<tr >
												<td class="subhead" style="border:0;">Tracking Reference:</td>
												<td class="data" style="border:0;">'.$itemDetail[0]["tracking_code"].'</td>
												
											</tr>';
								$content .= '<tr >
												<td class="subhead" style="border:0;">Booking Reference:</td>
												<td class="data" style="border:0;">B#'.$bookingID.'</td>
												
											</tr>';
									$dateF = new DateTime($itemDetail[0]["date_from"]);
								$content .= '<tr >
												<td class="subhead" style="border:0;">Date From:</td>
												<td class="data" style="border:0;">'.$dateF->format('l d, F Y').'</td>
												
											</tr>';
									
									$dateT = new DateTime($itemDetail[0]["date_to"]);
								$content .= '<tr >
												<td class="subhead" style="border:0;">Date To:</td>
												<td class="data" style="border:0;">'.$dateT->format('l d, F Y').'</td>
												
											</tr>';
								if($itemDetail[0]["id_service"] == 1){	
								$content .= '<tr >
												<td class="subhead" style="border:0;">Duration:</td>
												<td class="data" style="border:0;">'.$itemDetail[0]["DiffDate"].' night(s) </td>
												
											</tr>';
								}
				
								$content .= '		</tbody>
							 </table>';
								
					$content .= '</td>
							</tr>
						</tbody>';
								
					$content .='</table>';
					
					
					$content .= '
					 <table class="table-primary" >
						<thead>
							<tr class="table-primary-head">
								<th colspan=2>PLEASE PROVIDE THE FOLLOWING SERVICES</th>
								
							</tr>
						</thead>';
		$content .= '	<tbody>';
					$content .= '		<tr>
								<td valign="top"  class="data" width="50%" style="border:0px;">
								<span class="subhead">Service Type</span>
								</td>
								<td valign="top" class="data" width="50%" style="border:0px;">';
								if($itemDetail[0]["id_service"] == 1){
									$content .= 'Accommodation';
								}else{
									$content .= 'N/A';
								}
								
					$content .= '</td>
							</tr>';
							
							
					$content .= '		<tr>
								<td valign="top" class="data" style="border:0px;">
								<span class="subhead">Service Description</span>
								</td>
								<td valign="top" class="data" style="border:0px;">';
								
									foreach($itemNameSub as $itemSub){   			
										  
										$itemSubObj = new ItemSub($itemSub['id_item_sub']);
										$paxArr = $itemSubObj->getItemsSubPax();
										$numPaxThis = 0;
										foreach($paxArr as $itemSubPax){
											$numPaxThis++;
										}
										$content .= '<span class="subhead">'.$numPaxThis.' X 1 '.$itemSub["item_name"].' - '.$itemDetail[0]["item_type"].' </span><br>';
									
									}
							
								
					$content .= '</td>
							</tr>';
							
					$content .= '		<tr>
								<td valign="top" class="data" style="border:0px;">
								<span class="subhead">Booked For</span>
								</td>
								<td valign="top" class="data"  style="border:0px;">';
								
									$content .= $itemDetail[0]["number_of_pax"].' PAX';
							
								
					$content .= '</td>
							</tr>';
							
					$content .= '		<tr>
								<td valign="top" class="data" style="border:0px;">
								</td>
								<td valign="top" class="data" style="border:0px;">';
								$content .= '<span class="subhead">Inclusions:</span><br>';
								$content .= $itemDetail[0]["item_type"].'<br>';
								$content .= '<span class="subhead">Conditions:</span><br>';
								$content .= 'Confirmation No.:'.$itemDetail[0]["response_code"].'<br>';
								if($itemDetail[0]["id_supplier"] == 2){
									$content .= 'Met Global Tracking ID:'.$itemDetail[0]["tracking_code"].'<br>';
								}else{
									$content .= 'Tracking ID:'.$itemDetail[0]["tracking_code"].'<br>';
								}
								////////////////////////////////// SUPPLIER INFO EMRGENCY
						     	$content .= 'Emergency Contact No.:'.$supplierDetails[0]['phone_number'].'';
					$content .= '</td>
							</tr>';
							
							
		$content .= '	</tbody>';
					$content .='</table>';
					
					$content .= '
					 <table class="table-primary" >
						<thead>
							<tr class="table-primary-head">
								<th colspan=2>Please Forward Your Invoice To:</th>
								
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width="50%" class="data" valign="top">';
									$content .= '<span class="subhead">'.$supplierDetails[0]['supplier_name'].'</span>';
								
								
					$content .= '</td><td valign="top" class="data" width="50%">';
									$content .= '<span class="subhead">Emergency Phone No.:</span><br>';
										$content .= ''.$supplierDetails[0]['phone_number'].'<br>';
						
					
					
					$content .= '</td>		</tr>';
						
					$content .= '	</tbody>
						
					</table>';
					
					
					
					
						$content .= ' </div>';
					
					
	}
	 
	$content .= '<div style="font-size:12px;padding-top:10px;width:100%;text-align: center;">
          - Please present this voucher to the local representative -
        </div>';
				

	 
	 
	 
		 
		 $docID = self::generateDocs($content,'PREPAID VOUCHER','voucher-'.$voucherId.'.pdf','VCHR|'.$bookingID.'-'.$voucherId);
		 
		 $mysql = new Database; 
		 $mysql->execute_query("UPDATE voucher set id_document =  $docID where id_voucher = $voucherId;", false);
		 
		 return $docID;
	 
	 
	 }
	 
	 
	 public function itenerary($ItineraryId,$bookingID){
	$Itinerary = new Itinerary($ItineraryId); 
	$ItineraryDetails = $Itinerary->getItinerary();
	$ItineraryItems = $Itinerary->getItemsItinerary();
	//print_r($voucherItems);
	$booking = new Booking($bookingID); 
	
	$agency = new Agency($booking->getAgencyID()); 
	$agencyDetails = $agency->getAgency();
	$BusinessCardDetails = $agency->getBusinessCard();
	
	$bookingDetails = $booking->getBooking();
	$account = new Account($bookingDetails[0]['id_account']); 
	$res_account = $account->getAccount();
	
	
	$start_datetime = new DateTime($bookingDetails[0]["start_datetime"]);
	$end_datetime = new DateTime($bookingDetails[0]["end_datetime"]);
	$created_datetime = new DateTime($ItineraryDetails[0]["created_datetime"]);
	$Itinerary_date = new DateTime($ItineraryDetails[0]["Itinerary_date"]);
	$content = '
		<div class="row2">
		<div class="panel-heading">
            D E T A I L S
        </div>
			<div class="info1">
				<table class="tableInfo1 left">
							<tbody>
								<tr><td class="subhead">Name:</td>
									<td class="data">'.$agencyDetails[0]["agency_name"].'</td>
									<td class="subhead">Creation Date:</td>
									<td class="data">'.$start_datetime->format('d, M Y').'</td>
								</tr>
								<tr><td class="subhead">Address:</td>
									<td class="data">'.$BusinessCardDetails[0]['address'].', '.$BusinessCardDetails[0]['city'].', '.$BusinessCardDetails[0]['country'].'</td>
									<td class="subhead">Departure Date:</td>
									<td class="data">'.$end_datetime->format('d, M Y').'</td>
								</tr>
								<tr><td class="subhead">Business Phone:</td>
									<td class="data">'.$BusinessCardDetails[0]['phone'].'</td>
									<td class="subhead">Consultant:</td>
									<td class="data">'.$res_account[0]['account_name'].'</td>
								</tr>
								<tr><td class="subhead">Business Fax:</td>
									<td class="data">'.$BusinessCardDetails[0]['fax'].'</td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
				 </table>
			</div>	
			
			<div class="info2">
				<table class="tableInfo1 right">
					<tr><td class="subhead">Itinerary Reference:</td><td class="subhead">'.$ItineraryDetails[0]["ref"].'</td></tr>
					<tr><td class="subhead">&nbsp;</td><td>&nbsp;</td></tr>
					<tr><td class="subhead">Document Printed:</td><td>'.$created_datetime->format('d, M Y').'</td></tr>
					<tr><td class="subhead">Itinerary Date:</td><td>'.$Itinerary_date->format('d, M Y').'</td></tr>
					
				</table>
			</div>
	 </div>
	 ';

	  foreach($ItineraryItems as $ItineraryItem){   
					//print_r( $voucherItem);
					$item = new Item($ItineraryItem['id_item']); 
					$itemDetail = $item->getItem();
					$itemName = $item->getProductName();
					$lead = $item->getItemsLeadPax();
					$itemNameSub = $item->getItemsSub();
					$paxs = $item->getItemsPax();
					$product = new Product(0); 
					$productDetails = $product->getProductByCode($item->getProductCode());
					$supplier = new Supplier($itemDetail[0]["id_supplier"]);
					$supplierDetails = $supplier->getSupplier();
					
					$content .= '<div class="bodyrow">';
					
					$content .= '
					 <table class="table-primary" >
						<thead>
							<tr class="table-primary-head">
								<th colspan=2>'.$itemName.'</th>
								
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width="50%" class="data" valign="top">
									<span class="subhead"></span><br><br>';
									
									
						$content .=	'<span class="subhead">Address:</span><br>
									<span class="data">'.$productDetails[0]["product_address"].', '.$productDetails[0]["destination_city"].', '.$productDetails[0]["destination_country"].' '.$productDetails[0]["product_postal_code"].'</span><br>
									<span class="subhead">Phone:</span><br>
									<span class="data">'.$productDetails[0]["product_phone_number"].'</span><br>
									
									'.str_replace('&amp;','&',$productDetails[0]["amenities_internal"]).'<br><br>
									
								
						';
						$content .=	'</td><td width="50%" class="data" valign="top">';
						$content .=	'
									<center>
									<img src="http://maps.googleapis.com/maps/api/staticmap?center='.$productDetails[0]["product_coordinates_latitude"].','.$productDetails[0]["product_coordinates_longitude"].'&zoom=11&size=325x200&sensor=false&markers=color:red%7Ccolor:red%7C'.$productDetails[0]["product_coordinates_latitude"].','.$productDetails[0]["product_coordinates_longitude"].'" >
									</center>
									
						
								';
								
					$content .= '</td>
							</tr>';
					
							$content .= '<tr>
							<td colspan=2 style="text-align: justify;">
							<span class="subhead">Description:</span><br><br>
									<span class="data">'.$productDetails[0]["product_description"].'</span>
							</td>
							</tr>';
						$content .= '<tr>
										<td valign="top"  class="data" width="50%" style="border:0px;">
											<span class="subhead">CLIENTS INFO</span>
										</td>
										<td width="50%" class="data" valign="top" style="border:0px;">';
								
								
								foreach($itemNameSub as $itemSub){   			
									$content .= '<span class="subhead">'.$itemSub["item_name"].'</span><br>';  
									$itemSubObj = new ItemSub($itemSub['id_item_sub']);
									$paxArr = $itemSubObj->getItemsSubPax();
									foreach($paxArr as $itemSubPax){
										$content .= '&bull; '.$itemSubPax['title'].' '.$itemSubPax['first_name'].' '.$itemSubPax['last_name'];
										if($itemSubPax['is_lead'] == 1){
											$content .= ' <span style="color:red;">(Lead Guest/Pax) </span><br>';
										}else{
											$content .= '<br>';
										}
									}
									$content .= '<br>';
								}
								
						$content .= '<br></td></tr>';
	
					$content .= '<tr>
								<td valign="top"  class="data" width="50%" style="border:0px;">
								<span class="subhead">Service Type</span>
								</td>
								<td valign="top" class="data" width="50%" style="border:0px;">';
								if($itemDetail[0]["id_service"] == 1){
									$content .= 'Accommodation';
								}else{
									$content .= 'N/A';
								}
								
					$content .= '</td>
							</tr>';
							
							
					$content .= '		<tr>
								<td valign="top" class="data" style="border:0px;">
								<span class="subhead">Service Description</span>
								</td>
								<td valign="top" class="data" style="border:0px;">';
								
									foreach($itemNameSub as $itemSub){   			
										  
										$itemSubObjct = new ItemSub($itemSub['id_item_sub']);
										$paxArr = $itemSubObjct->getItemsSubPax();
										$numPaxThis = 0;
										foreach($paxArr as $itemSubPax){
											$numPaxThis++;
										}
										$content .= '<span class="subhead">'.$numPaxThis.' X 1 '.$itemSub["item_name"].' - '.$itemDetail[0]["item_type"].' </span><br>';
									
									}
								
									//$content .= $itemName.' - '.$itemDetail[0]["item_type"];
							
								
					$content .= '</td>
							</tr>';
							
					$content .= '		<tr>
								<td valign="top" class="data" style="border:0px;">
								<span class="subhead">Booked For</span>
								</td>
								<td valign="top" class="data"  style="border:0px;">';
								
									$content .= $itemDetail[0]["number_of_pax"].' PAX';
							
								
					$content .= '</td>
							</tr>';
							
					$dateF = new DateTime($itemDetail[0]["date_from"]);
					$dateT = new DateTime($itemDetail[0]["date_to"]);			
											
					$content .= '		<tr>
								<td valign="top" class="data" style="border:0px;">
								<span class="subhead">Date From</span>
								</td>
								<td valign="top" class="data"  style="border:0px;">';
								
									$content .= $dateF->format('l d, F Y');
							
								
					$content .= '</td>
							</tr>';
					$content .= '		<tr>
								<td valign="top" class="data" style="border:0px;">
								<span class="subhead">Date To</span>
								</td>
								<td valign="top" class="data"  style="border:0px;">';
								
									$content .= $dateT->format('l d, F Y');
							
								
					$content .= '</td>
							</tr>';
							
					$content .= '		<tr>
								<td valign="top" class="data" style="border:0px;">
								</td>
								<td valign="top" class="data" style="border:0px;">';
								$content .= '<span class="subhead">Inclusions:</span><br>';
								$content .= $itemDetail[0]["item_type"].'<br>';
								$content .= '<span class="subhead">Conditions:</span><br>';
								$content .= 'Confirmation No.:'.$itemDetail[0]["response_code"].'<br>';
								if($itemDetail[0]["id_supplier"] == 2){
									$content .= 'Met Global Tracking ID:'.$itemDetail[0]["tracking_code"].'<br>';
								}else{
									$content .= 'Tracking ID:'.$itemDetail[0]["tracking_code"].'<br>';
								}
								////////////////////////////////// SUPPLIER INFO EMRGENCY
						     	$content .= 'Emergency Contact No.:'.$supplierDetails[0]['phone_number'].'';
					$content .= '</td>
							</tr>';
							
							
		$content .= '	</tbody>';
					$content .='</table>';
					
					
						$content .= ' </div>';
					
					
	}
	 
	$content .= '<div style="font-size:12px;padding-top:10px;width:100%;text-align: justify;">
          Please note: that a separate city tax will be charged to the customers upon arrival in many European Cities. This city tax is not
			included in the room rates and is an additional governmental charge which needs to be paid in the hotel. Children from 0-6 years
			are free of charge when sharing the existing bedding with the adults.
				<br>
				<center><span>END ITINERARY</span></center><br>
				
On behalf of The Eastern Eurotours and Mediterranean Holidays teams we wish you a safe and memorable journey & we look
forward to booking your future travel arrangements. Please contact the local numbers given for emergency but otherwise
please contact Sally Barry, Operations Manager +61 0411 879738 or Ciprian Popescu, Managing Director +61 0433 161 250<br><br>
				IMPORTANT INFORMATION<br>
Flight Confirmation: Please note that all flight reservations are subject to change without notice. Most Airlines do not require re
confirmation, however we do suggest you contact the Airline to ensure your flight is running as per schedule. Please note that
failure to check in for a confirmed flight may result in all subsequent flight reservations being cancelled. Many Airlines will impose
a financial penalty for no-show and in some instances will not honor onward tickets.
Visas: It is the traveller�s responsibility to ensure all Visa requirements are met for the Country you are traveling to. For further
information refer to www.dfat.gov.au/visas/
Passport: You will require a valid passport for this journey. Many countries require passports to be valid for at least six months
after your date of entry. Please note that possession of a passport and/or visa does not guarantee entry to any country.
Admittance is always at the discretion of immigration officials of the country you are visiting.
Travel Insurance: You should be aware that travel arrangements may be subject to cancellation and/or amendment fees, which
can be quite severe in some cases. For this reason, plus the often extremely high cost of medical attention overseas, we strongly
recommend that you take out some form of insurance for your protection in unforeseen circumstance. Please note that any claim
for lost or stolen items must be accompanied by a written policy, or other authority, report to support your claim. Please note that
damage to luggage caused during a flight must be reported to the airline involved before you leave the airport.
Government Imposed Airport Taxes: Taxes and fees that are imposed by government authorities on the air carriage specified in
this itinerary and receipt are either included in the fare or shown separately under �Tax� or �GST� on the itinerary and receipt. You
will be required to pay any such taxes or fees not already collected.
Name Changes: Please check the name/s on this itinerary carefully as the name on your ticket must match the name on your
passport. Airlines do not allow names to be changed in reservations and any discrepancy will require a new reservation to be
made, resulting in the loss of existing booking and any special services already confirmed. Fees will apply.
Conditions: For full Booking Conditions please refer to the back of the Eastern European Tours Brochure.
Refunds: No refund will paid on unused services, unless the supplier has agreed in writing to refund the service in full.

				
        </div>';
				

	 
	 
	 
		 
		 $docID = self::generateDocs($content,'ITINERARY','Itinerary-'.$ItineraryId.'.pdf','VCHR|'.$bookingID.'-'.$ItineraryId);
		 
		 $mysql = new Database; 
		 $mysql->execute_query("UPDATE itinerary set id_document =  $docID where id_itinerary = $ItineraryId;", false);
		 
		 return $docID;
	 
	 
	 }
	 
	
	
	
}
?>