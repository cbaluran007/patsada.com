<?php
/** _loader.php
* 
* @desc Include this file on each new class to enable auto loading.
*/
require_once('_config.php');

set_include_path(get_include_path().';.');

function __autoload($class_name) {
require_once($class_name. '.class.php');
}
?>