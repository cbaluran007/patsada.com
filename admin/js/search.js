Ext.namespace("app_space");



var search = function () {
	return {
		
		view: function(fromAddNewItem,new_agencyId,new_agency_name,new_account_id,new_account_name,new_booking_id){
			
			fromAddNewItem = typeof fromAddNewItem !== 'undefined' ? fromAddNewItem : false;
			new_agencyId = typeof new_agencyId !== 'undefined' ? new_agencyId : '';
			new_agency_name = typeof new_agency_name !== 'undefined' ? new_agency_name : '';
			new_account_id = typeof new_account_id !== 'undefined' ? new_account_id : '';
			new_account_name = typeof new_account_name !== 'undefined' ? new_account_name : '';
			new_booking_id = typeof new_booking_id !== 'undefined' ? new_booking_id : '';
			
			if(Ext.getCmp('min_win_search_grid')){
			Ext.getCmp('win_search_grid').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_search_grid'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_search_grid').close();}catch(err){}
			
			if(fromAddNewItem){
				try{Ext.getCmp('win_create_bookings').close();}catch(err){}
				var surl = "pages/items.php?booking_id="+new_booking_id+"&isFromAdd=1";
			}else{
				var surl = "pages/searchDefault.php";
			}
			
			var tabs = new Ext.TabPanel({
				activeTab: 0,
				width:"100%",
				region: 'center',
				id:'tabForSearch',
				height:"100%",
				plain:true,
				autoDestroy: false,
				defaults:{autoScroll: true},
				items:[{
						title: 'Search Results',
						layout : 'fit',
						height: '100%',
						autoEl : {
						   tag : "iframe",
						   height: '100%',
						   id: "searchTabResults",
						   src : surl
						}
						//autoLoad: {url: 'pages/search.php', params: 'action=default'}
					}
				]
			});
			
			
			
			
			
			
			/*------------------search form ---------------------------*/
			
			
			var ds_service = proj_tools.ds_combo('_ajax/execute_service.php','getService','Sid','id_service','service_name');
			var service_combo = proj_tools.getSimpleCombo(ds_service,'Service Name','service_search_search','id_service','service_name',150,150);
			
			
			
			var ds_destination = proj_tools.ds_combo('_ajax/execute_destination.php','getDestination','Agencyid','code','destination');
			var destination_combo = proj_tools.getComboDestination(ds_destination,'Destination','destination_search_search','code','destination',150,150);
			
			
			
			
			var ds_agency = proj_tools.ds_combo('_ajax/execute_agency.php','getAgency','Agencyid','id_agency','agency_name');
			var agency_combo = proj_tools.getCombo(ds_agency,'Agency Name','agency_search_search','id_agency','agency_name',150,150);
			var ds_consultant = proj_tools.ds_combo('_ajax/execute_user_manager_agent.php','getConsultant','ConsultantIn','id_account','account_name');
			var consultant_combo = proj_tools.getSimpleCombo(ds_consultant,'Consultant Name','consultant_search','account_name','id_account',150,150);
			
			
			var ds_products = proj_tools.ds_combo('_ajax/execute_products.php','getProducts2','SearchProductsid','product_code','product_name');
			var products_combo = proj_tools.getSimpleCombo(ds_products,'Hotel Names','products_search_search','product_name','product_code',150,150);
			
			
			destination_combo.on('select', function(box, record, index) {
			products_combo.setValue('');
			products_combo.getStore().load({params: {destination_id: this.getValue()}}); 
			
			});
			
			agency_combo.on('select', function(box, record, index) {
			consultant_combo.setValue('');
			consultant_combo.getStore().load({params: {agency_id: this.getValue()}}); 
			
			});
			
			
			var store1 = new Ext.data.ArrayStore({
				fields: ['value','display'],
				data : [ ['0','0'], ['1','1'],['2','2'],['3','3'],['4','4'],['5','5'],['6','6'],['7','7'],['8','8'],['9','9'],['10','10']]
			});
			
			var store2 = new Ext.data.ArrayStore({
				fields: ['value','display'],
				data : [ ['0','0'], ['1','1'],['2','2'],['3','3'],['4','4'],['5','5']]
			});
			
			
			var storeAge = new Ext.data.ArrayStore({
				fields: ['value','display'],
				data : [ ['1','1'],['2','2'],['3','3'],['4','4'],['5','5'],['6','6'],['7','7'],['8','8'],['9','9'],['10','10'],['11','11'],['12','12'],['13','13'],['14','14'],['15','15']]
			});

			
			var store3 = new Ext.data.ArrayStore({
				fields: ['value','display'],
				data : [ ['RO','Room Only'], ['BB','Bed and Breakfast'],['EB','English Breakfast'],['CB','Continental Breakfast'],['HB','Half Board'],['FB','Full Board'],['AI','All Inclusive'],['SC','Self Catering']]
			});
			
			var store4 = new Ext.data.ArrayStore({
				fields: ['value','display'],
				data : [ ['1','1'],['2','2'],['3','3']]
			});
			
			var func = function(combo,value){
					//alert(combo.getValue());
					for(var i=1;i<=3;i++){
						if(i<=combo.getValue()){
						var child_Age_combo = Ext.getCmp('child_age_'+i);
						var comp = Ext.getCmp('room_field_'+i)
						comp.setVisible(true);
						//child_Age_combo.setVisible(true);
						}else{
						var child_Age_combo = Ext.getCmp('child_age_'+i);
						var comp = Ext.getCmp('room_field_'+i)
						comp.setVisible(false);
						child_Age_combo.setVisible(false);
						}
					}
                   
				};
				
			var func_age_1 = function(combo,value){
					
					//alert(parseInt(combo.getValue()));
					var child_Age_combo = Ext.getCmp('child_age_1');
					if(parseInt(combo.getValue())!=0 ||parseInt(combo.getValue()) <0){
					//alert(1);
					child_Age_combo.setVisible(true);
					}else{
					child_Age_combo.setVisible(false);
					//alert(0);
					}
					
					child_Age_combo.removeAll(true,true)
					for(var x=1;x<=parseInt(combo.getValue());x++){
						var comb = proj_tools.getSimpleCombo2(storeAge,'Child Age #'+x,'child_age_1_combo_'+x,'display','value',50,8);
						child_Age_combo.add(comb);
						child_Age_combo.doLayout();
					}
				};
				
			var func_age_2 = function(combo,value){
					
					//alert(parseInt(combo.getValue()));
					var child_Age_combo = Ext.getCmp('child_age_2');
					if(parseInt(combo.getValue())!=0 ||parseInt(combo.getValue()) <0){
					//alert(1);
					child_Age_combo.setVisible(true);
					}else{
					child_Age_combo.setVisible(false);
					//alert(0);
					}
					
					child_Age_combo.removeAll(true,true)
					for(var x=1;x<=parseInt(combo.getValue());x++){
						var comb = proj_tools.getSimpleCombo2(storeAge,'Child Age #'+x,'child_age_2_combo_'+x,'display','value',50,8);
						child_Age_combo.add(comb);
						child_Age_combo.doLayout();
					}
				};
				
			var func_age_3 = function(combo,value){
					
					//alert(parseInt(combo.getValue()));
					var child_Age_combo = Ext.getCmp('child_age_3');
					if(parseInt(combo.getValue())!=0 ||parseInt(combo.getValue()) <0){
					//alert(1);
					child_Age_combo.setVisible(true);
					}else{
					child_Age_combo.setVisible(false);
					//alert(0);
					}
					
					child_Age_combo.removeAll(true,true)
					for(var x=1;x<=parseInt(combo.getValue());x++){
						var comb = proj_tools.getSimpleCombo2(storeAge,'Child Age #'+x,'child_age_3_combo_'+x,'display','value',50,8);
						child_Age_combo.add(comb);
						child_Age_combo.doLayout();
					}
				};
			
			var today = new Date();
			var forDate = new Date();
			forDate.setDate(today.getDate()+2);
			var forDate2 = new Date();
			forDate2.setDate(today.getDate()+3);
			var fdate = forDate.format("d/m/Y");
			var tdate = forDate2.format("d/m/Y");
			var searchCriteriaForm = [
					agency_combo,
					consultant_combo,
					{id: "service_search_search_combo_id", name: "service_search_search_combo_name", xtype: "hidden", value: 1},
					destination_combo,
					{
						fieldLabel: 'Start Date',
						xtype: 'datefield',
						name: 'startdt',
						id: 'startdt',
						format: 'd/m/Y',
						width:'100%',
						vtype: 'daterange',
						allowBlank:false,
						minValue: forDate,
						value:fdate,
						listeners : {
							'select' : function(field, newValue, oldValue) {
								//alert(this.getValue());
								var fftodatate = new Date();
								var ffdate = new Date(this.getValue());
								fftodatate.setDate(ffdate.getDate()+1);
								var fthisdate = fftodatate.format("d/m/Y");
								Ext.getCmp("enddt").setValue(fthisdate);
								
							}
						},
						endDateField: 'enddt' // id of the end date field
					 },{
						fieldLabel: 'End Date',
						xtype: 'datefield',
						name: 'enddt',
						width:'100%',
						format: 'd/m/Y',
						allowBlank:false,
						id: 'enddt',
						//vtype: 'daterange',
						value:tdate,
						startDateField: 'startdt' // id of the start date field
					},
					proj_tools.getSimpleCombo3(store4,'# of Rooms','room_num','display','value',75, '1',func),
					{
						xtype: 'fieldset',
						title: 'R1 Number of Pax',
						autoHeight: true,
						id:'room_field_1',
						layout: 'column',
						width: '100%',
						items: [{
									// You can pass sub-item arrays along with width/columnWidth configs 
									// ColumnLayout-style for complete layout control.  In this example we
									// only want one item in the middle column, which would not be possible
									// using the columns config.  We also want to make sure that our headings
									// end up at the top of each column as expected.
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										proj_tools.getSimpleCombo2(store1,'# of Adult(s)','pax_adult1','display','value',75, '2'),
										{xtype: 'label', text: '# of Adult(s)'}
									]
								},{
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										proj_tools.getSimpleCombo3(store1,'# of Child(s)','pax_child1','display','value',75,'0', func_age_1),
										{xtype: 'label', text: '# of Child(s)'}
									]
								},{
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										
										proj_tools.getSimpleCombo2(store1,'# of Infant(s)','pax_infant1','display','value',75,'0'),
										{xtype: 'label', text: '# of Infant(s)'}
									]
								}
								
						]
					},
					
					{
						xtype: 'fieldset',
						title: 'Child Ages For Room #1',
						layout: 'form',
						autoHeight: true,
						hidden: true,
						id:'child_age_1',
						width: '100%',
						items: []
					},
					{
						xtype: 'fieldset',
						title: 'R2 Number of Pax',
						id:'room_field_2',
						autoHeight: true,
						hidden: true,
						layout: 'column',
						width: '100%',
						items: [{
									// You can pass sub-item arrays along with width/columnWidth configs 
									// ColumnLayout-style for complete layout control.  In this example we
									// only want one item in the middle column, which would not be possible
									// using the columns config.  We also want to make sure that our headings
									// end up at the top of each column as expected.
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										proj_tools.getSimpleCombo2(store1,'# of Adult(s)','pax_adult2','display','value',75, '2'),
										{xtype: 'label', text: '# of Adult(s)'}
									]
								},{
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										proj_tools.getSimpleCombo3(store1,'# of Child(s)','pax_child2','display','value',75,'0', func_age_2),
										{xtype: 'label', text: '# of Child(s)'}
									]
								},{
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										
										proj_tools.getSimpleCombo2(store1,'# of Infant(s)','pax_infant2','display','value',75,'0'),
										{xtype: 'label', text: '# of Infant(s)'}
									]
								}
								
						]
						
					},
					{
						xtype: 'fieldset',
						title: 'Child Ages For Room #2',
						layout: 'form',
						autoHeight: true,
						hidden: true,
						id:'child_age_2',
						width: '100%',
						items: []
					},
					{
						xtype: 'fieldset',
						title: 'R3 Number of Pax',
						autoHeight: true,
						id:'room_field_3',
						hidden: true,
						layout: 'column',
						width: '100%',
						items: [{
									// You can pass sub-item arrays along with width/columnWidth configs 
									// ColumnLayout-style for complete layout control.  In this example we
									// only want one item in the middle column, which would not be possible
									// using the columns config.  We also want to make sure that our headings
									// end up at the top of each column as expected.
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										proj_tools.getSimpleCombo2(store1,'# of Adult(s)','pax_adult3','display','value',75, '2'),
										{xtype: 'label', text: '# of Adult(s)'}
									]
								},{
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										proj_tools.getSimpleCombo3(store1,'# of Child(s)','pax_child3','display','value',75,'0', func_age_3),
										{xtype: 'label', text: '# of Child(s)'}
									]
								},{
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										
										proj_tools.getSimpleCombo2(store1,'# of Infant(s)','pax_infant3','display','value',75,'0'),
										{xtype: 'label', text: '# of Infant(s)'}
									]
								}
								
						]
					},
					{
						xtype: 'fieldset',
						title: 'Child Ages For Room #3',
						layout: 'form',
						autoHeight: true,
						hidden: true,
						id:'child_age_3',
						width: '100%',
						items: []
					},
					{
					xtype:'fieldset',
					checkboxToggle:true,
					title: 'Advance Search',
					autoHeight:true,
					defaultType: 'textfield',
					collapsed: true,
					id:"is_advance",
					width: "95%" ,
					items :[
						proj_tools.getSimpleCombo2(store3,'Room Type','room_type','display','value',150),
						proj_tools.getSimpleCombo2(store2,'Star Ratings','star','display','value',50),
						products_combo,
						{
							xtype: 'spinnerfield',
							fieldLabel: 'Results Limit',
							name: 'searchLimitResult',
							id: 'searchLimitResult',
							value: 200,
							width: 50,
							minValue: 100,
							maxValue: 999
						}
					]
				}
				];
				
			if(role != "S"){
				searchCriteriaForm = proj_tools.removedArray(searchCriteriaForm,0);		
				searchCriteriaForm = proj_tools.insertArray(searchCriteriaForm,{id: "agency_search_search_combo_id", name: "agency_search_search_combo_id", xtype: "hidden", value: agency_id},0);
				searchCriteriaForm = proj_tools.insertArray(searchCriteriaForm,{disabled: true, id: "agency_search_search_combo_name", fieldLabel: 'Agency Name', name: "agency_search_search_combo_name", xtype: "textfield", value: AGENCY_NAME},0);
				searchCriteriaForm = proj_tools.removedArray(searchCriteriaForm,2);		
				searchCriteriaForm = proj_tools.insertArray(searchCriteriaForm,{id: "consultant_search_combo_id", name: "consultant_search_combo_id", xtype: "hidden", value: user_id},2);
				searchCriteriaForm = proj_tools.insertArray(searchCriteriaForm,{disabled: true, id: "consultant_search_combo_name", fieldLabel: 'Consultant Name', name: "consultant_search_combo_name", xtype: "textfield", value: user_name},2);
			}
			
			if (typeof fromAddNewItem !== 'undefined') {
				if(role == "S" && fromAddNewItem == true){
					searchCriteriaForm = proj_tools.removedArray(searchCriteriaForm,0);		
					searchCriteriaForm = proj_tools.insertArray(searchCriteriaForm,{id: "agency_search_search_combo_id", name: "agency_search_search_combo_id", xtype: "hidden", value: new_agencyId},0);
					searchCriteriaForm = proj_tools.insertArray(searchCriteriaForm,{disabled: true, id: "agency_search_search_combo_name", fieldLabel: 'Agency Name', name: "agency_search_search_combo_name", xtype: "textfield", value: new_agency_name},0);
					searchCriteriaForm = proj_tools.removedArray(searchCriteriaForm,2);		
					searchCriteriaForm = proj_tools.insertArray(searchCriteriaForm,{id: "consultant_search_combo_id", name: "consultant_search_combo_id", xtype: "hidden", value: new_account_id},2);
					searchCriteriaForm = proj_tools.insertArray(searchCriteriaForm,{disabled: true, id: "consultant_search_combo_name", fieldLabel: 'Consultant Name', name: "consultant_search_combo_name", xtype: "textfield", value: new_account_name},2);
				}
			}
			
			var searchCriteria = new Ext.FormPanel({
				labelWidth: 100, // label settings here cascade unless overridden
				url:'pages/search.php',
				bodyStyle:'padding:5px 5px 0',
				border:false, 
				target:'searchTabResults',
				id: "searchFormPanel",
				width: '100%',
				defaults: {width: 150},
				defaultType: 'textfield',
				autoScroll:true,
				items: searchCriteriaForm,

				buttons: [
						{text: "Search", formBind: true, icon: "images/icons/magnifier.png", handler: search.search},
						{text: "Reset", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: search.searchReset}
						
					]
			});

			
			
			
			
			// Panel for the west
			var nav = new Ext.Panel({
				title: 'Search Product Criteria',
				id: 'SearchEngineNav',
				region: 'west',
				split: true,
				width: 300,
				collapsible: true,
				autoScroll:true,
				margins:'3 0 3 3',
				cmargins:'3 3 3 3',
				items: [searchCriteria]
			});
			if(role == "S"){
				var priceSource = {
                            "Orginal Cost": '',
                            "Conversion":'EUR - AUD',
                            "Converted in AUD":'',
                            "Cost": '',
							"Due": '',
                            "Markup": '',
                            "Gross": '',
                            "Agency Commission":'',
                            "Amount Display":''
                        };
			}else{
				var priceSource = {
                            "Gross": '',
                            "Agency Commission":'',
                            "Amount Display":''
                        };
			}
			var nav2 = new Ext.Panel({
				region: 'east',
				id: 'ProductInfoNav',
				split: true,
				width: 350,
				collapsible: true,
				title: 'Product Info',
				collapsed: true,
				margins:'3 0 3 3',
				cmargins:'3 3 3 3',
				layout: "fit",
				height: "100%",
				items:  new Ext.TabPanel({
                    border: false, // already wrapped so don't add another border
                    activeTab: 0, // second tab initially active
                    tabPosition: 'bottom',
					id: 'ProductInfoTab',
					height: "100%",
                    items: [ new Ext.grid.PropertyGrid({
                        title: 'Price Calculation Info',
						id: 'price_view_info',
						height: "100%",
                        closable: false,
                        source: priceSource
                    })/*,
					{
                        html: '<div id="subProductDetails" style="padding:10px;"></div>',
                        title: 'More Options',
						height: "100%",
                        autoScroll: true
                    }*/
					
					]
                })
			});
			validator.runThis();
			//search.store = store;
			search.searchCriteria = searchCriteria;
			search.tabs = tabs;
			proj_tools.new_win_grid('win_search_grid',1050,550,[nav,tabs,nav2],'Search List');
			
			if(Ext.getCmp('min_win_search_grid')){
			Ext.getCmp('SearchEngineNav').expand();
			
			}
			
		},
		search:function () {
			Ext.getCmp('win_search_grid').maximize();
			Ext.getCmp('SearchEngineNav').collapse();
			Ext.getCmp('ProductInfoNav').collapse();
			Ext.getCmp('tabForSearch').setActiveTab(0);
			try{Ext.getCmp('tabForSearch').remove(Ext.getCmp('tabProductPriceSummary'), true);}catch(err){}
			
			/*var data = {
                            "Orginal Nett": '',
                            "Conversion":'EUR - AUD',
                            "Converted":'',
                            "Nett": '',
                            "Markup": '',
                            "Gross": '',
                            "Agency Commission":'',
                            "Amount Display":''
                        };
			Ext.getCmp('price_view_info').setSource(data);
			document.getElementById('subProductDetails').innerHTML = "";
			*/
			var parr = {};
			parr["agency_id"] = Ext.getCmp("agency_search_search_combo_id").getValue();
			parr["service_id"] = Ext.getCmp("service_search_search_combo_id").getValue();
			parr["desitnation_code"] = Ext.getCmp("destination_search_search_combo_id").getValue();
			parr["searchLimitResult"] = Ext.getCmp("searchLimitResult").getValue();
			//alert(parr["desitnation_code"]);
			parr["date_start"] = Ext.getCmp("startdt").getValue().format('Y-m-d'); //2013-07-27T00:00:00
			parr["date_end"] = Ext.getCmp("enddt").getValue().format('Y-m-d');
			
			parr["star"] = Ext.getCmp("star_combo_id").getRawValue();
			parr["product_name"] = Ext.getCmp("products_search_search_combo_id").getValue();
			
			parr["consultant_id"] = Ext.getCmp("consultant_search_combo_id").getValue();
			if(Ext.getCmp("consultant_search_combo_name")){
			parr["consultant"] = Ext.getCmp("consultant_search_combo_name").getValue();
			}else{
			parr["consultant"] = Ext.getCmp("consultant_search_combo_id").getRawValue();
			}
			parr["board_type"] = Ext.getCmp("room_type_combo_id").getValue();
			
			var room_number = parseInt(Ext.getCmp("room_num_combo_id").getValue());
			//alert(room_number);
			var parStr = "";
			for(var i = 0; i<room_number;i++){
				var idnt = i+1;			
				parStr +="&room_num["+i+"][pax][adult]="+Ext.getCmp("pax_adult"+idnt+"_combo_id").getValue();
				parStr +="&room_num["+i+"][pax][child]="+Ext.getCmp("pax_child"+idnt+"_combo_id").getValue();
				parStr +="&room_num["+i+"][pax][infant]="+Ext.getCmp("pax_infant"+idnt+"_combo_id").getValue();
				for(var x=0; x<parseInt(Ext.getCmp("pax_child"+idnt+"_combo_id").getValue());x++){
					var idnt2 = x+1;	
					parStr +="&room_num["+i+"][pax][ages][]="+Ext.getCmp("child_age_"+idnt+"_combo_"+idnt2+"_combo_id").getValue();
				
				}
			
			}
			
			if(parr["searchLimitResult"] == "" || parr["desitnation_code"] == "" || parr["agency_id"] == "" || parr["service_id"] == "" || parr["date_start"] == "" || parr["date_end"] == ""){
				Ext.MessageBox.alert('Status', 'Unable to search products. Please fill requiered field.');
				return false;
			}
			/*Ext.getCmp('search-grid-result-products').getEl().mask('Loading...', 'x-mask-loading');
			search.store.load({ params: parr,scope: this,
			callback: function(records, operation, success) {
				if (success) {
					Ext.getCmp('search-grid-result-products').getEl().mask().hide();
				} else {
					Ext.getCmp('search-grid-result-products').getEl().mask().hide();
				}
			} });
			*/
			/*Ext.getDom(Ext.getCmp("searchFormPanel")).target = "_blank"; // where form is the BasicForm object
			Ext.getCmp("searchFormPanel").submit();*/
			
			//localhost/projects/administrator/modules/xml_gateway.php?action=viewAccommodation&_dc=1372138547861&agency_id=1&service_id=1&desitnation_code=Rome|Italy&date_start=2013-07-27T00:00:00&date_end=2013-07-28T00:00:00&adult=2&child=0&infant=0&star=&product_name=
			var url = "modules/xml_gateway.php?action=viewAccommodation&agency_id="+parr["agency_id"]+"&consultant_id="+parr["consultant_id"]+"&consultant="+parr["consultant"]+"&service_id="+parr["service_id"]+"&desitnation_code="+parr["desitnation_code"]+"&date_start="+parr["date_start"]+"&date_end="+parr["date_end"]+"&star="+parr["star"]+"&board_type="+parr["board_type"]+"&product_name="+parr["product_name"]+"&searchLimitResult="+parr["searchLimitResult"]+parStr; 
			
			document.getElementById('searchTabResults').src = url;
			
		},
		priceView: function(param){
			Ext.getCmp('ProductInfoNav').expand();
			Ext.getCmp('ProductInfoTab').setActiveTab(0);
			var priceSplit = param.split("|");
			
			if(role == "S"){
				var data = {
                            "Orginal Cost": priceSplit[0]+' EUR',
                            "Conversion":'EUR - AUD',
                            "Converted in AUD": priceSplit[1]+' AUD',
                            "Cost": priceSplit[2]+' AUD',
							"Due": priceSplit[6]+' AUD',
                            "Markup": priceSplit[3]+' AUD',
                            "Gross": priceSplit[4]+' AUD',
                            "Agency Commission": priceSplit[5]+' AUD',
                            "Amount Display":priceSplit[4]+' AUD'
                        };
			}else{
				var data = {
                            "Due": priceSplit[6]+' AUD',
                            "Gross": priceSplit[4]+' AUD',
                            "Agency Commission": priceSplit[5]+' AUD',
                            "Amount Display":priceSplit[4]+' AUD'
                        };
			}
			
			Ext.getCmp('price_view_info').setSource(data);
		
		},
		showSubProducts :function(name,param){
			var json_sub = Ext.util.base64.decode(param);
			//alert(json_sub);
			var data = Ext.util.JSON.decode(json_sub);
			
			//Ext.util.JSON.encode(formDataArray)
			//obj = jsonDecode(param);
			Ext.getCmp('ProductInfoNav').expand();
			Ext.getCmp('ProductInfoTab').setActiveTab(1);
			var subProducts = param.split("_-_");
			var str = '<span class="searchResultInfo1">'+name+'</span><br>'
			str +='<table class="tablesorter">';
			str += '<thead>';
			str += '<tr>';
			str += '<th>Room Type</th>';
			str += '<th>Board Type</th>';
			str += '<th>Total Price</th>';
			str += '<th>Action</th>';

			str += '<tr>';
			str += '</thead>';
			str += '<tbody>';
			for(var q=0;q<data.length;q++){
				
				str += '<tr>';
				str += '<td>';
				var ctr = 1;
				for(var w=0;w<data[q].room_deatils.length;w++){
					//var temp_room_str_obj = {"Room"+ctr};
					str += '<b>Room '+ctr+'</b> - '+data[q].room_deatils[w]["Room"+ctr][0]+' ($'+data[q].room_deatils[w]["Room"+ctr][1].amt+')<br>';
					ctr++;
				}
				
				str += '</td>';
				str += '<td>'+data[q].room_board_type+'</td>';
				str += '<td>$'+data[q].room_total_price.amt+'</td><td>';
				str +='<button type="button" class="searchButton2" onClick="search.allocateProduct(\''+data[q].hotel_booking_code+'\')">NOW</button></td></tr>';
				
			}
			
			str += '</tbody>';
			str += '</table>';
			//alert(str);
			document.getElementById('subProductDetails').innerHTML = str;
			
		},
		allocateProduct: function(params){
		//alert('Development has been paused for the meantime');
			
			if(Ext.getCmp('tabProductPriceSummary')){
				Ext.getCmp('tabProductPriceSummary').show();
				document.getElementById('TabResultsAllocation').src = "modules/xml_product_allocation.php?params="+params;
			}else{
				 Ext.getCmp('tabForSearch').add({
					title: 'Product Price/Summary',
					id: 'tabProductPriceSummary',
					autoDestroy: true,
					autoEl : {
						   tag : "iframe",
						   height: '100%',
						   id: "TabResultsAllocation",
						   src : "modules/xml_product_allocation.php?params="+params
						},
					closable:false
				}).show();
			}
				
		
			
			
		
			//alert(params);
		},
		viewDetails: function(pname,pcode,photo,address,longtitue,latitude,service,supplier,star,area,pphone,pdescription,aint,aext){
		if(Ext.getCmp('min_win_view_products_search')){
		Ext.getCmp('win_view_products_search').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_view_products_search'));
		Ext.getCmp('task_bar').doLayout();
		}
		try{Ext.getCmp('win_view_products_search').close();}catch(err){}
		
	
		var resultTpl = new Ext.Template(
			'<tpl for=".">',
			'<div class="viewProductDetails">',
				'<div class="info">',
				'<a href="javascript:products.viewPhotos(\''+pname+'\',\''+pcode+'\');"><img src="'+photo+'" class="photoDetails"></a>',
				'</div>',
				'<div class="info1">',
				'<h3>'+pname+'</h3>',
				'<span class="address">'+address+'  <a href="javascript:products.viewMap('+latitude+','+longtitue+',\''+pname+'\',\''+address+'\');"><img src="images/icons/map_magnify.png"></a></span>',
				'<p>',
				'<span class="subHeader">Service Type: </span> '+service+'</br>',
				'<span class="subHeader">Supplier Name: </span> '+supplier+'</br>',
				'</p>',
				'<p>',
				'<span class="subHeader">Star Rating: </span> '+star+' <img src="images/icons/star.png"></br>',
				'<span class="subHeader">Area: </span> '+area+'</br>',
				'<span class="subHeader">Phone Number: </span> '+pphone+'</br>',
				
				'</p>',
				'</div>',
				'<div class="info2">',
				'<h3>Description</h3>',
				'<p>'+pdescription+'</p>',
				'<div>',
				'<div class="info4">',
				'<h3>Amenities</h3>',
				'<p>',
				'</p>',
				'<span class="subHeader">Main Product:</span> '+aint+'</br>',
				'<span class="subHeader">Product:</span>'+aext+'',
				
				'<div>',
			'</div></tpl>'
		);
		
		
		var viewPanel = new Ext.Panel({
			autoScroll:true,
			id:'view-details-product-search',
			border: false,
			items: resultTpl,
			frame: false,
			layout:'fit'
		});
		
		
		proj_tools.new_win('win_view_products_search',800,500,viewPanel,'Product Details');
		search.viewPanel = viewPanel;
		
			
		},
		
		searchReset:function () {
			search.searchCriteria.getForm().reset();	
		},
		viewPhotos:function(ttitle,code){
			if(Ext.getCmp('min_win_create_products_view_photo')){
			Ext.getCmp('win_create_products_view_photo').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_products_view_photo'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_create_products_view_photo').close();}catch(err){}
		
			var tplPhoto = new Ext.Template(
                    '<iframe src="'+base_url+'photoGallery?code='+code+'" width=100% height=100%></iframe>'
                );
			proj_tools.new_win('win_create_products_view_photo',675,415,tplPhoto,'Photo Gallery');
			
		},
		viewMap:function(latitude,longitude,ttitle,address){
			if(Ext.getCmp('min_win_create_products_view_map')){
			Ext.getCmp('win_create_products_view_map').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_products_view_map'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_create_products_view_map').close();}catch(err){}
		
			var mapPanel = {
                    xtype: 'gmappanel',
                    zoomLevel: 14,
                    gmapType: 'map',
                    mapConfOpts: ['enableScrollWheelZoom','enableDoubleClickZoom','enableDragging'],
                    mapControls: ['GSmallMapControl','GMapTypeControl','NonExistantControl'],
                    setCenter: {
                        geoCodeAddr: address,
                        marker: {title: ttitle}
                    },
                    markers: [{
                        lat: latitude,
                        lng: longitude,
                        marker: {title: ttitle}
                    }]
                }; 
			proj_tools.new_win('win_create_products_view_map',450,450,mapPanel,'Geo Map ');
			
		},
		getNewSearch: function(){
			Ext.Ajax.request({
										url: "_ajax/execute_get_booking.php",
										method: "POST",
										success: function () {
											search.view();
											
										},
										params: {action: "getNewBooking"}
										
									});
		}
	}
}();