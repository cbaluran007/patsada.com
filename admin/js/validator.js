Ext.namespace("app_space");

var validator = function () {
	return {
		runThis: function(){
		
		Ext.apply(Ext.form.VTypes, {
			bloodValidator : validator.btype,
			midValidator : validator.midName,
			phoneValidator : validator.phone,
			phoneValidator : validator.phone,
			sssValidator : validator.sss,
			tinValidator : validator.tin,
			philhealthValidator : validator.philhealth,
			ipValidator : validator.ip,
			codeValidator : validator.code,
			passValidator : validator.validatePasswords,
			daterange: validator.daterange
		});
		
		},
		btype: function(value, field){
		var valid = false;
		var val = value.toUpperCase();
		
			if(val=='A' || val=='AB' || val=='O' || val=='B'){
				valid = true;
			}
		return valid;  
		},
		midName:function(value, field){
		var valid = false;
		var val = value.length;
			if(val==1){
				valid = true;
			}
		return valid;  
		},
		phone: function(value, field){
		var valid = false;
			if(value.match(/^(\+\d)*\s*(\(\d{3}\)\s*)*\d{3}(-{0,1}|\s{0,1})\d{2}(-{0,1}|\s{0,1})\d{2}$/)||value.match(/^((?:\+639|639)|09)(=05|06|07|08|09|10|12|15|16|17|18|19|20|21|22|23|26|27|28|29|38|73|74|79)(\d{7})$/) ){
				valid = true;
			}
		return valid;  
		},
		sss: function(value, field){
		var valid = false;
			if(value.match(/^(\d{2})-(\d{7})-(\d{1})$/)){
				valid = true;
			}
		return valid;  
		},
		sssKeyPress: function(val,cmp){
		
			if(val.length==2||val.length==10){
				var str = val+'-';
				Ext.get(cmp).dom.value=str;
			}
			if(val.length==12){
				Ext.get(cmp).dom.value=val.substr(0,11);
			}
		},
		philhealth: function(value, field){
		var valid = false;
			if(value.match(/^(\d{2})-(\d{9})-(\d{1})$/)){
				valid = true;
			}
		return valid;  
		},
		philhealthKeyPress: function(val,cmp){
		
			if(val.length==2||val.length==12){
				var str = val+'-';
				Ext.get(cmp).dom.value=str;
			}
			if(val.length==14){
				Ext.get(cmp).dom.value=val.substr(0,13);
			}
		},
		tin: function(value, field){
		var valid = false;
			if(value.match(/^(\d{3})-(\d{3})-(\d{3})$/)){
				valid = true;
			}
		return valid;  
		},
		tinKeyPress: function(val,cmp){
		
			if(val.length==3||val.length==7){
				var str = val+'-';
				Ext.get(cmp).dom.value=str;
			}
			if(val.length==11){
				Ext.get(cmp).dom.value=val.substr(0,10);
			}
		},
		ip: function(value, field){
		var valid = false;
			if(value.match(/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/)){
				valid = true;
			}
		return valid;  
		},
		code: function(value, field){
		var valid = false;
			if(value.match(/^(\d{3})-(\d{3})-(\d{3})$/)){
				valid = true;
			}
		return valid;  
		},
		validatePasswords: function (value, field) {
		
		var valid = false;  
		
		if (Ext.getCmp('pass')) {  
          var otherField = Ext.getCmp('pass');  
				if (value == otherField.getValue()) {  
				otherField.clearInvalid();  
				valid = true;  
				}  
			}  
		return valid;  
			 
		},
		daterange: function(val, field) {
					var date = field.parseDate(val);

					if(!date){
						return false;
					}
					if (field.startDateField) {
						var start = Ext.getCmp(field.startDateField);
						if (!start.maxValue || (date.getTime() != start.maxValue.getTime())) {
							start.setMaxValue(date);
							start.validate();
						}
					}
					else if (field.endDateField) {
						var end = Ext.getCmp(field.endDateField);
						if (!end.minValue || (date.getTime() != end.minValue.getTime())) {
							end.setMinValue(date);
							end.validate();
						}
					}
					/*
					 * Always return true since we're only using this vtype to set the
					 * min/max allowed values (these are tested for after the vtype test)
					 */
					return true;
		}
	}
	
}();