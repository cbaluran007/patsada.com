submituserExt.namespace("app_space");

var user_man = function () {
	return {
		getPanel: function(){
		
		if(Ext.getCmp('min_win_create_user')){
		Ext.getCmp('win_create_user').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_user'));
		Ext.getCmp('task_bar').doLayout();
		}
		
		try{Ext.getCmp('win_create_user').close();}catch(err){}
		
		var form = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Alias Name',	name:'aliasname', id:'aliasname',width: "95%", },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Username',	name:'username', id:'username',width: "95%" },
				{ xtype: 'textfield',inputType: 'password',allowBlank:false,  fieldLabel: 'Password',	name:'pass', id:'pass', width: "95%",minLength: 6 },
				{ xtype: 'textfield',inputType: 'password',allowBlank:false,  fieldLabel: 'Re-type Passowrd',	name:'repass', id:'repass',width: "95%" ,
				validator: function() {
					var passVal = Ext.getCmp("pass").getValue();
					if(this.getValue() != passVal){
						return false;
					}else{
						return true;
					}
				}},
				{id: "user-sys-id", name: "user-sys-id", xtype: "hidden", value: 0},
				{id: "user-role-id", name: "user-role-id", xtype: "hidden", value: 'S'}
       
				]
			});
		
		var store = new Ext.data.ArrayStore({
			fields: ['value','display'],
			data : [ ['Mr.','Mr.'], ['Mrs.','Mrs.'],['Mstr.','Mstr.'], ['Ms.','Ms.'],['Dr.','Dr.']]
		});
		
		var form2 = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				proj_tools.getSimpleCombo(store,'Title','user_sys_man_title','display','value'),
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'First Name',	name:'firstname', id:'firstname',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Last Name',	name:'lastname', id:'lastname',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Email',	name:'email', id:'email',width: "95%", vtype:'email' },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Phone #',	name:'phone_num', id:'phone_num',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Mobile #',	name:'mobile_num', id:'mobile_num',width: "95%" },
				proj_tools.getCountryCombo('user_sys'),
				proj_tools.getStateCombo('user_sys'),
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Address',	name:'address', id:'address',width: "95%" }
       
				]
			});
			
		var tabs = new Ext.TabPanel({
					activeTab: 0,
					width:"100%",
					border:false,
					id:'employee_tab_manager',
					items:[
						
						{
							title: 'User Access Credentials',
							id: 'user_form_id_1',
							items: [form]
						},
						{
							title: 'User Information Data',
							id: 'user_form_id_2',
							items: [form2]
						}
					]
				});
				
		var mainForm = new Ext.FormPanel({
					width:400,
					height:350,
					border:false,
					id:'employee_main_manager',
					items:[tabs	],
					buttons: [
						{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: user_man.submitUser},
						{text: "Reset", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: user_man.reset}
						
					]
				});
		
		
		
			
			//validator.runThis();
			proj_tools.new_win('win_create_user',400,350,mainForm,'Create New System User');
			user_man.mainForm = mainForm;	
						
		
		},
		submitUser: function(){
			var parr = {};
			parr["action"] = "submitCreateSysUser";
			parr["aliasname"] = Ext.getCmp("aliasname").getValue();
			parr["username"] = Ext.getCmp("username").getValue();
			parr["pass"] = Ext.getCmp("pass").getValue();
			parr["repass"] = Ext.getCmp("repass").getValue();
			parr["country"] = Ext.getCmp("country_user_sys_combo_id").getRawValue();
			parr["state"] = Ext.getCmp("state_user_sys_combo_id").getRawValue();
			parr["user_system_id"] = Ext.getCmp("user-sys-id").getValue();
			parr["user_role_id"] = Ext.getCmp("user-role-id").getValue();
			parr["user_title"] = Ext.getCmp("user_sys_man_title_combo_id").getRawValue();
			parr["firstname"] = Ext.getCmp("firstname").getValue();
			parr["lastname"] = Ext.getCmp("lastname").getValue();
			parr["email"] = Ext.getCmp("email").getValue();
			parr["phone_num"] = Ext.getCmp("phone_num").getValue();
			parr["mobile_num"] = Ext.getCmp("mobile_num").getValue();
			parr["address"] = Ext.getCmp("address").getRawValue();
			
			
			
			
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager.php",
				method: "POST",
				success: function ( result, request) {
						//alert(result.responseText);
						var jsonData = Ext.util.JSON.decode(result.responseText);
                        var stats = jsonData.status;
						if(stats){
							
							Ext.getCmp('win_create_user').close();
							user_man.viewUser();
							user_man.grid.getStore().load();
							Ext.MessageBox.alert('Sucess', 'System user successfully saved.');
						}else{
							Ext.MessageBox.alert('Failure', 'Unable to saved system user.');
						}
					},
				failure: function () {
						Ext.MessageBox.alert('Failure', 'Unable to connect.');
					
				},
				params: parr
				
			});
			
			
						
			
		
		},
		reset: function(){
			
		user_man.mainForm.getForm().reset();	
			
		
		},
		viewUser: function(){
			
			if(Ext.getCmp('min_win_userman_grid')){
			Ext.getCmp('win_userman_grid').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_userman_grid'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_userman_grid').close();}catch(err){}
			var store = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_user_manager.php?action=viewUser&role=S",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewUser",
		            id: "id_account",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "id_account", mapping: "id_account"},
            			{name: "account_code", mapping: "account_code"},
						{name: "account_name", mapping: "account_name"},
						{name: "username", mapping: "username"},
            			{name: "is_active", mapping: "is_active"},
						{name: "is_confirmed", mapping: "is_confirmed"},
						{name: "role", mapping: "role"},
						{name: "first_name", mapping: "first_name"},
						{name: "last_name", mapping: "last_name"},
						{name: "title", mapping: "title"},
						{name: "email", mapping: "email"},
						{name: "country", mapping: "country"},
						{name: "state", mapping: "state"},
						{name: "address", mapping: "address"},
						{name: "phone_number", mapping: "phone_number"},
						{name: "mobile_number", mapping: "mobile_number"},
						{name: "created_by", mapping: "created_by"},
						{name: "created_datetime", mapping: "created_datetime"}
						
			
        			]
					}),
					sortInfo:{field: 'created_datetime', direction: "DESC"}
				});
			var grid = new Ext.grid.GridPanel({
				store: store,
				cm: new Ext.grid.ColumnModel([
					{id: "id-grid-details-user",header: "ID", width: 30,  sortable: true, locked:true, dataIndex: "id_account"},
					{header: "Code", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "account_code"},
					{header: "Alias Name", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "account_name"},
					{header: "Fullname", width: 100,  sortable: true, locked:true,align:'left', dataIndex: "first_name"},
					{header: "Active", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_active", renderer: render.YesNo},
					{header: "Confirm", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_confirmed",renderer: render.YesNo},
					{header: "Email", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "email"},
					{header: "Country", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "country"},
					{header: "State", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "state"},
					{header: "Address", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "address"},
					{header: "Phone #", width: 30,  sortable: true, locked:true,align:'left', dataIndex: "phone_number"},
					{header: "Mobile #", width: 30,  sortable: true, locked:true,align:'left', dataIndex: "mobile_number"},
					{header: "Created By", width:50,  sortable: true, locked:true,align:'left', dataIndex: "created_by"}
					]),
				loadMask: true,	
				autoExpandColumn: "id-grid-details-user",
				border: false,
				viewConfig: {
					forceFit:true
				},
				listeners:  {
					rowcontextmenu: user_man.rowContextMenu
				},
				width: '100%',
				//border: true,
				layout: 'fit',
				height: 175,
				tbar: [
            '		Search System User Name: ', ' ',
					new Ext.ux.form.SearchField({
						store: store,
						width:320
					})
				],
				bbar: new Ext.PagingToolbar({
					pageSize: 15,
					store: store,
					displayInfo: true,
					displayMsg: 'Displaying topics {0} - {1} of {2}',
					emptyMsg: "No topics to display"//,
					/*items:[
						'-', {
						pressed: true,
						enableToggle:true,
						text: 'Show Preview',
						cls: 'x-btn-text-icon details',
						toggleHandler: function(btn, pressed){
							var view = grid.getView();
							view.showPreview = pressed;
							view.refresh();
						}
					}]*/
				})
			});

			
			
			user_man.grid = grid;
			user_man.grid.getStore().load();
			
			proj_tools.new_win('win_userman_grid','85%',400,grid,'System User List');
			
					
		},
		rowContextMenu: function (grid, rowIndex, e) {
								
			e.stopEvent(); 
			
	        grid.getSelectionModel().selectRow(rowIndex, false);
	        user_man.row = grid.getSelectionModel().getSelected();
	        	
	        var coords = e.getXY();

	        var myContextMenu = user_man.menus();
			
			if ( user_man.row.get("is_active") == 'Y' || user_man.row.get("is_active") == 1) {
				Ext.getCmp("user_man-menu-active").disable();
			} else {
				Ext.getCmp("user_man-menu-deactive").disable();
			}
			
			if ( user_man.row.get("is_confirmed") == 'Y' || user_man.row.get("is_confirmed") == 1) {
				Ext.getCmp("user_man-menu-confirm").disable();
			} else {
				Ext.getCmp("user_man-menu-unconfirm").disable();
			}
			
	        myContextMenu.showAt([coords[0], coords[1]]);
	        	    
		},
		menus: function(){
			
			if(Ext.getCmp('user_man-grid-context-menu')){
				Ext.getCmp('user_man-grid-context-menu').removeAll();
			}
			
			var myContextMenu = new Ext.menu.Menu({
				id: "user_man-grid-context-menu",
				items: [
					{
						text: "Activate",
						id: "user_man-menu-active",
						iconCls: "approvedIcon",  
						icon: "images/icons/accept.png",
						handler: user_man.setactive
					},
					{
						text: "Deactivate",		
						id: "user_man-menu-deactive",
						iconCls: "disapprovedIcon",  
						icon: "images/icons/cross.png",
						handler: user_man.setdeactive
					},
					'-',
					{
						text: "Confirm",
						id: "user_man-menu-confirm",
						iconCls: "approvedIcon",  
						icon: "images/icons/accept.png",
						handler: user_man.setConfirmactive
					},
					{
						text: "Unconfirm",		
						id: "user_man-menu-unconfirm",
						iconCls: "disapprovedIcon",  
						icon: "images/icons/cross.png",
						handler: user_man.setConfirmdeactive
					},
					'-',
					{
						text: "Edit",
						id: "user_man-menu-edit",
						iconCls: "approvedIcon",  
						icon: "images/icons/folder_edit.png",
						handler: user_man.edit
					}
				]
			});
			
			return myContextMenu;
		},
		edit: function(){
			user_man.getPanel();
	
	
			//alert(user_man.row.get("title"));
			Ext.getCmp('aliasname').setValue(user_man.row.get("account_name"));
			Ext.getCmp('username').setValue(user_man.row.get("username"));
			Ext.getCmp('user-sys-id').setValue(user_man.row.get("id_account"));
			Ext.getCmp('user-role-id').setValue(user_man.row.get("role"));
			
			Ext.getCmp('user_sys_man_title_combo_id').setValue(user_man.row.get("title"));
			Ext.getCmp('user_sys_man_title_combo_id').setRawValue(user_man.row.get("title"));
			Ext.getCmp('firstname').setValue(user_man.row.get("first_name"));
			Ext.getCmp('lastname').setValue(user_man.row.get("last_name"));
			Ext.getCmp('email').setValue(user_man.row.get("email"));
			Ext.getCmp('phone_num').setValue(user_man.row.get("phone_number"));
			Ext.getCmp('mobile_num').setValue(user_man.row.get("mobile_number"));
			Ext.getCmp('address').setValue(user_man.row.get("address"));
			
			
			Ext.getCmp("state_user_sys_combo_id").setDisabled(false);
    
			Ext.getCmp('country_user_sys_combo_id').setValue(user_man.row.get("country"));
			Ext.getCmp('country_user_sys_combo_id').setRawValue(country_arr.indexOf(user_man.row.get("country")));
			Ext.getCmp('state_user_sys_combo_id').setValue(user_man.row.get("state"));
			Ext.getCmp('state_user_sys_combo_id').setRawValue(user_man.row.get("state"));
			
			
			
			
						
			
		},
		setactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager.php",
				method: "POST",
				success: function () {
					user_man.grid.getStore().load();
				},
				params: {action: "activate", user_id: user_man.row.get("id_account")}
				
			});
		},
		setdeactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager.php",
				method: "POST",
				success: function () {
					user_man.grid.getStore().load();
				},
				params: {action: "deactivate", user_id: user_man.row.get("id_account")}
				
			});
		},
		setConfirmactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager.php",
				method: "POST",
				success: function () {
					user_man.grid.getStore().load();
				},
				params: {action: "activateConfirm", user_id: user_man.row.get("id_account")}
				
			});
		},
		setConfirmdeactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager.php",
				method: "POST",
				success: function () {
					user_man.grid.getStore().load();
				},
				params: {action: "deactivateConfirm", user_id: user_man.row.get("id_account")}
				
			});
		}
	}
}();