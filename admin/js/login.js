
Ext.onReady(function(){

     var login = new Ext.FormPanel({
        labelWidth: 75, // label settings here cascade unless overridden
        frame:true,
        bodyStyle:'padding:5px 5px 0',
        width: 350,
        defaults: {width: 230},
        defaultType: 'textfield',
		monitorValid:true,
        items: [{
                fieldLabel: 'User Name',
                name: 'user_name',
                allowBlank:false
				},{
					fieldLabel: 'Password',
					name: 'password',
					inputType:'password'
				}
			],

			buttons: [{
				text: 'Login',
				icon: "images/icons/key.png",
				iconCls: "blist",
				formBind: true,
				handler: function(){
						login.getForm().submit({
							url: '_ajax/execute_login.php',
							method: 'post',
							success: function () {
								window.location = "appv1.php";
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to login. Please try again.');
							},
							
							params: {action: "Login"}
							
						});
					
				}
			},{
				text: 'Reset',
				icon: "images/icons/arrow_rotate_clockwise.png",
				iconCls: "blist",
				handler: function(){
						
					login.getForm().reset();
					
				}
			}]
		});


	 var win = new Ext.Window({
				title:"Please Login",
                layout:'fit',
                width:350,
                height:150,
				items: login

                
            });
	win.show();
    
});