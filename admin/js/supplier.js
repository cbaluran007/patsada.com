Ext.namespace("app_space");

var supplier = function () {
	return {
		getPanel: function(){
		
		if(Ext.getCmp('min_win_create_supplier')){
		Ext.getCmp('win_create_supplier').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_supplier'));
		Ext.getCmp('task_bar').doLayout();
		}
		
		try{Ext.getCmp('win_create_supplier').close();}catch(err){}
		var store = new Ext.data.ArrayStore({
			fields: ['value','display'],
			data : [ ['Mr.','Mr.'], ['Mrs.','Mrs.'],['Mstr.','Mstr.'], ['Ms.','Ms.'],['Dr.','Dr.']]
		});
		var form = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:true, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Supplier Name',	name:'suppliername', id:'suppliername',width: "95%" },
				{
					xtype:'fieldset',
					title: 'Supplier Contact Information',
					collapsible: false,
					autoHeight:true,
					defaults: {width: 210},
					defaultType: 'textfield',
					items :[
						proj_tools.getSimpleCombo(store,'Title','title_supplier','display','value'),
						{
							fieldLabel: 'First Name',
							name: 'fname-supplier',
							id: 'fname-supplier'
						},{
							fieldLabel: 'Last Name',
							name: 'lname-supplier',
							id: 'lname-supplier'
						},{
							fieldLabel: 'Email',
							name: 'email-supplier',
							id: 'email-supplier',
							vtype:'email'
						},{
							fieldLabel: 'Phone #',
							name: 'phone_num_supplier',
							id: 'phone_num_supplier'
						},
						{
							fieldLabel: 'Mobile #',
							name: 'mobile_num_supplier',
							id: 'mobile_num_supplier'
						},
						proj_tools.getCountryCombo('supplier'),
						proj_tools.getStateCombo('supplier'),
						{
							fieldLabel: 'Address',
							name: 'address_supplier',
							id: 'address_supplier'
						}
					]
				},
				{id: "supplier-id", name: "supplier-id", xtype: "hidden", value: 0}
       
				],
				buttons: [
						{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: supplier.submitSupplier},
						{text: "Reset", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: supplier.reset}
						
					]
			});
		
		
		
		
		
		
			
			//validator.runThis();
			proj_tools.new_win('win_create_supplier',400,400,form,'Create New Supplier');
			supplier.mainForm = form;	
						
		
		},
		submitSupplier: function(){
			var parr = {};
			parr["action"] = "submitSupplier";
		
			supplier.mainForm.getForm().submit({
						
							url: '_ajax/execute_supplier.php',
							method: 'post',
							success: function () {
								Ext.getCmp('win_create_supplier').close();
								supplier.view();
								supplier.grid.getStore().load();
								Ext.MessageBox.alert('Sucess', 'Supplier successfully saved.');
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to saved supplier. Please try again.');
							},
							
							params: parr
							
						});
		
		},
		reset: function(){
			
		supplier.mainForm.getForm().reset();	
			
		
		},
		view: function(){
			
			if(Ext.getCmp('min_win_supplier_grid')){
			Ext.getCmp('win_supplier_grid').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_supplier_grid'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_supplier_grid').close();}catch(err){}
			
			var expander =  new Ext.ux.grid.RowExpander({
				tpl : new Ext.Template(
					'<div class="suppInfoDiv">',
					'<p class="supplieInfoHeader">Supplier Contact Info</p>',
					'<br>',
					'<table>',
					'<tr>',
					'<td>Full Name:</td><td>{title} {first_name} {last_name}</td>',
					'<td>Country:</td><td>{country}</td>',
					'</tr>',
					'<tr>',
					'<td>Email:</td><td>{email} </td>',
					'<td>State:</td><td>{state}</td>',
					'</tr>',
					'<tr>',
					'<td>Contact #:</td><td>Phone #: {phone_number} <br>Mobile #: {mobile_number}</td>',
					'<td>Address:</td><td>{address}</td>',
					'</tr>',
					'</table>',
					'</div>'
				)
			});
			
			var store = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_supplier.php?action=view",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewSupplier",
		            id: "id_supplier",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "id_supplier", mapping: "id_supplier"},
            			{name: "supplier_code", mapping: "supplier_code"},
						{name: "supplier_name", mapping: "supplier_name"},
						{name: "first_name", mapping: "first_name"},
						{name: "last_name", mapping: "last_name"},
						{name: "title", mapping: "title"},
						{name: "email", mapping: "email"},
						{name: "country", mapping: "country"},
						{name: "state", mapping: "state"},
						{name: "address", mapping: "address"},
						{name: "phone_number", mapping: "phone_number"},
						{name: "mobile_number", mapping: "mobile_number"},
						{name: "is_active", mapping: "is_active"},
            			{name: "created_by", mapping: "created_by"},
						{name: "created_datetime", mapping: "created_datetime"}
			
			
        			]
					}),
					sortInfo:{field: 'created_datetime', direction: "DESC"}
				});
			var grid = new Ext.grid.GridPanel({
				store: store,
				cm: new Ext.grid.ColumnModel([
					expander,
					{id: "id-grid-details-supplier",header: "ID", width: 30,  sortable: true, locked:true, dataIndex: "id_supplier"},
					{header: "Code", width: 30,  sortable: true, locked:true,align:'left', dataIndex: "supplier_code"},
					{header: "Supplier Name", width: 90,  sortable: true, locked:true,align:'left', dataIndex: "supplier_name"},
					{header: "Active", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_active", renderer: render.YesNo},
					{header: "Created By", width:50,  sortable: true, locked:true,align:'left', dataIndex: "created_by"}
					]),
				loadMask: true,	
				autoExpandColumn: "id-grid-details-supplier",
				border: false,
				viewConfig: {
					forceFit:true
				},
				listeners:  {
					rowcontextmenu: supplier.rowContextMenu
				},
				width: '100%',
				//border: true,
				layout: 'fit',
				plugins: expander,
				height: 175,
				tbar: [
            '		Search Supplier Name: ', ' ',
					new Ext.ux.form.SearchField({
						store: store,
						width:320
					})
				],
				bbar: new Ext.PagingToolbar({
					pageSize: 15,
					store: store,
					displayInfo: true,
					displayMsg: 'Displaying Supplier {0} - {1} of {2}',
					emptyMsg: "No supplier to display"
					
				})
			});

			
			
			supplier.grid = grid;
			supplier.grid.getStore().load();
			
			proj_tools.new_win('win_supplier_grid',550,400,grid,'Supplier List');
			
					
		},
		rowContextMenu: function (grid, rowIndex, e) {
								
			e.stopEvent(); 
			
	        grid.getSelectionModel().selectRow(rowIndex, false);
	        supplier.row = grid.getSelectionModel().getSelected();
	        	
	        var coords = e.getXY();

	        var myContextMenu = supplier.menus();
			
			if ( supplier.row.get("is_active") == 'Y' || supplier.row.get("is_active") == 1) {
				Ext.getCmp("supplier-menu-active").disable();
			} else {
				Ext.getCmp("supplier-menu-deactive").disable();
			}
			
			
			
	        myContextMenu.showAt([coords[0], coords[1]]);
	        	    
		},
		menus: function(){
			
			if(Ext.getCmp('supplier-grid-context-menu')){
				Ext.getCmp('supplier-grid-context-menu').removeAll();
			}
			
			var myContextMenu = new Ext.menu.Menu({
				id: "supplier-grid-context-menu",
				items: [
					{
						text: "Activate",
						id: "supplier-menu-active",
						iconCls: "approvedIcon",  
						icon: "images/icons/accept.png",
						handler: supplier.setactive
					},
					{
						text: "Deactivate",		
						id: "supplier-menu-deactive",
						iconCls: "disapprovedIcon",  
						icon: "images/icons/cross.png",
						handler: supplier.setdeactive
					},
					'-',
					{
						text: "Edit",
						id: "supplier-menu-edit",
						iconCls: "approvedIcon",  
						icon: "images/icons/folder_edit.png",
						handler: supplier.edit
					}
				]
			});
			
			return myContextMenu;
		},
		edit: function(){
			supplier.getPanel();
	
	
			//alert(user_man.row.get("title"));
			
			Ext.getCmp('supplier-id').setValue(supplier.row.get("id_supplier"));
			Ext.getCmp('suppliername').setValue(supplier.row.get("supplier_name"));
			Ext.getCmp('fname-supplier').setValue(supplier.row.get("first_name"));
			Ext.getCmp('lname-supplier').setValue(supplier.row.get("last_name"));
			Ext.getCmp('email-supplier').setValue(supplier.row.get("email"));
			Ext.getCmp('phone_num_supplier').setValue(supplier.row.get("phone_number"));
			Ext.getCmp('mobile_num_supplier').setValue(supplier.row.get("mobile_number"));
			Ext.getCmp('address_supplier').setValue(supplier.row.get("address"));
			
			
			
			Ext.getCmp('title_supplier_combo_id').setValue(supplier.row.get("title"));
			Ext.getCmp('title_supplier_combo_id').setRawValue(supplier.row.get("title"));
			
			Ext.getCmp('country_supplier_combo_id').setValue(supplier.row.get("country"));
			Ext.getCmp('country_supplier_combo_id').setRawValue(supplier.row.get("country"));
			Ext.getCmp("state_supplier_combo_id").setDisabled(false);
			Ext.getCmp('state_supplier_combo_id').setValue(supplier.row.get("state"));
			Ext.getCmp('state_supplier_combo_id').setRawValue(supplier.row.get("state"));
			
			
		
						
			
		},
		setactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_supplier.php",
				method: "POST",
				success: function () {
					supplier.grid.getStore().load();
				},
				params: {action: "activate", supplier_id: supplier.row.get("id_supplier")}
				
			});
		},
		setdeactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_supplier.php",
				method: "POST",
				success: function () {
					supplier.grid.getStore().load();
				},
				params: {action: "deactivate", supplier_id: supplier.row.get("id_supplier")}
				
			});
		}
	}
}();