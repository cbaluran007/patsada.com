Ext.namespace("app_space");

var patsada_users = function () {
    return {
        getPanel: function(){
            if(Ext.getCmp('min_win_patsada_users_form')){
                Ext.getCmp('win_patsada_users_form').show();
                Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_patsada_users_form'));
                Ext.getCmp('task_bar').doLayout();
            }

            try{Ext.getCmp('win_patsada_users_form').close();}catch(err){}
        },
        view: function(){
            if(Ext.getCmp('min_win_patsada_users_grid')){
                Ext.getCmp('win_patsada_users_grid').show();
                Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_patsada_users_grid'));
                Ext.getCmp('task_bar').doLayout();
            }

            try{Ext.getCmp('win_patsada_users_grid').close();}catch(err){}
            var storeJP = new Ext.data.Store({
                proxy: new Ext.data.HttpProxy({
                    url: "_ajax/execute_patsda_users_posted.php?action=view",
                    method: "GET"
                }),
                reader: new Ext.data.JsonReader({
                    root: "view_job_post",
                    id: "id_job_post_json",
                    totalProperty: "total",
                    fields: [
                        //"fullname",
                        {name: "login_credentials_id", mapping: "login_credentials_id"},
                        {name: "is_candidate", mapping: "is_candidate"},
                        {name: "is_employer", mapping: "is_employer"},
                        {name: "candidate_id", mapping: "candidate_id"},
                        {name: "employer_user_id", mapping: "employer_user_id"},
                        {name: "user_name", mapping: "user_name"},
                        {name: "password", mapping: "password"},
                        {name: "created_datetime", mapping: "created_datetime"},
                        {name: "is_active", mapping: "is_active"},
                        {name: "is_confirmed", mapping: "is_confirmed"},
                        {name: "is_admin", mapping: "is_admin"}
                    ]
                }),
                sortInfo:{field: 'login_credentials_id', direction: "DESC"}
            });

            var grid = new Ext.grid.GridPanel({
                store: storeJP,
                cm: new Ext.grid.ColumnModel([
                    {header: "ID", width: 30,  sortable: true, locked:true, dataIndex: "login_credentials_id"},
                    {id: "id-grid-details-patsada_users",header: "User Name", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "user_name"},
                    {header: "Active", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_active", renderer: render.YesNo},
                    {header: "Confirm", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_confirmed", renderer: render.YesNo},
                    {header: "Employer", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_employer", renderer: render.YesNo},
                    {header: "Candidate", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_candidate", renderer: render.YesNo},
                    {header: "Created Date", width:50,  sortable: true, locked:true,align:'left', dataIndex: "created_datetime"}
                ]),
                loadMask: true,
                autoExpandColumn: "id-grid-details-patsada_users",
                border: false,
                viewConfig: {
                    forceFit:true
                },
                listeners:  {
                    rowcontextmenu: patsada_users.rowContextMenu
                },
                width: '100%',
                //border: true,
                layout: 'fit',
                region:"center",
                height: 175,
                 tbar: [
                 'Search Service Users: ', ' ',
                 new Ext.ux.form.SearchField({
                 store: storeJP,
                 width:320
                 })
                 ],
               bbar: new Ext.PagingToolbar({
                 pageSize: 15,
                 store: storeJP,
                 displayInfo: true,
                 displayMsg: 'Displaying Users {0} - {1} of {2}',
                 emptyMsg: "No Users to display",
                     items:[
                     '-', {
                     pressed: true,
                     enableToggle:true,
                     text: 'Add New Users',
                     icon: "images/icons/application_add.png",
                     handler: function () {
                     service.getPanel();
                     }
                     }]
                 })
            });


            patsada_users.grid = grid;
            patsada_users.grid.getStore().load();

            proj_tools.new_win_grid('win_patsada_users_grid',550,400,[grid],'User List');
        },
        rowContextMenu: function (grid, rowIndex, e) {

            e.stopEvent();

            grid.getSelectionModel().selectRow(rowIndex, false);
            patsada_users.row = grid.getSelectionModel().getSelected();

            var coords = e.getXY();

            var myContextMenu = patsada_users.menus();

            if ( patsada_users.row.get("is_active") == 'Y' || patsada_users.row.get("is_active") == 1) {
                Ext.getCmp("job-posted-menu-active").disable();
            } else {
                Ext.getCmp("job-posted-menu-deactive").disable();
            }
            myContextMenu.showAt([coords[0], coords[1]]);

        },
        menus: function(){

            if(Ext.getCmp('service-grid-context-menu')){
                Ext.getCmp('service-grid-context-menu').removeAll();
            }

            var myContextMenu = new Ext.menu.Menu({
                id: "service-grid-context-menu",
                items: [
                    {
                        text: "Activate",
                        id: "job-posted-menu-active",
                        iconCls: "approvedIcon",
                        icon: "images/icons/accept.png",
                        handler: patsada_users.setconfirm
                    },
                    {
                        text: "Deactivate",
                        id: "job-posted-menu-deactive",
                        iconCls: "disapprovedIcon",
                        icon: "images/icons/cross.png",
                        handler: patsada_users.setdeconfirm
                    }
                ]
            });

            return myContextMenu;
        },

        setconfirm: function(){
            Ext.Ajax.request({
                url: "_ajax/execute_patsda_users_posted.php",
                method: "POST",
                success: function () {
                    patsada_users.grid.getStore().load();
                },
                params: {action: "activate", login_credentials_id: patsada_users.row.get("login_credentials_id")}

            });
        },
        setdeconfirm: function(){
            Ext.Ajax.request({
                url: "_ajax/execute_patsda_users_posted.php",
                method: "POST",
                success: function () {
                    patsada_users.grid.getStore().load();
                },
                params: {action: "deactivate", login_credentials_id: patsada_users.row.get("login_credentials_id")}

            });
        }
    }
}();