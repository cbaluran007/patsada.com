Ext.namespace("app_space");

var post_jobs = function () {
    return {
        getPanel: function(){
            if(Ext.getCmp('min_win_post_jobs_form')){
                Ext.getCmp('win_post_jobs_form').show();
                Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_post_jobs_form'));
                Ext.getCmp('task_bar').doLayout();
            }

            try{Ext.getCmp('win_post_jobs_form').close();}catch(err){}
            var store = new Ext.data.ArrayStore({
                fields: ['value','display'],
                data : [
                    ['Anywhere.','Anywhere.'],
                    ['Butuan','Butuan'],
                    ['Cagayan','Cagayan De Oro'],
                    ['Dapitan','Dapitan'],
                    ['Davao','Davao'],
                    ['General','General'],
                    ['Gingoog','Gingoog'],
                    ['Iligan','Iligan'],
                    ['Malaybalay','Malaybalay'],
                    ['Marawi','Marawi'],
                    ['Ozamis','Ozamis'],
                    ['Pagadian','Pagadian']
                ]
            });

           
            var ds_category = proj_tools.ds_combo('_ajax/execute_category.php','getCategory','Cid','category_id','category_name');
            var category_combo = proj_tools.getCombo(ds_category,'Category','category_list','category_id','category_name',150,150);
            var ds_company = proj_tools.ds_combo('_ajax/execute_company.php','getEmployer','Eid','employer_id','company_name');
            var company_combo = proj_tools.getCombo(ds_company,'Employer','employer_list','employer_id','company_name',150,150);
            var form_post_jobs = new Ext.FormPanel({
                labelWidth: 100,
                bodyStyle:'padding:10px 5px 10px 5px;',
                frame:true,
                title: 'Posted a Jobs',
                bodyStyle:'background-color:#fff;padding:5px 5px 0',
                width: 650,
                autoScroll:true,
                height: 1800,
                items: [  
                 company_combo,
                    {

                    xtype: 'radiogroup',
                    fieldLabel: 'Job Type',
                    defaultType: 'radio',
                    items: [
                        {boxLabel: 'Fulltime', name: 'jobtype',id: 'jobtype_1', inputValue: 'fulltime', checked: true},
                        {boxLabel: 'Parttime', name: 'jobtype',id: 'jobtype_2', inputValue: 'parttime'},
                        {boxLabel: 'Contract', name: 'jobtype',id: 'jobtype_3', inputValue: 'contract'}
                    ]

                },   {
                    layout:'column',
                    items:[{
                        columnWidth:2.5,
                        layout: 'form',
                        items: [{
                            xtype:'textfield',
                            fieldLabel: 'Position',
                            name: 'position',
                            id: 'position_id',
                            anchor:'38%'
                        },  proj_tools.getSimpleCombo(store,'Location','location','display','value')
                        ]
                    }


                    ]
                },{
                    xtype:'htmleditor',
                    id:'job_content',
                    fieldLabel:'Job Description',
                    height:200,
                    anchor:'98%'
                } ,
                    category_combo
                    ,{
                        layout:'column',
                        items:[{
                            columnWidth:1.5,
                            layout: 'form',

                            items: [{
                                xtype:'textfield',

                                fieldLabel: 'Experience from',
                                name: 'experience',
                                id: 'experience_id',
                                anchor:'60%'
                            }]
                        }  ]
                    }, {
                        xtype:'hidden',
                        id:"employer_id",
                        name:"employer_id"
                    },{
                        xtype:'hidden',
                        id:"job_post_id",
                        name:"job_post_id"
                    }
                ],
                buttons: [{
                    text: 'Save',
                    handler: function () {
                        var employer_id=Ext.getCmp('employer_list_combo_id').getValue();
                        var categor_id=Ext.getCmp('category_list_combo_id').getValue();
                        form_post_jobs.getForm().submit({
                            url: '_ajax/execute_posted_jobs.php',
                            method: 'post',
                            success: function () {
                                Ext.MessageBox.alert('Notice', 'Successfully Saved');
                                post_jobs.grid.getStore().load();
                                 Ext.getCmp('win_post_jobs_form').hide();
                            },
                            failure: function () {
                                Ext.MessageBox.alert('Notice', 'Opps Some fields are empty or Unable to save file');
                            },
                            params: {action: "add_post_jobs",employerid: employer_id,categorid: categor_id}
                        });
                    }
                },{
                    text: 'Cancel',
                    handler: function () {
                          try{post_jobs.grid.getStore().load();}catch(err){}
                        
                          Ext.getCmp('win_post_jobs_form').hide();
                    }
                }]
            });

            post_jobs.form_post_jobs = form_post_jobs;


            proj_tools.new_win('win_post_jobs_form',550,450,form_post_jobs,'Job Posted Form');
        },
        view: function(){

            post_jobs.getPanel();
        }
    }
}();