Ext.namespace("doc_space");

var proj_tools = function () {
	return {
		new_win: function(wid,w,h, witem, wtitle){
		var test_id = wid;
		
		var wid = new Ext.Window({
				id: wid,
				layout:'fit',
				title: wtitle,
				shim:true,
				constrain:true,
				renderTo: 'south_doc',
				width: w,
				height: h,
				closable: true,
				resizable: true,
				plain: true,
				border: false,
				tools:[
					{
					id:'minimize',
					handler: function(){
							Ext.getCmp(test_id).hide();
							Ext.getCmp('task_bar').getTopToolbar().add(new Ext.Toolbar.Button({
								text: '<span style="color: #000;font-weight:bold;">'+wtitle+'</span>',
								id:'min_'+test_id,
								icon: "images/icons/application_add.png",
								iconCls: "blist",
								handler: function(){
									Ext.getCmp(test_id).show();
									Ext.getCmp('task_bar').getTopToolbar().remove(this);
									}
							})); 
							Ext.getCmp('task_bar').doLayout();
						}
					}
				],
				items: [witem]
			});
			
		wid.show();
		proj_tools.wid = wid;
		
		
		},
		new_win_grid: function(wid,w,h, witem, wtitle){
		var test_id = wid;
		
		var wid = new Ext.Window({
				id: wid,
				layout:'border',
				title: wtitle,
				shim:true,
				constrain:true,
				renderTo: 'south_doc',
				width: w,
				height: h,
				closable: true,
				resizable: false,
				plain: true,
				border: false,
				maximizable: true,
				tools:[
					{
					id:'minimize',
					handler: function(){
							Ext.getCmp(test_id).hide();
							Ext.getCmp('task_bar').getTopToolbar().add(new Ext.Toolbar.Button({
								text: '<span style="color: #000;font-weight:bold;">'+wtitle+'</span>',
								id:'min_'+test_id,
								icon: "images/icons/application_add.png",
								iconCls: "blist",
								handler: function(){
									Ext.getCmp(test_id).show();
									Ext.getCmp('task_bar').getTopToolbar().remove(this);
									}
							})); 
							Ext.getCmp('task_bar').doLayout();
						}
					}
				],
				items: [witem]
			});
			
		wid.show();
		proj_tools.wid = wid;
		
		
		},
		forPrintView: function(urll,pid,par){
			var panel = new Ext.Panel({
					width:'100%',
					layout:'fit',
					id: pid+'_panel',
					autoLoad:{url:urll, params: "id="+par+""},
					autoScroll: true
					});
			return panel;

		},
		ds_combo: function(surl,act,rid,vVal,fVal){
		
		var ds = new Ext.data.Store({
				autoLoad: false,
				proxy: new Ext.data.HttpProxy({			
					url: surl+"?action="+act,
					method: "GET"
				}),
				reader: new Ext.data.JsonReader({
								id: rid
								}, [ vVal, fVal]
							),
				remoteSort: false
			});
		
				
		return ds;
				
		},
		getCombo: function (ds,fname,name,vVal,fVal,lw,w){
		
		var Combo = new Ext.form.ComboBox({
				align: 'left',
				fieldLabel: fname,
				name: name + "_combo_name",
				id: name + "_combo_id",
				store: ds,
				valueField: vVal,
				displayField: fVal,
				typeAhead: true,
				mode: "remote",				
				listWidth: lw,
				width: w,
				triggerAction: 'all',
				selectOnFocus: true,	
				emptyText: "Please Select"
			});
		return Combo;
		},
		getComboFunc: function (ds,fname,name,vVal,fVal,lw,w,func){
		
		var Combo = new Ext.form.ComboBox({
				align: 'left',
				fieldLabel: fname,
				name: name + "_combo_name",
				id: name + "_combo_id",
				store: ds,
				valueField: vVal,
				displayField: fVal,
				typeAhead: true,
				mode: "remote",				
				listWidth: lw,
				width: w,
				triggerAction: 'all',
				selectOnFocus: true,	
				emptyText: "Please Select",
				listeners: {
					'select': func
				}
			});
		return Combo;
		},
		ds_combo_product_destination: function(des){
		
		var ds = new Ext.data.Store({
				autoLoad: false,
				proxy: new Ext.data.HttpProxy({			
					url: "_ajax/execute_products.php?action=getProducts2&destination_id="+des,
					method: "GET"
				}),
				reader: new Ext.data.JsonReader({
								id: 'SearchProductsid'
								}, [ 'product_code', 'product_name']
							),
				remoteSort: false
			});
		
				
		return ds;
				
		},
		getComboDestination: function (ds,fname,name,vVal,fVal,lw,w){
		
		var Combo = new Ext.form.ComboBox({
				align: 'left',
				fieldLabel: fname,
				name: name + "_combo_name",
				id: name + "_combo_id",
				store: ds,
				valueField: vVal,
				displayField: fVal,
				typeAhead: true,
				mode: "remote",				
				listWidth: lw,
				width: w,
				triggerAction: 'all',
				selectOnFocus: true,	
				emptyText: "Please Select",
				listeners: {
					'select': function(combo,value){
						//alert(combo.getValue());
						var modelCmp = Ext.getCmp("products_search_search_combo_id");
						modelCmp.setValue('');
						modelCmp.setDisabled(false);
						modelCmp.bindStore( proj_tools.ds_combo_product_destination(combo.getValue())); 
					   
					}
				}
			});
		return Combo;
		},
		getSimpleCombo: function (ds,fname,name,fVal,vVal,lw,w){
		
		var Combo = new Ext.form.ComboBox({
			fieldLabel: fname,
			name: name + "_combo_name",
			id: name + "_combo_id",
			store: ds,
			displayField:fVal,
			valueField:vVal,
			typeAhead: true,
			listWidth: lw,
			width: w,
			mode: 'local',
			forceSelection: true,
			triggerAction: 'all',
			emptyText:'Please Select',
			selectOnFocus:true
		});
		return Combo;
		},
		getSimpleCombo2: function (ds,fname,name,fVal,vVal,w,v){
		
		var Combo = new Ext.form.ComboBox({
			fieldLabel: fname,
			name: name + "_combo_name",
			id: name + "_combo_id",
			store: ds,
			width:w,
			displayField:fVal,
			valueField:vVal,
			typeAhead: true,
			mode: 'local',
			forceSelection: true,
			triggerAction: 'all',
			selectOnFocus:true,
			value: v
		});
		return Combo;
		},
		getSimpleCombo3: function (ds,fname,name,fVal,vVal,w,v,func){
		
		var Combo = new Ext.form.ComboBox({
			fieldLabel: fname,
			name: name + "_combo_name",
			id: name + "_combo_id",
			store: ds,
			width:w,
			displayField:fVal,
			valueField:vVal,
			typeAhead: true,
			mode: 'local',
			forceSelection: true,
			triggerAction: 'all',
			selectOnFocus:true,
			value: v,
			listeners: {
				'select': func
			}
		});
		return Combo;
		},
		getCountryCombo: function (Cname){
		var store = new Ext.data.ArrayStore({
			fields: ['id','country'],
			data : proj_tools.getGeneratedCountry()
		});
		var Combo = new Ext.form.ComboBox({
			fieldLabel: 'Country',
			name: "country_"+Cname+"_combo_name",
			id: "country_"+Cname+"_combo_id",
			store: store,
			displayField:'country',
			valueField:'id',
			typeAhead: true,
			mode: 'local',
			forceSelection: true,
			triggerAction: 'all',
			emptyText:'Select a country...',
			selectOnFocus:true,
			listeners: {
				'select': function(combo,value){
					//alert(combo.getValue());
					var modelCmp = Ext.getCmp("state_"+Cname+"_combo_id");
                    modelCmp.setValue('');
                    modelCmp.setDisabled(false);
                    modelCmp.bindStore( proj_tools.getGeneratedState(combo.getValue()+1)); 
                   
				}
			}
		});
		return Combo;
		},
		getGeneratedCountry: function (){
			var country = [];
			for (var i=0; i<country_arr.length; i++) {
				country.push([i,country_arr[i]]); 
			}
		return country;
		},
		getStateCombo: function (Cname){
		
		var Combo = new Ext.form.ComboBox({
			fieldLabel: 'State',
			name: "state_"+Cname+"_combo_name",
			id: "state_"+Cname+"_combo_id",
			store: proj_tools.getGeneratedState(0),
			displayField:'state',
			valueField:'id',
			typeAhead: true,
			disabled: true,
			mode: 'local',
			forceSelection: true,
			triggerAction: 'all',
			emptyText:'Select a state...',
			selectOnFocus:true
		});
		return Combo;
		},
		getGeneratedState: function (state_index){
			var state = [];
			var state_arr = s_a[state_index].split("|");
			for (var i=0; i<state_arr.length; i++) {
				state.push([i,state_arr[i]]); 
			}
			var store = new Ext.data.ArrayStore({
				fields: ['id','state'],
				data : state
			});
			
		return store;
		},
		
		printing: function(divToPrint){
		
			try {
			var ifrm = document.getElementById('ifrmPrint');
			var content = document.getElementById(divToPrint).innerHTML;

			var printDoc = (ifrm.contentWindow || ifrm.contentDocument);
			if (printDoc.document) {
				printDoc = printDoc.document;
			}

			printDoc.write("<html><head><title>title</title>");
			//printDoc.write("<link rel='stylesheet' type='text/css' href='templates/css/print.css' media='print' />");
			printDoc.write("</head><body onload='this.focus(); this.print();'>");
			printDoc.write(content + "</body></html>");
			printDoc.close();
			
			}

			catch(e) {
				self.print();
			}
		
		},
		insertArray: function(arr,cont,index){
		
			
			arr.splice(index,0,cont);
			return arr;
		},
		removedArray: function(arr,index){
		
			
			arr.splice(index, 1);
			return arr;
		},
		generateCode: function(v){
			var temstr = v.replace(" ", "");
			var res = temstr.substring(0,5);
			return res.toUpperCase();
		},
		calGross: function(mark,gross){
			var tempAmt = (gross*mark);
			return parseFloat(gross-tempAmt)
		
		}
	}
}();