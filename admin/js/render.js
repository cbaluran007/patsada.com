Ext.namespace("app_space");

var render = function () {
	return {
		YesNo: function(val){
		
			if (val == 'Y' || val == 1) {
				return '<span style="color:green">Yes</span>';
			} else {
				return '<span style="color:red">No</span>';
			}
		
		},
		image: function(val){
		
			return '<img src=\''+val+'\' style="width:100px;height:100px; border: 1px solid #ccc; padding: 3px;">';
			
		
		},
		date_format: function (val) {
			
			if(val == '' || val == null || val == '0000-00-00'){
				return 'No Date';
			}else{
				var myMonth = ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.' ];

					if(val.substring(5,6)<1){
						var num_month = val.substring(6,7);
					}else{
						var num_month = val.substring(5,7);
					}
				var year = val.substring(0,4);
				var day = val.substring(8,10);	
				
				return myMonth[num_month - 1]+' '+day+', '+ year;
			}
		},
		MaleOrFemale: function(val){
		
			if (val == 'M') {
				return 'Male';
			} else {
				return 'Female';
			}
		
		},
		buttonViewBooking: function(val){
		
			var str = '<a href="#" class="searchButton2" onclick="booking.getPanel2('+val+',true);">View</a> ';		
			return str;
		},
		Civil: function(val){
		
			if (val == 'S') {
				return 'Single';
			}  
			if(val == 'M'){
				return 'Married';
			}
			if(val == 'W'){
				return 'Widow';
			}
		
		},
		changeRender: function(v, params, record){
			return "<a href=\"#\" onClick='user_man.change("+v+",\""+record.data.name+"\",\""+record.data.logid+"\",\""+record.data.logpass+"\")'><img src='templates/images/icons/key_add.png'></a>";
		}
	}
}();