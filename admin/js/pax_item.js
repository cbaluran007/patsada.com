Ext.namespace("app_space");

var paxitem = function () {
	return {
		getPanel: function(booking_id,consultant_id){
			if(Ext.getCmp('min_win_pax_item_grid')){
			Ext.getCmp('win_pax_item_grid').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_pax_item_grid'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_pax_item_grid').close();}catch(err){}
			
			var ds_pax_details = proj_tools.ds_combo('_ajax/execute_pax_item.php','getPaxdetails','Paxid','pax_name','id_pax_details');
			var ds_pax_details_combo = proj_tools.getSimpleCombo(ds_pax_details,'','pax_item_search','pax_name','id_pax_details',150,150);
			ds_pax_details_combo.setValue('');
			ds_pax_details_combo.getStore().load({params: {account_id: consultant_id}}); 
			
			var store = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_pax_item.php?action=view&booking_id="+booking_id,
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewPaxItem",
		            id: "id_pax",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "id_pax", mapping: "id_pax"},
            			{name: "first_name", mapping: "first_name"},
						{name: "last_name", mapping: "last_name"},
						{name: "age", mapping: "age"},
						{name: "date_of_birth", mapping: "date_of_birth"},
						{name: "title", mapping: "title"},
						{name: "is_active", mapping: "is_active"},
						{name: "is_adult", mapping: "is_adult"},
						{name: "is_child", mapping: "is_child"},
						{name: "is_infant", mapping: "is_infant"},
						{name: "email", mapping: "email"},
						{name: "updated_by", mapping: "updated_by"},
						{name: "updated_datetime", mapping: "updated_datetime"},
						{name: "created_by", mapping: "created_by"},
						{name: "created_datetime", mapping: "created_datetime"}
						
			
        			]
					}),
					sortInfo:{field: 'created_datetime', direction: "DESC"}
				});
			
			 var guestView = new Ext.DataView({
				cls: 'guest-view',
				autoScroll:true,
				tpl: '<div class="containerSourse"><tpl for=".">' +
						'<div class="guest-source"><table><tbody>' +
							'<tr><td class="guest-label">Full Name</td><td class="guest-name">{title} {first_name} {last_name}</td></tr>' +
							'<tr><td class="guest-label">Email</td><td class="guest-name">{email}</td></tr>' +
							'<tr><td class="guest-label">Age</td><td class="guest-name">{age}</td></tr>' +
							'<tr><td class="guest-label">Guest Type</td><td class="guest-name">' +
							'<tpl if="is_adult==1">'+
							'Adult'+
							'</tpl>'+
							'<tpl if="is_child==1">'+
							'Child'+
							'</tpl>'+
							'<tpl if="is_infant==1">'+
							'Infant'+
							'</tpl>'+
							'</td></tr>'+
						'</tbody></table></div>' +
					 '</tpl></div>',
				itemSelector: 'div.guest-source',
				store: store,
				id:'booking-pax-grid',
				listeners: {
					render: function (v) {
						v.dragZone = new Ext.dd.DragZone(v.getEl(), {

					//      On receipt of a mousedown event, see if it is within a draggable element.
					//      Return a drag data object if so. The data object can contain arbitrary application
					//      data, but it should also contain a DOM element in the ddel property to provide
					//      a proxy to drag.
							getDragData: function(e) {
								var sourceEl = e.getTarget(v.itemSelector, 10);
								if (sourceEl) {
									d = sourceEl.cloneNode(true);
									d.id = Ext.id();
									return v.dragData = {
										sourceEl: sourceEl,
										repairXY: Ext.fly(sourceEl).getXY(),
										ddel: d,
										guestData: v.getRecord(sourceEl).data
									}
								}
							},

					//      Provide coordinates for the proxy to slide back to on failed drag.
					//      This is the original XY coordinates of the draggable element.
							getRepairXY: function() {
								return this.dragData.repairXY;
							}
						});
					}
				}
			});

			// Panel for the west
			var DataGuestPanel = new Ext.Panel({
				id: 'DataGuestPanel',
				region: 'west',
				split: true,
				width: '65%',
				collapsible: true,
				autoScroll:true,
				margins:'3 0 3 3',
				cmargins:'3 3 3 3',
				items: [guestView],
				layout: 'fit',
				height: 500,
				tbar: [
					'		Pax : ', 
					' ',
					ds_pax_details_combo,
					{
						text: 'Add Guest\/Pax',
						icon: "images\/icons\/user_add.png",
						handler: function () {
							paxitem.addGuest2(booking_id,consultant_id);
						}
					},
					{
						text: 'Clear All Guest\/Pax',
						icon: "images\/icons\/group_delete.png",
						handler: function () {
							paxitem.clearGuest(booking_id);
							
						}
					},
					{
						text: 'Load All Guest\/Pax',
						icon: "images\/icons\/group_go.png",
						handler: function () {
							paxitem.loadGuest(booking_id,consultant_id);
						}
					},
					{
						text: 'Add New Guest\/Pax',
						icon: "images\/icons\/application_add.png",
						handler: function () {
							paxitem.addGuest(booking_id,consultant_id);
						}
					}
					
				]
			});
			
			
			
			
			var storeItems = new Ext.data.GroupingStore({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_pax_item.php?action=viewItems&booking_id="+booking_id,
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewPaxItems",
		            id: "id_item_sub",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "id_item_sub", mapping: "id_item_sub"},
            			{name: "product_name", mapping: "product_name"},
						{name: "id_item", mapping: "id_item"},
						{name: "item_name", mapping: "item_name"},
						{name: "num_pax", mapping: "num_pax"},
						{name: "num_adult", mapping: "num_adult"},
						{name: "num_child", mapping: "num_child"},
						{name: "num_infant", mapping: "num_infant"},
						{name: "items_pax", mapping: "items_pax"},
						{name: "count_pax", mapping: "count_pax"},
						{name: "count_lead", mapping: "count_lead"}
						
			
        			]
					}),
					sortInfo:{field: 'id_item_sub', direction: "ASC"},
					groupField:'product_name'
				});
			
			var gridItems = new Ext.grid.GridPanel({
				store: storeItems,
				id:'booking-paxItem-grid',
				cm: new Ext.grid.ColumnModel([
					{header: "ID", width: 30,  sortable: true, locked:true, dataIndex: "id_item_sub"},
					
					{id: "id-grid-details-item-sub",header: "Item Name", width: 90,  sortable: true, locked:true,align:'left', dataIndex: "item_name"},
					{header: "Max # Guest\/Pax", width: 100,  sortable: true, locked:true,align:'left', dataIndex: "num_pax"},
					{header: "Max # Adult", width: 100,  sortable: true, locked:true,align:'left', dataIndex: "num_adult"},
					{header: "Max # Child", width: 100,  sortable: true, locked:true,align:'left', dataIndex: "num_child"},
					{header: "Max # Infant", width: 100,  sortable: true, locked:true,align:'left', dataIndex: "num_infant"},
					{header: "Product Name", width: 150,  sortable: true, locked:true,align:'left', dataIndex: "product_name"}
					]),
				loadMask: true,	
				autoExpandColumn: "id-grid-details-item-sub",
				border: false,
				
				  view: new Ext.grid.GroupingView({
					forceFit:true,
					groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Sub Items" : "Sub Item"]})',
					tpl: new Ext.XTemplate('<tpl for=".">'+
					'<tpl if="count_pax == num_pax">'+
						'<tpl if="count_lead == 0">'+
							'<div class="guest-target" style="background-color:#D2691E;">&nbsp;'+
						'</tpl>'+
						'<tpl if="count_lead != 0">'+
							'<div class="guest-target" style="background-color:#32CD32;">&nbsp;'+
						'</tpl>'+
					'</tpl>'+
					'<tpl if="count_pax != num_pax">'+
					'<div class="guest-target" style="background-color:#DC143C;">&nbsp;'+
					'</tpl>'+
					'<tpl for="items_pax">'+ 
						'<span class="contGuest">'+
						'{title} {first_name} {last_name} '+
						'<tpl if="is_lead==1">'+
						'<span style="padding:1px 3px;background-color:red;color:#fff;font-weight:bold;" title="Lead Guest">L</span> '+
						'</tpl>'+
						'<img src="images\/icons\/delete.png" class="buttonGuestDel" title="Removed Guest" onClick="paxitem.removedGuest({id_item_pax});">'+
						'<\/span> '+
					'</tpl>'+
					'</div></tpl>'),
					enableRowBody: true,
					getRowClass: function(rec, idx, p, store) {
						p.body = this.tpl.apply(rec.data);
					}
				}),

				listeners:  {
					render: function (g) {
						g.dropZone = new Ext.dd.DropZone(g.getView().scroller, {

					//      If the mouse is over a target node, return that node. This is
					//      provided as the "target" parameter in all "onNodeXXXX" node event handling functions
							getTargetFromEvent: function(e) {
								return e.getTarget('.guest-target');
							},

					//      On entry into a target node, highlight that node.
							onNodeEnter : function(target, dd, e, data){ 
								Ext.fly(target).addClass('guest-target-hover');
							},

					//      On exit from a target node, unhighlight that node.
							onNodeOut : function(target, dd, e, data){ 
								Ext.fly(target).removeClass('guest-target-hover');
							},

					//      While over a target node, return the default drop allowed class which
					//      places a "tick" icon into the drag proxy.
							onNodeOver : function(target, dd, e, data){ 
								return Ext.dd.DropZone.prototype.dropAllowed;
							},

					//      On node drop, we can interrogate the target node to find the underlying
					//      application object that is the real target of the dragged data.
					//      In this case, it is a Record in the GridPanel's Store.
					//      We can use the data set up by the DragZone's getDragData method to read
					//      any data we decided to attach.
							onNodeDrop : function(target, dd, e, data){
								var rowIndex = g.getView().findRowIndex(target);
								var h = g.getStore().getAt(rowIndex);
								var maxPax =parseInt(h.data.num_pax);
								var maxAdult =parseInt(h.data.num_adult);
								var maxChild =parseInt(h.data.num_child);
								var maxInfant =parseInt(h.data.num_infant);
								
								var targetEl = Ext.get(target);
								
								
								Ext.Ajax.request({
									url: "_ajax/execute_pax_item.php",
									method: "POST",
									success: function (result, request) {
									
										var jsonData = Ext.util.JSON.decode(result.responseText);
										var stats = jsonData.status;
										var notification = jsonData.notification;
										if(stats){
							
											paxitem.gridItems.getStore().load();
										}else{
											Ext.MessageBox.alert('Failure', notification);
										}
										
										
									},
									params: {action: "addGuestToItem", pax_id: data.guestData.id_pax,item_sub_id:h.data.id_item_sub}
									
									
								});
								
								//var str ="";
								//alert(targetEl.dom.innerHTML);
								
								//str += "<span class=\"contGuest\">"+data.guestData.title+' '+data.guestData.first_name+' '+data.guestData.last_name+"<img src=\"images\/icons\/delete.png\" class=\"buttonGuestDel\"><\/span> ";
								//targetEl.update(str+targetEl.dom.innerHTML);
								
								return true;
							}
						});
					}
				},
				region: 'center',
				split: true,
				width: "50%",
				collapsible: false,
				autoScroll:true,
				margins:'3 0 3 3',
				cmargins:'3 3 3 3',
				//border: true,
				layout: 'fit',
				height: 500,
				tbar: [
					'->',
					{
						text: 'Auto Assign Guest\/Pax',
						icon: "images\/icons\/folder_user.png",
						handler: function () {
							paxitem.autoAssignGuest(booking_id,consultant_id);
						}
					},
					'-',
					{
						text: '<span style="color:red;font-weight:bold;">Finalize Guest\/Pax</span>',
						icon: "images\/icons\/group_gear.png",
						handler: function () {
							paxitem.allocateGuest(booking_id,consultant_id);
						}
					}
					
				]
			});
			
			paxitem.grid = guestView;
			//paxitem.grid.getStore().load();
			
			paxitem.gridItems = gridItems;
			//paxitem.gridItems.getStore().load();
			
			proj_tools.new_win_grid('win_pax_item_grid',1050,550,[DataGuestPanel,gridItems],'Guest\/Pax Allocation');
		
		
			Ext.getCmp('booking-paxItem-grid').getEl().mask('Loading...', 'x-mask-loading');
			paxitem.gridItems.getStore().load({scope: this,
			callback: function(records, operation, success) {
				if (success) {
					Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
				} else {
					Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
				}
			}});
			
			Ext.getCmp('booking-pax-grid').getEl().mask('Loading...', 'x-mask-loading');
			paxitem.grid.getStore().load({scope: this,
			callback: function(records, operation, success) {
				if (success) {
					Ext.getCmp('booking-pax-grid').getEl().mask().hide();
				} else {
					Ext.getCmp('booking-pax-grid').getEl().mask().hide();
				}
			}});
		
		},
		addGuest: function(booking_id,consultant_id){
		
		if(Ext.getCmp('min_win_create_guest_pax')){
		Ext.getCmp('win_create_guest_pax').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_guest_pax'));
		Ext.getCmp('task_bar').doLayout();
		}
		
		try{Ext.getCmp('win_create_guest_pax').close();}catch(err){}
		var store = new Ext.data.ArrayStore({
			fields: ['value','display'],
			data : [ ['Mr.','Mr.'], ['Mrs.','Mrs.'],['Mstr.','Mstr.'], ['Ms.','Ms.'],['Dr.','Dr.']]
		});
		var today = new Date();
			
			var fdate = today.format("d/m/Y");
		
		
		var form = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:true, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				proj_tools.getSimpleCombo(store,'Title','pax_guest_title','display','value'),
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'First Name',	name:'pax_firstname', id:'pax_firstname',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Last Name',	name:'pax_lastname', id:'pax_lastname',width: "95%" },
				{
					// Use the default, automatic layout to distribute the controls evenly
					// across a single row
					xtype: 'radiogroup',
					fieldLabel: '',
					width: "95%",
					items: [
						{boxLabel: 'By Age', name: 'byBOD', id: 'byBOD1',checked: true,inputValue:0},
						{boxLabel: 'Age By Birth Date', name: 'byBOD',id: 'byBOD2',inputValue:1}
					],
					listeners: {
						change: function(radiogroup, radio){
							var visible = radio.inputValue;
							//alert(visible);
							
							if(visible == 1){
								Ext.getCmp("pgstartdt").setDisabled(false);
								Ext.getCmp("age").setDisabled(true);
							}else{
								Ext.getCmp("pgstartdt").setDisabled(true);
								Ext.getCmp("age").setDisabled(false);
							}
								//form = radiogroup.findParentByType('form'),
								//combo = form.getForm().findField('combo');
							//combo.setVisible(visible);
							//combo.setDisabled(!visible);              
						}
					}
				},
				{ xtype: 'numberfield',allowBlank:true,  fieldLabel: 'Age',	name:'age', id:'age',width: "10%" },
				{
						fieldLabel: 'Date Of Birth',
						xtype: 'datefield',
						name: 'pgstartdt',
						id: 'pgstartdt',
						format: 'd/m/Y',
						width:'100%',
						value:fdate,
						//allowBlank:false,
						disabled: true
						
				},
				{
					// Use the default, automatic layout to distribute the controls evenly
					// across a single row
					xtype: 'radiogroup',
					fieldLabel: 'Guest\/Pax Type',
					width: "95%",
					items: [
						{boxLabel: 'Adult', name: 'paxType', id: 'paxType1',checked: true,value:0},
						{boxLabel: 'Child', name: 'paxType',id: 'paxType2',value:1},
						{boxLabel: 'Infant', name: 'paxType',id: 'paxType3',value:2}
					]
				},
				{ xtype: 'textfield',allowBlank:true,  fieldLabel: 'Email',	name:'pemail', id:'pemail',width: "95%",vtype:'email' },
				{id: "pax-id", name: "pax-name", xtype: "hidden", value: 0},
				{id: "booking_id", name: "booking_id", xtype: "hidden", value: booking_id},
				{id: "consultant_id", name: "consultant_id", xtype: "hidden", value: consultant_id}
       
				],
				buttons: [
						{text: "Save", formBind: false, icon: "images/icons/disk.png", handler: paxitem.submitUser},
						{text: "Reset", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: paxitem.reset}
						
					]
			});
		
		
		
		
		
		
			
			//validator.runThis();
			proj_tools.new_win('win_create_guest_pax',400,300,form,'Create New Guest\/Pax');
			paxitem.mainForm = form;	
						
		
		},
		reset: function(){
			
		paxitem.mainForm.getForm().reset();	
			
		
		},
		submitUser: function(){
			var parr = {};
			parr["action"] = "submitPax";
			parr["id"] = Ext.getCmp("pax-id").getValue();
			parr["paxType1"] = Ext.getCmp("paxType1").getValue();
			parr["paxType2"] = Ext.getCmp("paxType2").getValue();
			parr["paxType3"] = Ext.getCmp("paxType3").getValue();
			paxitem.mainForm.getForm().submit({
						
							url: '_ajax/execute_pax_item.php',
							method: 'post',
							success: function () {
								Ext.getCmp('win_create_guest_pax').close();
								paxitem.grid.getStore().load();
								paxitem.gridItems.getStore().load();
								Ext.MessageBox.alert('Sucess', 'Guest\/Pax successfully saved.');
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to saved Guest\/Pax. Please try again.');
							},
							
							params: parr
							
						});
		
		},
		clearGuest: function(booking_id){
			Ext.Ajax.request({
				url: "_ajax/execute_pax_item.php",
				method: "POST",
				success: function () {
				
				
					Ext.getCmp('booking-pax-grid').getEl().mask('Loading...', 'x-mask-loading');
					paxitem.grid.getStore().load({scope: this,
					callback: function(records, operation, success) {
						if (success) {
							Ext.getCmp('booking-pax-grid').getEl().mask().hide();
						} else {
							Ext.getCmp('booking-pax-grid').getEl().mask().hide();
						}
					}});
				
					
				},
				params: {action: "clearGuest", booking_id: booking_id}
				
			});
		
		},
		removedGuest: function(pax_id){
			Ext.Ajax.request({
				url: "_ajax/execute_pax_item.php",
				method: "POST",
				success: function () {
					Ext.getCmp('booking-paxItem-grid').getEl().mask('Loading...', 'x-mask-loading');
					paxitem.gridItems.getStore().load({scope: this,
					callback: function(records, operation, success) {
						if (success) {
							Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
						} else {
							Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
						}
					}});
					
					
					
				},
				params: {action: "removedGuest", paxid: pax_id}
				
			});
		
		},
		leadGuest: function(pax_id,item_subId){
			Ext.Ajax.request({
				url: "_ajax/execute_pax_item.php",
				method: "POST",
				success: function () {
				
					Ext.getCmp('booking-paxItem-grid').getEl().mask('Loading...', 'x-mask-loading');
					paxitem.gridItems.getStore().load({scope: this,
					callback: function(records, operation, success) {
						if (success) {
							Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
						} else {
							Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
						}
					}});
					
					
					
				},
				params: {action: "leadGuest", paxid: pax_id,itemsubId:item_subId}
				
			});
		
		},
		loadGuest: function(bookingid,consultantid){
			Ext.Ajax.request({
				url: "_ajax/execute_pax_item.php",
				method: "POST",
				success: function () {
				
					Ext.getCmp('booking-paxItem-grid').getEl().mask('Loading...', 'x-mask-loading');
					paxitem.gridItems.getStore().load({scope: this,
					callback: function(records, operation, success) {
						if (success) {
							Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
						} else {
							Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
						}
					}});
					
					Ext.getCmp('booking-pax-grid').getEl().mask('Loading...', 'x-mask-loading');
					paxitem.grid.getStore().load({scope: this,
					callback: function(records, operation, success) {
						if (success) {
							Ext.getCmp('booking-pax-grid').getEl().mask().hide();
						} else {
							Ext.getCmp('booking-pax-grid').getEl().mask().hide();
						}
					}});
					
				},
				params: {action: "loadGuest", booking_id: bookingid, consultant_id: consultantid}
				
			});
		
		},
		allocateGuest: function(bookingid,consultantid){
			 Ext.MessageBox.show({
				   msg: 'Allocating guest to the products ...',
				   progressText: 'Allocating guest to the products ...',
				   width:300,
				   wait:true,
				   waitConfig: {interval:200}
				   
			   });
			Ext.Ajax.request({
				url: "_ajax/execute_pax_item.php",
				method: "POST",
				success: function (result, request) {
									
					var jsonData = Ext.util.JSON.decode(result.responseText);
					var stats = jsonData.status;
					var notification = jsonData.notification;
					if(stats){
							
							
						Ext.getCmp('win_pax_item_grid').close();
											
						
						booking.getPanel2(bookingid,true);
						Ext.MessageBox.hide();
						Ext.MessageBox.alert('Status', notification);
					}else{
						Ext.getCmp('booking-paxItem-grid').getEl().mask('Loading...', 'x-mask-loading');
						paxitem.gridItems.getStore().load({scope: this,
						callback: function(records, operation, success) {
							if (success) {
								Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
							} else {
								Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
								Ext.MessageBox.hide();
								Ext.MessageBox.alert('Status', notification);
							}
						}});
						
						Ext.MessageBox.alert('Status', notification);
						Ext.MessageBox.hide();
						Ext.MessageBox.alert('Status', notification);
					}
										
										
				},
				params: {action: "allocateGuest", booking_id: bookingid, consultant_id: consultantid}
				
			});
		
		},
		autoAssignGuest: function(bookingid,consultantid){
			Ext.Ajax.request({
				url: "_ajax/execute_pax_item.php",
				method: "POST",
				success: function () {
					Ext.getCmp('booking-paxItem-grid').getEl().mask('Loading...', 'x-mask-loading');
					paxitem.gridItems.getStore().load({scope: this,
					callback: function(records, operation, success) {
						if (success) {
							Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
						} else {
							Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
						}
					}});
					
					
				},
				params: {action: "autoAssignGuest", booking_id: bookingid, consultant_id: consultantid}
				
			});
		
		},
		addGuest2: function(bookingid,consultantid){
			
			Ext.Ajax.request({
				url: "_ajax/execute_pax_item.php",
				method: "POST",
				success: function () {
					Ext.getCmp('booking-paxItem-grid').getEl().mask('Loading...', 'x-mask-loading');
					paxitem.gridItems.getStore().load({scope: this,
					callback: function(records, operation, success) {
						if (success) {
							Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
						} else {
							Ext.getCmp('booking-paxItem-grid').getEl().mask().hide();
						}
					}});
					
					Ext.getCmp('booking-pax-grid').getEl().mask('Loading...', 'x-mask-loading');
					paxitem.grid.getStore().load({scope: this,
					callback: function(records, operation, success) {
						if (success) {
							Ext.getCmp('booking-pax-grid').getEl().mask().hide();
						} else {
							Ext.getCmp('booking-pax-grid').getEl().mask().hide();
						}
					}});
				},
				params: {action: "addGuest", booking_id: bookingid, consultant_id: consultantid,pax_id: Ext.getCmp("pax_item_search_combo_id").getValue()}
				
			});
		
		}
	}
}();