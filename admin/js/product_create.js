Ext.namespace("app_space");

var product_create = function () {
	return {
		getPanel: function(type,serviceName){
		
		if(Ext.getCmp('min_win_create_accommodation_product')){
		Ext.getCmp('win_create_accommodation_product').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_accommodation_product'));
		Ext.getCmp('task_bar').doLayout();
		}
		
		try{Ext.getCmp('win_create_accommodation_product').close();}catch(err){}
		
		
		
			var maintabs = new Ext.TabPanel({
					activeTab: 0,
					width:"100%",
					border:false,
					frame: true,
					id:'product_tab_manager',
					items:[
						
						{
							title: 'Product Level',
							id: 'product_level_tab_id',
							frame: true,
							items: [product_create.product_level()],
							buttons: [
								{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: product_create.submitProductInfo}
								
							]
						}
					]
					
				});
				
			
			
			
			
			//validator.runThis();
			proj_tools.new_win('win_create_accommodation_product','75%',400,maintabs,'New Product');
						
			Ext.getCmp('product_tab_manager_cont').setActiveTab(5);
			Ext.getCmp('product_tab_manager_cont').setActiveTab(4);
			Ext.getCmp('product_tab_manager_cont').setActiveTab(3);	
			Ext.getCmp('product_tab_manager_cont').setActiveTab(2);
			Ext.getCmp('product_tab_manager_cont').setActiveTab(1);
			Ext.getCmp('product_tab_manager_cont').setActiveTab(0);
			
			var tabPanel = Ext.getCmp('product_tab_manager_cont');
			tabPanel.hideTabStripItem("ProductImagesTabID");
			tabPanel.hideTabStripItem("ProductSubproductsTabID");
		},
		product_level: function(){
			/*------------ Product Level ---------------*/
		var storeStar = new Ext.data.ArrayStore({
			fields: ['value','display'],
			data : [ ['1','1 Star'], ['2','2 Star'],['3','3 Star'],['4','4 Star'],['5','5 Star'],['6','6 Star'],['7','7 Star'],['8','8 Star'],['9','9 Star'],['10','10 Star']]
		});
		var ds_supplier = proj_tools.ds_combo('_ajax/execute_supplier.php','getSupplier','SupCid','id_supplier','supplier_name');
		var supplier_combo = proj_tools.getCombo(ds_supplier,'Supplier Name','supplier_create_product','id_supplier','supplier_name',150,150);
		
		var ds_service_product = proj_tools.ds_combo('_ajax/execute_service.php','getService','SCid','id_service','service_name');
		var service_combo_create_product = proj_tools.getCombo(ds_service_product,'Service Name','service_create_product_info','id_service','service_name',150,150);
		
		var storeStockType = new Ext.data.ArrayStore({
			fields: ['value','display'],
			data : [ ['1','Freesale'], ['2','Allotment'],['3','On Request(Over 24 Hours)'],['4','Live Inventory']]
		});	
		
		var storeRateType = new Ext.data.ArrayStore({
			fields: ['value','display'],
			data : [ 
			['1','Per Pax Room Per Block'], 
			['2','Per Pax Room Per Day/Night'],
			['3','Per Pax Item Per Block'],
			['4','Per Pax Item Per Day/Night'],
			['5','Per Room Per Block'],
			['6','Per Room Per Day/Night'],
			['7','Per Sliding Pax Per Block'],
			['8','Per Sliding Pax Per Day/Night']
			]
		});	
		
		var productInfo = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				id:'productInfo',
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Product Name',	name:'product_create_name', id:'product_create_name',width: "50%",
				enableKeyEvents: true,
				listeners: {
					 keyup: 
						 function (obj, e) 
						 {
							var code = proj_tools.generateCode(obj.getValue());
							Ext.getCmp('product_create_code').setValue(code);
						 }
				}

				},
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Product Code',	name:'product_create_code', id:'product_create_code',width: "20%" },
				supplier_combo,
				service_combo_create_product,
				proj_tools.getSimpleCombo(storeStockType,'Stock Type','stock_type','display','value'),
				proj_tools.getSimpleCombo(storeRateType,'Rate Type','rate_type','display','value'),
				proj_tools.getSimpleCombo(storeStar,'Star Rating','star_rating','display','value'),
				{xtype: 'checkbox', checked: true, fieldLabel: 'Is Active?',  name: 'is_product_active', id:'is_product_active'},
				{id: "product-create-id", name: "product-create-id", xtype: "hidden", value: 0}

				]
			});
		
		
		var store = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_products.php?action=viewPhotos",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewPhotos",
		            id: "id_viewPhotos_manager",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "id_product_photos", mapping: "id_product_photos"},
            			{name: "product_code", mapping: "product_code"},
						{name: "url", mapping: "url"}
			
        			]
					}),
					sortInfo:{field: 'url', direction: "DESC"}
				});
		
		var gridImageProduct = new Ext.grid.GridPanel({
				store: store,
				cm: new Ext.grid.ColumnModel({
						columns: [{
							id: 'cm_photo',
							header: 'Photo',
							dataIndex: 'url',
							renderer: render.image,
							width: 20
						}, 
						{
							header: 'Product Code',
							dataIndex: 'product_code',
							width: 50
						},
						{
							header: 'Name',
							id:'id-grid-details-photo-name',
							dataIndex: 'url',
							width: 95
						}]
					}),
				autoExpandColumn: "id-grid-details-photo-name",
				border: false,
				viewConfig: {
					forceFit:true
				},
				listeners:  {
					rowcontextmenu: product_create.rowImageContextMenu
				},
				width: '100%',
				//border: true,
				layout: 'fit',
				//plugins: expander,
				height: 275,
				tbar: [
					{
						text: 'Add Product Photos',
						icon: "images\/icons\/folder_user.png",
						handler: function () {
							product_create.formUploadImage();
						}
					}
				]
				
			});
		product_create.gridImageProduct = gridImageProduct;
		product_create.gridImageProduct.getStore().load();

		
		var storeSub = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_products.php?action=viewSubproduct",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewsubProduct",
		            id: "id_viewsubProduct_manager",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "sub_product_id", mapping: "sub_product_id"},
						{name: "sub_product_code", mapping: "sub_product_code"},
            			{name: "sub_product_name", mapping: "sub_product_name"},
						{name: "is_active", mapping: "is_active"},
						{name: "created_datetime", mapping: "created_datetime"},
						{name: "created_by", mapping: "created_by"},
						{name: "product_id", mapping: "product_id"}
        			]
					}),
					sortInfo:{field: 'created_datetime', direction: "DESC"}
				});
		
		var gridSubProduct = new Ext.grid.GridPanel({
				store: storeSub,
				cm: new Ext.grid.ColumnModel({
						columns: [{
							id: 'cm_subproduct_id',
							header: 'ID',
							dataIndex: 'sub_product_id',
							width: 20
						}, 
						{
							header: 'Code',
							dataIndex: 'sub_product_code',
							width: 20
						},
						{
							header: 'Name',
							id:'id-grid-details-subproduct-name',
							dataIndex: 'sub_product_name',
							width: 95
						},
						{header: "Active", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_active", renderer: render.YesNo},
						{header: "Created By", width:50,  sortable: true, locked:true,align:'left', dataIndex: "created_by"}
						]
					}),
				autoExpandColumn: "id-grid-details-subproduct-name",
				border: false,
				viewConfig: {
					forceFit:true
				},
				listeners:  {
					rowcontextmenu: product_create.rowContextMenuSub
				},
				width: '100%',
				//border: true,
				layout: 'fit',
				//plugins: expander,
				height: 275,
				tbar: [
					{
						text: 'Add Subproduct',
						icon: "images\/icons\/folder_user.png",
						handler: function () {
							sub_product.getPanel();
						}
					}
				]
				
			});
		product_create.gridSubProduct = gridSubProduct;
		product_create.gridSubProduct.getStore().load();
		
		var productBusiness = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				id: 'productBusiness',
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				proj_tools.getCountryCombo('product_create'),
				proj_tools.getStateCombo('product_create'),
				{ xtype: 'textfield',allowBlank:true,  fieldLabel: 'Address',	name:'addressProduct', id:'addressProduct',width: "50%" },
				{ xtype: 'textfield',allowBlank:true,  fieldLabel: 'Postal Code',	name:'codeProduct', id:'codeProduct',width: "50%" },
				{ xtype: 'textfield',allowBlank:true,  fieldLabel: 'Longitude',	name:'productlong', id:'productlong',width: "10%" },
				{ xtype: 'textfield',allowBlank:true,  fieldLabel: 'Latitude',	name:'productlat', id:'productlat',width: "10%" },
				{ xtype: 'textfield',allowBlank:true,  fieldLabel: 'URL',	name:'url_product', id:'url_product',width: "50%", vtype:'url' },
				{ xtype: 'textfield',allowBlank:true,  fieldLabel: 'Email',	name:'email_product', id:'email_product',width: "50%", vtype:'email' },
				{ xtype: 'textfield',allowBlank:true,  fieldLabel: 'Phone #',	name:'phone_num_product', id:'phone_num_product',width: "50%" },
				{ xtype: 'textfield',allowBlank:true,  fieldLabel: 'Fax #',	name:'fax_num_product', id:'fax_num_product',width: "50%" }
				

				]
			});
			
			
			//Location here
			var ds_destinationFrom = proj_tools.ds_combo('_ajax/execute_destination.php','getDestinationForSystem','productDesFromid','code','destination');
			var destination_combo_from = proj_tools.getCombo(ds_destinationFrom,'Location From','destination_create_product_from','code','destination',250,250);
			var ds_destinationTo = proj_tools.ds_combo('_ajax/execute_destination.php','getDestinationForSystem','productDesToid','code','destination');
			var destination_combo_to = proj_tools.getCombo(ds_destinationTo,'Location To','destination_create_product_to','code','destination',250,250);
			var ds_destination = proj_tools.ds_combo('_ajax/execute_destination.php','getDestinationForSystem','productDesid','code','destination');
			var destination_combo = proj_tools.getCombo(ds_destination,'Location','destination_create_product','code','destination',250,250);
			
			var productLocation = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				border:false, 
				id:'productLocation',
				defaultType:'textfield',
				monitorValid:true,
				items:[
				destination_combo_from,
				destination_combo_to,
				destination_combo

				]
			});
			
			//end location
			
			var productPanel =  new Ext.TabPanel({
					activeTab: 0,
					width:"100%",
					border:false,
					frame: true,
					id:'product_tab_manager_cont',
					items:[
						
						{
							title: 'Product ',
							id: 'ProductTabID',
							items: [productInfo]
						},
						
						{
							title: 'Location',
							id: 'ProductLocationTabID',
							items: [productLocation]
						},
						{
							title: 'Product Amenities',
							id: 'ProductAmmenitiesTabID',
							items: [{
									layout:'fit',
									height: '100%',
									items: {
										xtype:'htmleditor',
										enableFormat: false,
										height: '100%',
										enableLinks: false,
										enableFont: false,
										enableFontSize: false,
										enableColors: false,
										enableAlignments: false,
										value: '<b>Facilities:</b><br> &bull; Example Data 1 &bull; Example Data 2 &bull; Example Data 3 <br> <b>Room Facilities:</b><br> &bull; Example Data 1 &bull; Example Data 2',
										id:'product_amenities',
										name:'product_amenities'
									}
								}]
						},
						{
							title: 'Product Contact Details',
							id: 'ProductBusinessTabID',
							items: [productBusiness]
						},
						{
							title: 'Description',
							id: 'ProductDescriptionTabID',
							items: [{
									layout:'fit',
									height: '100%',
									items: {
										xtype:'htmleditor',
										height: '100%',
										enableFormat: false,
										enableLinks: false,
										enableFont: false,
										enableFontSize: false,
										enableColors: false,
										enableAlignments: false,
										enableLists: false,
										id:'product_description',
										name:'product_description'
									}
								}]
						},
						{
							title: 'Images',
							id: 'ProductImagesTabID',
							items: [gridImageProduct]
						},
						{
							title: 'Subproducts',
							id: 'ProductSubproductsTabID',
							items: [gridSubProduct]
						}
					]
					
				});
			
						// with tab id
			return productPanel;
			/*------------ End Product Level ---------------*/	
		},
		submitProductInfo: function(){
			if(Ext.getCmp("stock_type_combo_id").getValue() ==""){
				Ext.MessageBox.alert('Notice', 'Please select Stock Type');
				return false;
			}else if(Ext.getCmp("rate_type_combo_id").getValue() ==""){
				Ext.MessageBox.alert('Notice', 'Please select Rate Type');
				return false;
			}else if(Ext.getCmp("supplier_create_product_combo_id").getValue() ==""){
				Ext.MessageBox.alert('Notice', 'Please select Supplier');
				return false;
			}else if(Ext.getCmp("service_create_product_info_combo_id").getValue() ==""){
				Ext.MessageBox.alert('Notice', 'Please select Service Type');
				return false;
			}
			
			if(Ext.getCmp("service_create_product_info_combo_id").getValue() == 1 || Ext.getCmp("service_create_product_info_combo_id").getValue() =='1'){
				if(Ext.getCmp("destination_create_product_combo_id").getValue() == ""){
					Ext.MessageBox.alert('Notice', 'Please fill Location');
					return false;
				}
			}else{
				if(Ext.getCmp("destination_create_product_to_combo_id").getValue() == "" || Ext.getCmp("destination_create_product_from_combo_id").getValue() ==""){
					Ext.MessageBox.alert('Notice', 'Please fill Location From or TO');
					return false;
				}
			}
			
			var parr = {};
			parr['supplier_create_product_combo_id'] = Ext.getCmp("supplier_create_product_combo_id").getValue();
			parr['service_create_product_info_combo_id'] = Ext.getCmp("service_create_product_info_combo_id").getValue();
			parr['stock_type_combo_id'] = Ext.getCmp("stock_type_combo_id").getValue();
			parr['rate_type_combo_id'] = Ext.getCmp("rate_type_combo_id").getValue();
			parr['star_rating_combo_id'] = Ext.getCmp("star_rating_combo_id").getValue();
		
			
			parr['action'] = 'createProductsTab1';
			/*
			Ext.getCmp('productLocation').getForm().submit({
															url: '_ajax/execute_products.php',
															method: 'post',
															success: function (result, request) {
																
																
															},
															failure: function () {
																Ext.MessageBox.alert('Notice', 'Unable to save product info. Please try again.');
															},
															params: parr
															
															
														});
			*/
			Ext.MessageBox.show({
				   msg: 'Please wait while saving product ...',
				   progressText: 'Please wait while saving product ...',
				   width:300,
				   wait:true,
				   waitConfig: {interval:200}
				   
			   });
			Ext.getCmp('productInfo').getForm().submit({
							url: '_ajax/execute_products.php',
							method: 'post',
							success: function (result, request) {
								
								var parr = {};
								parr['destination_from'] = Ext.getCmp("destination_create_product_from_combo_id").getValue();
								parr['destination_to'] = Ext.getCmp("destination_create_product_to_combo_id").getValue();
								parr['destination'] = Ext.getCmp("destination_create_product_combo_id").getValue();
								parr['action'] = 'createProductsTab2';
								
								Ext.getCmp('productLocation').getForm().submit({
									url: '_ajax/execute_products.php',
									method: 'post',
									success: function (result, request) {
										
										var parr = {};
											parr['action'] = 'createProductsTab3';
											
											
											parr['product_amenities'] = Ext.getCmp("product_amenities").getValue();
											Ext.Ajax.request({
												url: "_ajax/execute_products.php",
												method: "POST",
												success: function ( result, request) {
													
														var parr = {};
														parr['action'] = 'createProductsTab4';
														
														
														parr['product_amenities'] = Ext.getCmp("product_amenities").getValue();
													
														Ext.getCmp('productBusiness').getForm().submit({
															url: '_ajax/execute_products.php',
															method: 'post',
															success: function (result, request) {
																var parr = {};
																parr['action'] = 'createProductsTab5';
																		
																		
																parr['product_description'] = Ext.getCmp("product_description").getValue();
																	
															
																Ext.Ajax.request({
																url: "_ajax/execute_products.php",
																method: "POST",
																success: function ( result, request) {
																		Ext.MessageBox.hide();
																		Ext.MessageBox.alert('Success', 'Products has been successfully saved.');
																		var tabPanel = Ext.getCmp('product_tab_manager_cont');
																		tabPanel.unhideTabStripItem("ProductImagesTabID");
																		tabPanel.unhideTabStripItem("ProductSubproductsTabID");
																	
																	},
																failure: function () {
																		Ext.MessageBox.alert('Failure', 'Unable to connect.');
																	
																},
																params: parr
																
															});
																
															},
															failure: function () {
																Ext.MessageBox.alert('Notice', 'Unable to save product info. Please try again.');
															},
															params: parr
															
															
														});
													
													},
												failure: function () {
														Ext.MessageBox.alert('Failure', 'Unable to connect.');
													
												},
												params: parr
												
											});
									},
									failure: function () {
										Ext.MessageBox.alert('Notice', 'Unable to save product info. Please try again.');
									},
									params: parr
									
									
								});
								
								
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to save product info. Please try again.');
							},
							params: parr
							
							
						});
			
		},
		
		formUploadImage: function(){
			if(Ext.getCmp('min_win_create_product_photo')){
			Ext.getCmp('win_create_product_photo').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_product_photo'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_create_product_photo').close();}catch(err){}
			
			
			var fp = new Ext.FormPanel({
				fileUpload: true,
				method : "POST",
				id:'imageUploads',
				width: 500,
				frame: true,
				autoHeight: true,
				bodyStyle: 'padding: 10px 10px 0 10px;',
				labelWidth: 50,
				defaults: {
					anchor: '95%',
					allowBlank: false
				},
				items: [{
					xtype: 'fileuploadfield',
					id: 'images',
					emptyText: 'Select an image',
					fieldLabel: 'Photo',
					name: 'images',
					buttonText: '',
					buttonCfg: {
						iconCls: 'upload-icon'
					}
				}],
				buttons: [{
					text: 'Save',
					icon: "images/icons/disk.png",
					handler: function(){
						if(Ext.getCmp('imageUploads').getForm().isValid()){
						
											
						Ext.getCmp('imageUploads').getForm().submit({
								url: '_ajax/execute_products.php?action=createProductsImages',
								waitMsg: 'Uploading your photo...',
								success: function(){
									try{Ext.getCmp('win_create_product_photo').close();}catch(err){}
									product_create.gridImageProduct.getStore().load();
								},
								failure: function () {
									try{Ext.getCmp('win_create_product_photo').close();}catch(err){}
									product_create.gridImageProduct.getStore().load();
								}
							});
						}
					}
				},{
					text: 'Reset',
					icon: "images/icons/arrow_rotate_anticlockwise.png",
					handler: function(){
						fp.getForm().reset();
					}
				}]
			});
			
			proj_tools.new_win('win_create_product_photo',300,140,fp,'Add Product Photos');
			
		},
		rowImageContextMenu: function (gridImageProduct, rowIndex, e) {
								
			e.stopEvent(); 
			
	        gridImageProduct.getSelectionModel().selectRow(rowIndex, false);
	        product_create.row = gridImageProduct.getSelectionModel().getSelected();
	        	
	        var coords = e.getXY();

	        var myContextMenu = product_create.Imagemenus();
			
			
		
			
			
	        myContextMenu.showAt([coords[0], coords[1]]);
	        	    
		},
		rowContextMenuSub: function (gridSubProduct, rowIndex, e) {
								
			e.stopEvent(); 
			
	        gridSubProduct.getSelectionModel().selectRow(rowIndex, false);
	        product_create.rowSub = gridSubProduct.getSelectionModel().getSelected();
	        	
	        var coords = e.getXY();

	        var myContextMenu = product_create.submenus();
			
			
		
			
			
	        myContextMenu.showAt([coords[0], coords[1]]);
	        	    
		},
		Imagemenus: function(){
			
			if(Ext.getCmp('product-image-manager-grid-context-menu')){
				Ext.getCmp('product-image-manager-grid-context-menu').removeAll();
			}
			
			var myContextMenu = new Ext.menu.Menu({
				id: "product-image-manager-grid-context-menu",
				items: [
					
					{
						text: "Removed",		
						id: "product-image-manager-menu-deactive",
						iconCls: "disapprovedIcon",  
						icon: "images/icons/cross.png",
						handler: product_create.removedImage
					}
				]
			});
			
			return myContextMenu;
		},
		submenus: function(){
			
			if(Ext.getCmp('product-sub-manager-grid-context-menu')){
				Ext.getCmp('product-sub-manager-grid-context-menu').removeAll();
			}
			
			var myContextMenu = new Ext.menu.Menu({
				id: "product-sub-manager-grid-context-menu",
				items: [
					
					{
						text: "Edit Subproduct",		
						id: "product-sub-manager-menu-edit",
						iconCls: "disapprovedIcon",  
						icon: "images/icons/folder_edit.png",
						handler: product_create.editSub
					}
				]
			});
			
			return myContextMenu;
		},
		removedImage: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_products.php",
				method: "POST",
				success: function () {
					product_create.gridImageProduct.getStore().load();
				},
				params: {action: "removeImage", photo_id: product_create.row.get("id_product_photos")}
				
			});
		},
		editSub: function(){
		
			Ext.Ajax.request({
				url: "_ajax/execute_subproduct.php",
				method: "POST",
				success: function () {
					sub_product.getPanel();
					
					
					Ext.getCmp("subproductnameid").setValue(product_create.rowSub.get("sub_product_name"));
					Ext.getCmp("subproductcodeid").setValue(product_create.rowSub.get("sub_product_code"));
					Ext.getCmp("subproductid").setValue(product_create.rowSub.get("sub_product_id"));
					
					Ext.Ajax.request({
						url: "_ajax/execute_subproduct.php",
						method: "POST",
						success: function (result, request) {
							var tabPanel = Ext.getCmp('subproduct_tab_manager');
							tabPanel.unhideTabStripItem("rate_level_tab_id");
							
							var jsonData = Ext.util.JSON.decode(result.responseText);
							var stat = jsonData.stats;
							if(stat){
								var ress = jsonData.res;
								
								Ext.getCmp("subproductratenameid").setValue(ress[0].rate_code);
								Ext.getCmp("subproductratecodeid").setValue(ress[0].rates_name);
								Ext.getCmp("subproductrateid").setValue(ress[0].rate_id);
								
								Ext.getCmp('subProductRateGridPaxId').show();
							
							}
						},
						params: {action: "getRatesData", subproduct_id: product_create.rowSub.get("sub_product_id")}
						
					});	
					
				},
				params: {action: "setSessionSub", subproduct_id: product_create.rowSub.get("sub_product_id")}
				
			});	
			
		}
	}
	
}();