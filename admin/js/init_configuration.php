<?php session_start(); ?>
<?php require_once('../_classes/system.config.php'); ?>
<?php if($_SERVER['HTTP_HOST'] != "localhost"): ?>
var base_url = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/'; ?>";
<?php else: ?>
var base_url = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/Projects/administrator/'; ?>";
<?php endif; ?>

var user_name = "<?php echo $_SESSION['USERNAME'];?>";
var user_id = <?php echo $_SESSION['ID'];?>;
var role = "<?php echo $_SESSION['ROLE'];?>";
var agency_id = <?php echo $_SESSION['AGENCY_ID'];?>;
var AGENCY_NAME = "<?php echo $_SESSION['AGENCY_NAME'];?>";
var IS_AGENCY_MANAGER = "<?php echo $_SESSION['IS_AGENCY_MANAGER'];?>";
var PAYMENT_IS_LIVE = <?php echo $configuration['SYSTEM']['PAYMENT_IS_LIVE'];?>;
