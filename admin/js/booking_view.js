Ext.namespace("app_space");

var booking_view = function () {
	return {
		getPanel: function(){
		
			if(Ext.getCmp('min_win_create_booking_view')){
			Ext.getCmp('win_create_booking_view').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_booking_view'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_create_booking_view').close();}catch(err){}
			
			var ds_agency = proj_tools.ds_combo('_ajax/execute_agency.php','getAgency','BAgencyid','id_agency','agency_name');
			var agency_combo = proj_tools.getCombo(ds_agency,'Agency Name','Bagency_search_search','id_agency','agency_name',200,200);
			var ds_consultant = proj_tools.ds_combo('_ajax/execute_user_manager_agent.php','getConsultant','BConsultantIn','id_account','account_name');
			var consultant_combo = proj_tools.getSimpleCombo(ds_consultant,'Consultant Name','Bconsultant_search','account_name','id_account',200,200);
			
			agency_combo.on('select', function(box, record, index) {
			consultant_combo.setValue('');
			consultant_combo.getStore().load({params: {agency_id: this.getValue()}}); 
			
			});
			
				
			if(role != "S"){
				var fieldSearch = [
							{id: "Bagency_search_search_combo_id", name: "Bagency_search_search_combo_id", xtype: "hidden", value: agency_id},
							{disabled: true, id: "Bagency_search_search_combo_name", fieldLabel: 'Agency Name', name: "Bagency_search_search_combo_name", xtype: "textfield", value: AGENCY_NAME},
							{id: "Bconsultant_search_combo_id", name: "Bconsultant_search_combo_id", xtype: "hidden", value: user_id},
							{disabled: true, id: "Bconsultant_search_combo_name", fieldLabel: 'Consultant Name', name: "Bconsultant_search_combo_name", xtype: "textfield", value: user_name},
							{
								xtype:'fieldset',
								checkboxToggle:true,
								title: 'Use Booking Date Search',
								autoHeight:true,
								id:"bookingDate",
								name:"bookingDate",
								width:'100%',
								defaultType: 'textfield',
								collapsed: true,
								items :[{
										fieldLabel: 'From',
										xtype: 'datefield',
										name: 'b1startdt',
										id: 'b1startdt',
										format: 'd/m/Y',
										width:'100%'
										
									 },{
										fieldLabel: 'To',
										xtype: 'datefield',
										name: 'b1enddt',
										width:'100%',
										format: 'd/m/Y',
										id: 'b1enddt',

									}
								]
							},
							{
								xtype:'fieldset',
								checkboxToggle:true,
								title: 'Use Start Date Search',
								autoHeight:true,
								id:"startDate",
								name:"startDate",
								width:'100%',
								defaultType: 'textfield',
								collapsed: true,
								items :[{
										fieldLabel: 'From',
										xtype: 'datefield',
										name: 'b2startdt',
										id: 'b2startdt',
										format: 'd/m/Y',
										width:'100%'
										
									 },{
										fieldLabel: 'To',
										xtype: 'datefield',
										name: 'b2enddt',
										width:'100%',
										format: 'd/m/Y',
										id: 'b2enddt',

									}
								]
							},
							{
								xtype:'fieldset',
								checkboxToggle:true,
								title: 'Use End Date Search',
								id:"endDate",
								name:"endDate",
								autoHeight:true,
								width:'100%',
								defaultType: 'textfield',
								collapsed: true,
								items :[{
										fieldLabel: 'From',
										xtype: 'datefield',
										name: 'b3startdt',
										id: 'b3startdt',
										format: 'd/m/Y',
										width:'100%'
										
									 },{
										fieldLabel: 'To',
										xtype: 'datefield',
										name: '3',
										width:'100%',
										format: 'd/m/Y',
										id: 'b3enddt',

									}
								]
							}
						];
			}else{
				var fieldSearch = [
							agency_combo,
							consultant_combo,
							{
								xtype:'fieldset',
								checkboxToggle:true,
								title: 'Use Booking Date Search',
								autoHeight:true,
								id:"bookingDate",
								name:"bookingDate",
								width:'100%',
								defaultType: 'textfield',
								collapsed: true,
								items :[{
										fieldLabel: 'From',
										xtype: 'datefield',
										name: 'b1startdt',
										id: 'b1startdt',
										format: 'd/m/Y',
										width:'100%'
										
									 },{
										fieldLabel: 'To',
										xtype: 'datefield',
										name: 'b1enddt',
										width:'100%',
										format: 'd/m/Y',
										id: 'b1enddt',

									}
								]
							},
							{
								xtype:'fieldset',
								checkboxToggle:true,
								title: 'Use Start Date Search',
								autoHeight:true,
								id:"startDate",
								name:"startDate",
								width:'100%',
								defaultType: 'textfield',
								collapsed: true,
								items :[{
										fieldLabel: 'From',
										xtype: 'datefield',
										name: 'b2startdt',
										id: 'b2startdt',
										format: 'd/m/Y',
										width:'100%'
										
									 },{
										fieldLabel: 'To',
										xtype: 'datefield',
										name: 'b2enddt',
										width:'100%',
										format: 'd/m/Y',
										id: 'b2enddt',

									}
								]
							},
							{
								xtype:'fieldset',
								checkboxToggle:true,
								title: 'Use End Date Search',
								id:"endDate",
								name:"endDate",
								autoHeight:true,
								width:'100%',
								defaultType: 'textfield',
								collapsed: true,
								items :[{
										fieldLabel: 'From',
										xtype: 'datefield',
										name: 'b3startdt',
										id: 'b3startdt',
										format: 'd/m/Y',
										width:'100%'
										
									 },{
										fieldLabel: 'To',
										xtype: 'datefield',
										name: '3',
										width:'100%',
										format: 'd/m/Y',
										id: 'b3enddt',

									}
								]
							}
						];
			}
			
			var formBookingView = new Ext.FormPanel({
					labelWidth: 100,
					bodyStyle:'padding:10px 5px 10px 5px;',
					frame:false, 
					border:false, 
					region: 'west',
					split: true,
					id:'SearchFormBooking',
					width: '35%',
					collapsible: true,
					autoScroll:true,
					margins:'3 0 3 3',
					cmargins:'3 3 3 3',
					defaultType:'textfield',
					items:[
						{
						xtype:'fieldset',
						title: 'Know your Booking Reference',
						collapsible: true,
						autoHeight:true,
						defaults: {width: 210},
						defaultType: 'textfield',
						items :[
							{ xtype: 'textfield',allowBlank:true,  fieldLabel: 'Booking ID',	name:'bookingId', id:'bookingId',width: "95%" },
							{ xtype: 'textfield',allowBlank:true,  fieldLabel: 'Item ID',	name:'itemId', id:'itemId',width: "95%" }
						],
						buttons: [
							{text: "Search", formBind: true, icon: "images/icons/magnifier.png", handler: booking_view.getThisBooking}
						]
						
						},
						{
						xtype:'fieldset',
						title: 'Search for a Booking',
						id:'searchForBookingId',
						collapsible: true,
						autoHeight:true,
						defaults: {width: 210},
						defaultType: 'textfield',
						items :fieldSearch,
						buttons: [
							{text: "Search", formBind: true, icon: "images/icons/magnifier.png", handler: booking_view.getThisSearchBooking}
						]
						
						}
						
					]
					
						
				});

			
				var store = new Ext.data.GroupingStore({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_get_booking.php?action=getBookingView",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewService",
		            id: "id_service",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "category", mapping: "category"},
            			{name: "id_booking", mapping: "id_booking"},
						{name: "booking_datetime", mapping: "booking_datetime"},
						{name: "created_datetime", mapping: "created_datetime"},
            			{name: "start_datetime", mapping: "start_datetime"},
						{name: "end_datetime", mapping: "end_datetime"},
						{name: "agency_name", mapping: "agency_name"},
						{name: "num_item", mapping: "num_item"},
						{name: "created_by", mapping: "created_by"}
						
			
        			]
					}),
					sortInfo:{field: 'created_datetime', direction: "DESC"},
					groupField:'category'
				});
			var grid = new Ext.grid.GridPanel({
				store: store,
				id:'booking-view-grid',
				cm: new Ext.grid.ColumnModel([
					{id: "id-grid-details-service",header: "ID", width: 30,  sortable: true, locked:true, dataIndex: "id_booking"},
					{header: "Bookings", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "category"},
					{header: "Agency Name", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "agency_name"},
					{header: "Created By", width: 90,  sortable: true, locked:true,align:'left', dataIndex: "created_by"},
					{header: "Created On", width: 90,  sortable: true, locked:true,align:'left', dataIndex: "created_datetime",renderer: render.date_format},
					{header: "Date Start", width: 90,  sortable: true, locked:true,align:'left', dataIndex: "start_datetime",renderer: render.date_format},
					{header: "Date End", width: 90,  sortable: true, locked:true,align:'left', dataIndex: "end_datetime",renderer: render.date_format},
					{header: "# of Guest\/Pax", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "num_item"},
					{header: "Action", width:100,  sortable: true, locked:true,align:'center', dataIndex: "id_booking", renderer: render.buttonViewBooking}
					]),
				loadMask: true,	
				autoExpandColumn: "id-grid-details-service",
				border: false,
				region:"center",
				view: new Ext.grid.GroupingView({
					forceFit:true,
					groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
				}),
				width: '100%',
				//border: true,
				layout: 'fit',
				height: 175
			});

				booking_view.mainForm = formBookingView;	
				booking_view.grid = grid;
				
				proj_tools.new_win_grid('win_create_booking_view',1050,550,[formBookingView,grid],'Booking View');
			Ext.getCmp('booking-view-grid').getEl().mask('Loading...', 'x-mask-loading');
			booking_view.grid.getStore().load({scope: this,
			callback: function(records, operation, success) {
				if (success) {
					Ext.getCmp('booking-view-grid').getEl().mask().hide();
				} else {
					Ext.getCmp('booking-view-grid').getEl().mask().hide();
				}
			}});
			
			
		
		},
		getThisSearchBooking: function(){
			var parr = {};
			parr["isSearch"] = true;
			parr["agency_id"] = Ext.getCmp("Bagency_search_search_combo_id").getValue();
			parr["consultant_id"] = Ext.getCmp("Bconsultant_search_combo_id").getRawValue();
			parr["isBookingDate"] = document.getElementsByName("bookingDate-checkbox")[0].checked;
			parr["b1startdt"] = Ext.getCmp("b1startdt").getValue();
			parr["b1enddt"] = Ext.getCmp("b1enddt").getValue();
			parr["isStartDate"] = document.getElementsByName("startDate-checkbox")[0].checked;
			parr["b2startdt"] = Ext.getCmp("b2startdt").getValue();
			parr["b2enddt"] = Ext.getCmp("b2enddt").getValue();
			parr["isEndDate"] = document.getElementsByName("endDate-checkbox")[0].checked;
			parr["b3startdt"] = Ext.getCmp("b3startdt").getValue();
			parr["b3enddt"] = Ext.getCmp("b3enddt").getValue();
			
			//alert(parr["isBookingDate"]);
			booking_view.grid.getStore().load({ params: parr });
		},
		getThisBooking: function(){
			var parr = {};
			parr["action"] = "getThisBooking";
		
			parr["booking_id"] = Ext.getCmp("bookingId").getValue();
			parr["item_id"] = Ext.getCmp("itemId").getValue();
			Ext.Ajax.request({
				url: "_ajax/execute_get_booking.php",
				method: "POST",
				success: function ( result, request) {
						//alert(result.responseText);
						var jsonData = Ext.util.JSON.decode(result.responseText);
                        var stats = jsonData.status;
						 var booking_id = jsonData.booking_id;
						var notification = jsonData.notification;
						if(stats){
							
							Ext.getCmp('win_create_booking_view').close();
							
							booking.getPanel2(booking_id,true);
							
						}else{
							Ext.MessageBox.alert('Failure', notification);
						}
				},
				params: parr
				
			});
		}
	}
}();