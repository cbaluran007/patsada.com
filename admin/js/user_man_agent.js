Ext.namespace("app_space");

var user_man_agent = function () {
	return {
		getPanel: function(){
		
		if(Ext.getCmp('min_win_create_user_agent')){
		Ext.getCmp('win_create_user_agent').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_user_agent'));
		Ext.getCmp('task_bar').doLayout();
		}
		
		try{Ext.getCmp('win_create_user_agent').close();}catch(err){}
		
		var ds_agency = proj_tools.ds_combo('_ajax/execute_agency.php','getAgency','AgencyManagerid','id_agency','agency_name');
		var agency_combo = proj_tools.getCombo(ds_agency,'Agency Name','agency_manager','id_agency','agency_name',150,150);
		
		var formContent = [
				agency_combo,
				{xtype: 'checkbox', checked: true, fieldLabel: 'Is Agency Manager?',  name: 'is_agency_manager', id:'is_agency_manager'},
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Alias Name',	name:'aliasname', id:'aliasnameAgent',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Username',	name:'username', id:'usernameAgent',width: "95%" },
				{ xtype: 'textfield',inputType: 'password',allowBlank:false,  fieldLabel: 'Password',	name:'passAgent', id:'passAgent', width: "95%",minLength: 6 },
				{ xtype: 'textfield',inputType: 'password',allowBlank:false,  fieldLabel: 'Re-type Password',	name:'repassAgent', id:'repassAgent',width: "95%" ,
				validator: function() {
					var passVal = Ext.getCmp("passAgent").getValue();
					if(this.getValue() != passVal){
						return false;
					}else{
						return true;
					}
				}},
				{id: "user-agent-id", name: "user-Agent-id", xtype: "hidden", value: 0},
				{id: "user-role-agent-id", name: "user-role-agent-id", xtype: "hidden", value: 'A'}
       
				];
		if(role != "S"){
		formContent = proj_tools.removedArray(formContent,0);		
		formContent = proj_tools.insertArray(formContent,{id: "agency_manager_combo_id", name: "agency_manager_combo_id", xtype: "hidden", value: agency_id},0);
		formContent = proj_tools.insertArray(formContent,{disabled: true, id: "agency_name_user_man", fieldLabel: 'Agency Name', name: "agency_name_user_man", xtype: "textfield", value: AGENCY_NAME},0);
		}
		var form = new Ext.FormPanel({
				labelWidth: 150,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items: formContent
			});
		// Mrs and Dr and Mstr
		var store = new Ext.data.ArrayStore({
			fields: ['value','display'],
			data : [ ['Mr.','Mr.'], ['Mrs.','Mrs.'],['Mstr.','Mstr.'], ['Ms.','Ms.'],['Dr.','Dr.']]
		});
		
		var form2 = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				proj_tools.getSimpleCombo(store,'Title','user_sys_man_title_agent','display','value'),
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'First Name',	name:'firstnameAgent', id:'firstnameAgent',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Last Name',	name:'lastnameAgent', id:'lastnameAgent',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Email',	name:'emailAgent', id:'emailAgent',width: "95%", vtype:'email' },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Phone #',	name:'phone_numAgent', id:'phone_numAgent',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Mobile #',	name:'mobile_numAgent', id:'mobile_numAgent',width: "95%" },
				proj_tools.getCountryCombo('user_sysAgent'),
				proj_tools.getStateCombo('user_sysAgent'),
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Address',	name:'addressAgent', id:'addressAgent',width: "95%" }
       
				]
			});
			
		var tabs = new Ext.TabPanel({
					activeTab: 0,
					width:"100%",
					border:false,
					id:'agent_tab_manager',
					items:[
						
						{
							title: 'User Access Credentials',
							id: 'user_agent_form_id_1',
							items: [form]
						},
						{
							title: 'User Information Data',
							id: 'user_agent_form_id_2',
							items: [form2]
						}
					],
					buttons: [
						{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: user_man_agent.submitUser},
						{text: "Reset", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: user_man_agent.reset}
						
					]
				});
				
		
		
		
		
			
			//validator.runThis();
			proj_tools.new_win('win_create_user_agent',400,350,tabs,'Create New Consultant');
			user_man_agent.mainForm = tabs;	
						
		
		},
		submitUser: function(){
			var parr = {};
			parr["action"] = "submitCreateSysUser";
			parr["aliasname"] = Ext.getCmp("aliasnameAgent").getValue();
			parr["username"] = Ext.getCmp("usernameAgent").getValue();
			parr["pass"] = Ext.getCmp("passAgent").getValue();
			parr["repass"] = Ext.getCmp("repassAgent").getValue();
			parr["agency_id"] = Ext.getCmp("agency_manager_combo_id").getValue();
			parr["is_agency_manager"] = Ext.getCmp("is_agency_manager").getValue();
			parr["country"] = Ext.getCmp("country_user_sysAgent_combo_id").getRawValue();
			parr["state"] = Ext.getCmp("state_user_sysAgent_combo_id").getRawValue();
			parr["user_system_id"] = Ext.getCmp("user-agent-id").getValue();
			parr["user_role_id"] = Ext.getCmp("user-role-agent-id").getValue();
			parr["user_title"] = Ext.getCmp("user_sys_man_title_agent_combo_id").getRawValue();
			parr["firstname"] = Ext.getCmp("firstnameAgent").getValue();
			parr["lastname"] = Ext.getCmp("lastnameAgent").getValue();
			parr["email"] = Ext.getCmp("emailAgent").getValue();
			parr["phone_num"] = Ext.getCmp("phone_numAgent").getValue();
			parr["mobile_num"] = Ext.getCmp("mobile_numAgent").getValue();
			parr["address"] = Ext.getCmp("addressAgent").getRawValue();
			
			
			
			
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager_agent.php",
				method: "POST",
				success: function ( result, request) {
						//alert(result.responseText);
						var jsonData = Ext.util.JSON.decode(result.responseText);
						var stats = jsonData.status;
                        var msg = jsonData.msg;
						
						if(stats){
							
							Ext.getCmp('win_create_user_agent').close();
							user_man_agent.viewUser();
							user_man_agent.grid.getStore().load();
							Ext.MessageBox.alert('Sucess', 'System user successfully saved.');
						}else{
							Ext.MessageBox.alert('Failure', msg);
						}
					},
				failure: function () {
						Ext.MessageBox.alert('Failure', msg);
					
				},
				params: parr
				
			});
			
			
						
			
		
		},
		reset: function(){
			
		user_man_agent.mainForm.getForm().reset();	
			
		
		},
		viewUser: function(isAgency){
			var isAgency = typeof isAgency !== 'undefined' ? isAgency : 0;
			
			if(Ext.getCmp('min_win_userman_grid_agent')){
			Ext.getCmp('win_userman_grid_agent').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_userman_grid_agent'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_userman_grid_agent').close();}catch(err){}
			var store = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_user_manager_agent.php?action=viewUser&role=A&agencyId="+isAgency,
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewUser",
		            id: "id_account",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "id_account", mapping: "id_account"},
            			{name: "account_code", mapping: "account_code"},
						{name: "account_name", mapping: "account_name"},
						{name: "username", mapping: "username"},
            			{name: "is_active", mapping: "is_active"},
						{name: "is_confirmed", mapping: "is_confirmed"},
						{name: "role", mapping: "role"},
						{name: "first_name", mapping: "first_name"},
						{name: "last_name", mapping: "last_name"},
						{name: "title", mapping: "title"},
						{name: "email", mapping: "email"},
						{name: "country", mapping: "country"},
						{name: "state", mapping: "state"},
						{name: "address", mapping: "address"},
						{name: "phone_number", mapping: "phone_number"},
						{name: "mobile_number", mapping: "mobile_number"},
						{name: "created_by", mapping: "created_by"},
						{name: "created_datetime", mapping: "created_datetime"},
						{name: "id_agency", mapping: "id_agency"},
						{name: "agency_name", mapping: "agency_name"},
						{name: "is_agency_manager", mapping: "is_agency_manager"}
						

        			]
					}),
					sortInfo:{field: 'created_datetime', direction: "DESC"}
				});
			var grid = new Ext.grid.GridPanel({
				store: store,
				cm: new Ext.grid.ColumnModel([
					{id: "id-grid-details-user-agent",header: "ID", width: 30,  sortable: true, locked:true, dataIndex: "id_account"},
					{header: "Agency", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "agency_name"},
					{header: "Code", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "account_code"},
					{header: "Alias Name", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "account_name"},
					{header: "Fullname", width: 100,  sortable: true, locked:true,align:'left', dataIndex: "first_name"},
					{header: "Active", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_active", renderer: render.YesNo},
					{header: "Confirm", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_confirmed",renderer: render.YesNo},
					{header: "Agency Manager", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_agency_manager",renderer: render.YesNo},
					{header: "Email", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "email"},
					{header: "Country", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "country"},
					{header: "State", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "state"},
					{header: "Address", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "address"},
					{header: "Phone #", width: 30,  sortable: true, locked:true,align:'left', dataIndex: "phone_number"},
					{header: "Mobile #", width: 30,  sortable: true, locked:true,align:'left', dataIndex: "mobile_number"},
					{header: "Created By", width:50,  sortable: true, locked:true,align:'left', dataIndex: "created_by"}
					]),
				loadMask: true,	
				autoExpandColumn: "id-grid-details-user-agent",
				border: false,
				viewConfig: {
					forceFit:true
				},
				listeners:  {
					rowcontextmenu: user_man_agent.rowContextMenu
				},
				width: '100%',
				//border: true,
				layout: 'fit',
				height: 175,
				tbar: [
            '		Search Agent User Name: ', ' ',
					new Ext.ux.form.SearchField({
						store: store,
						width:320
					})
				],
				bbar: new Ext.PagingToolbar({
					pageSize: 15,
					store: store,
					displayInfo: true,
					displayMsg: 'Displaying topics {0} - {1} of {2}',
					emptyMsg: "No topics to display"//,
					/*items:[
						'-', {
						pressed: true,
						enableToggle:true,
						text: 'Show Preview',
						cls: 'x-btn-text-icon details',
						toggleHandler: function(btn, pressed){
							var view = grid.getView();
							view.showPreview = pressed;
							view.refresh();
						}
					}]*/
				})
			});

			
			
			user_man_agent.grid = grid;
			user_man_agent.grid.getStore().load();
			
			proj_tools.new_win('win_userman_grid_agent','85%',400,grid,'Consultant List');
			
					
		},
		rowContextMenu: function (grid, rowIndex, e) {
								
			e.stopEvent(); 
			
	        grid.getSelectionModel().selectRow(rowIndex, false);
	        user_man_agent.row = grid.getSelectionModel().getSelected();
	        	
	        var coords = e.getXY();

	        var myContextMenu = user_man_agent.menus();
			
			if ( user_man_agent.row.get("is_active") == 'Y' || user_man_agent.row.get("is_active") == 1) {
				Ext.getCmp("user_agent_man-menu-active").disable();
			} else {
				Ext.getCmp("user_agent_man-menu-deactive").disable();
			}
			
			if ( user_man_agent.row.get("is_confirmed") == 'Y' || user_man_agent.row.get("is_confirmed") == 1) {
				Ext.getCmp("user_agent_man-menu-confirm").disable();
			} else {
				Ext.getCmp("user_agent_man-menu-unconfirm").disable();
			}
			if ( user_man_agent.row.get("is_agency_manager") == 'Y' || user_man_agent.row.get("is_agency_manager") == 1) {
				Ext.getCmp("user_agent_man-menu-manager").disable();
			} else {
				Ext.getCmp("user_agent_man-menu-unmanager").disable();
			}
			
	        myContextMenu.showAt([coords[0], coords[1]]);
	        	    
		},
		menus: function(){
			
			if(Ext.getCmp('user_agent_man-grid-context-menu')){
				Ext.getCmp('user_agent_man-grid-context-menu').removeAll();
			}
			
			var myContextMenu = new Ext.menu.Menu({
				id: "user_agent_man-grid-context-menu",
				items: [
					{
						text: "Activate",
						id: "user_agent_man-menu-active",
						iconCls: "approvedIcon",  
						icon: "images/icons/accept.png",
						handler: user_man_agent.setactive
					},
					{
						text: "Deactivate",		
						id: "user_agent_man-menu-deactive",
						iconCls: "disapprovedIcon",  
						icon: "images/icons/cross.png",
						handler: user_man_agent.setdeactive
					},
					'-',
					{
						text: "Confirm",
						id: "user_agent_man-menu-confirm",
						iconCls: "approvedIcon",  
						icon: "images/icons/accept.png",
						handler: user_man_agent.setConfirmactive
					},
					{
						text: "Unconfirm",		
						id: "user_agent_man-menu-unconfirm",
						iconCls: "disapprovedIcon",  
						icon: "images/icons/cross.png",
						handler: user_man_agent.setConfirmdeactive
					},
					'-',
					{
						text: "Set Manager",
						id: "user_agent_man-menu-manager",
						iconCls: "approvedIcon",  
						icon: "images/icons/accept.png",
						handler: user_man_agent.setManager
					},
					{
						text: "Unset Manager",		
						id: "user_agent_man-menu-unmanager",
						iconCls: "disapprovedIcon",  
						icon: "images/icons/cross.png",
						handler: user_man_agent.setUnmanager
					},					
					'-',
					{
						text: "Edit",
						id: "user_agent_man-menu-edit",
						iconCls: "approvedIcon",  
						icon: "images/icons/folder_edit.png",
						handler: user_man_agent.edit
					}
				]
			});
			
			return myContextMenu;
		},
		edit: function(){
			user_man_agent.getPanel();
	
	
			//alert(user_man.row.get("title"));
			Ext.getCmp('aliasnameAgent').setValue(user_man_agent.row.get("account_name"));
			Ext.getCmp('usernameAgent').setValue(user_man_agent.row.get("username"));
			Ext.getCmp('user-agent-id').setValue(user_man_agent.row.get("id_account"));
			Ext.getCmp('user-role-agent-id').setValue(user_man_agent.row.get("role"));
			
			Ext.getCmp('user_sys_man_title_agent_combo_id').setValue(user_man_agent.row.get("title"));
			Ext.getCmp('user_sys_man_title_agent_combo_id').setRawValue(user_man_agent.row.get("title"));
			Ext.getCmp('firstnameAgent').setValue(user_man_agent.row.get("first_name"));
			Ext.getCmp('lastnameAgent').setValue(user_man_agent.row.get("last_name"));
			Ext.getCmp('emailAgent').setValue(user_man_agent.row.get("email"));
			Ext.getCmp('phone_numAgent').setValue(user_man_agent.row.get("phone_number"));
			Ext.getCmp('mobile_numAgent').setValue(user_man_agent.row.get("mobile_number"));
			Ext.getCmp('addressAgent').setValue(user_man_agent.row.get("address"));
			
			
			Ext.getCmp("state_user_sysAgent_combo_id").setDisabled(false);
    
			Ext.getCmp('country_user_sysAgent_combo_id').setValue(user_man_agent.row.get("country"));
			Ext.getCmp('country_user_sysAgent_combo_id').setRawValue(country_arr.indexOf(user_man_agent.row.get("country")));
			Ext.getCmp('state_user_sysAgent_combo_id').setValue(user_man_agent.row.get("state"));
			Ext.getCmp('state_user_sysAgent_combo_id').setRawValue(user_man_agent.row.get("state"));
		
			Ext.getCmp('agency_manager_combo_id').setValue(user_man_agent.row.get("id_agency"));
			if(role == "S"){
				Ext.getCmp('agency_manager_combo_id').setRawValue(user_man_agent.row.get("agency_name"));
			}else{
				Ext.getCmp('agency_name_user_man').setValue(user_man_agent.row.get("agency_name"));
			}
			Ext.getCmp('is_agency_manager').setValue(user_man_agent.row.get("is_agency_manager"));

						
			
		},
		setactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager_agent.php",
				method: "POST",
				success: function () {
					user_man_agent.grid.getStore().load();
				},
				params: {action: "activate", user_id: user_man_agent.row.get("id_account")}
				
			});
		},
		setdeactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager_agent.php",
				method: "POST",
				success: function () {
					user_man_agent.grid.getStore().load();
				},
				params: {action: "deactivate", user_id: user_man_agent.row.get("id_account")}
				
			});
		},
		setConfirmactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager_agent.php",
				method: "POST",
				success: function () {
					user_man_agent.grid.getStore().load();
				},
				params: {action: "activateConfirm", user_id: user_man_agent.row.get("id_account")}
				
			});
		},
		setConfirmdeactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager_agent.php",
				method: "POST",
				success: function () {
					user_man_agent.grid.getStore().load();
				},
				params: {action: "deactivateConfirm", user_id: user_man_agent.row.get("id_account")}
				
			});
		},
		setManager: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager_agent.php",
				method: "POST",
				success: function () {
					user_man_agent.grid.getStore().load();
				},
				params: {action: "activateManager", user_id: user_man_agent.row.get("id_account")}
				
			});
		},
		setUnmanager: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_user_manager_agent.php",
				method: "POST",
				success: function () {
					user_man_agent.grid.getStore().load();
				},
				params: {action: "deactivateManager", user_id: user_man_agent.row.get("id_account")}
				
			});
		}
	}
}();