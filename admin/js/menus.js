Ext.namespace("app_space");
app_space.tbar = [
					{	
						text: "<strong>Refresh App</strong>",
						icon: "images/icons2/dark/24px/24_refresh.png",
						iconCls: "blist",
						iconAlign:'top',
						scale: 'medium',
						handler: function () {
							window.location.href = 'appv1.php ';
						}
					},
					'-',
					{
                        text: "<strong>Job Posted</strong>",
                        icon: "images/icons2/dark/24px/24_archive.png",
                        iconCls: "blist",
                        iconAlign:'top',
                        scale: 'medium',
                        handler: function () {
                            job_posted.view();
                        }
					},
					'-',{
                        text: "<strong>Post Jobs</strong>",
                        icon: "images/icons2/dark/24px/24_headlines.png",
                        iconCls: "blist",
                        iconAlign:'top',
                        scale: 'medium',
                        handler: function () {
                            post_jobs.view();
                        }
					},
					'-',
                    {
                        text: "<strong>Employer</strong>",
                        icon: "images/icons2/dark/24px/24_users.png",
                        iconCls: "blist",
                        iconAlign:'top',
                        scale: 'medium',
                        handler: function () {
                           employer.view();
                        }
                       
                        /*
                        text: "<strong>Employer</strong>",
                        icon: "images/icons2/dark/24px/24_users.png",
                        iconCls: "blist",
                        iconAlign:'top',
                        scale: 'medium',
                        handler: function () {
                            employer.view();
                        }*/
                    },
                    '-',
					{
                        text: "<strong>Users</strong>",
                        icon: "images/icons2/dark/24px/24_users.png",
                        iconCls: "blist",
                        iconAlign:'top',
                        scale: 'medium',
                        handler: function () {
                            patsada_users.view();
                        }
					},

					'-',

					{	
						text: "<strong>LOGOUT</strong>",
						iconAlign:'top',
						icon: "images/icons2/dark/24px/24_logout.png",
						iconCls: "blist",
						scale: 'medium',
						handler: function () {
							window.location.href = 'logout.php ';
						}
					}
			
		];