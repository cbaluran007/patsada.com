Ext.namespace("app_space");

var agency = function () {
	return {
		getPanel: function(){
		
		if(Ext.getCmp('min_win_create_agency')){
		Ext.getCmp('win_create_agency').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_agency'));
		Ext.getCmp('task_bar').doLayout();
		}
		
		try{Ext.getCmp('win_create_agency').close();}catch(err){}
		var store = new Ext.data.ArrayStore({
			fields: ['value','display'],
			data : [ ['Direct','Direct'], ['Trade Cash','Trade Cash'],['Trade Terms','Trade Terms']]
		});
		

		var form = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Agency Name',	name:'agencyname', id:'agencyname',width: "95%" },
				proj_tools.getSimpleCombo(store,'Agency Type','agency_type','display','value'),
				{xtype: 'checkbox', checked: true, fieldLabel: 'Is Auto Invoice?',  name: 'is_agency_auto_invoice', id:'is_agency_auto_invoice'},
				{ xtype: 'textarea',allowBlank:false, fieldLabel: 'Internal Notes', name:'internal_notes', id:'internal_notes',width: "95%", height: 150,autoScroll: true },
				{id: "agency-id", name: "agency-id", xtype: "hidden", value: 0}
       
				]
			});
		
		var form2 = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				{ xtype: 'numberfield',allowBlank:false,  fieldLabel: 'Markup%',	name:'agency_markup', id:'agency_markup',width: "95%",value:35 },
				{ xtype: 'numberfield',allowBlank:false,  fieldLabel: 'Agency Commission%',	name:'agency_comm', id:'agency_comm',width: "95%",value:13 },
				{ xtype: 'numberfield',allowBlank:false,  fieldLabel: 'Agency Discount%',	name:'agency_disc', id:'agency_disc',width: "95%" ,value:0 },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Currency',	name:'currency_agency', id:'currency_agency',width: "95%", value:"AUD" }
       
				]
			});
		
		var form3 = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Card Name',	name:'cardname', id:'cardname',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Position',	name:'position', id:'position',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Department',	name:'department', id:'department',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'URL',	name:'url_agency', id:'url_agency',width: "95%", vtype:'url' },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Email',	name:'email_agency', id:'email_agency',width: "95%", vtype:'email' },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Phone #',	name:'phone_num_agency', id:'phone_num_agency',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Fax #',	name:'fax_num_agency', id:'fax_num_agency',width: "95%" },
				proj_tools.getCountryCombo('agency_manager'),
				proj_tools.getStateCombo('agency_manager'),
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'City',	name:'cityAgency', id:'cityAgency',width: "95%" },
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Address',	name:'addressAgency', id:'addressAgency',width: "95%" }

				]
			});
		
			var tabs = new Ext.TabPanel({
					activeTab: 0,
					width:"100%",
					border:false,
					id:'agency_tab_manager',
					items:[
						
						{
							title: 'Agency Info',
							id: 'agency_form_id_1',
							items: [form]
						},
						{
							title: 'Calculations',
							id: 'agency_form_id_2',
							items: [form2]
						},
						{
							title: 'Business Card',
							id: 'user_agent_form_id_3',
							items: [form3]
						}
					],
					buttons: [
						{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: agency.submitAgency},
						{text: "Reset", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: agency.reset}
						
					]
				});
				
			var mainForm = new Ext.FormPanel({
					width:400,
					height:350,
					border:false,
					id:'agency_main_manager',
					items:[tabs	],
					buttons: [
						{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: agency.submitAgency},
						{text: "Reset", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: agency.reset}
						
					]
				});
		
		
		
			
			//validator.runThis();
			proj_tools.new_win('win_create_agency',400,400,tabs,'Create New Agency');
			agency.mainForm = form;	
			agency.mainForm2 = form2;	
			agency.mainForm3 = form3;			
		
		},
		submitAgency: function(){
			var parr = {};
			parr["action"] = "submitAgency";
		
			parr["agencyname"] = Ext.getCmp("agencyname").getValue();
			parr["agency-id"] = Ext.getCmp("agency-id").getValue();
			parr["internal_notes"] = Ext.getCmp("internal_notes").getValue();
			parr["agency_type_combo_id"] = Ext.getCmp("agency_type_combo_id").getValue();
			parr["is_agency_auto_invoice"] = Ext.getCmp("is_agency_auto_invoice").getValue();
			
			parr["agency_markup"] = Ext.getCmp("agency_markup").getValue();
			parr["agency_comm"] = Ext.getCmp("agency_comm").getValue();
			parr["agency_disc"] = Ext.getCmp("agency_disc").getValue();
			parr["currency_agency"] = Ext.getCmp("currency_agency").getValue();
			
			parr["country"] = Ext.getCmp("country_agency_manager_combo_id").getRawValue();
			parr["state"] = Ext.getCmp("state_agency_manager_combo_id").getRawValue();

			parr["cardname"] = Ext.getCmp("cardname").getValue();
			parr["position"] = Ext.getCmp("position").getValue();
			parr["department"] = Ext.getCmp("department").getValue();
			parr["email_agency"] = Ext.getCmp("email_agency").getValue();
			parr["phone_num_agency"] = Ext.getCmp("phone_num_agency").getValue();
			parr["url_agency"] = Ext.getCmp("url_agency").getValue();
			parr["fax_num_agency"] = Ext.getCmp("fax_num_agency").getValue();
			parr["addressAgency"] = Ext.getCmp("addressAgency").getValue();
			parr["cityAgency"] = Ext.getCmp("cityAgency").getValue();
			
			
			
			Ext.Ajax.request({
				url: "_ajax/execute_agency_manager.php",
				method: "POST",
				success: function ( result, request) {
						//alert(result.responseText);
						var jsonData = Ext.util.JSON.decode(result.responseText);
                        var stats = jsonData.status;
						if(stats){
							
							Ext.getCmp('win_create_agency').close();
							
							agency.view();
							agency.grid.getStore().load();
							Ext.MessageBox.alert('Sucess', 'Agency successfully saved.');
						}else{
							Ext.MessageBox.alert('Failure', 'Unable to saved agency.');
						}
					},
				failure: function () {
						Ext.MessageBox.alert('Failure', 'Unable to connect.');
					
				},
				params: parr
				
			});
		
		},
		reset: function(){
			
		agency.mainForm.getForm().reset();	
		agency.mainForm2.getForm().reset();		
		agency.mainForm3.getForm().reset();
		},
		view: function(){
			
			if(Ext.getCmp('min_win_agency_grid')){
			Ext.getCmp('win_agency_grid').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_agency_grid'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_agency_grid').close();}catch(err){}
				
			var expander =  new Ext.ux.grid.RowExpander({
				tpl : new Ext.Template(
					'<div class="suppInfoDiv">',
					'<p class="supplieInfoHeader">Business Card</p>',
					'<br>',
					'<table>',
					'<tr>',
					'<td>Card Name:</td><td>{card_name}</td>',
					'<td>Country:</td><td>{country}</td>',
					'</tr>',
					'<tr>',
					'<td>Email:</td><td>{email} </td>',
					'<td>City:</td><td>{city}</td>',
					'</tr>',
					'<tr>',
					'<td>Contact #:</td><td>Phone #: {phone_number} <br>Fax #: {fax}</td>',
					'<td>Address:</td><td>{address},{state}</td>',
					'</tr>',
					'<tr>',
					'<td>Url:</td><td>{web_url}</td>',
					'<td>Notes:</td><td>{internal_notes}</td>',
					'</tr>',
					'</table>',
					'</div>'
				)
			});
			
			var store = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_agency_manager.php?action=view",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewAgency",
		            id: "id_agency_manager",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "id_agency", mapping: "id_agency"},
            			{name: "agency_code", mapping: "agency_code"},
						{name: "agency_name", mapping: "agency_name"},
						{name: "is_active", mapping: "is_active"},
						{name: "is_confirmed", mapping: "is_confirmed"},
						{name: "created_by", mapping: "created_by"},
						{name: "created_datetime", mapping: "created_datetime"},
						{name: "is_autoinvoice", mapping: "is_autoinvoice"},
						{name: "markup", mapping: "markup"},
						{name: "agency_commision", mapping: "agency_commision"},
						{name: "agency_discount", mapping: "agency_discount"},
						{name: "card_name", mapping: "card_name"},
						{name: "position", mapping: "position"},
						{name: "department", mapping: "department"},
            			{name: "internal_notes", mapping: "internal_notes"},
						{name: "country", mapping: "country"},
						{name: "city", mapping: "city"},
						{name: "state", mapping: "state"},
						{name: "address", mapping: "address"},
						{name: "email", mapping: "email"},
						{name: "currency", mapping: "currency"},
						{name: "phone", mapping: "phone"},
						{name: "fax", mapping: "fax"},
						{name: "agency_type", mapping: "agency_type"},
						{name: "web_url", mapping: "web_url"}
			
        			]
					}),
					sortInfo:{field: 'created_datetime', direction: "DESC"}
				});
			var grid = new Ext.grid.GridPanel({
				store: store,
				cm: new Ext.grid.ColumnModel([
					expander,
					{id: "id-grid-details-agency",header: "ID", width: 30,  sortable: true, locked:true, dataIndex: "id_agency"},
					{header: "Code", width: 30,  sortable: true, locked:true,align:'left', dataIndex: "agency_code"},
					{header: "Agency Name", width: 90,  sortable: true, locked:true,align:'left', dataIndex: "agency_name"},
					{header: "Agency Type", width: 30,  sortable: true, locked:true,align:'left', dataIndex: "agency_type"},
					{header: "Mark Up%", width: 30,  sortable: true, locked:true,align:'left', dataIndex: "markup"},
					{header: "Agency Commision%", width: 30,  sortable: true, locked:true,align:'left', dataIndex: "agency_commision"},
					{header: "Agency Discount%", width: 30,  sortable: true, locked:true,align:'left', dataIndex: "agency_discount"},
					{header: "Currency", width: 30,  sortable: true, locked:true,align:'left', dataIndex: "currency"},
					{header: "Active", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_active", renderer: render.YesNo},
					{header: "Confirm", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_confirmed", renderer: render.YesNo},
					{header: "Created By", width:50,  sortable: true, locked:true,align:'left', dataIndex: "created_by"}
					]),
				loadMask: true,	
				autoExpandColumn: "id-grid-details-agency",
				border: false,
				viewConfig: {
					forceFit:true
				},
				listeners:  {
					rowcontextmenu: agency.rowContextMenu
				},
				width: '100%',
				//border: true,
				layout: 'fit',
				plugins: expander,
				height: 175,
				tbar: [
            '		Search Agency Name: ', ' ',
					new Ext.ux.form.SearchField({
						store: store,
						width:320
					})
				],
				bbar: new Ext.PagingToolbar({
					pageSize: 15,
					store: store,
					displayInfo: true,
					displayMsg: 'Displaying Agency {0} - {1} of {2}',
					emptyMsg: "No agency to display"
					
				})
			});

			
			
			agency.grid = grid;
			agency.grid.getStore().load();
			
			proj_tools.new_win('win_agency_grid',950,400,grid,'Agency List');
			
					
		},
		rowContextMenu: function (grid, rowIndex, e) {
								
			e.stopEvent(); 
			
	        grid.getSelectionModel().selectRow(rowIndex, false);
	        agency.row = grid.getSelectionModel().getSelected();
	        	
	        var coords = e.getXY();

	        var myContextMenu = agency.menus();
			
			if ( agency.row.get("is_active") == 'Y' || agency.row.get("is_active") == 1) {
				Ext.getCmp("agency-manager-menu-active").disable();
			} else {
				Ext.getCmp("agency-manager-menu-deactive").disable();
			}
			if ( agency.row.get("is_confirmed") == 'Y' || agency.row.get("is_confirmed") == 1) {
				Ext.getCmp("agency-manager-menu-confirm").disable();
			} else {
				Ext.getCmp("agency-manager-menu-unconfirm").disable();
			}
			
			
	        myContextMenu.showAt([coords[0], coords[1]]);
	        	    
		},
		menus: function(){
			
			if(Ext.getCmp('agency-manager-grid-context-menu')){
				Ext.getCmp('agency-manager-grid-context-menu').removeAll();
			}
			
			var myContextMenu = new Ext.menu.Menu({
				id: "agency-manager-grid-context-menu",
				items: [
					{
						text: "Activate",
						id: "agency-manager-menu-active",
						iconCls: "approvedIcon",  
						icon: "images/icons/accept.png",
						handler: agency.setactive
					},
					{
						text: "Deactivate",		
						id: "agency-manager-menu-deactive",
						iconCls: "disapprovedIcon",  
						icon: "images/icons/cross.png",
						handler: agency.setdeactive
					},
					'-',
					{
						text: "Confirm",
						id: "agency-manager-menu-confirm",
						iconCls: "approvedIcon",  
						icon: "images/icons/accept.png",
						handler: agency.setconfirm
					},
					{
						text: "Unconfirm",		
						id: "agency-manager-menu-unconfirm",
						iconCls: "disapprovedIcon",  
						icon: "images/icons/cross.png",
						handler: agency.setunconfirm
					},
					'-',
					{
						text: "Edit",
						id: "agency-manager-menu-edit",
						iconCls: "approvedIcon",  
						icon: "images/icons/folder_edit.png",
						handler: agency.edit
					},
					'-',
					{
						text: "View Consultant",
						id: "agency-manager-menu-consultant",
						iconCls: "approvedIcon",  
						icon: "images/icons/application_cascade.png",
						handler: agency.consultant
					}
				]
			});
			
			return myContextMenu;
		},
		edit: function(){
			agency.getPanel();
	
	
			//alert(user_man.row.get("title"));
			Ext.getCmp("agencyname").setValue(agency.row.get("agency_name"));
			Ext.getCmp("agency-id").setValue(agency.row.get("id_agency"));
			Ext.getCmp("internal_notes").setValue(agency.row.get("internal_notes"));
			Ext.getCmp("agency_type_combo_id").setValue(agency.row.get("agency_type"));
			Ext.getCmp("agency_type_combo_id").setRawValue(agency.row.get("agency_type"));
			Ext.getCmp("is_agency_auto_invoice").setValue(agency.row.get("is_autoinvoice"));
			
			Ext.getCmp("agency_markup").setValue(agency.row.get("markup"));
			Ext.getCmp("agency_comm").setValue(agency.row.get("agency_commision"));
			Ext.getCmp("agency_disc").setValue(agency.row.get("agency_discount"));
			Ext.getCmp("currency_agency").setValue(agency.row.get("currency"));
			
			Ext.getCmp("country_agency_manager_combo_id").setValue(agency.row.get("country"));
			Ext.getCmp("country_agency_manager_combo_id").setRawValue(agency.row.get("country"));
			Ext.getCmp("state_agency_manager_combo_id").setDisabled(false);
			Ext.getCmp("state_agency_manager_combo_id").setValue(agency.row.get("state"));
			Ext.getCmp("state_agency_manager_combo_id").setRawValue(agency.row.get("state"));

			Ext.getCmp("cardname").setValue(agency.row.get("card_name"));
			Ext.getCmp("position").setValue(agency.row.get("position"));
			Ext.getCmp("department").setValue(agency.row.get("department"));
			Ext.getCmp("email_agency").setValue(agency.row.get("email"));
			Ext.getCmp("phone_num_agency").setValue(agency.row.get("phone"));
			Ext.getCmp("url_agency").setValue(agency.row.get("web_url"));
			Ext.getCmp("fax_num_agency").setValue(agency.row.get("fax"));
			Ext.getCmp("addressAgency").setValue(agency.row.get("address"));
			Ext.getCmp("cityAgency").setValue(agency.row.get("city"));
			
		
		
						
			
		},
		setactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_agency_manager.php",
				method: "POST",
				success: function () {
					agency.grid.getStore().load();
				},
				params: {action: "activate", agency_id: agency.row.get("id_agency")}
				
			});
		},
		setdeactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_agency_manager.php",
				method: "POST",
				success: function () {
					agency.grid.getStore().load();
				},
				params: {action: "deactivate", agency_id: agency.row.get("id_agency")}
				
			});
		},
		setconfirm: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_agency_manager.php",
				method: "POST",
				success: function () {
					agency.grid.getStore().load();
				},
				params: {action: "confirm", agency_id: agency.row.get("id_agency")}
				
			});
		},
		setunconfirm: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_agency_manager.php",
				method: "POST",
				success: function () {
					agency.grid.getStore().load();
				},
				params: {action: "unconfirm", agency_id: agency.row.get("id_agency")}
				
			});
		},
		consultant: function(){
			user_man_agent.viewUser(agency.row.get("id_agency"));
		}
	}
}();