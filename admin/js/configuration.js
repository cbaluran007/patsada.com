Ext.namespace("app_space");

var configuration = function () {
	return {
		getLoadPanel: function(){
			var parr = {};
			parr["action"] = "loadConfig";
			Ext.Ajax.request({
				url: "_ajax/execute_configuration.php",
				method: "POST",
				success: function ( result, request) {
						
						var jsonData = Ext.util.JSON.decode(result.responseText);
                        var stats = jsonData.status;
						if(stats){
							
							configuration.getPanel(jsonData.res)
						}else{
							Ext.MessageBox.alert('Failure', 'Unable to load configuration.');
						}
						
					},
				failure: function () {
						Ext.MessageBox.alert('Failure', 'Unable to connect.');
					
				},
				params: parr
				
			});
		},
		getPanel: function(text){
		
		if(Ext.getCmp('min_win_create_configuration')){
		Ext.getCmp('win_create_configuration').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_configuration'));
		Ext.getCmp('task_bar').doLayout();
		}
		
		try{Ext.getCmp('win_create_configuration').close();}catch(err){}
		
		var form = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:true, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				{ xtype: 'textarea',allowBlank:false, hideLabel: true,	name:'config', id:'config',width: "95%", height: 300,value: text,autoScroll: true }
       
				],
				buttons: [
						{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: configuration.submitConfig},
						{text: "Reload", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: configuration.reload}
						
					]
			});
		
		
		
		
		
		
			
			//validator.runThis();
			proj_tools.new_win('win_create_configuration',800,400,form,'Update Configuration');
			configuration.mainForm = form;	
						
		
		},
		submitConfig: function(){
			var parr = {};
			parr["action"] = "updateConfig";
			
			configuration.mainForm.getForm().submit({
						
							url: '_ajax/execute_configuration.php',
							method: 'post',
							success: function () {
								
								Ext.MessageBox.alert('Sucess', 'Configuration successfully updated.');
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to saved configuration. Please try again.');
							},
							
							params: parr
							
						});
		
		},
		reload: function(){
			
		configuration.getLoadPanel();	
			
		
		}
	}
}();