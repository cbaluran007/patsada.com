Ext.namespace("app_space");

var booking = function () {
	return {
		getPanel: function(param){
			Ext.Ajax.request({
				url: "_ajax/execute_get_booking.php",
				method: "POST",
				success: function (result, request) {
					var jsonData = Ext.util.JSON.decode(result.responseText);
                    var bookingId = jsonData.booking_id;
					var stats = jsonData.stats;
					if(bookingId != 0 && stats){
					Ext.MessageBox.confirm('Confirm', 'Booking (B#'+bookingId+') already exist.\nDo you want to retain this booking?', function(btn){
						if(btn == 'no'){
						booking.getPanel2(0,false);
						}else{
						booking.getPanel2(bookingId,false);
						}
					 });
					}else{
					Ext.MessageBox.alert('Notice', 'Fresh booking will be created.', function(btn){
						if(btn == 'ok'){
						booking.getPanel2(0,false);
						}
					 });
					
					}
					 
					
					
				},
				params: {action: "getBooking",par: param}
				
			});
		
		},
		getPanel2: function(bookingId,isPax){
		
		if(Ext.getCmp('min_win_create_bookings')){
		Ext.getCmp('win_create_bookings').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_bookings'));
		Ext.getCmp('task_bar').doLayout();
		}
		
		try{Ext.getCmp('win_create_bookings').close();}catch(err){}
		var isNewBooking = 0;
		if(bookingId==0){
			var isNewBooking = 1;
		}
		if(isPax){
			var link = "pages/booking.php?booking_id="+bookingId
		}else{
			
			var link = "modules/xml_booking.php?isNew="+isNewBooking+"&booking_id="+bookingId
		}
		var tabsBooking = new Ext.TabPanel({
				activeTab: 0,
				width:"100%",
				region: 'center',
				id:'tabForBookings',
				height:"100%",
				plain:true,
				autoDestroy: false,
				defaults:{autoScroll: true},
				items:[{
						title: 'Booking Details',
						layout : 'fit',
						height: '100%',
						autoEl : {
						   tag : "iframe",
						   height: '100%',
						   id: "searchTabBookingsInfo",
						   src : link
						}
						//autoLoad: {url: 'pages/search.php', params: 'action=default'}
					},
					{
						title: 'Items',
						layout : 'fit',
						height: '100%',
						autoEl : {
						   tag : "iframe",
						   height: '100%',
						   id: "searchTabBookingsItems",
						   src : "pages/items.php"
						}
						//autoLoad: {url: 'pages/search.php', params: 'action=default'}
					},
					/*{
						title: 'Quotations',
						layout : 'fit',
						height: '100%',
						autoEl : {
						   tag : "iframe",
						   height: '100%',
						   id: "searchTabQuotations",
						    src : "pages/quotation.php?booking_id="+bookingId
						}
						//autoLoad: {url: 'pages/search.php', params: 'action=default'}
					},*/
					
					{
						title: 'Invoice',
						layout : 'fit',
						height: '100%',
						autoEl : {
						   tag : "iframe",
						   height: '100%',
						   id: "searchTabBookingsInvoice",
						   src : "pages/invoice.php?booking_id="+bookingId
						}
						//autoLoad: {url: 'pages/search.php', params: 'action=default'}
					},
					/*{
						title: 'Payments',
						layout : 'fit',
						height: '100%',
						autoEl : {
						   tag : "iframe",
						   height: '100%',
						   id: "searchTabBookingsPayments",
						   src : ""
						}
						//autoLoad: {url: 'pages/search.php', params: 'action=default'}
					},*/
					{
						title: 'Receipts',
						layout : 'fit',
						height: '100%',
						autoEl : {
						   tag : "iframe",
						   height: '100%',
						   id: "searchTabBookingsReceipts",
						    src : "pages/receipt.php?booking_id="+bookingId
						}
						//autoLoad: {url: 'pages/search.php', params: 'action=default'}
					},
					{
						title: 'Voucher',
						layout : 'fit',
						height: '100%',
						autoEl : {
						   tag : "iframe",
						   height: '100%',
						   id: "searchTabBookingsVoucher",
						   src : "pages/voucher.php?booking_id="+bookingId
						}
						//autoLoad: {url: 'pages/search.php', params: 'action=default'}
					},
					{
						title: 'Itinerary',
						layout : 'fit',
						height: '100%',
						autoEl : {
						   tag : "iframe",
						   height: '100%',
						   id: "searchTabBookingsitenerary",
						   src : "pages/itinerary.php?booking_id="+bookingId
						}
						//autoLoad: {url: 'pages/search.php', params: 'action=default'}
					},
					{
						title: 'Refundables',
						layout : 'fit',
						height: '100%',
						autoEl : {
						   tag : "iframe",
						   height: '100%',
						   id: "searchTabBookingsRefundables",
						   src : "pages/refunds.php?booking_id="+bookingId
						}
						//autoLoad: {url: 'pages/search.php', params: 'action=default'}
					}
				]
			});
		
		
		
		
		
			
			//validator.runThis();
			proj_tools.new_win_grid('win_create_bookings',1050,600,tabsBooking,'Bookings');
		
			if(isNewBooking == 1){
			Ext.MessageBox.alert('Status', 'Please complete booking within 20 min.');
			}
						
		
		},
		convertingItems: function(bookingId,consultantId){
			 Ext.MessageBox.show({
				   msg: 'Checking for products availability...',
				   progressText: 'Checking for products availability....',
				   width:300,
				   wait:true,
				   waitConfig: {interval:200}
				   
			   });
			   
			   Ext.Ajax.request({
				url: "_ajax/execute_booking.php",
				method: "POST",
				success: function (result, request) {
									
					var jsonData = Ext.util.JSON.decode(result.responseText);
					var stats = jsonData.status;
					var isCreditCards = jsonData.isCreditCard;
					var canPayLaters = jsonData.canPayLater;
					var booking_ids = jsonData.booking_id;
					var notifications = jsonData.notification;
					if(stats){
						
////////////////////////////////////generating Invoice
						Ext.MessageBox.updateProgress('Generating Invoice....');
						Ext.Ajax.request({
							url: "_ajax/execute_booking.php",
							method: "POST",
							success: function (result, request) {
								var jsonData = Ext.util.JSON.decode(result.responseText);
								var stats1 = jsonData.status;
								var invoice_ID = jsonData.invoiceID;
								var notification = jsonData.notifications;
								if(stats1){
									if(canPayLaters){
										Ext.MessageBox.updateProgress('Generating Invoice....');
										Ext.MessageBox.hide();
										Ext.MessageBox.confirm('Confirm', 'Do you want to pay the invoice ('+invoice_ID+') ?', function(btn){
											if(btn == 'yes'){
												Ext.MessageBox.confirm('Confirm', 'Yes, if payment as Agent.', function(btn){
													if(btn == 'yes'){
														payment.getPanel(bookingId,invoice_ID,1);
													}else{
														payment.getPanel(bookingId,invoice_ID,0);
													}
												 });
											}else{
												booking.getPanel2(bookingId,true);
												payment.getPanel(bookingId,invoice_ID,3);
												
												
												Ext.Ajax.request({
													url: "_ajax/execute_booking.php",
													method: "POST",
													success: function (result, request) {
														var jsonData = Ext.util.JSON.decode(result.responseText);
														var notification12 = jsonData.notifications;
														Ext.MessageBox.alert('Status', notification12);
													},
													params: {action: "autoConvertBooking", booking_id: bookingId, consultant_id: consultantId,invoiceID: invoice_ID}
													
												});
											}
										 });
									}else{
																				
										Ext.MessageBox.hide();
										Ext.MessageBox.confirm('Confirm', 'Yes, if payment as Agent.', function(btn){
											if(btn == 'yes'){
												payment.getPanel(bookingId,invoice_ID,1);
											}else{
												payment.getPanel(bookingId,invoice_ID,0);
											}
										 });
										
									
									}
								}else{
									Ext.MessageBox.hide();
									Ext.MessageBox.alert('Status', notification);
								}
								
							},
							params: {action: "autoGenerateInvoice", booking_id: bookingId, consultant_id: consultantId}
							
						});
/////////////////////////////////////////////////////////
						
					
						
						
					}else{
						Ext.MessageBox.hide();
						Ext.MessageBox.alert('Status', notifications);
					}
										
										
				},
				params: {action: "checkBooking", booking_id: bookingId, consultant_id: consultantId}
				
			});
			   
				
		
		},
		autoCancelExpiredItem: function(id_item,id_booking){
		
		
			Ext.Ajax.request({
				url: "_ajax/execute_get_booking.php",
				method: "POST",
				success: function (result, request) {
					var jsonData = Ext.util.JSON.decode(result.responseText);
                    var stats = jsonData.stats;
					var idBooking = jsonData.id_booking;
					var idItem = jsonData.id_item;
					if(stats){
						booking.getPanel2(idBooking,true);
						Ext.MessageBox.alert('Sucess', 'Item #'+idItem+' has been cancelled.');
					}else{
						booking.getPanel2(idBooking,true);
						Ext.MessageBox.alert('failure', 'Item #'+idItem+' has not been cancelled.');
					}
					 
					
					
				},
				params: {action: "autoCancelItemExpired",idItem: id_item, idbooking: id_booking}
				
			});
		
		}
	}
}();