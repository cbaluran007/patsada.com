Ext.namespace("app_space");

var tbar = function () {
	return {
		client: function(){
		
		var ds_employee_client = proj_tools.ds_combo('execute_employee.php','getClient','E_CSid','id_com','name');
		var employee_combo_client = proj_tools.getCombo(ds_employee_client,'Client\'s Name','employee_client_search','id_com','name',215,215);
		
		
		Tbar =	[	
				{xtype: "panel", border: false, bodyStyle: "padding: 0px 10px 0px 5px; background-color: #ececec; font-weigth:bold", html: "<span>Client's Name:</span>"},
				"-",
				employee_combo_client,
				'-',
				{xtype: "button",
				icon: "templates/images/icons/magnifier.png",
				iconCls: "x-btn-icon",
				text: "Search", 
				handler: function (){
				
					var id_c_combo = Ext.getCmp('employee_client_search_combo_id').getValue();
					client.grid.store.removeAll();
					client.grid.getStore().load({params: {action: "searchClient", c_id: id_c_combo}});
					
					}
				
				},
				"-",
				{xtype: "button", 
				icon: "templates/images/icons/magnifier.png",
				iconCls: "x-btn-icon", 
				text: "View All", 
				handler: function (){
				
						Ext.getCmp('employee_client_search_combo_id').setValue(0);
						Ext.getCmp('employee_client_search_combo_id').setRawValue('');
						
						client.reload();				
					}
				
				},
				"->",
				{xtype: "button", 
				icon: "templates/images/icons/printer.png",
				iconCls: "x-btn-icon", 
				id: "pro-id-print",
				text: "View/Print Summary",
				handler: function (){
						if(0 < Ext.getCmp('client-grid-id').store.getCount())
						{
							client.lookout();
						}else{
							Ext.MessageBox.alert('Notice', 'No data found.');
						}
					}
				
				}
			];
			
		return Tbar;
		},
		staff: function(){
		
		var ds_employee_staff = proj_tools.ds_combo('execute_employee.php','getStaff','E_SSid','id_client','name');
		var employee_combo_staff = proj_tools.getCombo(ds_employee_staff,'Staff\'s Name','employee_staff_search','id_client','name',215,215);
		
		
		Tbar =	[	
				{xtype: "panel", border: false, bodyStyle: "padding: 0px 10px 0px 5px; background-color: #ececec; font-weigth:bold", html: "<span>Staff's Name:</span>"},
				"-",
				employee_combo_staff,
				'-',
				{xtype: "button",
				icon: "templates/images/icons/magnifier.png",
				iconCls: "x-btn-icon",
				text: "Search", 
				handler: function (){
				
					var id_s_combo = Ext.getCmp('employee_staff_search_combo_id').getValue();
					staff.grid.store.removeAll();
					staff.grid.getStore().load({params: {action: "searchStaff", s_id: id_s_combo}});
					
					}
				
				},
				"-",
				{xtype: "button", 
				icon: "templates/images/icons/magnifier.png",
				iconCls: "x-btn-icon", 
				text: "View All", 
				handler: function (){
						Ext.getCmp('employee_staff_search_combo_id').setValue(0);
						Ext.getCmp('employee_staff_search_combo_id').setRawValue('');
						staff.reload();		
					}
				
				},
				"->",
				{xtype: "button", 
				icon: "templates/images/icons/printer.png",
				iconCls: "x-btn-icon", 
				id: "pro-id-print",
				text: "View/Print Summary",
				handler: function (){
					
						if(0 < Ext.getCmp('staff-grid-id').store.getCount())
						{
							staff.lookout();
						}else{
							Ext.MessageBox.alert('Notice', 'No data found.');
						}
			
					}
				
				}
			];
			
		return Tbar;
		},
		employee: function() {
		
		Tbar =	[	
				{xtype: "button",
				icon: "templates/images/icons/magnifier.png",
				iconCls: "x-btn-icon",
				text: "Search Category", 
				handler: function (){
				
						employee.searchPanel();				
					}
				
				},
				"-",
				{xtype: "button", 
				icon: "templates/images/icons/magnifier.png",
				iconCls: "x-btn-icon", 
				text: "View All Active", 
				handler: function (){
				
						employee.reload();
							
					}
				
				},
				"-",
				{xtype: "button", 
				icon: "templates/images/icons/magnifier.png",
				iconCls: "x-btn-icon", 
				text: "View All Inactive", 
				handler: function (){
				
						employee.grid.store.removeAll();
						employee.grid.getStore().load({params: {action: "viewEmployeeIn"}});
							
					}
				
				},
				"->",
				{xtype: "button", 
				icon: "templates/images/icons/printer.png",
				iconCls: "x-btn-icon", 
				id: "pro-id-print",
				text: "View/Print Summary",
				handler: function (){
					
						employee.lookout();
			
					}
				
				}
			];
			
		return Tbar;
		
		}
	}
}();