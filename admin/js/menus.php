<?php session_start(); ?>
Ext.namespace("app_space");

app_space.tbar = [
		
					{	
						text: "<strong>Refresh App</strong>",
						icon: "images/icons2/dark/24px/24_refresh.png",
						iconCls: "blist",
						iconAlign:'top',
						scale: 'medium',
						handler: function () {
							window.location.href = 'appv1.php ';
						}
					},
					'-',
					<?php if($_SESSION["ROLE"]=="S" || $_SESSION["IS_AGENCY_MANAGER"]==1): ?>
					{	
						text: "<strong>System</strong>",
						icon: "images/icons2/dark/24px/24_settings.png",
						iconCls: "blist",
						xtype:'splitbutton',
						iconAlign:'top',
						scale: 'medium',
						menu: new Ext.menu.Menu({
						items: [
								<?php if($_SESSION["ROLE"]=="S"):?>
								{	
								text: "Configuration",
								icon: "images/icons/cog.png",
								iconCls: "blist",
								handler: function () {
									configuration.getLoadPanel();
									}
								},
								'-',
								<?php endif; ?>
								{
								text: "Operators",
								icon: "images/icons/group_add.png",
								iconCls: "blist",
								menu: new Ext.menu.Menu({
									items: [{	
											text: "Consultant Manager",
											icon: "images/icons/group.png",
											iconCls: "blist",
											menu: new Ext.menu.Menu({
												items: [{	
													text: "Add New Consultant",
													icon: "images/icons/user_add.png",
													iconCls: "blist",
													handler: function () {
														user_man_agent.getPanel();
														}
													},
													{	
													text: "View/Update Consultant",
													icon: "images/icons/user_edit.png",
													iconCls: "blist",
													handler: function () {
														user_man_agent.viewUser();
														}
													}
													
													]
											})
											}
											<?php if($_SESSION["ROLE"]=="S"):?>
											,'-',
											{	
											text: "System User Manager",
											icon: "images/icons/group.png",
											iconCls: "blist",
											menu: new Ext.menu.Menu({
											items: [{	
													text: "Add New System User",
													icon: "images/icons/user_add.png",
													iconCls: "blist",
													handler: function () {
														user_man.getPanel();
														}
													},
													{	
													text: "View/Update System User",
													icon: "images/icons/user_edit.png",
													iconCls: "blist",
													handler: function () {
														user_man.viewUser();
														}
													}
													
													]
												})
											}
											<?php endif; ?>
										]
											
									})
								}
								
							]
								
						})
					},
					'-',
					<?php endif; ?>
					<?php if($_SESSION["ROLE"]=="S"):?>
					{	
						text: "<strong>Agency Manager</strong>",
						icon: "images/icons2/dark/24px/24_account.png",
						iconCls: "blist",
						xtype:'splitbutton',
						iconAlign:'top',
						scale: 'medium',
						menu: new Ext.menu.Menu({
						items: [{	
								text: "Add New Agency",
								icon: "images/icons/group_add.png",
								iconCls: "blist",
								handler: function () {
									agency.getPanel();
									}
								},
								{	
								text: "View/Update Agency",
								icon: "images/icons/group_edit.png",
								iconCls: "blist",
								handler: function () {
										agency.view();
									}
								}
										
								]
						})
					},
					'-',
					<?php endif; ?>
					<?php if($_SESSION["ROLE"]=="S"):?>
					{	
						text: "<strong>Supplier Manager</strong>",
						icon: "images/icons2/dark/24px/24_dropdown.png",
						iconCls: "blist",
						iconAlign:'top',
						scale: 'medium',
						menu: new Ext.menu.Menu({
						items: [{	
								text: "Add New Supplier",
								icon: "images/icons/group_add.png",
								iconCls: "blist",
								handler: function () {
									supplier.getPanel();
									}
								},
								{	
								text: "View/Update Supplier",
								icon: "images/icons/group_edit.png",
								iconCls: "blist",
								handler: function () {
										supplier.view();
									}
								}
										
								]
						})
					},
					'-',
					<?php endif; ?>
					<?php if($_SESSION["ROLE"]=="S"):?>
					{	
						text: "<strong>Service Manager</strong>",
						icon: "images/icons2/dark/24px/24_archive.png",
						iconCls: "blist",
						iconAlign:'top',
						scale: 'medium',
						handler: function () {
								service.view();
							}
						
					},
					'-',
					<?php endif; ?>
					<?php if($_SESSION["ROLE"]=="S"):?>
					{	
						text: "<strong>Products Manager</strong>",
						icon: "images/icons2/dark/24px/24_catalogue.png",
						iconCls: "blist",
						iconAlign:'top',
						scale: 'medium',
						handler: function () {
							products.view();
						}
						
					},
					'-',
					<?php endif; ?>
					{	
						text: "<strong>Search</strong>",
						icon: "images/icons2/dark/24px/24_search.png",
						iconCls: "blist",
						iconAlign:'top',
						scale: 'medium',
						handler: function () {
						
								Ext.Ajax.request({
									url: "_ajax/execute_get_booking.php",
									method: "POST",
									success: function () {
										
										
										
									},
									params: {action: "getNewBooking"}
									
								});
								search.view();
							}
						
					},
					'-',
					{	
						text: "<strong>Bookings</strong>",
						icon: "images/icons2/dark/24px/24_form.png",
						iconCls: "blist",
						iconAlign:'top',
						scale: 'medium',
						handler: function () {
								booking_view.getPanel();
							}
						
					},
					'-',
					<?php if($_SESSION["ROLE"]=="S"):?>
					{	
						text: "<strong>Accounting</strong>",
						icon: "images/icons2/dark/24px/24_carousel.png",
						iconCls: "blist",
						xtype:'splitbutton',
						iconAlign:'top',
						scale: 'medium'
					},
					'-',
					<?php endif; ?>
					{	
						text: "<strong>LOGOUT</strong>",
						iconAlign:'top',
						icon: "images/icons2/dark/24px/24_logout.png",
						iconCls: "blist",
						scale: 'medium',
						handler: function () {
							window.location.href = 'logout.php ';
						}
					},
					'->',
					{xtype: "panel",  border: false, html: "<img src='images/logo2.png' style=\"height: 48px;\">"}
			
			
		];