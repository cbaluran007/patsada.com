Ext.namespace("app_space");

var payment = function () {
	return {
		getPanel: function(bookingID,invoiceID,asAgent){
		
			if(Ext.getCmp('min_win_invoice_payment')){
			Ext.getCmp('win_invoice_payment').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_invoice_payment'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_invoice_payment').close();}catch(err){}
			
			if(asAgent != 3){
				var panelInv = {
							title: 'I N V O I C E',
							width: '50%',
							height: '100%',
							region:'west',
							margins:'3 0 3 3',
							cmargins:'3 3 3 3',
							layout: "fit",
							collapsible: true,
							split: true,
							autoScroll:true,
							autoEl : {
									title: 'I N V O I C E',
									   tag : "iframe",
									   height: '100%',
									   width: '100%',
									   id: "invoicePageId",
									   src : "_documents/invoice-"+invoiceID+".pdf"
									}
						};
			}else{
				var panelInv = {
							title: 'I N V O I C E',
							width: '100%',
							height: '100%',
							region:'center',
							margins:'3 0 3 3',
							cmargins:'3 3 3 3',
							layout: "fit",
							autoScroll:true,
							autoEl : {
									title: 'I N V O I C E',
									   tag : "iframe",
									   height: '100%',
									   width: '100%',
									   id: "invoicePageId",
									   src : "_documents/invoice-"+invoiceID+".pdf"
									}
						};
			}
			var invoiceView = new Ext.Panel(panelInv);

			if(PAYMENT_IS_LIVE == 1){
			
				var panelPay =	{
						title: 'P A Y M E N T',
						height: '100%',
						region:'center',
						margins:'3 0 3 3',
						cmargins:'3 3 3 3',
						layout: "fit",
						collapsible: true,
						split: true,
						autoScroll:true,
						autoEl : {
								title: 'I N V O I C E',
								   tag : "iframe",
								   width: '100%',
								   height: '100%',
								   id: "paymentPageId",
								   src : "modules/payment_gateway.php?action=invoicePaymentAuto&inv_booking_id="+bookingID+"&invoice_id="+invoiceID+"&asAgent="+asAgent
						}
					};
		
			}else{
				var panelPay =	{
						title: 'P A Y M E N T',
						height: '100%',
						region:'center',
						margins:'3 0 3 3',
						cmargins:'3 3 3 3',
						layout: "fit",
						collapsible: true,
						split: true,
						autoScroll:true,
						autoEl : {
								title: 'I N V O I C E',
								   tag : "iframe",
								   width: '100%',
								   height: '100%',
								   id: "paymentPageId",
								   src : "modules/payment_landing_test.php?payment_reference="+bookingID+"-"+invoiceID+"|00000&payment_amount=00.00&asAgent="+asAgent
						}
					};
			}
			
			
			var paymentView = new Ext.Panel(panelPay);
			if(asAgent != 3){
			proj_tools.new_win_grid('win_invoice_payment','100%',550,[invoiceView,paymentView],'Invoice & Payments');
			}else{
			proj_tools.new_win_grid('win_invoice_payment','100%',550,[invoiceView],'Invoice');
			}
		
		},
		close: function(){
		Ext.getCmp('win_invoice_payment').close();

		},
		manualPayInvoice: function(bookingId,invoice_ID){
							Ext.MessageBox.confirm('Confirm', 'Yes, if payment as Agent.', function(btn){
											if(btn == 'yes'){
												payment.getPanel(bookingId,invoice_ID,1);
											}else{
												payment.getPanel(bookingId,invoice_ID,0);
											}
										 });
		}
	}
}();