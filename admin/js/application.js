Ext.namespace("app_space");


app_space.app = function () {
	return {
				
		init: function () {

			var iconsHtml = ' ';
			app_space.tb = new Ext.Toolbar();
			
			app_space.viewPort = new Ext.Viewport({			
			    layout: "border",	
				items: [{
			        region: "north",			   
			    	tbar: app_space.tbar,
					height:55,
					bodyStyle: "background-color:#C0C0C0;",
					margins:'0 0 5px 0',
					border: false
			    }, 
				{
			        region: "west",		
					bodyStyle: "position: relative;background-color:#eeeeee;background: white url('images/cross-pattern.png')  repeat; color:#15428b; font-size:11px; padding-top:5px;",
					width: '100%',
					html: iconsHtml,
					height: '100%',
					id:'south_doc',
					border: false
			    }, 
				{
			        region: "center",			   
			    	margins:'0 0 0 0',
					border: false
			    }, 
				{
			        region: "south",
					margins:'0 0 0 0',
					html: "<div align='center' style=\"background-color:#e7e7e7; color:#434343; font-size:11px; padding:8px 0 8px 0;\">Eastern Eurotours &copy; All Rights Reserved 2013</div>",
			        split: false,
					height: 50,
			        minHeight: 100,
					id:'task_bar',
					tbar:[{
							xtype: "button",
							icon: "images/icons2/dark/16px/16_search.png",
							iconCls: "blist",
							handler: function () {
						
									Ext.Ajax.request({
										url: "_ajax/execute_get_booking.php",
										method: "POST",
										success: function () {
											search.view();
										},
										params: {action: "getNewBooking"}
										
									});
								
								}
							},
							'-',
							{
							icon: "images/icons2/dark/16px/16_form.png",
							iconCls: "blist",
							handler: function () {

									booking_view.getPanel();
									//paxitem.getPanel(2,1);
								}
							},
							'-',
							'-'
						]
					
			    }]
			});
			
			
		}
	} //end return
}();