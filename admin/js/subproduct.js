Ext.namespace("app_space");

var sub_product = function () {
	return {
		getPanel: function(){
		
		if(Ext.getCmp('min_win_create_sub_product_form')){
		Ext.getCmp('win_create_sub_product_form').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_sub_product_form'));
		Ext.getCmp('task_bar').doLayout();
		}
		
		try{Ext.getCmp('win_create_sub_product_form').close();}catch(err){}
		
		
		
			var maintabs = new Ext.TabPanel({
					activeTab: 0,
					width:"100%",
					border:false,
					frame: true,
					id:'subproduct_tab_manager',
					items:[
						
						{
							title: 'Subproduct Level',
							id: 'product_level_tab_id',
							frame: true,
							items: [sub_product.sub_product_level()],
							buttons: [
								{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: sub_product.submitSubProductInfo}
								
							]
						},
						{
							title: 'Rate Level',
							id: 'rate_level_tab_id',
							frame: true,
							items: [sub_product.rate_level()],
							buttons: [
								{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: sub_product.submitSubProductRate}
								
							]
						}
					]
					
				});
				
			
			
			
			
			//validator.runThis();
			proj_tools.new_win('win_create_sub_product_form','75%',400,maintabs,'New Subproduct');
			Ext.getCmp('subProductRateGridPaxId').hide();
			var tabPanel = Ext.getCmp('subproduct_tab_manager');
			tabPanel.hideTabStripItem("rate_level_tab_id");
			/*Ext.getCmp('product_tab_manager_cont').setActiveTab(5);
			Ext.getCmp('product_tab_manager_cont').setActiveTab(4);
			Ext.getCmp('product_tab_manager_cont').setActiveTab(3);	
			Ext.getCmp('product_tab_manager_cont').setActiveTab(2);
			Ext.getCmp('product_tab_manager_cont').setActiveTab(1);
			Ext.getCmp('product_tab_manager_cont').setActiveTab(0);*/
		},
		sub_product_level: function(){
		
			
		
			var form = new Ext.Panel({
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				id:"formsubproductid",
				border:false, 
				layout:'column',
                autoScroll:true,
				items:[
					{
						columnWidth:.50,
						baseCls:'x-plain',
						bodyStyle:'padding:10px',
						items:	new Ext.FormPanel({
										frame:false,
										bodyStyle:'padding:10px 5px 10px 5px;',
										frame:false, 
										border:false, 
										defaultType:'textfield',
										id:"subProductFormID",
										items:[
											{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Subproduct Name',	name:'subproductnameid', id:'subproductnameid',width: "95%",
											enableKeyEvents: true,
											listeners: {
												 keyup: 
													 function (obj, e) 
													 {
														var code = proj_tools.generateCode(obj.getValue());
														Ext.getCmp('subproductcodeid').setValue(code);
													 }
											}

											},
											{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Subproduct Code',	name:'subproductcodeid', id:'subproductcodeid',width: "50%" },
											{ xtype: 'hidden',allowBlank:false, name:'subproductid', id:'subproductid',width: "50%" ,value:0},
											{xtype: 'checkbox', checked: true, fieldLabel: 'Is Active?',  name: 'is_sub_product_active', id:'is_sub_product_active'}
											]
									})
						
					}
       
				]
			});
			return form;
		},
		rate_level: function(){
		
			var store = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_subproduct.php?action=viewSubPax",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewsubProductPaxConfig",
		            id: "id_viewSubPaxRate_manager",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "sub_product_pax_id", mapping: "sub_product_pax_id"},
            			{name: "sub_product_id", mapping: "sub_product_id"},
						{name: "from", mapping: "from"},
						{name: "to", mapping: "to"},
						{name: "is_adult", mapping: "is_adult"},
						{name: "is_child", mapping: "is_child"},
						{name: "is_infant", mapping: "is_infant"},
						{name: "mark_up", mapping: "mark_up"},
						{name: "name", mapping: "name"},
						{name: "sub_pax_rate_id", mapping: "sub_pax_rate_id"},
						{name: "rate_id", mapping: "rate_id"},
						{name: "gross_amount", mapping: "gross_amount"},
						{name: "nett_amount", mapping: "nett_amount"}
        			]
					}),
					sortInfo:{field: 'sub_product_pax_id', direction: "DESC"}
				});
		
			var gridSubProductPaxRate = new Ext.grid.GridPanel({
				store: store,
				id: "subProductRateGridPaxId",
				cm: new Ext.grid.ColumnModel({
						columns: [{
							id: 'cm_subproductIDRate',
							header: 'ID',
							dataIndex: 'sub_product_pax_id',
							width: 20
						}, 
						{
							header: 'Name',
							id:'id-grid-details-subpaxconfigrate-name',
							dataIndex: 'name',
							width: 50
						},
						{header: "Is Adult", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_adult", renderer: render.YesNo},
						{header: "Is Child", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_child", renderer: render.YesNo},
						{header: "Is Infant", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_infant", renderer: render.YesNo},
						{header: "Pax Value From", width:50,  sortable: true, locked:true,align:'left', dataIndex: "from"},
						{header: "Pax Value To", width:50,  sortable: true, locked:true,align:'left', dataIndex: "to"},
						{header: "Gross Amt", width:50,  sortable: true, locked:true,align:'left', dataIndex: "gross_amount"},
						{header: "Nett Amt", width:50,  sortable: true, locked:true,align:'left', dataIndex: "nett_amount"},
						{header: "Action", width:50,  sortable: true, locked:true,align:'left', dataIndex: "sub_product_pax_id", renderer: function(val, params, record){
							
							return "<a href=\"#\" onClick=\"sub_product.editSubpaxRate('"+val+"' );\"><img src='images/icons/folder_edit.png' alt='edit'></a> <a href=\"#\" onClick=\"sub_product.removedSubpaxRate('"+val+"','"+record.data.sub_pax_rate_id+"' );\"><img src='images/icons/delete.png' alt='remove'></a>";
						}
						
						}
						]
					}),
				autoExpandColumn: "id-grid-details-subpaxconfigrate-name",
				border: false,
				viewConfig: {
					forceFit:true
				}/*,
				listeners:  {
					rowcontextmenu: product_create.rowImageContextMenu
				}*/,
				width: '100%',
				//border: true,
				layout: 'fit',
				//plugins: expander,
				height: 275,
				tbar: [
					{
						text: 'Add Pax Configuration',
						icon: "images\/icons\/folder_user.png",
						handler: function () {
							sub_product.formSubproductConfigPax();
						}
					}
				]
				
			});
		sub_product.gridSubProductPaxRate = gridSubProductPaxRate;
		sub_product.gridSubProductPaxRate.getStore().load();
		
			var form = new Ext.Panel({
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				id:"formsubproductrateid",
				border:false, 
				layout:'column',
                autoScroll:true,
				items:[
					{
						columnWidth:.30,
						baseCls:'x-plain',
						bodyStyle:'padding:10px',
						items:	new Ext.FormPanel({
										frame:false,
										bodyStyle:'padding:10px 5px 10px 5px;',
										frame:false, 
										border:false, 
										defaultType:'textfield',
										id:"ratesubProductFormID",
										items:[
											{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Rate Name',	name:'subproductratenameid', id:'subproductratenameid',width: "95%",
											enableKeyEvents: true,
											listeners: {
												 keyup: 
													 function (obj, e) 
													 {
														var code = proj_tools.generateCode(obj.getValue());
														Ext.getCmp('subproductratecodeid').setValue(code);
													 }
											}

											},
											{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Rate Code',	name:'subproductratecodeid', id:'subproductratecodeid',width: "50%" },
											{ xtype: 'hidden',allowBlank:false, name:'subproductrateid', id:'subproductrateid',width: "50%" },
											{xtype: 'checkbox', checked: true, fieldLabel: 'Is Active?',  name: 'is_sub_product_rate_active', id:'is_sub_product_rate_active'}
											]
									})
						
					},
					{
						columnWidth:.70,
						baseCls:'x-plain',
						id:"subConfigRatePaxId",
						bodyStyle:'padding:10px',
						items:
						gridSubProductPaxRate
						
					}
       
				]
			});
			return form;
		},
		formInExclusion: function(){
		
			if(Ext.getCmp('min_win_create_sub_product_inex')){
			Ext.getCmp('win_create_sub_productinex').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_sub_product_inex'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_create_sub_productinex').close();}catch(err){}
			
			var storeInExType = new Ext.data.ArrayStore({
				fields: ['value','display'],
				data : [ ['1','Inclusion'], ['2','Exclusion']]
			});	
			var today = new Date();
			var forDate = new Date();
			forDate.setDate(today.getDate()+2);
			var forDate2 = new Date();
			forDate2.setDate(today.getDate()+3);
			var fdate = forDate.format("d/m/Y");
			var tdate = forDate2.format("d/m/Y");
			
			var form123 = new Ext.FormPanel({
										bodyStyle:'padding:10px 5px 10px 5px;',
										frame:false, 
										border:false, 
										defaultType:'textfield',
										id:"rateInExFormID",
										items:[
											proj_tools.getSimpleCombo(storeInExType,'Type','inex_type','display','value'),
											{
												fieldLabel: 'Start Date',
												xtype: 'datefield',
												name: 'startdt2',
												id: 'startdt2',
												format: 'd/m/Y',
												width:'100%',
												vtype: 'daterange',
												allowBlank:false,
												minValue: forDate,
												value:fdate,
												listeners : {
													'select' : function(field, newValue, oldValue) {
														//alert(this.getValue());
														var fftodatate = new Date();
														var ffdate = new Date(this.getValue());
														fftodatate.setDate(ffdate.getDate()+1);
														var fthisdate = fftodatate.format("d/m/Y");
														Ext.getCmp("enddt2").setValue(fthisdate);
														
													}
												},
												endDateField: 'enddt2' // id of the end date field
											 },{
												fieldLabel: 'End Date',
												xtype: 'datefield',
												name: 'enddt2',
												width:'100%',
												format: 'd/m/Y',
												allowBlank:false,
												id: 'enddt2',
												//vtype: 'daterange',
												value:tdate,
												startDateField: 'startdt2' // id of the start date field
											}
											],
										buttons: [
											{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: sub_product.submitInEx}
											
										]
									});
			validator.runThis();						
			proj_tools.new_win('win_create_sub_productinex',350,300,form123,'Inclusion Exclusion Form');
			
		},
		submitSubProductInfo: function(){
			//alert(1);
			Ext.getCmp('subProductFormID').getForm().submit({
						
							url: '_ajax/execute_subproduct.php',
							method: 'post',
							success: function () {
								product_create.gridSubProduct.getStore().load();
								//Ext.getCmp('subConfigPaxId').show();
								var tabPanel = Ext.getCmp('subproduct_tab_manager');
								tabPanel.unhideTabStripItem("rate_level_tab_id");
								Ext.MessageBox.alert('Success', 'Subproducts has been successfully saved.');
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to save subproduct. Please try again.');
							},
							
							params: {action: "submitSubproduct"}
							
			});
		
			
		},
		submitSubProductRate: function(){
		
			Ext.getCmp('ratesubProductFormID').getForm().submit({
						
							url: '_ajax/execute_subproduct.php',
							method: 'post',
							success: function () {
								Ext.getCmp('subProductRateGridPaxId').show();
								Ext.MessageBox.alert('Success', 'Rate has been successfully saved.');
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to save rates. Please try again.');
							},
							
							params: {action: "submitSubproductRate"}
							
			});
		},
		formSubproductConfigPax: function(){
		
			if(Ext.getCmp('min_win_create_subproduct_config_form')){
			Ext.getCmp('win_create_subproduct_config_form').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_subproduct_config_form'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_create_subproduct_config_form').close();}catch(err){}
			
			
			var storeSeasons = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_subproduct.php?action=viewSubSeasons",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "view_in_ex_clussion",
		            id: "id_viewSubPaxRateSeasons_manager",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "in_ex_clussion_id", mapping: "in_ex_clussion_id"},
            			{name: "subproduct_id", mapping: "subproduct_id"},
						{name: "is_in", mapping: "is_in"},
						{name: "is_ex", mapping: "is_ex"},
						{name: "date_from", mapping: "date_from"},
						{name: "date_to", mapping: "date_to"}
			
        			]
					}),
					sortInfo:{field: 'in_ex_clussion_id', direction: "DESC"}
				});
		
			var gridSubProductPaxRateSeasons = new Ext.grid.GridPanel({
				store: storeSeasons,
				id: "subProductRateGridPaxSeasonsId",
				cm: new Ext.grid.ColumnModel({
						columns: [{
							id: 'cm_subproductIDRateSeasons',
							header: 'ID',
							dataIndex: 'in_ex_clussion_id',
							width: 20
						}, 
						{
							header: 'Subproduct ID',
							dataIndex: 'subproduct_id',
							width: 20
						}, 
						
						{header: "Is Inclusion", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_in", renderer: render.YesNo},
						{header: "Is Exclusion", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_ex", renderer: render.YesNo},

						{header: "From", width:50,  sortable: true, locked:true,align:'left', dataIndex: "date_from"},
						{header: "To", width:50,  sortable: true, locked:true,align:'left', dataIndex: "date_to"},
						{header: "Action", width:50,  sortable: true, locked:true,align:'left', dataIndex: "in_ex_clussion_id", renderer: function(val, params, record){
								return "<a href=\"#\" onClick=\"sub_product.removedInEx('"+val+"');\"><img src='images/icons/delete.png'></a>";
							}
						}
						]
					}),
				autoExpandColumn: "id-grid-details-subpaxconfigrateseasons-name",
				border: false,
				viewConfig: {
					forceFit:true
				}/*,
				listeners:  {
					rowcontextmenu: product_create.rowImageContextMenu
				}*/,
				width: '100%',
				//border: true,
				layout: 'fit',
				//plugins: expander,
				height: 275,
				tbar: [
					{
						text: 'Add Inclusion or Exclusion Seasons',
						icon: "images\/icons\/folder_user.png",
						handler: function () {
							sub_product.formInExclusion();
						}
					}
				]
				
			});
		sub_product.gridSubProductPaxRateSeasons = gridSubProductPaxRateSeasons;
		sub_product.gridSubProductPaxRateSeasons.getStore().load();
			
			
			var paxtType = new Ext.data.ArrayStore({
				fields: ['value','display'],
				data : [ ['1','Adult'], ['2','Child'],['3','Infant']]
			});		
			
			var subproductConfigSetup = new Ext.Panel({
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:false, 
				border:false, 
				layout:'column',
				items:[
				{
						columnWidth:.40,
						baseCls:'x-plain',
						bodyStyle:'padding:10px',
						items:	new Ext.FormPanel({
										frame:false,
										bodyStyle:'padding:10px 5px 10px 5px;',
										frame:false, 
										border:false, 
										defaultType:'textfield',
										id:"subproductConfigSetupID",
										items:[
											proj_tools.getSimpleCombo(paxtType,'Pax Type','sub_prod_config_pax_type','display','value'),
											{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Name',	name:'conf_name', id:'conf_name',width: "95%"},
											{ xtype: 'numberfield',allowBlank:false,  fieldLabel: 'Pax Value From',	name:'val_from', id:'val_from',width: "50%" ,value:0 },
											{ xtype: 'numberfield',allowBlank:false,  fieldLabel: 'Pax Value To',	name:'val_to', id:'val_to',width: "50%", value:0 },
											{ xtype: 'numberfield',allowBlank:false,  fieldLabel: 'Markup %',	name:'mark_up', id:'mark_up',width: "50%", value:.35 },
											{ xtype: 'numberfield',allowBlank:false,  fieldLabel: 'Gross',	name:'pax_gross', id:'pax_gross',width: "50%", value:0 ,
											enableKeyEvents: true,
											listeners: {
												 keyup: 
													 function (obj, e) 
													 {
														var markupAmt = Ext.getCmp('mark_up').getValue();
														var nettAmt = proj_tools.calGross(markupAmt,obj.getValue());
														
														Ext.getCmp('pax_nett').setValue(nettAmt);
													 }
											}
											},
											{ xtype: 'numberfield',allowBlank:false,  fieldLabel: 'Nett',	name:'pax_nett', id:'pax_nett',width: "50%", value:0 },
											{id: "sub-prod-config-pax-id", name: "sub-prod-config-pax-id", xtype: "hidden", value: 0}
											]
									})
						
				},
				{
						columnWidth:.60,
						baseCls:'x-plain',
						bodyStyle:'padding:10px',
						items:	gridSubProductPaxRateSeasons
				}
				

       
				],
				buttons: [
					{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: sub_product.submitconfigPax}
				]
			});
			
			proj_tools.new_win('win_create_subproduct_config_form',850,300,subproductConfigSetup,'Add Configuration Pax');
		
		},
		submitconfigPax: function(){
		
			Ext.getCmp('subproductConfigSetupID').getForm().submit({
						
							url: '_ajax/execute_subproduct.php',
							method: 'post',
							success: function () {
								
								sub_product.gridSubProductPaxRate.getStore().load();
								Ext.MessageBox.alert('Success', 'Rates Configuration has been successfully saved.');
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to save subproduct. Please try again.');
							},
							
							params: {action: "submitSubproductConfigPax", paxType: Ext.getCmp("sub_prod_config_pax_type_combo_id").getValue()}
							
			});
		},
		submitInEx: function(){
		
			Ext.getCmp('rateInExFormID').getForm().submit({
						
							url: '_ajax/execute_subproduct.php',
							method: 'post',
							success: function () {
								try{Ext.getCmp('win_create_sub_productinex').close();}catch(err){}
								sub_product.gridSubProductPaxRateSeasons.getStore().load();
								Ext.MessageBox.alert('Success', 'Seasons has been successfully saved.');
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to save subproduct. Please try again.');
							},
							params: {action: "submitSubproductInEX", inexType: Ext.getCmp("inex_type_combo_id").getValue()}
							
			});
		
		},
		removedSubpaxRate: function(v,v2){
			Ext.Ajax.request({
				url: "_ajax/execute_subproduct.php",
				method: "POST",
				success: function () {
					Ext.Ajax.request({
						url: "_ajax/execute_subproduct.php",
						method: "POST",
						success: function () {
							sub_product.gridSubProductPaxRate.getStore().load();
							Ext.MessageBox.alert('Success', 'Data has been successfully removed.');
						},
						params: {action: "removed", did:v2, tbl: 'sub_pax_rate'}
						
					});
					
				},
				params: {action: "removed", did:v, tbl: 'sub_product_pax'}
				
			});
		},
		editSubpaxRate: function(val){
			
			Ext.Ajax.request({
						url: "_ajax/execute_subproduct.php",
						method: "POST",
						success: function (result, request) {
							var tabPanel = Ext.getCmp('subproduct_tab_manager');
							tabPanel.unhideTabStripItem("rate_level_tab_id");
							
							var jsonData = Ext.util.JSON.decode(result.responseText);
							var stat = jsonData.stats;
							if(stat){
								sub_product.formSubproductConfigPax();
								var ress = jsonData.res;
								
								Ext.getCmp("conf_name").setValue(ress[0].name);
								Ext.getCmp("val_from").setValue(ress[0].from);
								Ext.getCmp("val_to").setValue(ress[0].to);
								Ext.getCmp("mark_up").setValue(ress[0].mark_up);
								Ext.getCmp("pax_gross").setValue(ress[0].gross_amount);
								Ext.getCmp("pax_nett").setValue(ress[0].nett_amount);
								Ext.getCmp("sub-prod-config-pax-id").setValue(ress[0].sub_product_pax_id);
								
								if(ress[0].is_adult =='Y'){
									Ext.getCmp("sub_prod_config_pax_type_combo_id").setValue(1);
								}else if(ress[0].is_child =='Y'){
									Ext.getCmp("sub_prod_config_pax_type_combo_id").setValue(2);
								}else{
									Ext.getCmp("sub_prod_config_pax_type_combo_id").setValue(3);
								}
								
								
				
							
							
							}
						},
						params: {action: "getsub_product_pax_data", sub_product_pax_id: val}
						
			});	
		
		},
		removedInEx: function(v){
			Ext.Ajax.request({
						url: "_ajax/execute_subproduct.php",
						method: "POST",
						success: function () {
							sub_product.gridSubProductPaxRateSeasons.getStore().load();
							Ext.MessageBox.alert('Success', 'Data has been successfully removed.');
						},
						params: {action: "removed", did:v, tbl: 'in_ex_clussion'}
						
					});
		}
	}
	
}();