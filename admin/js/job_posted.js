Ext.namespace("app_space");

var job_posted = function () {
    return {
        getPanel: function(){
            if(Ext.getCmp('min_win_job_posted_form')){
                Ext.getCmp('win_job_posted_form').show();
                Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_job_posted_form'));
                Ext.getCmp('task_bar').doLayout();
            }

            try{Ext.getCmp('win_job_posted_form').close();}catch(err){}
            var store = new Ext.data.ArrayStore({
                fields: ['value','display'],
                data : [
                    ['Anywhere.','Anywhere.'],
                    ['Butuan','Butuan'],
                    ['Cagayan','Cagayan De Oro'],
                    ['Dapitan','Dapitan'],
                    ['Davao','Davao'],
                    ['General','General'],
                    ['Gingoog','Gingoog'],
                    ['Iligan','Iligan'],
                    ['Malaybalay','Malaybalay'],
                    ['Marawi','Marawi'],
                    ['Ozamis','Ozamis'],
                    ['Pagadian','Pagadian']
                ]
            });

            var ds_category = proj_tools.ds_combo('_ajax/execute_category.php','getCategory','Cid','category_id','category_name');
            var category_combo = proj_tools.getCombo(ds_category,'Category','category_list','category_id','category_name',150,150);
            var form_job_posted = new Ext.FormPanel({
                labelAlign: 'top',
                frame:true,
                title: 'Edit Posted Jobs',
                bodyStyle:'background-color:#fff;padding:5px 5px 0',
                //width: 650,
                autoScroll:true,
                // height: 750,
                items: [{
                    fieldLabel:"Job Type",
                    xtype: 'radiogroup',
                   // title: 'Job Type',
                    defaultType: 'radio',
                    height: '50px',
                    items: [
                        {boxLabel: 'Fulltime', name: 'jobtype',id: 'jobtype_1', inputValue: 'fulltime', checked: true},
                        {boxLabel: 'Parttime', name: 'jobtype',id: 'jobtype_2', inputValue: 'parttime'},
                        {boxLabel: 'Contract', name: 'jobtype',id: 'jobtype_3', inputValue: 'contract'}
                    ]

                },   {
                    layout:'column',
                    items:[{
                        columnWidth:2.5,
                        layout: 'form',
                        items: [{
                            xtype:'textfield',
                            fieldLabel: 'Position',
                            name: 'position',
                            id: 'position_id',
                            anchor:'58%'
                        },  proj_tools.getSimpleCombo(store,'Location','location','display','value')
                        ]
                    }


                     ]
                },{
                    xtype:'htmleditor',
                    id:'job_content',
                    fieldLabel:'Job Description',
                    height:200,
                    anchor:'98%'
                } ,
                    category_combo
                    ,{
                    layout:'column',
                    items:[{
                        columnWidth:1.5,
                        layout: 'form',

                        items: [{
                            xtype:'textfield',

                            fieldLabel: 'Experience from',
                            name: 'experience',
                            id: 'experience_id',
                            anchor:'60%'
                        }]
                    }  ]
                }, {
                    xtype:'textfield',
                    fieldLabel: 'Company',
                    id:"company_id",
                    name:"company_name",
                    anchor:'60%'
                }, {
                    xtype:'hidden',
                    id:"employer_id",
                    name:"employer_id"
                },{
                    xtype:'hidden',
                    id:"job_post_id",
                    name:"job_post_id"
                }
                ],
                buttons: [{
                    text: 'Save',
                    handler: function () {
                          var categor_id=Ext.getCmp('category_list_combo_id').getValue();
                        form_job_posted.getForm().submit({
                            url: '_ajax/execute_posted_jobs.php',
                            method: 'post',
                            success: function () {
                           
                                 Ext.getCmp('win_job_posted_form').hide();
                                 Ext.MessageBox.alert('Notice', 'Successfully Saved');
                                 job_posted.grid.getStore().load();

                            },
                            failure: function () {
                                Ext.MessageBox.alert('Notice', 'Opps Some fields are empty or Unable to save file');
                            },
                            params: {action: "update_postedjobs",categorid: categor_id}
                        });
                    }
                },{
                    text: 'Cancel',
                    handler: function () {
                          job_posted.grid.getStore().load();
                        job_posted.view();


                        Ext.getCmp('win_job_posted_form').hide();
                    }
                }]
            });

            job_posted.form_job_posted = form_job_posted;


            proj_tools.new_win('win_job_posted_form',550,680,form_job_posted,'Job Posted Form');
        },
        view: function(){
            if(Ext.getCmp('min_win_job_posted_grid')){
                Ext.getCmp('win_job_posted_grid').show();
                Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_job_posted_grid'));
                Ext.getCmp('task_bar').doLayout();
            }
            try{Ext.getCmp('win_job_posted_grid').close();}catch(err){}
            var storeJP = new Ext.data.Store({
                proxy: new Ext.data.HttpProxy({
                    url: "_ajax/execute_jobs_posted.php?action=view",
                    method: "GET"
                }),
                reader: new Ext.data.JsonReader({
                    root: "view_job_post",
                    id: "id_job_post_json",
                    totalProperty: "total",
                    fields: [
                        //"fullname",
                        {name: "job_post_id", mapping: "job_post_id"},
                        {name: "jobtype", mapping: "jobtype"},
                        {name: "category_id", mapping: "category_id"},
                        {name: "position", mapping: "position"},
                        {name: "location", mapping: "location"},
                        {name: "experience", mapping: "experience"},
                        {name: "job_content", mapping: "job_content"},
                        {name: "request_deletion", mapping: "request_deletion"},
                        {name: "created_date_time", mapping: "created_date_time"},
                        {name: "employer_id", mapping: "employer_id"},
                        {name: "is_paid", mapping: "is_paid"},
                        {name: "is_active", mapping: "is_active"},
                        {name: "is_confirm", mapping: "is_confirm"},
                        {name: "is_premium", mapping: "is_premium"},
                        {name: "employer_name", mapping: "employer_name"},
                        {name: "category_name", mapping: "category_name"}
                    ]
                }),
                sortInfo:{field: 'job_post_id', direction: "DESC"}
            });

            var grid = new Ext.grid.GridPanel({
                store: storeJP,
                cm: new Ext.grid.ColumnModel([
                    {header: "Job ID", width: 30,  sortable: true, locked:true, dataIndex: "job_post_id"},
                    {header: "Employer Name", width:50,  sortable: true, locked:true,align:'left', dataIndex: "employer_name"},
                    {id: "id-grid-details-job_posted",header: "Position", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "position"},
                    {header: "Job Description", width:50,  sortable: true, locked:true,align:'left', dataIndex: "job_content"},
                    {header: "Confirm", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_confirm", renderer: render.YesNo},
                    {header: "Created Date", width:50,  sortable: true, locked:true,align:'left', dataIndex: "created_date_time"},
                    {header: "location", width:50,  sortable: true, locked:true,align:'left', dataIndex: "location"},
                    {header: "Request for Deletion", width:50,  sortable: true, locked:true,align:'left', dataIndex: "request_deletion",renderer: render.YesNo}
                ]),
                loadMask: true,
                autoExpandColumn: "id-grid-details-job_posted",
                border: false,
                viewConfig: {
                    forceFit:true
                },
                listeners:  {
                    rowcontextmenu: job_posted.rowContextMenu
                },
                 tbar: [
                    'Search Job Title: ', ' ',
                    new Ext.ux.form.SearchField({
                        store: storeJP,
                        width:520
                    })
                ],
                  bbar: new Ext.PagingToolbar({
                        pageSize: 15,
                        store: storeJP,
                        displayInfo: true,
                        displayMsg: 'Displaying Job Posted {0} - {1} of {2}',
                        emptyMsg: "No Job Posted to display",
                        items:[
                            '-', {
                                pressed: true,
                                enableToggle:true,
                                text: 'Add New Jobs',
                                icon: "images/icons/application_add.png",
                                handler: function () {
                                    post_jobs.getPanel();
                                }
                            }]
                       })
                 });


            job_posted.grid = grid;
            job_posted.grid.getStore().load();
            proj_tools.new_win('win_job_posted_grid','75%',400,grid,'Job Posted List');
        },
        rowContextMenu: function (grid, rowIndex, e) {
            e.stopEvent();
            grid.getSelectionModel().selectRow(rowIndex, false);
            job_posted.row = grid.getSelectionModel().getSelected();
            var coords = e.getXY();
            var myContextMenu = job_posted.menus();
            if ( job_posted.row.get("is_confirm") == 'Y' || job_posted.row.get("is_confirm") == 1) {
                Ext.getCmp("job-posted-menu-active").disable();
            } else {
                Ext.getCmp("job-posted-menu-deactive").disable();
            }
             if ( job_posted.row.get("request_deletion") == 'Y' || job_posted.row.get("request_deletion") == 1) {
                
            } else {
                Ext.getCmp("job-posted-menu-delete_request").disable();
            }
            myContextMenu.showAt([coords[0], coords[1]]);

        },
        menus: function(){

            if(Ext.getCmp('service-grid-context-menu')){
                Ext.getCmp('service-grid-context-menu').removeAll();
            }

            var myContextMenu = new Ext.menu.Menu({
                id: "service-grid-context-menu",
                items: [
                    {
                        text: "Confirm",
                        id: "job-posted-menu-active",
                        iconCls: "approvedIcon",
                        icon: "images/icons/accept.png",
                        handler: job_posted.setactive
                    },
                    {
                        text: "Un Confirm",
                        id: "job-posted-menu-deactive",
                        iconCls: "disapprovedIcon",
                        icon: "images/icons/cross.png",
                        handler: job_posted.setdeactive
                    },
                    '-',
                    {
                        text: "Edit",
                        id: "job-posted-menu-edit",
                        iconCls: "approvedIcon",
                        icon: "images/icons/folder_edit.png",
                        handler: job_posted.edit
                    },

                    '-',
                    {
                        text: "Confirm Deletion",
                        id: "job-posted-menu-delete_request",
                        iconCls: "disapprovedIcon",
                        icon: "images/icons/folder_delete.png",
                        handler: job_posted.delete_request
                    },
                    '-',
                    {
                        text: "Permanent Delete",
                        id: "job-posted-menu-delete",
                        iconCls: "approvedIcon",
                        icon: "images/icons/folder_delete.png",
                        handler: job_posted.delete2
                    },
                ]
            });

            return myContextMenu;
        },
        edit: function(){
            job_posted.getPanel();

            Ext.getCmp('position_id').setValue(job_posted.row.get("position"));
            Ext.getCmp('job_content').setValue(job_posted.row.get("job_content"));
            Ext.getCmp('experience_id').setValue(job_posted.row.get("experience"));
            Ext.getCmp('company_id').setValue(job_posted.row.get("employer_name"));
            Ext.getCmp('employer_id').setValue(job_posted.row.get("employer_id"));
            Ext.getCmp('job_post_id').setValue(job_posted.row.get("job_post_id"));
            var jbtype= job_posted.row.get("jobtype");
            if(jbtype == 'fulltime'){
                Ext.getCmp('jobtype_1').setChecked(true);
            }else if (jbtype == 'parttime'){
                Ext.getCmp('jobtype_2').setChecked(true);
            }else if(jbtype == 'contract'){
                Ext.getCmp('jobtype_3').setChecked(true);
            }

            Ext.getCmp('category_list_combo_id').setValue(job_posted.row.get('category_id'));
            Ext.getCmp('category_list_combo_id').setRawValue(job_posted.row.get('category_name'));
            Ext.getCmp('location_combo_id').setValue(job_posted.row.get('location'));
            Ext.getCmp('location_combo_id').setRawValue(job_posted.row.get('location'));
           /* service.getPanel();
            //alert(user_man.row.get("title"));
            Ext.getCmp('serviceid').setValue(service.row.get("service_name"));
            Ext.getCmp('service-id').setValue(service.row.get("id_service"));
*/

        },
        setactive: function(){
            Ext.Ajax.request({
                url: "_ajax/execute_jobs_posted.php",
                method: "POST",
                success: function () {
                    job_posted.grid.getStore().load();
                },
                params: {action: "activateConfirm", jobposted_id: job_posted.row.get("job_post_id")}

            });
        },
        setdeactive: function(){
            Ext.Ajax.request({
                url: "_ajax/execute_jobs_posted.php",
                method: "POST",
                success: function () {
                    job_posted.grid.getStore().load();
                },
                params: {action: "deactivateConfirm", jobposted_id: job_posted.row.get("job_post_id")}

            });
        },

        delete2: function(){
            var jp_id = job_posted.row.get("job_post_id");
             Ext.Ajax.request({
                url: "_ajax/execute_jobs_posted.php",
                method: "POST",
                success: function () {
                    job_posted.grid.getStore().load();
                },
                params: {action: "perDelete", jobposted_id:jp_id}

            });
        },


        delete_request: function(){
            var jp_id = job_posted.row.get("job_post_id");
              
            //Ext.MessageBox.confirm('Confirm', 'Are you sure you want to do that?');
            
        
             Ext.Ajax.request({
                url: "_ajax/execute_jobs_posted.php",
                method: "POST",
                success: function () {
                    job_posted.grid.getStore().load();
                },
                params: {action: "tempDelete", jobposted_id:jp_id}

            });
        }

    }
}();