Ext.namespace("app_space");

var search = function () {
	return {
		
		view: function(){
			
			
			
			if(Ext.getCmp('min_win_search_grid')){
			Ext.getCmp('win_search_grid').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_search_grid'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_search_grid').close();}catch(err){}
			
			
			var store = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_search.php?action=viewAccommodation",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewSearch",
		            id: "id_search",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "id_product_details", mapping: "id_product_details"},
            			{name: "product_code", mapping: "product_code"},
						{name: "product_name", mapping: "product_name"},
						{name: "is_hotel", mapping: "is_hotel"},
						{name: "is_flight", mapping: "is_flight"},
						{name: "is_tour", mapping: "is_tour"},
						{name: "is_live", mapping: "is_live"},
						{name: "service_name", mapping: "service_name"},
						{name: "supplier_name", mapping: "supplier_name"},
						{name: "is_active", mapping: "is_active"},
            			{name: "created_by", mapping: "created_by"},
						{name: "created_datetime", mapping: "created_datetime"},
						{name: "product_address", mapping: "product_address"},
						{name: "product_postal_code", mapping: "product_postal_code"},
						{name: "product_phone_number", mapping: "product_phone_number"},
						{name: "product_area", mapping: "product_area"},
						{name: "product_chain", mapping: "product_chain"},
						{name: "product_description", mapping: "product_description"},
						{name: "product_coordinates_longitude", mapping: "product_coordinates_longitude"},
						{name: "product_coordinates_latitude", mapping: "product_coordinates_latitude"},
						{name: "destination_country", mapping: "destination_country"},
						{name: "destination_city", mapping: "destination_city"},
						{name: "is_best_seller", mapping: "is_best_seller"},
						{name: "star_rating", mapping: "star_rating"},
						{name: "amenities_internal", mapping: "amenities_internal"},
						{name: "amenities_external", mapping: "amenities_external"},
						{name: "url", mapping: "url"}
						
			
        			]
					}),
					sortInfo:{field: 'product_name', direction: "DESC"}
				});
			

			
			// Custom rendering Template for the View
			var resultTpl = new Ext.XTemplate(
				'<tpl for=".">',
				'<div class="search-item">',
					'<table>',
						'<tr>',
							'<td style="width:100px;"><div class="info"><a href="javascript:products.viewPhotos(\'{product_name}\',\'{product_code}\');"><img src="{url}" class="photoDetails2"></div></td>',
							'<td style="vertical-align: top; padding: 0px 5px 0px 5px;"><h3><a>{product_name}</a> <div class="star_rating">{star_rating}</div></h3>',
							'<span class="address">{product_address},{destination_city}, {destination_country}, {product_postal_code}  <a href="javascript:products.viewMap({product_coordinates_latitude},{product_coordinates_longitude},\'{product_name}\',\'{product_address}\');"><img src="images/icons/map_magnify.png"></a></span>',
							'</td>',
						'</tr>',
						'<tr>',
							'<td colspan=2>',
								'<span class="subHeader">Description: </span> {product_description}',
							'</td>',
						'</tr>',
						
						
					'</table>',
					
				'</div></tpl>'
			);

			var grid = new Ext.Panel({
					autoScroll:true,
					layout: 'fit',
					region: 'center',
					id:'search-grid-result-products',
					loadMask: true,
					items: new Ext.DataView({
						tpl: resultTpl,
						store: store,
						loadMask: true,
						itemSelector: 'div.search-item'
					}),

					tbar: [
						'Search: ', ' ',
						new Ext.ux.form.SearchField({
							store: store,
							width:320
						})
					],

					bbar: new Ext.PagingToolbar({
						store: store,
						id:'paging',
						pageSize: 10,
						displayInfo: true,
						displayMsg: 'Topics {0} - {1} of {2}',
						emptyMsg: "No topics to display",
						listeners : { 
							beforechange: function(){ 
								Ext.getCmp('search-grid-result-products').getEl().mask('Loading...', 'x-mask-loading');
								
							},
							change: function(){ 
								
								try{Ext.getCmp('search-grid-result-products').getEl().mask().hide();}catch(err){}
							}
						}
					})
				});

			
			/*------------------search form ---------------------------*/
			
			
			var ds_service = proj_tools.ds_combo('_ajax/execute_service.php','getService','Sid','id_service','service_name');
			var service_combo = proj_tools.getCombo(ds_service,'Service Name','service_search_search','id_service','service_name',150,150);
			
			var ds_agency = proj_tools.ds_combo('_ajax/execute_agency.php','getAgency','Agencyid','id_agency','agency_name');
			var agency_combo = proj_tools.getCombo(ds_agency,'Agency Name','agency_search_search','id_agency','agency_name',150,150);
			
			var ds_destination = proj_tools.ds_combo('_ajax/execute_destination.php','getDestination','Agencyid','code','destination');
			var destination_combo = proj_tools.getCombo(ds_destination,'Destination','destination_search_search','code','destination',150,150);
			
			var ds_products = proj_tools.ds_combo('_ajax/execute_products.php','getProducts','SearchProductsid','product_code','product_name');
			var products_combo = proj_tools.getCombo(ds_products,'Hotel Names','products_search_search','product_code','product_name',150,150);
			
			var store1 = new Ext.data.ArrayStore({
				fields: ['value','display'],
				data : [ ['0','0'], ['1','1'],['2','2'],['3','3'],['4','4'],['5','5'],['6','6'],['7','7'],['8','8'],['9','9'],['10','10']]
			});
			
			var store2 = new Ext.data.ArrayStore({
				fields: ['value','display'],
				data : [ ['0','0'], ['1','1'],['2','2'],['3','3'],['4','4'],['5','5']]
			});
			
			var today = new Date();
			var forDate = new Date();
			forDate.setDate(today.getDate()+2);
			
			var searchCriteria = new Ext.FormPanel({
				labelWidth: 100, // label settings here cascade unless overridden
				url:'save-form.php',
				bodyStyle:'padding:5px 5px 0',
				border:false, 
				width: '100%',
				defaults: {width: 150},
				defaultType: 'textfield',

				items: [
					agency_combo,
					service_combo,
					destination_combo,
					{
						fieldLabel: 'Start Date',
						xtype: 'datefield',
						name: 'startdt',
						id: 'startdt',
						format: 'M d, Y',
						width:'100%',
						vtype: 'daterange',
						allowBlank:false,
						minValue: tomorrow,
						endDateField: 'enddt' // id of the end date field
					 },{
						fieldLabel: 'End Date',
						xtype: 'datefield',
						name: 'enddt',
						width:'100%',
						format: 'M d, Y',
						allowBlank:false,
						id: 'enddt',
						vtype: 'daterange',
						startDateField: 'startdt' // id of the start date field
					},
					{
						xtype: 'fieldset',
						title: 'Number of Pax',
						autoHeight: true,
						layout: 'column',
						width: '100%',
						items: [{
									// You can pass sub-item arrays along with width/columnWidth configs 
									// ColumnLayout-style for complete layout control.  In this example we
									// only want one item in the middle column, which would not be possible
									// using the columns config.  We also want to make sure that our headings
									// end up at the top of each column as expected.
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										proj_tools.getSimpleCombo2(store1,'# of Adult(s)','pax_adult','display','value',75, '2'),
										{xtype: 'label', text: '# of Adult(s)'}
									]
								},{
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										proj_tools.getSimpleCombo2(store1,'# of Child(s)','pax_child','display','value',75,'0'),
										{xtype: 'label', text: '# of Child(s)'}
									]
								},{
									columnWidth: '.30',
									border: false,
									labelWidth: 70,
									items: [
										
										proj_tools.getSimpleCombo2(store1,'# of Infant(s)','pax_infant','display','value',75,'0'),
										{xtype: 'label', text: '# of Infant(s)'}
									]
								}
								
						]
					},
					{
					xtype:'fieldset',
					checkboxToggle:true,
					title: 'Advance Search',
					autoHeight:true,
					defaultType: 'textfield',
					collapsed: true,
					id:"is_advance",
					width: "95%" ,
					items :[
						proj_tools.getSimpleCombo2(store2,'Star Ratings','star','display','value',50),
						products_combo
					]
				}
				],

				buttons: [
						{text: "Search", formBind: true, icon: "images/icons/magnifier.png", handler: search.search},
						{text: "Reset", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: search.searchReset}
						
					]
			});

			
			
			// Panel for the west
			var nav = new Ext.Panel({
				title: 'Search Product Criteria',
				region: 'west',
				split: true,
				width: 300,
				collapsible: true,
				margins:'3 0 3 3',
				cmargins:'3 3 3 3',
				items: [searchCriteria]
			});
			validator.runThis();
			search.store = store;
			search.searchCriteria = searchCriteria;
			search.grid = grid;
			proj_tools.new_win_grid('win_search_grid',1050,550,[nav,grid],'Search List');
			
			
		},
		search:function () {
			
			var parr = {};
			parr["agency_id"] = Ext.getCmp("agency_search_search_combo_id").getValue();
			parr["service_id"] = Ext.getCmp("service_search_search_combo_id").getValue();
			parr["desitnation_code"] = Ext.getCmp("destination_search_search_combo_id").getValue();
			parr["date_start"] = Ext.getCmp("startdt").getValue();
			parr["date_end"] = Ext.getCmp("enddt").getValue();
			parr["adult"] = Ext.getCmp("pax_adult_combo_id").getValue();
			parr["child"] = Ext.getCmp("pax_child_combo_id").getValue();
			parr["infant"] = Ext.getCmp("pax_infant_combo_id").getValue();
			parr["star"] = Ext.getCmp("star_combo_id").getValue();
			parr["product_name"] = Ext.getCmp("products_search_search_combo_id").getValue();
			
			if(parr["agency_id"] == "" || parr["service_id"] == "" || parr["date_start"] == "" || parr["date_end"] == "" || parr["adult"] == "" || parr["child"] == "" || parr["infant"] == "" ){
				Ext.MessageBox.alert('Status', 'Unable to search products. Please fill requiered field.');
				return false;
			}
			Ext.getCmp('search-grid-result-products').getEl().mask('Loading...', 'x-mask-loading');
			search.store.load({ params: parr,scope: this,
			callback: function(records, operation, success) {
				if (success) {
					Ext.getCmp('search-grid-result-products').getEl().mask().hide();
				} else {
					Ext.getCmp('search-grid-result-products').getEl().mask().hide();
				}
			} });
			
			
		},
		searchReset:function () {
			search.searchCriteria.getForm().reset();	
		},
		rowContextMenu: function (grid, rowIndex, e) {
								
			e.stopEvent(); 
			
	        grid.getSelectionModel().selectRow(rowIndex, false);
	        products.row = grid.getSelectionModel().getSelected();
	        	
	        var coords = e.getXY();

	        var myContextMenu = products.menus();
			
			
			
			
			
	        myContextMenu.showAt([coords[0], coords[1]]);
	        	    
		},
		menus: function(){
			
			if(Ext.getCmp('products-grid-context-menu')){
				Ext.getCmp('products-grid-context-menu').removeAll();
			}
			
			var myContextMenu = new Ext.menu.Menu({
				id: "products-grid-context-menu",
				items: [
					
					{
						text: "View Product Details",
						id: "products-menu-view-details",
						iconCls: "approvedIcon",  
						icon: "images/icons/magnifier.png",
						handler: products.viewDetails
					},
					'-',
					{
						text: "Edit",
						id: "products-menu-edit",
						iconCls: "approvedIcon",  
						icon: "images/icons/folder_edit.png",
						handler: products.edit
					}
				]
			});
			
			return myContextMenu;
		},
		viewDetails: function(){
		if(Ext.getCmp('min_win_view_products')){
		Ext.getCmp('win_view_products').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_view_products'));
		Ext.getCmp('task_bar').doLayout();
		}
		try{Ext.getCmp('win_view_products').close();}catch(err){}
		var ds_details = new Ext.data.Store({
				proxy: new Ext.data.HttpProxy({
					url: '_ajax/execute_products.php?action=viewDetails&code='+products.row.get("product_code"),
					method: "GET"
					}),
				reader: new Ext.data.JsonReader({
		        root: "viewProducts",
		        id: "id_products",
		        totalProperty: "total",
		        fields: [
            			//"fullname",
					{name: "product_code", mapping: "product_code"},
            		{name: "product_name", mapping: "product_name"},
					{name: "service_name", mapping: "service_name"},
					{name: "supplier_name", mapping: "supplier_name"},
					{name: "star_rating", mapping: "star_rating"},
					{name: "product_address", mapping: "product_address"},
					{name: "destination_country", mapping: "destination_country"},
					{name: "destination_city", mapping: "destination_city"},
					{name: "product_postal_code", mapping: "product_postal_code"},
					{name: "product_phone_number", mapping: "product_phone_number"},
					{name: "product_area", mapping: "product_area"},
            		{name: "is_best_seller", mapping: "is_best_seller"},
					{name: "product_coordinates_latitude", mapping: "product_coordinates_latitude"},
					{name: "product_coordinates_longitude", mapping: "product_coordinates_longitude"},
					{name: "product_amenities_internal", mapping: "product_amenities_internal"},
					{name: "product_amenities_external", mapping: "product_amenities_external"},
					{name: "product_description", mapping: "product_description"},
					{name: "url", mapping: "url"},
					
					
					
				]
				}),
				baseParams: {limit:1}
			});
	
		var resultTpl = new Ext.XTemplate(
			'<tpl for=".">',
			'<div class="viewProductDetails">',
				'<div class="info">',
				'<a href="javascript:products.viewPhotos(\'{product_name}\',\'{product_code}\');"><img src="{url}" class="photoDetails"></a>',
				'</div>',
				'<div class="info1">',
				'<h3>{product_name}</h3>',
				'<span class="address">{product_address},{destination_city}, {destination_country}, {product_postal_code}  <a href="javascript:products.viewMap({product_coordinates_latitude},{product_coordinates_longitude},\'{product_name}\',\'{product_address}\');"><img src="images/icons/map_magnify.png"></a></span>',
				'<p>',
				'<span class="subHeader">Service Type: </span> {service_name}</br>',
				'<span class="subHeader">Supplier Name: </span> {supplier_name}</br>',
				'</p>',
				'<p>',
				'<span class="subHeader">Star Rating: </span> {star_rating} <img src="images/icons/star.png"></br>',
				'<span class="subHeader">Area: </span> {product_area}</br>',
				'<span class="subHeader">Phone Number: </span> {product_phone_number}</br>',
				
				'</p>',
				'</div>',
				'<div class="info2">',
				'<h3>Description</h3>',
				'<p>{product_description}</p>',
				'<div>',
				'<div class="info4">',
				'<h3>Amenities</h3>',
				'<p>',
				'</p>',
				'<span class="subHeader">Main Product:</span> {product_amenities_internal}</br>',
				'<span class="subHeader">Product:</span> {product_amenities_external}',
				
				'<div>',
			'</div></tpl>'
		);
		
		
		var viewPanel = new Ext.Panel({
			height:500,
			autoScroll:true,
			loadMask: true,
			items: new Ext.DataView({
				tpl: resultTpl,
				store: ds_details
			})
		});
		
		ds_details.load();
		proj_tools.new_win('win_view_products',800,500,viewPanel,'Product Details');
		products.viewPanel = viewPanel;
		viewPanel.body.highlight('#c3daf9', {block:true});
			
		},
		edit: function(){
			alert("under construction");
			
			
			
			
						
			
		},
		viewPhotos:function(ttitle,code){
			if(Ext.getCmp('min_win_create_products_view_photo')){
			Ext.getCmp('win_create_products_view_photo').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_products_view_photo'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_create_products_view_photo').close();}catch(err){}
		
			var tplPhoto = new Ext.Template(
                    '<iframe src="'+base_url+'photoGallery?code='+code+'" width=100% height=100%></iframe>'
                );
			proj_tools.new_win('win_create_products_view_photo',675,415,tplPhoto,'Photo Gallery');
			
		},
		viewMap:function(latitude,longitude,ttitle,address){
			if(Ext.getCmp('min_win_create_products_view_map')){
			Ext.getCmp('win_create_products_view_map').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_products_view_map'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_create_products_view_map').close();}catch(err){}
		
			var mapPanel = {
                    xtype: 'gmappanel',
                    zoomLevel: 14,
                    gmapType: 'map',
                    mapConfOpts: ['enableScrollWheelZoom','enableDoubleClickZoom','enableDragging'],
                    mapControls: ['GSmallMapControl','GMapTypeControl','NonExistantControl'],
                    setCenter: {
                        geoCodeAddr: address,
                        marker: {title: ttitle}
                    },
                    markers: [{
                        lat: latitude,
                        lng: longitude,
                        marker: {title: ttitle}
                    }]
                }; 
			proj_tools.new_win('win_create_products_view_map',450,450,mapPanel,'Geo Map ');
			
		}
	}
}();