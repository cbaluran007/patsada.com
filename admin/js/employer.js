Ext.namespace("app_space");

var employer = function () {
    return {
        getPanel: function(){
            if(Ext.getCmp('min_win_employer_form')){
                Ext.getCmp('win_employer_form').show();
                Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_employer_form'));
                Ext.getCmp('task_bar').doLayout();
            }

            try{Ext.getCmp('win_employer_form').close();}catch(err){}
            
              var form = new Ext.FormPanel({
                labelWidth: 100,
                bodyStyle:'padding:10px 5px 10px 5px;',
                frame:true, 
                border:false, 
                defaultType:'textfield', 
                items:[ 
                { xtype: 'textfield',allowBlank:false,  fieldLabel: 'Company Name',   name:'company_name', id:'company_id',width: "95%" },
                { xtype: 'textfield',allowBlank:false,  fieldLabel: 'User name',   name:'username', id:'user_name_id',width: "95%" },
                { xtype: 'textfield',allowBlank:false,    fieldLabel: 'Password', name:'password_r', id:'password_r_id',width: "95%" },
               { xtype: 'textfield',allowBlank:false,  fieldLabel: 'Email',   name:'email', id:'email_id',width: "95%",vtype: "email" },  
                {id: "employer_id", name: "employer_id", xtype: "hidden", value: 0}, 
                {id: "employer_user_id", name: "employer_user_id", xtype: "hidden", value: 0},
                {id: "login_credentials_id", name: "login_credentials_id", xtype: "hidden", value: 0},
                ], 
                buttons: [{
                    text: 'Save',
                    handler: function () {
                     
                        form.getForm().submit({
                            url: '_ajax/execute_get_employer.php',
                            method: 'post',
                            success: function (formPanel, action) {
                                 Ext.getCmp('win_employer_form').hide();
                                 Ext.MessageBox.alert('Notice', 'Successfully Saved');
                                 employer.grid.getStore().load();
                            },
                            failure: function (formPanel, action) {
                                var data = Ext.decode(action.response.responseText);
                                 Ext.MessageBox.alert('Notice',data.message);
                            },

                            params: {action: "newEmployer",}
                        });
                    }
                },{
                    text: 'Cancel',
                    handler: function () {
                       Ext.getCmp('win_employer_form').hide();
                    }
                }] 
            });  
  
            employer.form = form;
            proj_tools.new_win('win_employer_form',400,210,form,'Create New Employer');  
        },
        view: function(){
            if(Ext.getCmp('min_win_employer_grid')){
                Ext.getCmp('win_employer_grid').show();
                Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_employer_grid'));
                Ext.getCmp('task_bar').doLayout();
            }
            try{Ext.getCmp('win_employer_grid').close();}catch(err){}
            var storeJP = new Ext.data.Store({
                proxy: new Ext.data.HttpProxy({
                    url: "_ajax/execute_get_employer.php?action=view",
                    method: "GET"
                }),
                reader: new Ext.data.JsonReader({
                    root: "view_employer_post",
                    id: "id_employer_json",
                    totalProperty: "total",
                    fields: [
                        //"fullname",
                        {name: "employer_id", mapping: "employer_id"},
                        {name: "employer_user_id", mapping: "employer_user_id"},
                        {name: "login_credentials_id", mapping: "login_credentials_id"},
                        {name: "company_name", mapping: "company_name"},
                        {name: "password_r", mapping: "password_r"},
                        {name: "email", mapping: "email"},
                        {name: "user_name", mapping: "user_name"},
                        {name: "created_datetime", mapping: "created_datetime"},
                        {name: "is_active", mapping: "is_active"},
                        {name: "is_confirmed", mapping: "is_confirmed"}

                        ],
                    sortableortInfo:{field: 'employer_id', direction: "ASC"}
                })
            });

            var grid = new Ext.grid.GridPanel({
                store: storeJP,
                cm: new Ext.grid.ColumnModel([
                    {header: "Employer ID", width: 30,  sortable: true, locked:true, dataIndex: "employer_id"},
                    {id: "id-grid-details-employer",header: "Company Name", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "company_name"},
                    {header: "Email", width:50,  sortable: true, locked:true,align:'left', dataIndex: "email"},
                    {header: "User Name", width:50,  sortable: true, locked:true,align:'left', dataIndex: "user_name"}, 
                    {header: "Password", width:50,  sortable: true, locked:true,align:'left', dataIndex: "password_r"}, 
                    {header: "Active", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_active", renderer: render.YesNo},
                    {header: "Confirm", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_confirmed", renderer: render.YesNo},
                    {header: "Created Date", width:50,  sortable: true, locked:true,align:'left', dataIndex: "created_datetime"} 
                ]),
                loadMask: true,
                autoExpandColumn: "id-grid-details-employer",
                border: false,
                viewConfig: {
                    forceFit:true
                },
                listeners:  {
                    rowcontextmenu: employer.rowContextMenu
                },
                 tbar: [
                    'Search Employer Name: ', ' ',
                    new Ext.ux.form.SearchField({
                        store: storeJP,
                        width:520
                    })
                ],
                  bbar: new Ext.PagingToolbar({
                        pageSize: 15,
                        store: storeJP,
                        displayInfo: true,
                        displayMsg: 'Displaying Employer {0} - {1} of {2}',
                        emptyMsg: "No Employer to display",
                        items:[
                            '-', {
                                pressed: true,
                                enableToggle:true,
                                text: 'Add New Employer',
                                icon: "images/icons/application_add.png",
                                handler: function () {
                                    employer.getPanel();
                                }
                            }]
                       })
                 });


            employer.grid = grid;
            employer.grid.getStore().load();
            proj_tools.new_win('win_employer_grid','75%',400,grid,'Employer List');
        },
        rowContextMenu: function (grid, rowIndex, e) {

            e.stopEvent();
            grid.getSelectionModel().selectRow(rowIndex, false);
            employer.row = grid.getSelectionModel().getSelected();


            var coords = e.getXY();
            var myContextMenu = employer.menus();
            if ( employer.row.get("is_confirmed") == 'Y' || employer.row.get("is_confirmed") == 1) {
                Ext.getCmp("employer-menu-confirm").disable();
            } else {
                Ext.getCmp("employer-menu-unconfirm").disable();
            }

             if ( employer.row.get("is_active") == 'Y' || employer.row.get("is_active") == 1) {
                Ext.getCmp("employer-menu-active").disable();
            } else {
                Ext.getCmp("employer-menu-deactive").disable();
            }
            myContextMenu.showAt([coords[0], coords[1]]);

        },
       
        menus: function(){

            if(Ext.getCmp('service-grid-context-menu')){
                Ext.getCmp('service-grid-context-menu').removeAll();
            }

            var myContextMenu = new Ext.menu.Menu({
                id: "service-grid-context-menu",
                items: [
                     {
                        text: "Active",
                        id: "employer-menu-active",
                        iconCls: "approvedIcon",
                        icon: "images/icons/accept.png",
                        handler: employer.setactive
                    },
                    {
                        text: "Deactivate",
                        id: "employer-menu-deactive",
                        iconCls: "disapprovedIcon",
                        icon: "images/icons/cross.png",
                        handler: employer.setdeactive
                    },
                    '-',
                    {
                        text: "Confirm",
                        id: "employer-menu-confirm",
                        iconCls: "approvedIcon",
                        icon: "images/icons/accept.png",
                        handler: employer.setConfirm
                    },
                    {
                        text: "Un Confirm",
                        id: "employer-menu-unconfirm",
                        iconCls: "disapprovedIcon",
                        icon: "images/icons/cross.png",
                        handler: employer.setUnconfirm
                    },
                    '-',
                    {
                        text: "Edit",
                        id: "employer-menu-edit",
                        iconCls: "approvedIcon",
                        icon: "images/icons/folder_edit.png",
                        handler: employer.edit
                    },
                    '-',
                    {
                        text: "Permanent Delete",
                        id: "employer-menu-delete",
                        iconCls: "approvedIcon",
                        icon: "images/icons/folder_edit.png",
                        handler: employer.delete2
                    }
                ]
            });

            return myContextMenu;
        },
        edit: function(){
            employer.getPanel();

         // get the value of columns to put in the fields
         // then call the update function to Mysql

          Ext.getCmp('company_id').setValue(employer.row.get("company_name"));
          Ext.getCmp('user_name_id').setValue(employer.row.get("user_name"));
          Ext.getCmp('password_r_id').setValue(employer.row.get("password_r"));
          Ext.getCmp('email_id').setValue(employer.row.get("email")); 
          Ext.getCmp('employer_id').setValue(employer.row.get("employer_id"));
          Ext.getCmp('employer_user_id').setValue(employer.row.get("employer_user_id"));    
          Ext.getCmp('login_credentials_id').setValue(employer.row.get("login_credentials_id"));      
         // Ext.getCmp('password_idas').setValue(employer.row.get("password_r"));
          
        },
        setactive: function(){
            Ext.Ajax.request({
                url: "_ajax/execute_get_employer.php",
                method: "POST",
                success: function () {
                    employer.grid.getStore().load();
                },
                params: {action: "activate", employer_id: employer.row.get("employer_id")}

            });
        },
        setdeactive: function(){
            Ext.Ajax.request({
                url: "_ajax/execute_get_employer.php",
                method: "POST",
                success: function () {
                    employer.grid.getStore().load();
                },
                params: {action: "deactivate", employer_id: employer.row.get("employer_id")}

            });
        },
        setConfirm: function(){
            Ext.Ajax.request({
                url: "_ajax/execute_get_employer.php",
                method: "POST",
                success: function () {
                    employer.grid.getStore().load();
                },
                params: {action: "activateConfirm", employer_id: employer.row.get("employer_id")}

            });
        },
        setUnconfirm: function(){
            Ext.Ajax.request({
                url: "_ajax/execute_get_employer.php",
                method: "POST",
                success: function () {
                    employer.grid.getStore().load();
                },
                params: {action: "deactivateConfirm", employer_id: employer.row.get("employer_id")}

            });
        },
        delete2: function(){
            var e_id = employer.row.get("employer_id");
            var e_user_id = employer.row.get("employer_user_id");
            var login_cred_id = employer.row.get("login_credentials_id");
             Ext.Ajax.request({
                url: "_ajax/execute_get_employer.php",
                method: "POST",
                success: function () {
                    employer.grid.getStore().load();
                },
                params: {action: "perDelete", employer_id:e_id, emp_user_id:e_user_id, log_cred_id:login_cred_id }

            });
        }
    }
}();