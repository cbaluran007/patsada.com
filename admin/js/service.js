Ext.namespace("app_space");

var service = function () {
	return {
		getPanel: function(){
		
		if(Ext.getCmp('min_win_create_service')){
		Ext.getCmp('win_create_service').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_service'));
		Ext.getCmp('task_bar').doLayout();
		}
		
		try{Ext.getCmp('win_create_service').close();}catch(err){}
		
		var form = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:true, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Service Name',	name:'servicename', id:'serviceid',width: "95%" },
				{id: "service-id", name: "service-name", xtype: "hidden", value: 0}
       
				],
				buttons: [
						{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: service.submitUser},
						{text: "Reset", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: service.reset}
						
					]
			});

			//validator.runThis();
			proj_tools.new_win('win_create_service',400,125,form,'Create New Service');
			service.mainForm = form;	
						
		
		},
		submitUser: function(){
			var parr = {};
			parr["action"] = "submitService";
			parr["servicename"] = Ext.getCmp("serviceid").getValue();
			parr["serviceid"] = Ext.getCmp("service-id").getValue();
			service.mainForm.getForm().submit({
						
							url: '_ajax/execute_service.php',
							method: 'post',
							success: function () {
								Ext.getCmp('win_create_service').close();
								service.view();
								service.grid.getStore().load();
								Ext.MessageBox.alert('Sucess', 'Service successfully saved.');
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to saved service. Please try again.');
							},
							
							params: parr
							
						});
		
		},
		reset: function(){
			
		service.mainForm.getForm().reset();	
			
		
		},
		view: function(){
			
			if(Ext.getCmp('min_win_service_grid')){
			Ext.getCmp('win_service_grid').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_service_grid'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_service_grid').close();}catch(err){}
			var store = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_service.php?action=view",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewService",
		            id: "id_service",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "id_service", mapping: "id_service"},
            			{name: "service_code", mapping: "service_code"},
						{name: "service_name", mapping: "service_name"},
						{name: "is_active", mapping: "is_active"},
            			{name: "created_by", mapping: "created_by"},
						{name: "created_datetime", mapping: "created_datetime"}
						
			
        			]
					}),
					sortInfo:{field: 'created_datetime', direction: "DESC"}
				});
			var grid = new Ext.grid.GridPanel({
				store: store,
				cm: new Ext.grid.ColumnModel([
					{id: "id-grid-details-service",header: "ID", width: 30,  sortable: true, locked:true, dataIndex: "id_service"},
					{header: "Code", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "service_code"},
					{header: "Service Name", width: 90,  sortable: true, locked:true,align:'left', dataIndex: "service_name"},
					{header: "Active", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_active", renderer: render.YesNo},
					{header: "Created By", width:50,  sortable: true, locked:true,align:'left', dataIndex: "created_by"}
					]),
				loadMask: true,	
				autoExpandColumn: "id-grid-details-service",
				border: false,
				viewConfig: {
					forceFit:true
				},
				listeners:  {
					rowcontextmenu: service.rowContextMenu
				},
				width: '100%',
				//border: true,
				layout: 'fit',
				height: 175,
				tbar: [
            '		Search Service Name: ', ' ',
					new Ext.ux.form.SearchField({
						store: store,
						width:320
					})
				],
				bbar: new Ext.PagingToolbar({
					pageSize: 15,
					store: store,
					displayInfo: true,
					displayMsg: 'Displaying service {0} - {1} of {2}',
					emptyMsg: "No service to display",
					items:[
						'-', {
						pressed: true,
						enableToggle:true,
						text: 'Add New Service',
						icon: "images/icons/application_add.png",
						handler: function () {
							service.getPanel();
						}
					}]
				})
			});

			
			service.grid = grid;
			service.grid.getStore().load();
			
			proj_tools.new_win('win_service_grid',550,400,grid,'Service List');
			
					
		},
		rowContextMenu: function (grid, rowIndex, e) {
								
			e.stopEvent(); 
			
	        grid.getSelectionModel().selectRow(rowIndex, false);
	        service.row = grid.getSelectionModel().getSelected();
	        	
	        var coords = e.getXY();

	        var myContextMenu = service.menus();
			
			if ( service.row.get("is_active") == 'Y' || service.row.get("is_active") == 1) {
				Ext.getCmp("service-menu-active").disable();
			} else {
				Ext.getCmp("service-menu-deactive").disable();
			}
			
			
			
	        myContextMenu.showAt([coords[0], coords[1]]);
	        	    
		},
		menus: function(){
			
			if(Ext.getCmp('service-grid-context-menu')){
				Ext.getCmp('service-grid-context-menu').removeAll();
			}
			
			var myContextMenu = new Ext.menu.Menu({
				id: "service-grid-context-menu",
				items: [
					{
						text: "Activate",
						id: "service-menu-active",
						iconCls: "approvedIcon",  
						icon: "images/icons/accept.png",
						handler: service.setactive
					},
					{
						text: "Deactivate",		
						id: "service-menu-deactive",
						iconCls: "disapprovedIcon",  
						icon: "images/icons/cross.png",
						handler: service.setdeactive
					},
					'-',
					{
						text: "Edit",
						id: "service-menu-edit",
						iconCls: "approvedIcon",  
						icon: "images/icons/folder_edit.png",
						handler: service.edit
					}
				]
			});
			
			return myContextMenu;
		},
		edit: function(){
			service.getPanel();
	
	
			//alert(user_man.row.get("title"));
			Ext.getCmp('serviceid').setValue(service.row.get("service_name"));
			Ext.getCmp('service-id').setValue(service.row.get("id_service"));
			
			
			
			
						
			
		},
		setactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_service.php",
				method: "POST",
				success: function () {
					service.grid.getStore().load();
				},
				params: {action: "activate", service_id: service.row.get("id_service")}
				
			});
		},
		setdeactive: function(){
			Ext.Ajax.request({
				url: "_ajax/execute_service.php",
				method: "POST",
				success: function () {
					service.grid.getStore().load();
				},
				params: {action: "deactivate", service_id: service.row.get("id_service")}
				
			});
		}
	}
}();