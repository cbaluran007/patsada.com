Ext.namespace("app_space");

var documents = function () {
	return {
		getDocument: function(docId){
		
			Ext.Ajax.request({
				url: "_ajax/execute_document.php",
				method: "POST",
				success: function (result, request) {
						var jsonData = Ext.util.JSON.decode(result.responseText);
						//var notification12 = jsonData.notifications;
						var stats1 = jsonData.status;
						var filename = jsonData.filename;
						if(stats1){
							documents.view(filename);
						}
				},
				params: {action: "getData", doc_id: docId}
													
			});
		
		},
		view: function(name){
			if(Ext.getCmp('min_win_documents')){
			Ext.getCmp('win_documents').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_documents'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_documents').close();}catch(err){}
			var invoiceView = new Ext.Panel({
							width: '100%',
							height: '100%',
							autoScroll:true,
							region:'center',
						margins:'3 0 3 3',
						cmargins:'3 3 3 3',
						layout: "fit",
						collapsible: true,
							autoEl : {
									title: 'D O C U M E N T S',
									   tag : "iframe",
									   height: '100%',
									   width: '100%',
									   id: "documentPageId",
									   src : "_documents/"+name
									}
						});

			proj_tools.new_win_grid('win_documents','90%',415,invoiceView,'Documents');
		},
		email: function(id){
			if(Ext.getCmp('min_win_documents_email')){
			Ext.getCmp('win_documents_email').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_documents_email'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_documents_email').close();}catch(err){}
			
			var formMail = new Ext.form.FormPanel({
					frame:true, 
					border:false, 
					labelWidth: 55,
					url: 'save-form.php',
					layout: {
						type: 'vbox',
						align: 'stretch'  // Child items are stretched to full width
					},
					defaults: {
						xtype: 'textfield'
					},

					items: [
					{
						plugins: [Ext.ux.FieldLabeler ],
						fieldLabel: 'From',
						name: 'from',
						vtype:'email'
					},
					{
						plugins: [Ext.ux.FieldLabeler ],
						fieldLabel: 'Send To',
						name: 'to',
						vtype:'email'
					},{
						plugins: [ Ext.ux.FieldLabeler ],
						fieldLabel: 'Subject',
						name: 'subject'
					},
					{xtype: "panel",  border: false, html: "<img src='images/icons/attach.png' > Attached Document ID: "+id},

					{
						xtype: 'textarea',
						fieldLabel: 'Message text',
						hideLabel: true,
						name: 'msg',
						flex: 1  // Take up all *remaining* vertical space
					},
					{id: "email-doc-id", name: "email-doc-id", xtype: "hidden", value: id}
					],
					buttons: [
						{text: "Email", formBind: true, icon: "images/icons/email_go.png", handler: documents.submitEmail},
						{text: "Close", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: documents.cancelEmail}
						
					]
				});

			documents.mainformMail = formMail;	
			proj_tools.new_win('win_documents_email','50%',415,formMail,'Email');
		},
		emailAll: function(id){
			var str_html = "";
			var n=id.split("|");
			for(var x=0;x<n.length;x++){
				if(n[x] !=""){
					str_html +="<img src='images/icons/attach.png' > Attached Document ID: "+n[x]+"<br>";
				}
			}
			
		
			if(Ext.getCmp('min_win_documents_email_all')){
			Ext.getCmp('win_documents_email_all').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_documents_email_all'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_documents_email_all').close();}catch(err){}
			
			var formMail = new Ext.form.FormPanel({
					frame:true, 
					border:false, 
					labelWidth: 55,
					url: 'save-form.php',
					layout: {
						type: 'vbox',
						align: 'stretch'  // Child items are stretched to full width
					},
					defaults: {
						xtype: 'textfield'
					},

					items: [
					{
						plugins: [Ext.ux.FieldLabeler ],
						fieldLabel: 'From',
						name: 'from',
						vtype:'email'
					},
					{
						plugins: [Ext.ux.FieldLabeler ],
						fieldLabel: 'Send To',
						name: 'to',
						vtype:'email'
					},{
						plugins: [ Ext.ux.FieldLabeler ],
						fieldLabel: 'Subject',
						name: 'subject'
					},
					{xtype: "panel",  border: false, html: str_html},

					{
						xtype: 'textarea',
						fieldLabel: 'Message text',
						hideLabel: true,
						name: 'msg',
						flex: 1  // Take up all *remaining* vertical space
					},
					{id: "email-doc-id", name: "email-doc-id", xtype: "hidden", value: id}
					],
					buttons: [
						{text: "Email", formBind: true, icon: "images/icons/email_go.png", handler: documents.submitEmailAll},
						{text: "Close", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: documents.cancelEmailAll}
						
					]
				});

			documents.mainformMailAll = formMail;	
			proj_tools.new_win('win_documents_email_all','50%',415,formMail,'Email');
		},
		submitEmail: function(){
			documents.mainformMail.getForm().submit({
						
							url: '_ajax/execute_document.php',
							method: 'post',
							success: function () {
								Ext.MessageBox.alert('Success', 'Email has been successfully sent.');
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to send mail.');
							},
							
							params: {action: "Email"}
							
			});
			
		},
		submitEmailAll: function(){
			documents.mainformMailAll.getForm().submit({
						
							url: '_ajax/execute_document.php',
							method: 'post',
							success: function () {
								Ext.MessageBox.alert('Success', 'Email has been successfully sent.');
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to send mail.');
							},
							
							params: {action: "EmailAll"}
							
			});
			
		},
		cancelEmail: function(){
			Ext.getCmp('win_documents_email').close();
			
		},
		cancelEmailAll: function(){
			Ext.getCmp('win_documents_email_all').close();
			
		}
		
	}
}();