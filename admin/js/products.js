Ext.namespace("app_space");

var products = function () {
	return {
		getPanel: function(){
		
		if(Ext.getCmp('min_win_create_products')){
		Ext.getCmp('win_create_products').show();
		Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_products'));
		Ext.getCmp('task_bar').doLayout();
		}
		
		try{Ext.getCmp('win_create_products').close();}catch(err){}
		
		var form = new Ext.FormPanel({
				labelWidth: 100,
				bodyStyle:'padding:10px 5px 10px 5px;',
				frame:true, 
				border:false, 
				defaultType:'textfield',
				monitorValid:true,
				items:[
				{ xtype: 'textfield',allowBlank:false,  fieldLabel: 'Service Name',	name:'servicename', id:'serviceid',width: "95%" },
				{id: "service-id", name: "service-name", xtype: "hidden", value: 0}
       
				],
				buttons: [
						{text: "Save", formBind: true, icon: "images/icons/disk.png", handler: service.submitUser},
						{text: "Reset", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: service.reset}
						
					]
			});
		
		
		
		
		
		
			
			//validator.runThis();
			proj_tools.new_win('win_create_products',400,125,form,'Create New Product');
			products.mainForm = form;	
						
		
		},
		submitUser: function(){
			var parr = {};
			parr["action"] = "submitService";
			parr["servicename"] = Ext.getCmp("serviceid").getValue();
			parr["serviceid"] = Ext.getCmp("service-id").getValue();
			service.mainForm.getForm().submit({
						
							url: '_ajax/execute_service.php',
							method: 'post',
							success: function () {
								Ext.getCmp('win_create_service').close();
								service.view();
								service.grid.getStore().load();
								Ext.MessageBox.alert('Sucess', 'Service successfully saved.');
							},
							failure: function () {
								Ext.MessageBox.alert('Notice', 'Unable to saved service. Please try again.');
							},
							
							params: parr
							
						});
		
		},
		reset: function(){
			
		service.mainForm.getForm().reset();	
			
		
		},
		view: function(){
			
			if(Ext.getCmp('min_win_products_grid')){
			Ext.getCmp('win_products_grid').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_products_grid'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_products_grid').close();}catch(err){}
			var store = new Ext.data.Store({
					proxy: new Ext.data.HttpProxy({
						url: "_ajax/execute_products.php?action=view",
						method: "GET"
					}),
					reader: new Ext.data.JsonReader({
		            root: "viewProducts",
		            id: "id_products",
		            totalProperty: "total",
		            fields: [
            			//"fullname",
						{name: "id_product", mapping: "id_product"},
            			{name: "product_code", mapping: "product_code"},
						{name: "product_name", mapping: "product_name"},
						{name: "is_hotel", mapping: "is_hotel"},
						{name: "is_flight", mapping: "is_flight"},
						{name: "is_tour", mapping: "is_tour"},
						{name: "is_live", mapping: "is_live"},
						{name: "service_name", mapping: "service_name"},
						{name: "supplier_name", mapping: "supplier_name"},
						{name: "id_service", mapping: "id_service"},
						{name: "id_supplier", mapping: "id_supplier"},
						{name: "is_active", mapping: "is_active"},
            			{name: "created_by", mapping: "created_by"},
						{name: "created_datetime", mapping: "created_datetime"},
						{name: "product_address", mapping: "product_address"},
						{name: "product_postal_code", mapping: "product_postal_code"},
						{name: "product_phone_number", mapping: "product_phone_number"},
						{name: "product_area", mapping: "product_area"},
						{name: "product_chain", mapping: "product_chain"},
						{name: "product_description", mapping: "product_description"},
						{name: "product_coordinates_longitude", mapping: "product_coordinates_longitude"},
						{name: "product_coordinates_latitude", mapping: "product_coordinates_latitude"},
						{name: "destination_country", mapping: "destination_country"},
						{name: "destination_city", mapping: "destination_city"},
						{name: "destination_id", mapping: "destination_id"},
						{name: "destination_from_id", mapping: "destination_from_id"},
						{name: "destination_to_id", mapping: "destination_to_id"},
						{name: "is_best_seller", mapping: "is_best_seller"},
						{name: "star_rating", mapping: "star_rating"},
						{name: "amenities_internal", mapping: "amenities_internal"},
						{name: "amenities_external", mapping: "amenities_external"},
						{name: "url", mapping: "url"},
						{name: "stock_type", mapping: "stock_type"},
						{name: "rate_type", mapping: "rate_type"},
						{name: "product_fax_number", mapping: "product_fax_number"},
						{name: "product_url", mapping: "product_url"},
						{name: "product_email", mapping: "product_email"}
						
			
        			]
					}),
					sortInfo:{field: 'product_name', direction: "DESC"}
				});
			var grid = new Ext.grid.GridPanel({
				store: store,
				cm: new Ext.grid.ColumnModel([
					{id: "id-grid-details-products",header: "ID", width: 30,  sortable: true, locked:true, dataIndex: "id_product"},
					{header: "Code", width: 50,  sortable: true, locked:true,align:'left', dataIndex: "product_code"},
					{header: "Product Name", width: 120,  sortable: true, locked:true,align:'left', dataIndex: "product_name"},
					{header: "Service Type", width: 90,  sortable: true, locked:true,align:'left', dataIndex: "service_name"},
					{header: "Supplier Name", width: 90,  sortable: true, locked:true,align:'left', dataIndex: "supplier_name"},
					{header: "Hotels", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_hotel", renderer: render.YesNo},
					{header: "Tours", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_tour", renderer: render.YesNo},
					{header: "Flight", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_flight", renderer: render.YesNo},
					{header: "Live", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_live", renderer: render.YesNo},
					{header: "Active", width: 30,  sortable: true, locked:true,align:'center', dataIndex: "is_active", renderer: render.YesNo},
					{header: "Created By", width:50,  sortable: true, locked:true,align:'left', dataIndex: "created_by"}
					]),
				loadMask: true,	
				autoExpandColumn: "id-grid-details-products",
				border: false,
				region: 'center',
				viewConfig: {
					forceFit:true
				},
				listeners:  {
					rowcontextmenu: products.rowContextMenu
				},
				width: '100%',
				//border: true,
				layout: 'fit',
				id:'product-grid',
				height: 175,
				tbar: [
            '		Search Product Name: ', ' ',
					new Ext.ux.form.SearchField({
						store: store,
						width:320
					})
				],
				bbar: new Ext.PagingToolbar({
					pageSize: 50,
					store: store,
					displayInfo: true,
					displayMsg: 'Displaying Products {0} - {1} of {2}',
					emptyMsg: "No products to display"
				})
			});

			
			var ds_service = proj_tools.ds_combo('_ajax/execute_service.php','getService','Sid','id_service','service_name');
			var service_combo = proj_tools.getCombo(ds_service,'Service Name','service_search_product','id_service','service_name',150,150);
			var service_combo2 = proj_tools.getCombo(ds_service,'Service Name','service_create_product','id_service','service_name',150,150);
			
			var ds_supplier = proj_tools.ds_combo('_ajax/execute_supplier.php','getSupplier','Supid','id_supplier','supplier_name');
			var supplier_combo = proj_tools.getCombo(ds_supplier,'Supplier Name','supplier_search_product','id_supplier','supplier_name',150,150);
			
			var searchCriteria = new Ext.FormPanel({
				labelWidth: 70, // label settings here cascade unless overridden
				url:'save-form.php',
				bodyStyle:'padding:5px 5px 0',
				border:false, 
				width: '100%',
				title: 'Search Products',
				defaults: {width: 150},
				defaultType: 'textfield',

				items: [
					service_combo,
					supplier_combo,
					{
						fieldLabel: 'Product Code',
						name: 'search_product_code',
						id: 'search_product_code'
					}, {
						fieldLabel: 'Product Name',
						name: 'search_product_name',
						id: 'search_product_name'
					},
					{xtype: 'checkbox', checked: false, fieldLabel: 'Is Live?',  name: 'is_search_live', id:'is_search_live'}
				],

				buttons: [
						{text: "Search", formBind: true, icon: "images/icons/magnifier.png", handler: products.search},
						{text: "Reset", formBind: false,icon: "images/icons/arrow_rotate_anticlockwise.png", handler: products.searchReset}
						
					]
			});

			var createFormProducts = new Ext.FormPanel({
				labelWidth: 70, // label settings here cascade unless overridden
				url:'save-form.php',
				bodyStyle:'padding:5px 5px 0',
				border:false, 
				width: '100%',
				title: 'Create New Products',
				defaults: {width: 150},
				defaultType: 'textfield',

				items: [
					service_combo2
				],

				buttons: [
						{text: "Create", formBind: true, icon: "images/icons/magnifier.png", handler: products.createNewProducts}						
					]
			});
			
			// Panel for the west
			var nav = new Ext.Panel({
				title: ' ',
				region: 'west',
				split: true,
				width: 250,
				collapsible: true,
				margins:'3 0 3 3',
				cmargins:'3 3 3 3',
				layout:'accordion',
				items: [searchCriteria,createFormProducts]
			});

			products.grid = grid;
			proj_tools.new_win_grid('win_products_grid',1050,550,[nav,grid],'Product List');
			Ext.getCmp('product-grid').getEl().mask('Loading...', 'x-mask-loading');
			products.grid.getStore().load({scope: this,
			callback: function(records, operation, success) {
				if (success) {
					Ext.getCmp('product-grid').getEl().mask().hide();
				} else {
					Ext.getCmp('product-grid').getEl().mask().hide();
				}
			}});
			
			
					
		},
		search:function () {
			var parr = {};
			parr["isSearch"] = 1;
			parr["service_id"] = Ext.getCmp("service_search_product_combo_id").getValue();
			parr["supplier_id"] = Ext.getCmp("supplier_search_product_combo_id").getValue();
			parr["search_product_code"] = Ext.getCmp("search_product_code").getValue();
			parr["search_product_name"] = Ext.getCmp("search_product_name").getValue();
			parr["search_product_live"] = Ext.getCmp("is_search_live").getValue();
			products.grid.getStore().load({ params: parr });
		},
		rowContextMenu: function (grid, rowIndex, e) {
								
			e.stopEvent(); 
			
	        grid.getSelectionModel().selectRow(rowIndex, false);
	        products.row = grid.getSelectionModel().getSelected();
	        	
	        var coords = e.getXY();

	        var myContextMenu = products.menus();
			
			
			
			
			
	        myContextMenu.showAt([coords[0], coords[1]]);
	        	    
		},
		menus: function(){
			
			if(Ext.getCmp('products-grid-context-menu')){
				Ext.getCmp('products-grid-context-menu').removeAll();
			}
			
			var myContextMenu = new Ext.menu.Menu({
				id: "products-grid-context-menu",
				items: [
					
					{
						text: "View Product Details",
						id: "products-menu-view-details",
						iconCls: "approvedIcon",  
						icon: "images/icons/magnifier.png",
						handler: products.viewDetails
					},
					'-',
					{
						text: "Edit",
						id: "products-menu-edit",
						iconCls: "approvedIcon",  
						icon: "images/icons/folder_edit.png",
						handler: products.edit
					}
				]
			});
			
			return myContextMenu;
		},
		viewDetails: function(param){
		if(typeof(param) === "object"){
		   param = products.row.get("product_code")
		 }
		
		
		if(Ext.getCmp('min_win_create_products_view_details')){
			Ext.getCmp('win_create_products_view_details').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_products_view_details'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_create_products_view_details').close();}catch(err){}
		
			var tplProduct = new Ext.Template(
                    '<iframe src="'+base_url+'modules/xml_product.php?code='+param+'" width=100% height=100%></iframe>'
                );
			proj_tools.new_win('win_create_products_view_details','90%',600,tplProduct,'Product Info');
			
		},
		edit: function(){
		
			if(products.row.get("is_live") == 1 ||products.row.get("is_live") == '1' ){
				Ext.MessageBox.alert('Notice', 'You are trying to update a live product.');
			}
			
			Ext.Ajax.request({
				url: "_ajax/execute_products.php",
				method: "POST",
				success: function () {
					product_create.getPanel();
					
					
					var tabPanel = Ext.getCmp('product_tab_manager_cont');
					tabPanel.unhideTabStripItem("ProductImagesTabID");
					tabPanel.unhideTabStripItem("ProductSubproductsTabID");
					Ext.getCmp("product_create_name").setValue(products.row.get("product_name"));
					Ext.getCmp("product_create_code").setValue(products.row.get("product_code"));
					Ext.getCmp("product-create-id").setValue(products.row.get("id_product"));
					
					Ext.getCmp("addressProduct").setValue(products.row.get("product_address"));
					Ext.getCmp("codeProduct").setValue(products.row.get("product_postal_code"));
					Ext.getCmp("productlong").setValue(products.row.get("product_coordinates_longitude"));
					Ext.getCmp("productlat").setValue(products.row.get("product_coordinates_latitude"));
					Ext.getCmp("url_product").setValue(products.row.get("product_url"));
					Ext.getCmp("email_product").setValue(products.row.get("product_email"));
					Ext.getCmp("phone_num_product").setValue(products.row.get("product_phone_number"));
					Ext.getCmp("fax_num_product").setValue(products.row.get("product_fax_number"));
					Ext.getCmp("product_amenities").setValue(products.row.get("amenities_internal"));
					Ext.getCmp("product_description").setValue(products.row.get("product_description"));
					
					
					Ext.getCmp("supplier_create_product_combo_id").setValue(products.row.get("id_supplier"));
					Ext.getCmp("supplier_create_product_combo_id").setRawValue(products.row.get("supplier_name"));
					Ext.getCmp("service_create_product_info_combo_id").setValue(products.row.get("id_service"));
					Ext.getCmp("service_create_product_info_combo_id").setRawValue(products.row.get("service_name"));
					
					Ext.getCmp("destination_create_product_from_combo_id").setValue(products.row.get("destination_from_id"));
					Ext.getCmp("destination_create_product_from_combo_id").setRawValue(products.row.get("destination_from_id"));
					Ext.getCmp("destination_create_product_to_combo_id").setValue(products.row.get("destination_to_id"));
					Ext.getCmp("destination_create_product_to_combo_id").setRawValue(products.row.get("destination_to_id"));
					Ext.getCmp("destination_create_product_combo_id").setValue(products.row.get("destination_id"));
					Ext.getCmp("destination_create_product_combo_id").setRawValue(products.row.get("destination_id"));
					
					Ext.getCmp("country_product_create_combo_id").setValue(products.row.get("destination_country"));
					Ext.getCmp("country_product_create_combo_id").setRawValue(products.row.get("destination_country"));
					
					Ext.getCmp("state_product_create_combo_id").setDisabled(false);
					Ext.getCmp("state_product_create_combo_id").setValue(products.row.get("destination_city"));
					Ext.getCmp("state_product_create_combo_id").setRawValue(products.row.get("destination_city"));
					
					Ext.getCmp("stock_type_combo_id").setValue(products.row.get("stock_type"));
					Ext.getCmp("rate_type_combo_id").setValue(products.row.get("rate_type"));
					Ext.getCmp("star_rating_combo_id").setValue(products.row.get("star_rating"));
					
					
					
				},
				params: {action: "setSession", product_id: products.row.get("id_product"),product_code: products.row.get("product_code")}
				
			});	
			
			
			
			
			
			
			
	
		},
		viewPhotos:function(ttitle,code,supplier){
			if(Ext.getCmp('min_win_create_products_view_photo')){
			Ext.getCmp('win_create_products_view_photo').show();
			Ext.getCmp('task_bar').getTopToolbar().remove(Ext.getCmp('min_win_create_products_view_photo'));
			Ext.getCmp('task_bar').doLayout();
			}
			
			try{Ext.getCmp('win_create_products_view_photo').close();}catch(err){}
		
			var tplPhoto = new Ext.Template(
                    '<iframe src="'+base_url+'photoGallery?code='+code+'&supp='+supplier+'" width=100% height=100%></iframe>'
                );
			proj_tools.new_win('win_create_products_view_photo',675,415,tplPhoto,'Photo Gallery');
			
		},
		createNewProducts:function(){

			var serviceID = Ext.getCmp("service_create_product_combo_id").getValue();
			var serviceName = Ext.getCmp("service_create_product_combo_id").getRawValue();
			/*switch(serviceID)
			{
			case '1':
						
				product_create.getPanel(serviceID,'Accommodation');
				Ext.getCmp("service_create_product_info_combo_id").setValue(serviceID);
					Ext.getCmp("service_create_product_info_combo_id").setRawValue('Accommodation');
				
			  break;
			default:
			  Ext.MessageBox.alert('Notification', 'No service selected.');
			}*/
			
			product_create.getPanel(serviceID,serviceName);
				Ext.getCmp("service_create_product_info_combo_id").setValue(serviceID);
					Ext.getCmp("service_create_product_info_combo_id").setRawValue(serviceName);
			var parr = {};
			parr['action'] = 'createNewSession';
			
			Ext.Ajax.request({
					url: "_ajax/execute_products.php",
					method: "POST",
					success: function ( result, request) {
						Ext.MessageBox.alert('Success', 'New Product Form.');
					},
					failure: function () {
						Ext.MessageBox.alert('Failure', 'Unable to connect.');
					},
					params: parr
			});
			
		}
	}
}();