-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2014 at 03:40 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jobposts`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`post_id` int(11) NOT NULL,
  `comp_code` varchar(50) NOT NULL,
  `pdate` varchar(50) NOT NULL,
  `post_type` varchar(50) NOT NULL,
  `job_type` varchar(50) NOT NULL,
  `position` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `category` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `yrs_exp` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `comp_code`, `pdate`, `post_type`, `job_type`, `position`, `description`, `category`, `location`, `yrs_exp`, `action`) VALUES
(4, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full Time\r', ' IT Developer\r', ' To maintain website\r', ' IT\r', ' CDO\r', ' 1-5\r', ''),
(5, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full Time\r', ' IT Developer\r', ' To maintain website\r', ' IT\r', ' CDO\r', ' 1-5\r', ''),
(6, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', ' Cebu\r', ' 1-3\r', ''),
(7, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', '', ' 1-3\r', ''),
(8, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(9, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(10, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full Time\r', ' IT Developer\r', ' To maintain website\r', ' IT\r', ' CDO\r', ' 1-5\r', ''),
(11, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', ' Cebu\r', ' 1-3\r', ''),
(12, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', '', ' 1-3\r', ''),
(13, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(14, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(15, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full Time\r', ' IT Developer\r', ' To maintain website\r', ' IT\r', ' CDO\r', ' 1-5\r', ''),
(16, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', ' Cebu\r', ' 1-3\r', ''),
(17, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full-Time\r', ' Engineer\r', ' The quick brown fox jumps over the lazy dog\r', ' Engineering\r', '', ' 1-3\r', ''),
(18, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(19, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Non-Urgent\r', ' Part Time\r', ' Accountant\r', ' blahblah\r', '  Accountancy\r', ' CDO\r', ' 3\r', ''),
(20, 'TVZ', 'Wed, 3 Dec 2014 11:42:45 +0800', ' Urgent\r', ' Full Time\r', ' IT Developer\r', ' To maintain website\r', ' IT\r', ' CDO\r', ' 1-5\r', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`post_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
