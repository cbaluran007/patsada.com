<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Candidate extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        /* Load the libraries and helpers */
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('recaptcha');

        $this->load->model('tools_model');
        $this->load->model('Category');
    }
    public function index()
    {
        $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
        $this->load->model('Category');
        $catResults = $this->Category->get_all_category();
        $data['category_list'] = $catResults;
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $this->load->model('candidate_model');
            $candidate = $this->candidate_model->get_candidate($data['id']);
            $objectives = $this->candidate_model->get_candidate_objective($data['id']);
            $experience = $this->candidate_model->get_candidate_experience($data['id']);
            $skills = $this->candidate_model->get_candidate_skills($data['id']);
            $education = $this->candidate_model->get_candidate_education($data['id']);
            $resume = $this->candidate_model->get_resume($data['id']);
            $get_application = $this->tools_model->get_candidate_apply($data['id']);
            $data['get_application']=$get_application;
            $catResults = $this->Category->get_all_category();
            $data['category_list'] = $catResults;
            $data['resume']=$resume;
            $data['education']=$education;
            $data['skills']=$skills;
            $data['experience']=$experience;
            $data['objective']=$objectives;
            $data['candidate']=$candidate;
            $this->load->view('header');
            $this->load->view('menu',$data);
            $this->load->view('candidate/index',$data);
            $this->load->view('candidate/sidebar',$data);
            $this->load->view('footer');
        }else{
            redirect('patsada/');
        }
    }
	public function public_profile($name,$id)
    {
        
		$data['id'] = $id;
		$data['roll'] = 'public';

		$this->load->model('candidate_model');

        $candidate = $this->candidate_model->get_candidate($data['id']);
        $objectives = $this->candidate_model->get_candidate_objective($data['id']);
        $experience = $this->candidate_model->get_candidate_experience($data['id']);
        $skills = $this->candidate_model->get_candidate_skills($data['id']);
        $education = $this->candidate_model->get_candidate_education($data['id']);
        $resume = $this->candidate_model->get_resume($data['id']);
        $get_application = $this->tools_model->get_candidate_apply($data['id']);
        $data['get_application']=$get_application;
		$data['resume']=$resume;
        $data['education']=$education;
        $data['skills']=$skills;
        $data['experience']=$experience;
        $data['objective']=$objectives;
        $data['candidate']=$candidate;
		$this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('candidate/index',$data);
        $this->load->view('footer');
    }
	public function verify($param)
    {
		$this->load->model('candidate_model');
		$stats=$this->candidate_model->candidate_get_validated($param);
		if($stats[0]){

			$this->session->set_userdata('logged_in', $stats[1]);

            redirect('candidate/edit_profile');
        }

    }
    public function edit_profile(){
        if($this->session->userdata('logged_in'))
        {
           $title='';
           $first_name='';
           $last_name='';
           $address='';
           $city='';
           $province='';
           $zip_code='';
           $country='';
           $telephone='';
           $mobile='';
           $skype_name='';
           $facebook_url='';
           $twitter_url='';
           $g_plus_url='';
           $birthdate='';
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $this->load->model('candidate_model');
            $this->load->model('tools_model');

            $catResults = $this->Category->get_all_category();
            $data['category_list'] = $catResults;
            $candidate = $this->candidate_model->get_candidate($data['id']);
            $objectives = $this->candidate_model->get_candidate_objective($data['id']);
            $experience = $this->candidate_model->get_candidate_experience($data['id']);
            $skills = $this->candidate_model->get_candidate_skills($data['id']);
            $education = $this->candidate_model->get_candidate_education($data['id']);
            $resume = $this->candidate_model->get_resume($data['id']);
            $get_application = $this->tools_model->get_candidate_apply($data['id']);
            $data['get_application']=$get_application;
            $data['resume']=$resume;
            $data['education']=$education;
            $data['skills']=$skills;
            $data['objective']=$objectives;

            $data['experience']=$experience;

            $this->load->model('Category');
            $catResults = $this->Category->get_all_category();
            $data['catResults']=$catResults[0];

            $countries = array(
                'PH' => 'Philippines',
            );



            $data['candidate']=$candidate;
            $data['countries']=$countries;

            $cadindate_array=array();
            if($candidate){
                foreach($candidate as $row){
                    $cadindate_array=array(
                        'title'         => $row->title,
                        'first_name'    => $row->first_name,
                        'last_name'     => $row->last_name,
                        'address'       => $row->address,
                        'city'          => $row->city,
                        'province'      => $row->province,
                        'zip_code'      => $row->zip_code,
                        'country'       => $row->country,
                        'telephone'     => $row->telephone,
                        'mobile'        => $row->mobile,
                        'skype_name'    => $row->skype_name,
                        'facebook_url'  => $row->facebook_url,
                        'twitter_url'   => $row->twitter_url,
                        'g_plus_url'    => $row->g_plus_url,
                        'birthdate'     => $row->birthdate,
                    );
                }
            }
            $this->load->view('header');
            $this->load->view('menu',$data);
            $this->load->view('candidate/edit_profile',$data);
            $this->load->view('candidate/edit_sidebar',$data);
            $this->load->view('candidate/footer');
        }else{
            redirect('patsada/');
        }
    }
    public function delete_candidate_profile(){
        $this->load->model('tools_model');
        $_POST['tbl_name']='candidate_education';
        $this->tools_model->deleteDatas($_POST);
        redirect('candidate/edit_profile');
    }

    public function delete_candidate_skills_profile(){
        $this->load->model('tools_model');
        $_POST['tbl_name']='skills';
        $this->tools_model->deleteDatas($_POST);
        redirect('candidate/edit_profile');
    }

    public function process_candidate_profile(){

        $this->load->model('tools_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $this->load->model('candidate_model');

            $_POST['tbl_name']='candidate';

            $this->tools_model->saveDatas($_POST);

            redirect('candidate/edit_profile');

        }else{
            redirect('patsada/');
        }

    }

    public function process_candidate_education_profile(){

        $this->load->model('tools_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $this->load->model('candidate_model');

            $_POST['tbl_name']='candidate_education';

            $this->tools_model->saveDatas($_POST);

            redirect('candidate/edit_profile');

        }else{
            redirect('patsada/');
        }

    }

    public function process_candidate_obj_profile(){

        $this->load->model('tools_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $this->load->model('candidate_model');

            $_POST['tbl_name']='candidate_objective';

            $this->tools_model->saveDatas($_POST);

            redirect('candidate/edit_profile');

        }else{
            redirect('patsada/');
        }

    }

    public function process_candidate_skills_profile(){

        $this->load->model('tools_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $this->load->model('candidate_model');

            $_POST['tbl_name']='skills';

            $this->tools_model->saveDatas($_POST);

            redirect('candidate/edit_profile');

        }else{
            redirect('patsada/');
        }

    }
    public function process_candidate_exp_profile(){

        $this->load->model('tools_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $this->load->model('candidate_model');

            $_POST['tbl_name']='candidate_experience';
            unset($_POST['dd']);
            $this->tools_model->saveDatas($_POST);


            redirect('candidate/edit_profile');

        }else{
            redirect('patsada/');
        }

    }

    public function do_upload($id){
        $this->load->library('upload');

        $this->load->model('tools_model');

            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];


            $user_num= $data['id'];

            $image_upload_folder = FCPATH . 'assets/images/job_seeker/';

            if (!file_exists($image_upload_folder)) {
                mkdir($image_upload_folder, DIR_WRITE_MODE, true);
            }
            $this->upload_config = array(
                'upload_path'   => $image_upload_folder,
                'allowed_types' => '*',
                'max_size'      => 2048,
                'remove_space'  => TRUE,
                'encrypt_name'  => TRUE,
            );

            $this->upload->initialize($this->upload_config);

            if (!$this->upload->do_upload()) {
                $upload_error = $this->upload->display_errors();
                echo json_encode($upload_error);
                //echo  $user_num;
            } else {

                $file_info = $this->upload->data();
                $data_post=array(
                    'avatar'=>$file_info['file_name'],
                    'candidate_id'=>$id,
                    'tbl_name' => 'candidate'
                );
                //print_r($data_post);
                 $var=$this->tools_model->saveDatas($data_post);
               echo json_encode($file_info);
            }

    }


    public function resume_upload($id){
        $this->load->library('upload');
        $this->load->model('tools_model');
        $this->load->model('candidate_model');
            $image_upload_folder = FCPATH . 'file/'.$id;
            $resume = $this->candidate_model->get_resume($id);

                if($resume){
                    $resume_details = $resume[0];
                    $resume_det=$resume_details->resume_id;
                }else{$resume_det = '0';}
            if (!file_exists($image_upload_folder)) {
                mkdir($image_upload_folder, DIR_WRITE_MODE, true);
            }
            $this->upload_config = array(
                'upload_path'   => $image_upload_folder,
                'allowed_types' => '*',
                'max_size'      => 2048,
                'remove_space'  => TRUE,
                'encrypt_name'  => TRUE,
            );

            $this->upload->initialize($this->upload_config);

            if (!$this->upload->do_upload()) {
                $upload_error = $this->upload->display_errors();
                echo json_encode($upload_error);
                //echo  $user_num;
            } else {

                $file_info = $this->upload->data();
                $data_post=array(
                    'resume_filename'=>$file_info['file_name'],
                    'candidate_id'=>$id,
                    'tbl_name' => 'resume',
                    'resume_id' => $resume_det
                );
                //print_r($data_post);
                 $var=$this->tools_model->saveDatas($data_post);
               echo json_encode($file_info);
            }

    }

    public function logout(){
        $this->session->unset_userdata('logged_in');

        redirect('patsada', 'refresh');
    }


}
