<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Patsada extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        /* Load the libraries and helpers */
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('recaptcha');
        $this->load->library('pagination');
    }
    public function index()
    {
        $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();

        $this->load->model('Category');
        $this->load->model('tools_model');
		$catResults = $this->Category->get_all_category();
		$data['category_list'] = $catResults;
        //---- SEARCH
        $search_term = $this->input->get_post('search_term', TRUE);


        if(isset($_SESSION['results_items'])){
            if(isset($_SESSION['search_term'])){
                if($_SESSION['search_term'] == $search_term){
                    $search_result_premium=$this->tools_model->search_record_premium($search_term);
                    $search_result=$this->tools_model->search_record($search_term);
                    $_SESSION['results_items'] =  array_merge($search_result_premium,$search_result);
                    $item_results = $_SESSION['results_items'];
                    $_SESSION['search_term'] =  $search_term;
                }else{

                     $search_result_premium=$this->tools_model->search_record_premium($search_term);
                    $search_result=$this->tools_model->search_record($search_term);
                    $_SESSION['results_items'] =  array_merge($search_result_premium,$search_result);
                    $item_results = $_SESSION['results_items'];
                    $_SESSION['search_term'] =  $search_term;

                }
            }else{
                $search_result_premium=$this->tools_model->search_record_premium($search_term);
                $search_result=$this->tools_model->search_record($search_term);

                $_SESSION['results_items'] = array_merge($search_result_premium,$search_result);
                $item_results = $_SESSION['results_itsems'];
                $_SESSION['search_term'] =  $search_term;
            }
        }else{
            $search_result_premium=$this->tools_model->search_record_premium($search_term);
            $search_result=$this->tools_model->search_record($search_term);
            $_SESSION['results_items'] =  array_merge($search_result_premium,$search_result);
            $item_results = $_SESSION['results_items'];
            $_SESSION['search_term'] =  $search_term;


        }



            $config = array();
            $limit = ($this->uri->segment(3) > 0)?$this->uri->segment(3):0;
            $config['per_page'] = 50;

            $config['base_url'] = base_url() . 'patsada/index/';
            $config['total_rows'] = count($item_results);
            $config['uri_segment'] = 3;
            $choice =$config['total_rows'] / $config['per_page'];
            $config['num_links'] = round($choice);
            $config['next_link'] = '&raquo;';
            $config['prev_link'] = '&laquo;';
            $config['first_link'] = '&laquo;&laquo;';
            $config['last_link']  = '&raquo;&raquo;';
            $this->pagination->initialize($config);

            $data['total_rows']=$config['total_rows'];

            if(empty($item_results)){
                $results_jobs = array();
            }else{
                $results_jobs =  array_slice($item_results,$limit,$config['per_page']);
            }

            $data["results"] =  $results_jobs;
            $data['links'] = $this->pagination->create_links();

            $data['search_term'] = $search_term;

        //
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
        }else{
            $data['roll'] = '0';
        }

        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('ads');
        $this->load->view('sidebar',$data);
        $this->load->view('home/index',$data);
        $this->load->view('footer');
    }

    function candidate_registration(){
        $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
        $this->recaptcha->recaptcha_check_answer();

            if( $this->recaptcha->getIsValid()){
                $this->load->model('tools_model');
               $resutl=  $this->tools_model->create_candidate_and_login_credentials($_POST);
                if($resutl){
                    $return['success'] = true;
                    $return['message'] = "Your account has been successfully created as Job Seeker. Please check your email as we are going to send you a confirmation link. THANK YOU!";
                }else{
                    $return['success'] = false;
                    $return['message']="Error connecting database";
                }

            }else{
                $return['success'] = false;
                if(!$this->recaptcha->getIsValid()) {
                    $return['message']=$this->session->set_flashdata('incorrect captcha');
                }

            }

        echo json_encode($return);
    }

    function jobs($id){
        $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();

        $this->load->model('Category');
        $this->load->model('tools_model');

        $catResults = $this->Category->get_all_category();
        $job_description = $this->tools_model->get_job_posted($id);
        $data['category_list'] = $catResults;

        if($this->session->userdata('logged_in'))
         {
                $session_data = $this->session->userdata('logged_in');
                $data['roll'] = $session_data['roll'];
                $data['id'] = $session_data['id'];

                 $this_valid=$this->tools_model->verify_candidate_application($id,$data['id']);
                 $data['result_can']=$this_valid;
        }else{
                $data['roll'] = '0';
        }
         if(!empty($job_description)){
             $cat_name=array();
             foreach ($job_description as $row) {
                        $cat_name=array(
                            'category_id' => $row->category_id,
                            'job_post_id' => $row->job_post_id
                        );
             }
             $related_jobs = $this->tools_model->get_related_jobs($cat_name['category_id'],$cat_name['job_post_id']);
             $data['related_jobs']=$related_jobs;

         }


         $answer =  $this->genNum(5); 
         $_SESSION['answer'] =$answer;
            $data['results'] = $job_description;
            $data['captcha_text'] = $answer;
            $this->load->view('header');
            $this->load->view('menu',$data);
            $this->load->view('sidebar',$data);
            $this->load->view('job_description/job_description',$data);
            $this->load->view('footer');
    }


private function genNum( $length = 6) { // to generate a company code
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnop';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    
    return  $randomString; 
  }

    function application(){
        $this->load->model('tools_model');
        $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
     
       // $this->recaptcha->recaptcha_check_answer();
        $status['data_status']=true;
        $id=$_POST['job_post_id'];
        $email = $_POST['email'];
        $this_valid=$this->tools_model->verify_apply($id,$email);
        if(!$this_valid){
        if (strtolower($_SESSION['answer']) == strtolower($_POST['captcha'])){

            $this->load->library('upload');
       
            if(isset($_FILES['resume_filename']['name'])){ 
                $image_upload_folder = FCPATH . 'file/applicants/';
                if (!file_exists($image_upload_folder)) {
                    mkdir($image_upload_folder, DIR_WRITE_MODE, true);
                }
                $this->upload_config = array(
                    'upload_path'   => $image_upload_folder,
                    'allowed_types' => '*',
                    'max_size'      => 2048,
                    'remove_space'  => TRUE,
                    'encrypt_name'  => TRUE,
                );

                $this->upload->initialize($this->upload_config);

                if (!$this->upload->do_upload('resume_filename')) {
                    $upload_error = $this->upload->display_errors();
                    $status['data_status']=false;
                    $status['data_result']='Please Select A file';
 
                } else { 
                    $file_info = $this->upload->data(); 
                    //print_r($data_post);
                     $data_post=array(
                        'job_post_id'=>$_POST['job_post_id'],
                        'applied_datetime'=>$_POST['applied_datetime'],
                        'first_name'=>$_POST['first_name'],
                        'last_name'=>$_POST['last_name'],
                        'title'=>$_POST['title'],
                        'email'=>$_POST['email'],
                        'contact_no'=>$_POST['contact_no'],
                        'cover_letter'=>$_POST['cover_letter'],
                        'resume_filename'=>$file_info['file_name'],
                        'tbl_name' => 'job_post_event',
                        'job_post_event_id' => '0'

                             );
                    $var=$this->tools_model->saveDatas($data_post);
 
                     $answer =  $this->genNum(5); 
                    $status['data_status']=true;
                    $status['data_result']='Your application has been sent to the Employer. We will send you an approval if the employer picks your application or wait for a call. THANK YOU FOR TRUSTING US!';
                    $status['captca_text'] = $answer;     
                }

            }else{ 
                   $data_post=array(
                        'job_post_id'=>$_POST['job_post_id'],
                        'applied_datetime'=>$_POST['applied_datetime'],
                        'first_name'=>$_POST['first_name'],
                        'last_name'=>$_POST['last_name'],
                        'title'=>$_POST['title'],
                        'email'=>$_POST['email'],
                        'contact_no'=>$_POST['contact_no'],
                        'cover_letter'=>$_POST['cover_letter'],
                        'resume_text'=>$_POST['resume_text'],
                        'tbl_name' => 'job_post_event',
                        'job_post_event_id' => '0'

                             );
                $var=$this->tools_model->saveDatas($data_post);
                 $answer =  $this->genNum(5); 
                $_SESSION['answer'] =$answer;
                $status['data_status']=true;
                $status['data_result']='Your application has been sent to the Employer. We will send you an approval if the employer picks your application or wait for a call. THANK YOU FOR TRUSTING US!';
                $status['captca_text'] = $answer;
            }
        }
        else{
             $answer =  $this->genNum(5); 
             $_SESSION['answer'] =$answer;
            $status['data_status']=false;
            $status['data_result']='Ooops! captcha does not match';
            $status['captca_text'] = $answer;
            
        }


        }else{
            $status['data_status']=false;
            $status['data_result']='Ooops! You already applied for this job';
             $status['captca_text'] = $_SESSION['answer'];
           
        }

         print json_encode($status);

    }

    function post_jobs(){

        $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();

        $this->load->model('Category');
        $this->load->model('tools_model');

        $catResults = $this->Category->get_all_category();

        $data['category_list'] = $catResults;
        $cat_name=array();
        if(!empty($job_description)){

            foreach ($job_description as $row) {
                $cat_name=array(
                    'category_id' => $row->category_id,
                    'job_post_id' => $row->job_post_id
                );
            }
            $related_jobs = $this->tools_model->get_related_jobs($cat_name['category_id'],$cat_name['job_post_id']);
            $data['related_jobs']=$related_jobs;

        }
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];


        }else{
            $data['roll'] = '0';
        }


        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('post_jobs/post_jobs',$data);
        $this->load->view('footer');
    }

    function post_jobs_process(){
              $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
        $this->recaptcha->recaptcha_check_answer();
        $this->form_validation->set_rules('euser_name', 'Username', 'trim|required|min_length[5]|xss_clean');
        $this->form_validation->set_rules('jp_password', 'Password', 'trim|required|xss_clean'); 
        $this->form_validation->set_rules('eemail', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('jp_password', 'Password', 'trim|required|xss_clean'); 
        $this->form_validation->set_rules('job_content', 'Job Desciption', 'trim|required|xss_clean'); 
        $this->form_validation->set_rules('jp_jobtype', 'Job type', 'trim|required|xss_clean'); 
        $this->form_validation->set_rules('jp_location', 'Location', 'trim|required|xss_clean'); 
        $this->form_validation->set_rules('category_id', 'Category', 'trim|required|xss_clean');  

        if ($this->form_validation->run()){
            if($this->recaptcha->getIsValid()){
                $status['res']=true;
                $data['roll'] = '0';
                $experience =$_POST['jp_experience_from']." to ".$_POST['jp_experience_to']."yrs";
                $employer = array(
                    'ecompany_name' => $this->input->post('ecompany_name'),
                    'euser_name' => $this->input->post('euser_name'),
                    'eemail' => $this->input->post('eemail'),
                    'epassword' => $this->input->post('jp_password'),
                );
                $resutl=  $this->tools_model->create_employer_and_login_credentials($employer);
                $date=date('Y-m-d h:i:s');
                $date2=date('Y-m-d h:i:s',strtotime("+29 days"));
                if($resutl[0]){
                        $job_content=$this->input->post('job_content');
                    $job_post = array(
                        'tbl_name' => 'job_post',
                        'job_post_id' => '0',
                        'is_paid' => 'N',
                        'employer_id' => $resutl[3],
                        'is_confirm' => 'N',
                        'is_premium' => 'N',
                        'request_deletion' => 'N',
                        'created_date_time' => $date,
                        'expiration_datetime' => $date2,
                        'jobtype' => $this->input->post('jp_jobtype'),
                        'position' => $this->input->post('jp_position'),
                        'location' => $this->input->post('jp_location'),
                        'experience' => $experience,
                        'job_content' => $job_content,
                        'category_id' => $this->input->post('category_id'),
                    );
                    $var=$this->tools_model->saveDatas($job_post);
                   
                    $status['res']=true;
                    
                    $status['data_m']="<p class='text-success text-center' type='font-size:18px;'>Your job has been successfully created please wait for the approval and please check your email as we are going to send you a confirmation link. THANK YOU!</p>";
                
                }else{

                    $status['res']=false;
                    $status['res2']=false;
                    $status['data_m']="<p class='text-danger text-center' type='font-size:18px;'>Oppsss! Somethings Wrong please do it again</p><br/><br/>";
                

               
                }
            }else{
                $status['res']=false;
                $status['res2']=true;
                $status['data_m']="<p class='text-danger text-center' type='font-size:18px;'>Oppsss! Captcha  did not match</p><br/><br/>";
                
            }
        }else{
            
                $status['res']=false;
                $status['res2']=true;
                $status['data_m']="<p class='text-danger text-center' type='font-size:18px;'>".validation_errors()."</p>";
                 
        }
        echo json_encode($status);


    }

    public function categories($id){
        $data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();

        $this->load->model('Category');
        $this->load->model('tools_model');
        $catResults = $this->Category->get_all_category();
        $data['category_list'] = $catResults;
        $catname = $this->Category->get_category_name($id);
        $data['category_name']=$catname;
        //---- SEARCH
        $search_term = $this->input->get_post('search_term', TRUE);


        if(isset($_SESSION['results_items'])){
            if(isset($_SESSION['search_term'])){
                if($_SESSION['search_term'] == $search_term){
                    $search_result_premium=$this->tools_model->search_record_by_category_premium($search_term,$id);
                    $search_result=$this->tools_model->search_record_by_category($search_term,$id);
                    $_SESSION['results_items'] =  array_merge($search_result_premium,$search_result);
                    $item_results = $_SESSION['results_items'];
                    $_SESSION['search_term'] =  $search_term;
                }else{

                    $search_result_premium=$this->tools_model->search_record_by_category_premium($search_term,$id);
                    $search_result=$this->tools_model->search_record_by_category($search_term,$id);
                    $_SESSION['results_items'] =  array_merge($search_result_premium,$search_result);
                    $item_results = $_SESSION['results_items'];
                    $_SESSION['search_term'] =  $search_term;

                }
            }else{
                $search_result_premium=$this->tools_model->search_record_by_category_premium($search_term,$id);
                $search_result=$this->tools_model->search_record_by_category($search_term,$id);

                $_SESSION['results_items'] = array_merge($search_result_premium,$search_result);
                $item_results = $_SESSION['results_itsems'];
                $_SESSION['search_term'] =  $search_term;
            }
        }else{
            $search_result_premium=$this->tools_model->search_record_by_category_premium($search_term,$id);
            $search_result=$this->tools_model->search_record_by_category($search_term,$id);
            $_SESSION['results_items'] =  array_merge($search_result_premium,$search_result);
            $item_results = $_SESSION['results_items'];
            $_SESSION['search_term'] =  $search_term;


        }

        $limit = ($this->uri->segment(4) > 0)?$this->uri->segment(4):0;
        $config['per_page'] = 50;

        $config['base_url'] = base_url() . 'patsada/categories/'.$id.'/';
        $config['total_rows'] = count($item_results);
        $config['uri_segment'] = 3;
        $choice =$config['total_rows'] / $config['per_page'];
        $config['num_links'] = round($choice);
        $config['next_link'] = '&raquo;';
        $config['prev_link'] = '&laquo;';
        $config['first_link'] = '&laquo;&laquo;';
        $config['last_link']  = '&raquo;&raquo;';
        $this->pagination->initialize($config);

        $data['total_rows']=$config['total_rows'];
        if(empty($item_results)){
            $results_jobs = array();
        }else{
            $results_jobs =  array_slice($item_results,$limit,$config['per_page']);
        }
        $data["results"] =  $results_jobs;
        $data['links'] = $this->pagination->create_links();
        $data['search_term'] = $search_term;
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
        }else{
            $data['roll'] = '0';
        }
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('sidebar',$data);
        $this->load->view('categories/categories',$data);
        $this->load->view('footer');
    }
}
