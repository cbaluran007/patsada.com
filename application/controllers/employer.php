<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Employer extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        /* Load the libraries and helpers */
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('recaptcha');
        $this->load->library('pagination');
    }
    public function index()
    {


		
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];

			
            $this->load->model('employer_model');
			
			$employer_user = $this->employer_model->get_employer_user($data['id']);
			$employer = $this->employer_model->get_employer($employer_user[0]->employer_id);
			$data['employer_user']=$employer_user[0];
            $data['employer']=$employer;
            $countries = array(
                'PH' => 'Philippines',
            );
            $data['countries']=$countries;
            $this->load->view('header',$data);
            $this->load->view('menu',$data);
            $this->load->view('employer/sidebar',$data);
            $this->load->view('employer/index',$data);

            $this->load->view('employer/footer');
            $this->load->view('employer/foots');
        }else{
            redirect('patsada/');
        }
    }

    public function edit_profile(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $this->load->model('employer_model');
            $employer = $this->employer_model->get_employer($data['id']);
            $data['employer']=$employer;

            $countries = array(
                'PH' => 'Philippines',
            );
            $data['countries']=$countries;
            $this->load->view('header',$data);
            $this->load->view('menu',$data);
            $this->load->view('employer/sidebar',$data);
            $this->load->view('employer/edit_profile',$data);
            $this->load->view('employer/footer');
            $this->load->view('employer/foots');
        }else{
            redirect('patsada/');
        }
    }

    public function process_employer_profile(){

        $this->load->model('tools_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];

            $_POST['tbl_name']='employer';
           
            $this->tools_model->saveDatas($_POST);

            redirect('employer/');

        }else{
            redirect('patsada/');
        }

    }


     public function add_sub_users(){

        $this->load->model('tools_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            
			
			
			$empData = array();
			$empData['tbl_name'] = 'employer_user';
			$empData['employer_id'] = $_POST['employer_id'];
			$empData['is_manager']  = 'N';
			
			$empData['is_active'] = 'Y';
			$empData['email'] = $_POST['email'];
			$empData['title'] = $_POST['title'];
			$empData['first_name'] = $_POST['first_name'];
			$empData['last_name'] = $_POST['last_name'];
			$empData['position'] = $_POST['position'];
			$empData['created_datetime'] = date("Y-m-d");
			$empData['is_confirmed'] = 'N';

		
			$id = $this->tools_model->saveDatas($empData,true);
			$loginsData = array();
			$loginsData['tbl_name'] = 'login_credentials';
			$loginsData['password'] = md5($_POST['password']);
			$loginsData['user_name']  = $_POST['user_name'];
			$loginsData['is_employer'] = 'Y';
			
			$loginsData['employer_user_id'] = $id;
			$loginsData['candidate_id'] = 0;
			$loginsData['is_confirmed'] = 'N';
			$loginsData['is_admin'] = 'N';
			$loginsData['created_datetime'] = date("Y-m-d");
			$res = $this->tools_model->saveDatas($loginsData);
			
			if($res){
				$for_verification = array(
					'email' => $_POST['email'],
					'user_name' => $_POST['user_name'],
					'password' => md5($_POST['password'])
				);
				
				$verification_params = base64_encode(json_encode($for_verification));
				$this->tools_model->send_verification_links($verification_params,'employer',$_POST['email'],$_POST['user_name']);
			}
			
			
            redirect('employer/add_users');

        }else{
            redirect('patsada/');
        }

    }

    public function activate(){


        $this->load->model('employer_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];


            $is_active = $_POST['is_active'];
            $employer_id = $_POST['employer_user_id'];

            $this->employer_model->employer_user_activate_deactivate($employer_id,$is_active);


            redirect('employer/add_users');

        }else{
            redirect('patsada/');
        }
    }

    public function do_upload($id){
        $this->load->library('upload');

        $this->load->model('tools_model');

        $session_data = $this->session->userdata('logged_in');
        $data['roll'] = $session_data['roll'];
        $data['id'] = $session_data['id'];


        $user_num= $data['id'];

        $image_upload_folder = FCPATH . 'assets/images/employer_pic/';

        if (!file_exists($image_upload_folder)) {
            mkdir($image_upload_folder, DIR_WRITE_MODE, true);
        }
        $this->upload_config = array(
            'upload_path'   => $image_upload_folder,
            'allowed_types' => '*',
            'max_size'      => 2048,
            'remove_space'  => TRUE,
            'encrypt_name'  => TRUE,
        );

        $this->upload->initialize($this->upload_config);

        if (!$this->upload->do_upload()) {
            $upload_error = $this->upload->display_errors();
            echo json_encode($upload_error);
            //echo  $user_num;
        } else {

            $file_info = $this->upload->data();
            $data_post=array(
                'company_logo'=>$file_info['file_name'],
                'employer_id'=>$id,
                'tbl_name' => 'employer'
            );
            //print_r($data_post);
            $var=$this->tools_model->saveDatas($data_post);
            echo json_encode($file_info);
        }

    }

    public function add_users(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $this->load->model('employer_model');
            $employer_user = $this->employer_model->get_employer_user($data['id']);
			$employer = $this->employer_model->get_employer($employer_user[0]->employer_id);
			$data['employer_user']=$employer_user[0];
			
            $employer_users_sub = $this->employer_model->get_employer_sub_user($employer_user[0]->employer_id);
            $data['employer_users_sub']=$employer_users_sub;
            $data['employer']=$employer;
            $countries = array(
                'PH' => 'Philippines',
            );
            $data['countries']=$countries;
            $this->load->view('header',$data);
            $this->load->view('menu',$data);
            $this->load->view('employer/sidebar',$data);
            $this->load->view('employer/add_users',$data);

            $this->load->view('employer/footer');
        }else{
            redirect('patsada/');
        }
    }

    public function my_profile(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $this->load->model('employer_model');
            $employer_user = $this->employer_model->get_employer_user($data['id']);
			$employer = $this->employer_model->get_employer($employer_user[0]->employer_id);
			$data['employer_user']=$employer_user[0];

            $employer_users_sub = $this->employer_model->get_employer_sub_user($employer_user[0]->employer_id);
            $data['employer_users_sub']=$employer_users_sub;
            $data['employer']=$employer;
            $countries = array(
                'PH' => 'Philippines',
            );
            $data['countries']=$countries;
            $this->load->view('header');
            $this->load->view('menu',$data);
            $this->load->view('employer/sidebar',$data);
            $this->load->view('employer/my_profile',$data);

            $this->load->view('employer/footer');
        }else{
            redirect('patsada/');
        }
    }
    public function process_employer_user_profile(){

        $this->load->model('tools_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];



            $this->tools_model->saveDatas($_POST);

            redirect('employer/my_profile');

        }else{
            redirect('patsada/');
        }

    }

   public function post_job(){
       if($this->session->userdata('logged_in'))
       {
           $session_data = $this->session->userdata('logged_in');
           $data['roll'] = $session_data['roll'];
           $data['id'] = $session_data['id'];
           $this->load->model('employer_model');
           $employer_user = $this->employer_model->get_employer_user($data['id']);
           $employer = $this->employer_model->get_employer($employer_user[0]->employer_id);
           $data['employer_user']=$employer_user[0];
           $this->load->model('Category');
           $catResults = $this->Category->get_all_category();
           $data['category_list'] = $catResults;
           $employer_users_sub = $this->employer_model->get_employer_sub_user($employer_user[0]->employer_id);
           $data['employer_users_sub']=$employer_users_sub;
           $data['employer']=$employer;
           $countries = array(
               'PH' => 'Philippines',
           );
           $data['countries']=$countries;
           $this->load->view('header',$data);
           $this->load->view('menu',$data);
           $this->load->view('employer/sidebar',$data);
           $this->load->view('employer/post_job',$data);
           $this->load->view('employer/footer');
           $this->load->view('employer/foots_job_post');
       }else{
           redirect('patsada/');
       }
   }
    public function job_post(){

        $this->load->model('tools_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $post_array=array();
            $post_array['experience']=$_POST['experience_from']." to ".$_POST['experience_to']."yrs";
            $post_array['tbl_name']='job_post';
            $post_array['job_post_id']=$_POST['job_post_id'];
            $post_array['employer_id']=$_POST['employer_id'];
            $post_array['is_paid']=$_POST['is_paid'];
            $post_array['is_confirm']=$_POST['is_confirm'];
            $post_array['is_premium']=$_POST['is_premium'];
            $post_array['request_deletion']=$_POST['request_deletion'];
            $post_array['created_date_time']=$_POST['created_date_time'];
            $post_array['expiration_datetime']=$_POST['expiration_datetime'];
            $post_array['is_urgent']=$_POST['is_urgent'];
            $post_array['jobtype']=$_POST['jobtype'];
            $post_array['position']=$_POST['position'];
            $post_array['job_content']=$_POST['job_content'];
            $post_array['category_id']=$_POST['category_id'];
            $post_array['location']=$_POST['location'];


           $this->tools_model->saveDatas($post_array);

            redirect('employer/post_job');

        }else{
            redirect('patsada/');
        }

    }
    public function applicants(){
 
        $this->load->model('employer_model');

        $this->load->model('tools_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
             $employer_user = $this->employer_model->get_employer_user($data['id']);
            
            $search_term = $this->input->post('search_term', TRUE);


            if(isset($_SESSION['results_items'])){
                if(isset($_SESSION['search_term'])){
                    if($_SESSION['search_term'] == $search_term){
                        $search_result=$this->employer_model->search_applicants($employer_user[0]->employer_id,$search_term);
                        $_SESSION['results_items'] =  array_merge($search_result);
                        $item_results = $_SESSION['results_items'];
                        $_SESSION['search_term'] =  $search_term;
                    }else{

                        $search_result=$this->employer_model->search_applicants($employer_user[0]->employer_id,$search_term);
                        $_SESSION['results_items'] =  array_merge($search_result);
                        $item_results = $_SESSION['results_items'];
                        $_SESSION['search_term'] =  $search_term;

                    }
                }else{
                    $search_result=$this->employer_model->search_applicants($employer_user[0]->employer_id,$search_term);
                    $_SESSION['results_items'] =  array_merge($search_result);
                    $item_results = $_SESSION['results_itsems'];
                    $_SESSION['search_term'] =  $search_term;
                }
            }else{
                $search_result=$this->employer_model->search_applicants($employer_user[0]->employer_id,$search_term);
                $_SESSION['results_items'] =  array_merge($search_result);
                $item_results = $_SESSION['results_items'];
                $_SESSION['search_term'] =  $search_term;
            }


            $config = array();
            $limit = ($this->uri->segment(3) > 0)?$this->uri->segment(3):0;
            $config['per_page'] = 10;

            $config['base_url'] = base_url() . 'employer/applicants/';
            $config['total_rows'] = count($item_results);
            $config['uri_segment'] = 3;
            $choice =$config['total_rows'] / $config['per_page'];
            $config['num_links'] = round($choice);
            $config['next_link'] = '&raquo;';
            $config['prev_link'] = '&laquo;';
            $config['first_link'] = '&laquo;&laquo;';
            $config['last_link']  = '&raquo;&raquo;';
            $this->pagination->initialize($config);

            $data['total_rows']=$config['total_rows'];

            if(empty($item_results)){
                $results_jobs = array();
            }else{
                $results_jobs =  array_slice($item_results,$limit,$config['per_page']);
            }

            $data["results"] =  $results_jobs;
            $data['links'] = $this->pagination->create_links();

            $data['search_term'] = $search_term;




            $employer_user = $this->employer_model->get_employer_user($data['id']);
            $applicants = $this->employer_model->get_all_applicants($employer_user[0]->employer_id);
            $data['applicants']=$applicants;

            $employer = $this->employer_model->get_employer($employer_user[0]->employer_id);
            $data['employer_user']=$employer_user[0];
            $data['employer']=$employer;
            $countries = array(
                'PH' => 'Philippines',
            );
            $data['countries']=$countries;
            $this->load->view('header',$data);
            $this->load->view('menu',$data);
            $this->load->view('employer/sidebar',$data);
            $this->load->view('employer/applicants',$data);

            $this->load->view('employer/footer');
         }


    }

    public function job_posted(){


        $this->load->model('tools_model');
        $this->load->model('employer_model');
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
             $employer_user = $this->employer_model->get_employer_user($data['id']);
             
            $search_term = $this->input->post('search_term', TRUE);


            if(isset($_SESSION['results_items'])){
                if(isset($_SESSION['search_term'])){
                    if($_SESSION['search_term'] == $search_term){
                        $search_result=$this->employer_model->search_employer_posted_jobs($employer_user[0]->employer_id,$search_term);
                        $_SESSION['results_items'] =  array_merge($search_result);
                        $item_results = $_SESSION['results_items'];
                        $_SESSION['search_term'] =  $search_term;
                    }else{

                        $search_result=$this->employer_model->search_employer_posted_jobs($employer_user[0]->employer_id,$search_term);
                        $_SESSION['results_items'] =  array_merge($search_result);
                        $item_results = $_SESSION['results_items'];
                        $_SESSION['search_term'] =  $search_term;

                    }
                }else{
                    $search_result=$this->employer_model->search_employer_posted_jobs($employer_user[0]->employer_id,$search_term);
                    $_SESSION['results_items'] =  array_merge($search_result);
                    $item_results = $_SESSION['results_itsems'];
                    $_SESSION['search_term'] =  $search_term;
                }
            }else{
                $search_result=$this->employer_model->search_employer_posted_jobs($employer_user[0]->employer_id,$search_term);
                $_SESSION['results_items'] =  array_merge($search_result);
                $item_results = $_SESSION['results_items'];
                $_SESSION['search_term'] =  $search_term;
            }


            $config = array();
            $limit = ($this->uri->segment(3) > 0)?$this->uri->segment(3):0;
            $config['per_page'] = 10;

            $config['base_url'] = base_url() . 'employer/applicants/';
            $config['total_rows'] = count($item_results);
            $config['uri_segment'] = 3;
            $choice =$config['total_rows'] / $config['per_page'];
            $config['num_links'] = round($choice);
            $config['next_link'] = '&raquo;';
            $config['prev_link'] = '&laquo;';
            $config['first_link'] = '&laquo;&laquo;';
            $config['last_link']  = '&raquo;&raquo;';
            $this->pagination->initialize($config);

            $data['total_rows']=$config['total_rows'];

            if(empty($item_results)){
                $results_jobs = array();
            }else{
                $results_jobs =  array_slice($item_results,$limit,$config['per_page']);
            }

            $data["results"] =  $results_jobs;

            $data['links'] = $this->pagination->create_links();

            $data['search_term'] = $search_term;




            $this->load->model('employer_model');

            $employer_user = $this->employer_model->get_employer_user($data['id']);
            $applicants = $this->employer_model->get_all_applicants($data['id']);
            $data['applicants']=$applicants;

            $employer = $this->employer_model->get_employer($employer_user[0]->employer_id);
            $data['employer_user']=$employer_user[0];
            $data['employer']=$employer;
            $countries = array(
                'PH' => 'Philippines',
            );
            $data['countries']=$countries;
            $this->load->view('header',$data);
            $this->load->view('menu',$data);
            $this->load->view('employer/sidebar',$data);
            $this->load->view('employer/job_posted',$data);

            $this->load->view('employer/footer');
        }

    }
	public function verify($param)
    {
		$this->load->model('employer_model');
		$stats=$this->employer_model->employer_get_validated($param);
		if($stats[0]){
			$this->session->set_userdata('logged_in', $stats[1]);
            $this->load->model('tools_model');
         
             
			$this->tools_model->employersercretcode($stats[1]);
           
            

            //update the row where id is equal to id, insert the value to column authentication code

            redirect('employer/');
        }

    }
    public function logout(){
        $this->session->unset_userdata('logged_in');

        redirect('patsada', 'refresh');

    }

    function job_posted_description($id){
        if($this->session->userdata('logged_in'))
        {
            $this->load->model('tools_model');
            $this->load->model('employer_model');
            $job_description = $this->tools_model->get_job_posted($id);
            $total_applicants = $this->employer_model->get_total_applicants($id);

            $session_data = $this->session->userdata('logged_in');
            $data['roll'] = $session_data['roll'];
            $data['id'] = $session_data['id'];
            $data['job_description'] = $job_description;
            $data['total_applicants'] = $total_applicants[0]->total;



            $this->load->model('employer_model');

            $employer_user = $this->employer_model->get_employer_user($data['id']);
            $employer = $this->employer_model->get_employer($employer_user[0]->employer_id);
            $data['employer_user']=$employer_user[0];
            $data['employer']=$employer;
            $countries = array(
                'PH' => 'Philippines',
            );
            $data['countries']=$countries;
            $this->load->view('header',$data);
            $this->load->view('menu',$data);
            $this->load->view('employer/sidebar',$data);
            $this->load->view('employer/job_posted_descriptions',$data);

            $this->load->view('employer/footer');
            $this->load->view('employer/foots');
        }else{
            redirect('patsada/');
        }



    }

    function jobs_applicants($id){
        $this->load->model('employer_model');
        $total_applicants = $this->employer_model->get_this_job_applicants($id);
        $data['result']=$total_applicants;
        $this->load->view('employer/job_applicants',$data);
    }

    function flags($basemail, $jobpost_id){

        $mail = base64_decode($basemail);
        $res = $this->tools_model->get_employers_info($mail);
        $user['user_name']=$res[0]->user_name;

        $this->load->model('tools_model');
        $result=  $this->tools_model->login($user);
        if($result[0]){
                $status['status']=true;
            if($result[1]=='Y' && $result[2] == 'N'){
                $status['data']=array('type'=>'candidate','id'=>$result[4]);

                $sess_array = array(
                    'id' => $result[4],
                    'roll' => 'candidate',
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }else{
                $status['data']=array('type'=>'employer','id'=>$result[3]);
                $sess_array = array(
                    'id' => $result[3],
                    'roll' => 'employer',
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }


                redirect('/employer/job_posted_description/'.$jobpost_id);
        }else{
        //    $status['status']=false;
          //  $status['data']="Oppsss! We are unable to validate the login credentials. Please try again";
        }
        


    }
}
