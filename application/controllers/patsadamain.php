<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Patsadamain extends CI_Controller {
    public function __construct()
    {
        parent::__construct(); 
        /* Load the libraries and helpers */
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('recaptcha');
        $this->load->library('pagination');
    }
    public function index()
    { 
	 $this->load->view('admin/index');
    }
 }