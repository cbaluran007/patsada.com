<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        /* Load the libraries and helpers */
        $this->load->library(array('form_validation', 'session'));
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('recaptcha');
    }

    function candidate_registration(){
        
        $this->load->model('tools_model');
        $resutl=  $this->tools_model->create_candidate_and_login_credentials($_POST);
        if($resutl[0]){
                    $return['success'] = true;
                    $return['message'] ="Your account has been successfully created as Job Seeker. Please check your email as we are going to send you a confirmation link. THANK YOU!";
					$return['type'] = "candidate/edit_profile";
					$sess_array = array(
						'id' => $resutl[4],
						'roll' => 'candidate',
					);
					$this->session->set_userdata('logged_in', $sess_array);
					
		}else{
                    $return['success'] = false;
                    $return['message']="Error connecting database";
					$return['type'] = "";
        }
           

        echo json_encode($return);
    }
	function employer_registration(){
        
        $this->load->model('tools_model');
        $resutl=  $this->tools_model->create_employer_and_login_credentials($_POST);
        if($resutl[0]){
                    $return['success'] = true;
                    $return['message'] ="Your account has been successfully created as Employers User. Please check your email as we are going to send you a confirmation link. THANK YOU!";
					$return['type'] = "employer";
					$sess_array = array(
						'id' => $resutl[3],
						'roll' => 'employer',
					);
					
					$this->session->set_userdata('logged_in', $sess_array);
					
		}else{
                    $return['success'] = false;
                    $return['message']="Error connecting database";
					$return['type'] = "";
        }
           

        echo json_encode($return);
    }
	
	function get_validate_company_duplication(){
		$this->load->model('tools_model');
		$stats = $this->tools_model->get_validate_company_duplication($_POST['ecompany_name']);
		if(!$stats){
			$status = true;
		}else{
			$status = false;
		}
		echo json_encode($status);
	}
	function get_validate_user_name_duplication($isCandidate, $isEmployer){
		$this->load->model('tools_model');


		if($isCandidate == 'Y'){
            $user_name=$_POST['user_name'];
			$isDup = $this->tools_model->get_validate_user_name_duplication($user_name,$isCandidate,$isEmployer);
		}else{
            $user_name=$_POST['euser_name'];
			$isDup = $this->tools_model->get_validate_user_name_duplication($user_name,$isCandidate,$isEmployer);
		}
		
		if(!$isDup){
			$status = true;
		}else{
			$status = false;
		}
		
	
		echo json_encode($status);
	}
	
	function get_validate_email_duplication($type){
		switch($type){
			case 'candidate':{
			$this->load->model('candidate_model');
			$isDup = $this->candidate_model->get_validate_email_duplication($_POST['email']);
			if(!$isDup){
				$status = true;
			}else{
				$status = false;
			}
			
		
			echo json_encode($status);
			break;
			}
			case 'employer':{
			$this->load->model('employer_model');
			$isDup = $this->employer_model->get_validate_email_duplication($_POST['eemail']);
			if(!$isDup){
				$status = true;
			}else{
				$status = false;
			}
			
		
			echo json_encode($status);
			break;
			}
		}
	}

	function get_validate_email_user_duplication($type){
		switch($type){
			case 'candidate':{
			$this->load->model('candidate_model');
			$isDup = $this->candidate_model->get_validate_email_duplication($_POST['email']);
			if(!$isDup){
				$status = true;
			}else{
				$status = false;
			}


			echo json_encode($status);
			break;
			}
			case 'employer':{
			$this->load->model('employer_model');
               $email = $_POST['email'];

			$isDup = $this->employer_model->get_validate_email_duplication($email);
			if(!$isDup){
				$status = true;
			}else{
				$status = false;
			}


			echo json_encode($status);
			break;
			}
		}
	}

    function login_credintials(){

        $this->load->model('tools_model');
        $result=  $this->tools_model->login($_POST);
        if($result[0]){
                $status['status']=true;
            if($result[1]=='Y' && $result[2] == 'N'){
                $status['data']=array('type'=>'candidate','id'=>$result[4]);

                $sess_array = array(
                    'id' => $result[4],
                    'roll' => 'candidate',
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }else{
                $status['data']=array('type'=>'employer','id'=>$result[3]);
                $sess_array = array(
                    'id' => $result[3],
                    'roll' => 'employer',
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }

        }else{
            $status['status']=false;
            $status['data']="Oppsss! We are unable to validate the login credentials. Please try again";
        }
        echo json_encode($status);
    }

    function update_viewedapplicant(){
        $data['tbl_name'] = 'job_post_event';
        $data['job_post_event'] = 1;
        echo $data['job_post_id'] = $_POST['job_post_id'];
        $data['is_viewed'] = 'Y';
        $this->tools_model->update_viewedapplicant($data,false);  
            
    }

    function emailemployerapplicants(){
        $employers = $this->tools_model->get_employers();

        foreach ($employers as $key) {
            $jobpost = $this->tools_model->get_jobpost($key->employer_id);
            $to = $key->email;
            $user = $key->company_name;

            foreach ($jobpost as $key) {
                $post_id = $key->job_post_id;
                print_r($post_id);
                $applicants = $this->tools_model->count_applicants($post_id);
                print_r($applicants);
                $numofapplicants =  $applicants[0]->applicants;

                if ($numofapplicants >= 1) {
                    $viewed = $this->tools_model->notify_newapplicants($to, $user, $post_id, $numofapplicants);
                }
                
                
            }

        }    
        
    }

    function application(){

        $this->load->model('tools_model');
        $_POST['tbl_name']='job_post_event';
        unset($_POST['_wysihtml5_mode']);
        $var=$this->tools_model->saveDatas($_POST);

        $status['data_status']=true;
        $status['data_result']='Your application has been send to Employer. we send you an approval if the employer approve your application or they will call you. THANK YOU FOR TRUSTING WITH US!';

        echo json_encode($status);
    }

    function accept_applicants(){

        $this->load->model('employer_model');
        $stats = $this->employer_model->accepted_applicants($_POST['job_post_event_id']);
        echo json_encode($stats);
    }

    function decline_applicants(){

        $this->load->model('employer_model');
        $stats = $this->employer_model->declined_applicants($_POST['job_post_event_id']);

        echo json_encode($stats);
    }

    function remove_applicants(){

        $this->load->model('employer_model');

        /*$select_array=array();
        foreach($selected as $row){
            #print_r($row);
            $this->tools_model->delete_applicants($row);
        }

*/

        $stats = $this->employer_model->delete_applicants($_POST);

        echo json_encode($stats);

         $status['status']=true;
        echo json_encode($status);
    }

    function remove_selected_applicants(){
        $this->load->model('employer_model');

        $select_array=array();
        $seleceted=$_POST['job_post_event_id'];

        foreach($seleceted as $row){
            #print_r($row);
            $this->employer_model->delete_selected_applicants($row);
        }
        redirect('employer/applicants');
    }



    function accounts_settings(){

        $this->load->model('tools_model');
        $array_post=array(
            'tbl_name'=>$_POST['tbl_name'],
            'login_credentials_id'=>$_POST['login_credentials_id'],
            'candidate_id'=>$_POST['candidate_id'],
            'password'=>$_POST['password'],
        );
        $var=$this->tools_model->saveDatas($array_post);

        $status['data_results']='Your password successfully changed!';

        echo json_encode($status);
    }
    function employer_accounts_settings(){

        $this->load->model('tools_model');
        $array_post=array(
            'tbl_name'=>'login_credentials',
            'login_credentials_id'=>$_POST['login_credentials_id'],
            'employer_user_id'=>$_POST['employer_user_id'],
            'password'=>$_POST['password'],
        );
        $var=$this->tools_model->saveDatas($array_post);

        $status['data_results']='Your password successfully changed!';

        echo json_encode($status);
    }

    function forgot_password(){

        $this->load->model('tools_model');

       $result=$this->tools_model->forgot_pass_word($_POST);
        if($result){
            $status['success']=true;
            $status['message']='Your password has been sent to your email. Please Check Your Email';
        }else{
            $status['success']=false;
            $status['message']='Sorry this email has not yet been registered.';
        }
        echo json_encode($status);

    }

}
