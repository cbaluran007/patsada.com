<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * reCaptcha File Config
 *
 * File     : recaptcha.php
 * Created  : May 14, 2013 | 4:22:40 PM
 * 
 * Author   : Andi Irwandi Langgara <irwandi@ourbluecode.com>
 */

$config['public_key']   = '6Lf-TOcSAAAAAEKwrTw0TQWlqoCiuQ0s3Y7LsUFv';
$config['private_key']  = '6Lf-TOcSAAAAABQtF_27P5i6Y7etUG4ZYVlTXCWG';

// Set Recaptcha theme, default red (red/white/blackglass/clean)
$config['recaptcha_theme']  = 'white';



?>
