<?php
$employer_details=$employer[0];


$full_name=$employer_user->title." ".$employer_user->first_name." ".$employer_user->last_name;
$full_address=$employer_user->address.", ".$employer_user->city.", ".$employer_user->province.", ".$employer_user->country.", ".$employer_user->zip_code;
//$full_address= $employer_details->company_address.', '.$employer_details->company_city.', '.$employer_details->company_province.', '.$employer_details->company_country.', '.$employer_details->company_zip_code
?>

<div class='col-sm-9 col-lg-9' id="job_contain">

    <div class="panel panel-default" >
        <div class="panel-heading">
            <i class='icon-user main-color-yellow'> </i>Employer User Profile
            <a data-toggle="modal" style="margin-top:-7px;" id="save-btn" href="#edit" class="btn btn-primary pull-right"><i class='icon-plus'></i> Edit Profile</a>

        </div>
        <div class='panel-body'>



                    <dl class="dl-horizontal">
                        <dt>Name:</dt>
                        <dd><?php if($employer_user->title !='' &&  $employer_user->first_name  != '' &&  $employer_user->last_name != ''):?>
                               <?php echo $full_name;?>
                            <?php else:?>
                               Mr. Juan dela Cruz
                            <?php endif;?> </dd>
                        <dt>Position:</dt>
                        <dd><?php if($employer_user->position !=''):?>
                            <?php echo $employer_user->position?>
                            <?php else:?>
                            Add Position
                           <?php endif;?>
                        </dd>
                        <dt>Email:</dt>
                        <dd><?php if($employer_user->email !=''):?>
                                <?php echo $employer_user->email?>
                            <?php else:?>
                                Add Email
                            <?php endif;?>
                        </dd>
                        <dt>Address:</dt>
                        <dd><?php if($full_address !=', , , , '):?>
                                <?php echo  $full_address?>
                            <?php else:?>
                                Add Address
                            <?php endif;?>
                        </dd>
                    </dl>


        </div>

    </div>
    <div class=""style="height: 296px!important;"></div>
    <div class="clearfix"></div>
</div>




<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Users</h4>
            </div>
            <div class="modal-body">


                <form class='form-horizontal' action="<?php echo base_url()?>employer/process_employer_user_profile" method="POST">
                    <input type="hidden" name='tbl_name' value="employer_user">
                    <input type="hidden" name='employer_id' value="<?php echo $employer_details->employer_id;?>">
                    <input type="hidden" name='employer_user_id' value="<?php echo $employer_user->employer_user_id?>">
                    <div class='form-group'>
                        <label class='control-label col-sm-3  col-md-3 col-lg-3'>Title</label>
                        <div class='col-lg-4'>
                            <select class='form-control' name='title' id='title'>

                                <option <?php if($employer_user->title=='Mr'){echo "selected";}?>>Mr.</option>
                                <option <?php if($employer_user->title=='Ms'){echo "selected";}?> >Ms.</option>
                                <option <?php if($employer_user->title=='Mrs'){echo "selected";}?> >Mrs.</option>
                                <option <?php if($employer_user->title=='Dr'){echo "selected";}?> >Dr.</option>
                            </select>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-sm-3  col-md-3 col-lg-3'>First name</label>
                        <div class='col-sm-9  col-md-9 col-lg-9'>
                            <input type="text" class='form-control' name='first_name' id='first_name' value="<?php if($employer_user->first_name !=''){echo $employer_user->first_name;}?>" placeholder="First Name"/>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-sm-3  col-md-3 col-lg-3'>Last name</label>
                        <div class='col-sm-9  col-md-9 col-lg-9'>
                            <input type="text" class='form-control' name='last_name' id='last_name' value="<?php if($employer_user->last_name !=''){echo $employer_user->last_name;}?>" placeholder="Last Name"/>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-sm-3  col-md-3 col-lg-3'>Position</label>
                        <div class='col-sm-9  col-md-9 col-lg-9'>
                            <input type="text" class='form-control' name='position' id='position' value="<?php if($employer_user->position !=''){echo $employer_user->position;}?>"  placeholder="Position"/>
                        </div>
                    </div>

                    <div class='form-group'>
                        <label class='control-label col-sm-3  col-md-3 col-lg-3'>Email</label>
                        <div class='col-sm-9  col-md-9 col-lg-9'>
                            <input type="text" class='form-control' name='email' id='email' value="<?php if($employer_user->email !=''){echo $employer_user->email;}?>"  placeholder="Email"/>
                        </div>
                    </div>

                    <div class='form-group'>
                        <label class='control-label col-sm-3  col-md-3 col-lg-3'>Address</label>
                        <div class='col-sm-9  col-md-9 col-lg-9'>
                            <input type="text" class='form-control' name='address' id='address' value="<?php if($employer_user->address !=''){echo $employer_user->address;}?>"  placeholder="Address"/>
                        </div>
                    </div>

                    <div class='form-group'>
                        <label class='control-label col-sm-3  col-md-3 col-lg-3'>City</label>
                        <div class='col-sm-9  col-md-9 col-lg-9'>
                            <input type="text" class='form-control' name='city' id='city' value="<?php if($employer_user->city !=''){echo $employer_user->city;}else{echo "Cagayan de Oro City";}?>" placeholder="Email"/>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-sm-3  col-md-3 col-lg-3'>Province</label>
                        <div class='col-lg-9'>
                            <input name='province' id='province' class='form-control' value="<?php if($employer_user->province !=''){echo $employer_user->province;}else{echo "Misamis Oriental";}?>" value=" ">
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-sm-3  col-md-3 col-lg-3'>Country</label>
                        <div class='col-lg-9'>
                            <select name='country' id='country' class="form-control">
                                <?php if($employer_user->country !=''){$selected='selected';}else{$selected='';}

                                foreach($countries as $rows){
                                    ?>

                                    <option><?=$rows?></option>
                                <?php
                                }?>
                            </select>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-sm-3  col-md-3 col-lg-3'>Zip Code</label>
                        <div class='col-lg-9'>
                            <input name='zip_code' id='zip_code' class='form-control' value="<?php if($employer_user->zip_code !=''){echo $employer_user->zip_code;}else{echo "9000";}?>" >
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class='control-label col-sm-3  col-md-3 col-lg-3'> </label>
                        <div class='col-sm-9  col-md-9 col-lg-9'>
                            <button type="submit" class='btn btn-success'>Save</button>
                        </div>
                    </div>
                </form>


            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<hr style="border:none!important;">
</div>
<div id="push"></div>
</div>