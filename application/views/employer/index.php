<?php
    $employer_details=$employer[0];
    $full_address= $employer_details->company_address.', '.$employer_details->company_city.', '.$employer_details->company_province.', '.$employer_details->company_country.', '.$employer_details->company_zip_code
?>
<script type="text/javascript">


    $(document).ready(function (){
        $('iframe').each(function(){
            var url = $(this).attr("src");
            $(this).attr("src",url+"?wmode=transparent");
        });

    });
</script>
<div class='col-sm-9 col-lg-9' id="job_contain">
<div class="container">
<div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
               <i class='icon-group main-color-yellow'> </i>Employer Profile
			   <?php if($employer_user->is_manager == 'Y'): ?>
                <a data-toggle="modal" style="margin-top:-7px;" id="save-btn" href="#edit" class="btn btn-primary pull-right"><i class='icon-pencil'></i> Edit Profile</a>
				<?php endif; ?>
            </div>
            <div class='panel-body'>
                <div class='col-sm-5 col-md-3  col-lg-3 '>
                    <ul class="unstyled" id="hover-img" style='list-style: none!important; '>
                        <li class="col-sm-12" style='text-decoration:none!important; padding:0!important;margin-left: -20px!important;'>
                            <a href="#uploads" data-toggle="modal" id="hover-img" style="left:1px!important;">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <br/>
                                        <br/>
                                        <br/>
                                        <p class='text-center btn-lg'> <i class='icon-camera' ></i></p>
                                    </div>
                                    <?php if($employer_details->company_logo !=''){
                                        ?>
                                        <img src="<?php echo base_url()?>assets/images/employer_pic/<?php echo $employer_details->company_logo;?>" alt="ALT NAME" id='img_upload' style="height: 180px;width: 180px;" >
                                    <?php
                                    }else{
                                        ?>
                                        <img src="http://placehold.it/180x180" alt="ALT NAME" id='img_upload' style="height: 180px;width: 180px;" >
                                    <?php
                                    }?>

                                </div>
                            </a>
                        </li>
                    </ul>



                </div>

                <div class="col-sm-6 col-md-9  col-lg-9 ">
                    <blockquote>
                        <p><?php echo $employer_details->company_name;?></p>
                        <small><cite title="Source Title"><?php if($full_address !=', , , , '){echo $full_address;}else{echo 'Add Phone if any';} ?> <i class="icon-map-marker"></i></cite></small>
                    </blockquote>
                    <p>
                        <i class="icon-phone"></i> <?php if($employer_details->phone_no !=''){echo $employer_details->phone_no;}else{echo 'Add Phone if any';} ?><br>
                        <i class="icon-print"></i> <?php if($employer_details->fax_no !=''){echo $employer_details->fax_no;}else{ echo 'Add fax number if any';}?><br>
                        <i class="icon-globe"></i> <?php if($employer_details->url !=''){echo $employer_details->url;}else{echo 'Add url if any';}?>
                    </p>

                </div>

            </div>

        </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class='icon-group main-color-yellow'> </i>Company Description
        </div>
        <div class='panel-body'>

            <?php if($employer_details->company_description !=''){echo $employer_details->company_description;}else{echo 'Add Company Description';} ?>

        </div>

    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class='icon-group main-color-yellow'> </i>Company Video
        </div>
        <div class='panel-body'>
            <div class='col-lg-12'>
                <iframe title="YouTube video player" width="480" height="390" src="<?php if($employer_details->video_url !=''){echo $employer_details->video_url."?wmode=transparent";}else{echo 'http://player.vimeo.com/video/24535181?title=0&amp;byline=0?wmode=transparent';}?>" frameborder="0" wmode="Opaque" ></iframe>

             </div>
        </div>
    </div>


        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 999999!important;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Edit Company Profile</h4>
                    </div>
                    <div class="modal-body">
                        <form class='form-horizontal' id='' action="<?php echo base_url()?>employer/process_employer_profile" method="POST" >
                            <input type="hidden" name='employer_id' value="<?php echo $employer_details->employer_id;?>">
                            <div class='col-sm-12 col-lg-12' id='form-emp'>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'>Name</label>
                                    <div class='col-lg-9'>
                                        <input name='company_name' id='company_name' class="form-control" value="<?php echo $employer_details->company_name?>">
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'>Description</label>
                                    <div class='col-lg-9'>
                                        <textarea name='company_description' id='company_description' class="form-control" ><?php echo $employer_details->company_description?> </textarea>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'>Address</label>
                                    <div class='col-lg-9'>
                                        <input name='company_address' id='company_address' class="form-control" value='<?php if($employer_details->company_address !=''){echo $employer_details->company_address;}?>'>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'>City</label>
                                    <div class='col-lg-9'>
                                        <input name='company_city' id='company_city' class="form-control" value='<?php if($employer_details->company_city !=''){echo $employer_details->company_city;}else{echo "Cagayan de Oro City";}?>'>

                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'>Province</label>
                                    <div class='col-lg-9'>
                                        <input name='company_province' id='company_province' class="form-control" value='<?php if($employer_details->company_province !=''){echo $employer_details->company_province;}else{echo "Misamis Oriental";}?>'>

                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'>Zip Code</label>
                                    <div class='col-lg-9'>
                                        <input name='company_zip_code' id='company_zip_code' class="form-control" value='<?php if($employer_details->company_zip_code !=''){echo $employer_details->company_zip_code;}else{echo "9000";}?>'>

                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'>Country</label>
                                    <div class='col-lg-9'>
                                        <select name='company_country' id='company_country' class="form-control">
                                            <?php if($employer_details->company_country !=''){$selected='selected';}else{$selected='';}
                                            foreach($countries as $rows){
                                                ?>

                                                <option><?=$rows?></option>
                                            <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'> </label>
                                    <div class='col-lg-9'>
                                        <button type="button" id='next' class="btn btn-primary">Next</button>
                                    </div>
                                </div>
                            </div>
                            <div class='col-sm-12 col-lg-12 hide' id='form-emp_d'>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'>Phone</label>
                                    <div class='col-lg-9'>
                                        <input name='phone_no' id='phone_no' class="form-control" value="<?php echo $employer_details->phone_no?>">
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'>fax no</label>
                                    <div class='col-lg-9'>
                                        <input name='fax_no' id='fax_no' class="form-control" value="<?php echo $employer_details->fax_no?> ">
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'>Website</label>
                                    <div class='col-lg-9'>
                                        <input name='url' id='url' class="form-control" value='<?php if($employer_details->url !=''){echo $employer_details->url;}?>'>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'>YouTube Url</label>
                                    <div class='col-lg-9'>
                                        <input name='video_url' id='video_url' class="form-control" value='<?php if($employer_details->video_url !=''){echo $employer_details->video_url;}?>'>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-lable col-lg-3'> </label>
                                    <div class='col-lg-9'>
                                        <button type="button"  class="btn btn-primary" id='back'>Back</button>
                                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                                        <button type="submit"  class="btn btn-primary pull-right">Save</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="uploads" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Upload Logo</h4>
                    </div>
                    <div class="modal-body">
                        <form class='form-horizontal' action="" method="POST">
                            <div class='form-group'>
                                <div class='col-lg-4'>
                                    <input type="file" name='userfile' id='userfile'>
                                </div>
                                <label class='col-lg-8 control-label'>
                                    <span style="font-size: 11px; padding-left:15px;"><em> Please limit file size to 90kb. only JPG, GIF and PNG files are accepted.  Recommended size 200 x 200 px</em></span>
                                </label>
                            </div>
                        </form>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->




</div>
    </div>
</div>



