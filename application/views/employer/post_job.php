<?php
$employer_details=$employer[0];
$full_address= $employer_details->company_address.', '.$employer_details->company_city.', '.$employer_details->company_province.', '.$employer_details->company_country.', '.$employer_details->company_zip_code
?>

<div class="col-xs-12 col-sm-9" id="job_contain" xmlns="http://www.w3.org/1999/html">


    <div class="panel panel-default">
        <div class="panel-heading">
            <i class='icon-group main-color-yellow'> </i>Job Post
            <p class="pull-left visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
            </p>
        </div>
        <div class='panel-body'>
            <form class='form-horizontal' id='form_post' action='<?php echo base_url()?>employer/job_post' method="POST">

                <input type="hidden" name='job_post_id' value="0">
                <input type="hidden" name='employer_id' value="<?php echo $employer_details->employer_id;?>">
                <input type="hidden" name='is_paid' value="N">
                <input type="hidden" name='is_confirm' value="N">
                <input type="hidden" name='is_premium' value="N">
              <input type="hidden" name="created_date_time" id='created_date_time' value="<?php echo $date=date('Y-m-d h:i:s')?>">
              <input type="hidden" name="expiration_datetime" id='expiration_datetime' value="<?php echo $date=date('Y-m-d h:i:s',strtotime("+29 days"))?>">
            <div class="form-group">
                <label class="col-lg-3 control-label">Job Post Type</label>
                <div class="col-lg-9 controls">
                    <fieldset>
                        <div class="input-group">
                            <div class="btn-group" data-toggle="buttons"  >
                                <input type='hidden' name='is_urgent' value='N' id='is_urgent'>
                                <label class="btn btn-warning "  id='urg'>
                                    <input type="radio"  value="Y" checked> Urgent
                                </label>
                                <label class="btn btn-warning active" id='non_urg'>
                                    <input type="radio"  value="N"> Non-urgent
                                </label>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Job type</label>
                <div class="col-lg-9 controls">
                    <fieldset>
                        <div class="input-group">

                            <div class="btn-group" data-toggle="buttons"  >
                                <input type='hidden' name='jobtype' value='Fulltime' id='jbtype'>
                                <label class="btn btn-primary active"  id='ful'>
                                    <input type="radio"  value="fulltime" checked> Full-time
                                </label>
                                <label class="btn btn-primary" id='partym'>
                                    <input type="radio"  value="parttime"> Part-time
                                </label>
                                <label class="btn btn-primary" id='fre'>
                                    <input type="radio"  value="contract"> Contract
                                </label>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3  control-label">Position</label>
                <div class="col-lg-5">

                    <input  type="text" name="position" id="position" tabindex="2" class="form-control" value=" " />


                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3  control-label">Job Description</label>
                <div class="col-lg-8">

                    <textarea class='form-control'  tabindex="6" name='job_content' id='job_content' placeholder="Enter Description..." style="height: 200px; ">   <?php echo set_value('jobdescription'); ?> </textarea>

                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3  control-label">Category</label>
                <div class="col-lg-5">

                    <select name="category_id" id="category_id" class="form-control"  tabindex="1">
                        <option></option>
                        <?php foreach($category_list[0] as $cat_list):?>
                            <optgroup label=" <?php echo $cat_list[1]; ?>"
                                    <?php foreach($category_list[1][$cat_list[0]] as $cat_list_sub):?>
                                             <option value="<?php echo $cat_list_sub[0]; ?>" ><?php echo $cat_list_sub[1]; ?></option>
                                    <?php endforeach; ?>
                            </optgroup>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3  control-label">Location</label>
                <div class="col-lg-3">
                    <select name="location" id="location" class="form-control" tabindex="3" >
                        <option value="Anywhere"  >Anywhere</option>
                        <option value="Butuan"    >Butuan</option>
                        <option value="Cagayan"   selected="selected">Cagayan de Oro</option>
                        <option value="Dapitan"   >Dapitan</option>
                        <option value="Davao"     >Davao</option>
                        <option value="General"   >General Santos</option>
                        <option value="Gingoog"   >Gingoog</option>
                        <option value="Iligan"    >Iligan</option>
                        <option value="Malaybalay">Malaybalay</option>
                        <option value="Marawi"    >Marawi</option>
                        <option value="Ozamis"    >Ozamis</option>
                        <option value="Pagadian"  >Pagadian</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">Years of experience</label>
                <div class="col-lg-5 ">
                    <div class="input-group">
                        <span class="input-group-addon" style='hieght:25px;'>From</span>
                        <input type="text" class="form-control" id='experience_from' name='experience_from' placeholder="0" value="0">
                        <span class="input-group-addon">TO</span>
                        <input type="text" class="form-control" id='experience_to' name='experience_to' placeholder="3" value="0">
                </div>

                </div>

            </div>


            <div class="form-group">
                <label class="col-lg-3  control-label"> </label>
                <div class="col-lg-9">
                    <input type="submit"  class="btn btn-success"  value="Post Job"/>
                </div>
            </div>
            </form>
        </div>
    </div>

</div>



<hr style="border:none!important;">
</div>

<script src="<?php echo base_url();?>assets/js/ckeditor/ckeditor.js"></script>
  