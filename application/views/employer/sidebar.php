<script>
    $(document).ready(function(){
        $('[data-toggle=offcanvas]').click(function() {
            $('.row-offcanvas').toggleClass('active');

        });
    });
</script>



<div class="modal fade" id="change_pass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
                <form id="accounts_settings" action="<?php echo base_url()?>ajax/employer_accounts_settings" method="post" class='form-horizontal'>
                    <div id="alerts_message"></div>
                    <input type="hidden" name='login_credentials_id'   value="1">
                    <input type="hidden" name='employer_user_id' value="<?php echo $id;?>">
                    <div class='form-group'>
                        <label class="control-label col-lg-3">New Password</label>
                        <div class='col-lg-9'>
                            <input id="password_1" class='form-control' name="password" type="password" />

                        </div>
                    </div>
                    <div class='form-group'>
                        <label class="control-label col-lg-3">Confirm Password</label>
                        <div class='col-lg-9'>
                            <input id="password_2" class='form-control' name="password_2" type="password" />

                        </div>
                    </div>
                    <div class='form-group'>
                        <label class="control-label col-lg-3"> </label>
                        <div class='col-lg-9'>
                            <button type="submit" class='btn btn-success btn-lg'>Save</button>

                        </div>
                    </div>
                </form>

            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
            <div class="col-xs-6 col-sm-3 hidden-xs " id="job_contain";>
                <div class='col-sm-12'>
                    <div class="list-group">
                        <a href="<?php echo base_url()?>employer/"  <?php if($this->uri->segment(1) == 'employer'){ echo 'class="list-group-item active"'; }else{echo 'class="list-group-item "';}?>  ><i class="icon-user main-color"></i> Company Profile</a>
						<a href="<?php echo base_url()?>employer/my_profile"  <?php if($this->uri->segment(2) == 'my_profile'){echo 'class="list-group-item active"';}else{echo 'class="list-group-item "';}?>   ><i class="icon-file main-color"></i> My Profile</a>
						<?php if($employer_user->is_manager == 'Y' && $employer_user->is_confirmed =='Y'): ?>
                        <a href="<?php echo base_url()?>employer/add_users"  <?php if($this->uri->segment(2) == 'add_users' ){echo 'class="list-group-item active"';}else{echo 'class="list-group-item "';}?>   ><i class="icon-group main-color"></i> Add Employers Users</a>
						<?php endif; ?>
                        <a href="<?php echo base_url()?>employer/post_job"   <?php if($this->uri->segment(2) == 'post_job' ){echo 'class="list-group-item active"';}else{echo 'class="list-group-item "';}?>  ><i class="icon-flag main-color"></i> Post A Job</a>
                        <a href="<?php echo base_url()?>employer/job_posted"  <?php if($this->uri->segment(2) == 'job_posted'){echo 'class="list-group-item active"';}else{echo 'class="list-group-item "';}?>   ><i class="icon-bookmark main-color"></i> Job Posted</a>
                        <a href="<?php echo base_url()?>employer/applicants"  <?php if($this->uri->segment(2) == 'applicants'){echo 'class="list-group-item active"';}else{echo 'class="list-group-item "';}?> ><i class="icon-group main-color"></i> Applicants</a>
                        <a href="#change_pass" data-toggle="modal" class="list-group-item"><i class="icon-cog main-color"></i> Accounts Settings</a>
                    </div>
                </div>
            </div>



<script src="<?php echo base_url()?>assets/js/jquery.form.js"></script>
<script>
    $(document).ready(function(){
        $("#accounts_settings").ajaxForm({ // initialize the plugin
            // any other options,
            beforeSubmit: function () {
                return $("#accounts_settings").valid(); // TRUE when form is valid, FALSE will cancel submit
            },
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                $('#alerts_message').html('<p class="text-success text-center">'+obj.data_results +'</p>').fadeOut(5000);
                $('#account_settings').children('input[type=password]').val();
            },
            target:"#alerts_message",
            clearForm: true,
            resetForm: true

        });
    });
</script>