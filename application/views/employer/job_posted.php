<div class='col-sm-9 col-lg-9' id="job_contain">
    <div class="panel panel-default">
        <div class="panel-heading">Job Posted
        </div>
        <div class="panel-body">
            <div class="col-xs-6  col-sm-6  col-lg-6">
            </div>
            <div class="col-xs-6  col-sm-6  col-lg-6">
                <form class="form-horizontal" action="<?php echo base_url();?>employer/job_posted" method="post">
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" id="search_term" name="search_term" class='search_term' type="text"   placeholder="Search Jobs">
                                     <span class="input-group-btn">
                                       <button class="btn btn-default" type="submit"><i class="icon-search"></i></button>
                                     </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            function toggle(source) {
                checkboxes = document.getElementsByName('job_post_event_id[]');
                for(var i=0, n=checkboxes.length;i<n;i++) {
                    checkboxes[i].checked = source.checked;
                }
            }
        </script>
        <table class="table  table-responsive table-striped table-hover table-condensed">
            <thead>
            <tr>

                <th style="width:400px;">Job Position</th>
                <th>Date Posted</th>
                <th>Date Expired</th>
                <th>Total Applicants</th>
                <th>Status</th>

            </tr>
            </thead>
            <tbody class="job_tbody">
            <?php if(!empty($results)):?>

                <?php foreach ($results as $row): ?>
                    <tr style="cursor: pointer;">

                        <td><a href="<?php echo base_url()?>employer/job_posted_description/<?php echo $row->job_post_id;?>"></a><?php echo $row->position ?></td>
                        <td>
                            <?php
                            $date = $row->created_date_time;
                            echo  $your_date = date("M d Y", strtotime($date));
                            ?>
                        </td>
                        <td>
                            <?php
                            $date = $row->expiration_datetime;
                            echo  $your_date = date("M d Y", strtotime($date));
                            ?>
                        </td>
                        <td><?php echo $row->total ?></td>
                        <td>
                            <?php if($row->is_confirm =='Y'){?>
                                <p class="text-success">Confirmed</p>
                            <?php }else{?>
                                <p class="text-danger">Not Confirmed</p>
                            <?php }?>
                        </td>

                    </tr>
                <?php  endforeach;  ?>


            <?php else:?>
                <tr>
                    <td colspan="6" class="text-center">No Posted Jobs Yet</td>
                </tr>
            <?php endif;?>
            </tbody>

        </table>
        <hr style="border: none!important;">
        <table class="table table-condensed">
            <tr>
                <td style="border:none!important;">

                    <?php if($search_term !=''): ?>

                        <p class="text-muted"><span style="font-size: 18px;">total results for <em><b><?php echo $search_term; ?></b></em> is:
                                <?php if(!empty($results)): ?>
                                    <?php echo $total_rows; ?>
                                <?php else: ?>
                                    0
                                <?php endif; ?>
                                                        </span></p>
                    <?php else: ?>
                        <p class="text-muted"><span style="font-size: 18px;">Total Job Posted:<em style="font-size: 24px;"><?php echo $total_rows; ?></em></span></p>
                    <?php endif; ?>
                </td>
                <td style="border:none!important;"> <span class='pull-right'><?php echo $links; ?></span></td>
            </tr>
        </table>
    </div>

    <?php if(!empty($applicants)):?>
        <?php foreach ($applicants as $row): ?>
            <div class="modal fade" id="app_<?php echo $row->job_post_event_id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Job Application</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-xs-12 col-sm-12  col-lg-12">

                                <div class="col-xs-4 col-sm-4 col-lg-4">
                                    <?php if($row->applicants_avatar !='no'):?>
                                        <img src="<?php echo base_url()?>assets/images/job_seeker/<?php echo $row->applicants_avatar?>" style="height:140px;width: 140px; ">
                                    <?php else:?>
                                        <img src="http://placehold.it/140x140" >
                                    <?php endif;?>
                                </div>
                                <div class="col-xs-8 col-sm-8  col-lg-8">
                                    <h3><?php echo $row->full_name?></h3>
                               <span id="stats">
                                    <?php if($row->is_accepted =='Y' && $row->is_declined=="N"):?>
                                       <p class="text-success"> - Accepted </p>
                                   <?php elseif($row->is_accepted=='N' && $row->is_declined=="Y"):?>
                                       <p class="text-danger"> - Declined</p>
                                   <?php endif;?>

                               </span>
                                    <p>
                                        <?php echo $row->applicants_email?><br/>
                                        <?php
                                        $date = $row->applied_datetime;
                                        echo  $your_date = date("M d Y", strtotime($date));
                                        ?> <br/>
                                        <?php
                                        echo  $row->applicants_contact_no;
                                        ?><br/><br/>
                                        <br/><br/>
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12  col-lg-12">
                                <?php echo $row->cover_letter?>
                                <div class="clearfix">
                                </div>

                                <?php if($row->applicants_resume_text!=''):?>
                                    <?php echo $row->applicants_resume_text?>
                                <?php endif;?>

                            </div>

                        </div>
                        <div class="modal-footer">
                            <?php if($row->applicants_resume_filename!=''):?>
                                <a class="btn btn-info" style="color:#fff;margin-right: 10px;" href="<?php echo base_url();?>file/<?php  echo $row->candidate_id;?>/<?php echo $row->applicants_resume_filename?>" download="<?php echo $row->applicants_resume_filename ?>"><i class='icon-download'></i> Download Resume</a>
                            <?php endif;?>


                            <button type="button" class="tooltips_this btn btn-success"  onclick="acc_decc(<?php echo $row->job_post_event_id;?>,'accept');" id='<?php echo $row->job_post_event_id;?>'   title='Accept Applicant'><i class="icon-thumbs-up"></i></button>
                            <button  type="button" class="tooltips_this btn btn-danger" onclick="acc_decc(<?php echo $row->job_post_event_id;?>,'decline');" id='<?php echo $row->job_post_event_id;?>' title='Decline Applicant'><i class="icon-thumbs-down"></i></button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        <?php  endforeach;  ?>
    <?php endif;?>



</div>
<hr style="border:none!important;">
</div>