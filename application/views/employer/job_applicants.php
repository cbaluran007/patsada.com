<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/reveal.js-master/css/reveal.min.css">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/js/reveal.js-master/css/theme/beige.css" id="theme">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Monda:400,700">
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <script>
        var baseURL = '<?php echo base_url();?>';
    </script>

</head>
<style type="text/css">

        .reveal .controls {
            bottom: -4px!important;
            display: none;
            height: 110px;
            position: fixed;
            right: -6px!important;
            width: 110px;
            z-index: 30;
        }

</style>
<body>

<div class="reveal">

    <div class="slides">
        <?php if(!empty($result)):?>
          <?php foreach ($result as $row):?>

                <section>
                    <section>
                        <div class="col-xs-12 col-sm-12 col-lg-12">
                            <div class="col-xs-4 col-sm-4 col-lg-4">
                                <?php if($row->applicants_avatar !='no'):?>
                                    <img src="<?php echo base_url()?>assets/images/job_seeker/<?php echo $row->applicants_avatar?>" style="height:340px;width: 340px; ">
                                <?php else:?>
                                    <img src="http://placehold.it/340x340" style="height:340px;width: 340px;">
                                <?php endif;?>
                            </div>
                            <div class="col-xs-8 col-sm-8  col-lg-8">
                                <h2 class="text-left"><?php echo $row->full_name?></h2>
                                  <span id="stats">
                                    <?php if($row->is_accepted =='Y' && $row->is_declined=="N"):?>
                                          <p class="text-success text-left"> - Accepted </p>
                                      <?php elseif($row->is_accepted=='N' && $row->is_declined=="Y"):?>
                                          <p class="text-danger text-left"> - Declined</p>
                                      <?php endif;?>

                               </span>
                                <p  class="text-left" >

                                    <?php echo $row->applicants_email?><br/>
                                    <?php
                                    $date = $row->applied_datetime;
                                    echo  $your_date = date("M d Y", strtotime($date));
                                    ?> <br/>
                                    <?php
                                    echo  $row->applicants_contact_no;
                                    ?><br/><br/>
                                    <br/><br/>
                                    <br/><br/>
                                </p>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-xs-12 col-sm-12  col-lg-12">
                                <div class='text-center'>
                                    <?php if($row->applicants_resume_filename!=''):?>
                                        <a class="btn btn-primary btn-lg" style="color:#fff;padding:18px 13px!important;top:8px;font-size: 45px!important;" href="<?php echo base_url();?>file/<?php echo $row->applicants_resume_filename?>" download="<?php echo $row->applicants_resume_filename ?>"> Download Resume</a>
                                    <?php endif;?>


                                    <button type="button" class="tooltips_this btn btn-success btn-lg" style="font-size: 45px!important;"  onclick="acc_decc(<?php echo $row->job_post_event_id;?>,'accept');" id='<?php echo $row->job_post_event_id;?>'   title='Accept Applicant'>Accept</button>
                                    <button  type="button" class="tooltips_this btn btn-danger btn-lg" style="font-size: 45px!important;" onclick="acc_decc(<?php echo $row->job_post_event_id;?>,'decline');" id='<?php echo $row->job_post_event_id;?>' title='Decline Applicant'>Decline</button>
                                </div>

                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="col-xs-12 col-sm-12  col-lg-12" style="height: 600px!important;overflow-y:auto;">
                            <p  class="text-left"  >
                                <?php echo $row->cover_letter?>

                            </p>

                        </div>
                    </section>
                    <?php if($row->applicants_resume_text!=''):?>
                    <section>
                        <div class="col-xs-12 col-sm-12  col-lg-12" style="height: 500px!important;overflow-y:auto;">
                            <p  class="text-left"  >

                                    <?php echo $row->applicants_resume_text?>

                            </p>

                        </div>
                    </section>
                    <?php endif;?>







                </section>

          <?php endforeach;?>


        <?php endif;?>
    </div>

</div>

<script src="<?php echo base_url()?>assets/js/reveal.js-master/lib/js/head.min.js"></script>
<script src="<?php echo base_url()?>assets/js/reveal.js-master/js/reveal.min.js"></script>


<script>

    Reveal.initialize({
        transition: 'linear'
    });



    function acc_decc(id,action_type){
        if(action_type == 'accept'){
            $.ajax({
                type:"POST",
                url:baseURL+"ajax/accept_applicants/",
                data: {job_post_event_id: id},
                success: function (data) {
                    var  modal_name='#app_'+id;
                    $(modal_name).find('#stats').html('<p class="text-success"> - Accepted </p>');
                    $('#rec_stats_'+id).html('Accepted');
                    $('#rec_stats_'+id).attr('class','text-success');

                }
            });
        }else{
            $.ajax({
                type:"POST",
                url:baseURL+"ajax/decline_applicants/",
                data: {job_post_event_id: id},
                success: function (data) {
                    var  modal_name='#app_'+id;
                    $(modal_name).find('#stats').html('<p class="text-success">  - Declined </p>');
                    $('#rec_stats_'+id).html('Declined');
                    $('#rec_stats_'+id).attr('class','text-danger');


                }
            });
        }

    }

</script>

</body>
</html>
