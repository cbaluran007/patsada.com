<div class='col-sm-9 col-lg-9' id="job_contain">
    <div class="panel panel-default">
        <div class="panel-heading">Job Description
        </div>
        <div class="panel-body">
            <?php if(!empty($job_description)):?>
                <?php foreach ($job_description as $row):?>
                    <div class='col-xs-12 col-sm-12'>
                        <h4 class="heading" style="color: #545454!important;">
                            <span><?php $position=$row->position;$job_post_id=$row->job_post_id; echo $row->position?></span>
                            <p class='pull-right' style="font-size: 12px!important;"><i class='icon-calendar main-color'></i>  <?php     $date = $row->created_date_time;
                                echo  $your_date = date("M d Y", strtotime($date));?></p>
                            <span class="left"></span>
                        </h4>

                    </div>



                    <div class='col-xs-12 col-sm-12  col-md-12'>
                        <div class='clearfix visible-xs visible-sm visible-md'>      </div>
                        <?php echo $row->job_content?>
                        <div class='clearfix'></div>
                    </div>

                    <div class='col-xs-12 col-sm-12  col-md-12'>
                        <hr style="border:none!important;">
                        <br />
                        <div class='clearfix '>      </div>
                        <ul class="job_details">
                            <li>
                                <i class="icon-time main-color"></i>
                                <?php echo $row->experience?>
                            </li>
                            <li>
                                <i class="icon-map-marker main-color"></i>
                                <?php echo $row->location?>

                            </li>
                            <li>
                                <i class="icon-money main-color"></i>

                                <?php if($row->salary_type =='R'):?>
                                    P<?php echo number_format($row->salary_from);  ?> to P<?php  echo number_format($row->salary_to);?>
                                <?php elseif($row->salary_type =='T'):?>
                                    TBA
                                <?php elseif($row->salary_type =='A'):?>
                                    Agreements
                                <?php elseif($row->salary_type =='N'):?>
                                    Negotiable
                                <?php endif;?>
                            </li>
                            <li>
                                <i class="icon-bolt main-color"></i>
                                Full-time
                            </li>
                        </ul>
                    </div>

                    <div class='col-xs-12 col-sm-12  col-md-12'>
                        <p class='text-center'>

                          <a href='#modal_applicants' data-toggle="modal" id='openBtn' class='btn btn-success btn-lg'>Total Applicants: <?php echo $total_applicants;?></a>
                            <input type="hidden" id="job_post_id" value="<?php echo $job_post_id=$row->job_post_id;?>">
                        </p>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>

    <div class="modal fade" id="modal_applicants" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Applicants</h4>
                </div>
                <div class="modal-body iframe_body"  >

                    <iframe id="iframe_applicants" src="" style="zoom:2;overflow:hidden; width: 100%; height: 350px; position: relative;"   frameborder="0"></iframe>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

</div>
<hr style="border:none!important;">
</div>