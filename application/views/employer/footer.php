<div class="clearfix"></div>
<footer  style="margin-bottom: 0!important; ">
    <div class='container'>
        <div class="footers">

            <div class="row">
                <img  src="<?php echo base_url();?>assets/img/rafting.png" style="position:absolute; !important; left: 5%;margin-top: -70px;  ">

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 hidden-xs">
                    <h4><span>Job Seekers</span></h4>
                    <small><a href="#">Create Account &amp; Resume</a> </small><br>
                    <small><a href="#">Search Jobs</a></small><br>
                    <small><a href="#">Job Seeker Login</a></small>

                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 hidden-xs">
                    <h4><span>Employers</span></h4>
                    <small><a href="#">Create Company Profile</a> </small><br>
                    <small><a href="#">Post a Job</a></small><br>
                    <small><a href="#">Employer Login</a></small>

                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 hidden-xs">
                    <h4><span>Resources</span></h4>
                    <small><a href="#">About PaTsada.com</a> </small><br>
                    <small><a href="#">Our Sponsors</a> </small><br>
                    <small><a href="#">Contact Us</a></small><br>
                    <small><a href="#">Help/Tips</a></small>

                </div>

                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <h6 >  <strong><img src="<?php echo base_url();?>assets/img/patsada_logo.PNG" style='height:30px;width:150px;padding-top:3px;'></strong><br></h6>
                    <small>
                        The word PaTsada is a local word from Cagayan de Oro City.<br>
                        Hence <b>PATSADA.com</b> is an online Job Search Site for kagay-anons. <br>
                        We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                        <b>PATSADA.com</b>... Jobs, Right at Your Fingertips<br/><br/><br/>
                    </small>
                </div>
            </div>
            <div class='row  hidden-xs'>
                <div class="social" id="social-media-links">
                    <legend style="border:none!important;color:#f5f5f5!important;">Socialize with us</legend>
                       <a href='#' class="icon-stack" rel="tooltip" data-placement="top" title="RSS"> <i class="icon-rss icon-light icon-large"></i> </a>
                    <a href='https://www.facebook.com/pages/Patsada/190587914447922' target="_blank" class="icon-stack" rel="tooltip" data-placement="top" title="Facebook"><i class="icon-facebook icon-light icon-large"></i>  </a>
                    <a href='#' class="icon-stack" rel="tooltip" data-placement="top" title="Twitter"><i class="icon-twitter icon-light icon-large"></i></a>
                    <a href='#' class="icon-stack" rel="tooltip" data-placement="top" title="Google plus"><i class="icon-google-plus icon-light icon-large"></i></a>
                </div><!-- END SOCIAL -->

            </div>
            <div class="row" style="border-top: 1px solid #717171;text-align: right;">
                <div class='col-lg-12 '><small>&copy; Copyright 2013. PaTsada.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359</small>
                </div>
            </div>

        </div><!-- CONTAINER FOOTER-->
    </div>
</footer>

<div id="fb-root"></div>
</div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="<?php echo base_url();?>assets/js/carousel.js"></script>
<script src="<?php echo base_url();?>assets/js/holder.js"></script>
<script src="<?php echo base_url();?>assets/js/collapse.js"></script>
<script src="<?php echo base_url();?>assets/js/tooltip.js"></script>
<script src="<?php echo base_url();?>assets/js/modal.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>

<?php if($employer_user->is_manager == 'Y' && $employer_user->is_confirmed =='Y'): ?>
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/company.js"></script>
<?php endif; ?>
<script src="<?php echo base_url();?>assets/js/uploadify_31/jquery.uploadify-3.1.min.js"></script>

<script src="<?php echo base_url()?>assets/js/jquery.form.js"></script>

<script src="<?php echo base_url();?>assets/js/form_employer_validation.js"></script>

<script>
    $(document).ready(function(){
        $('#accounts_settings').validate({
            rules:{
                password:{
                    required:true,
                    minlength: 6
                },
                password_2:{
                    required:true,
                    equalTo: "#password_1"
                }

            },
            messages: {
                password_2: {
                    equalTo: 'Password did not match'
                }
            },

            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            }
        });
    });
</script>

</body>
</html>