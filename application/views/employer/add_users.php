<?php
$employer_details=$employer[0];
$full_address= $employer_details->company_address.', '.$employer_details->company_city.', '.$employer_details->company_province.', '.$employer_details->company_country.', '.$employer_details->company_zip_code
?>

<div class='col-sm-9 col-lg-9' id="job_contain">

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class='icon-group main-color-yellow'> </i>Sub Users
            <a data-toggle="modal" style="margin-top:-7px;" id="save-btn" href="#add" class="btn btn-primary pull-right"><i class='icon-plus'></i> Add Users</a>

        </div>
        <div class='panel-body'>
          <div class="col-sm-12 col-md-12 col-lg-12">
                 <table class="table   table-hover">
                     <thead>
                        <tr>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Email</th>
							<th>Status</th>
							<th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php if($employer_users_sub!=''){
                            foreach($employer_users_sub as $row){
                                if($row->is_manager =='N'){
                                    ?>
                                    <tr>
                                        <td><?=$row->title?> <?=$row->first_name?> <?=$row->last_name?></td>
                                        <td><?=$row->position?></td>
                                        <td><?=$row->email?></td>
										
										  <td>
										  <?php if($row->is_confirmed == 'Y'): ?>
										  Verified
										  <?php else:?>
										  Un-verify
										  <?php endif; ?>
										  </td>
										<td>
                                            <?php if($row->is_active =='Y'):?>
                                            <form action="<?php echo base_url()?>employer/activate" method="POST" >
                                                <input type="hidden" name='is_active' id='is_active' value="N">
                                                <input type="hidden" name='employer_user_id' id='employer_user_id' value="<?php  echo $row->employer_user_id?>">
                                                <button class='btn btn-warning'>Deactivate</button>
                                            </form>
                                                <?php else:?>
                                                <form action="<?php echo base_url()?>employer/activate" method="POST" >
                                                    <input type="hidden" name='is_active' id='is_active' value="Y">
                                                    <input type="hidden" name='employer_user_id' id='employer_user_id' value="<?php  echo $row->employer_user_id?>">
                                                    <button class='btn btn-success'>Activate</button>
                                                </form>
                                           <?php endif;?>

                                        </td>
                                    </tr>
                        <?php
                                }
                            }
                        }?>
                     </tbody>
                 </table>

            </div>
        </div>

    </div>
    </div>




<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Users</h4>
            </div>
            <div class="modal-body">


                    <form class='form-horizontal' id='add_user' action="<?php echo base_url()?>employer/add_sub_users" method="POST">
                        <input type="hidden" name='employer_id' value="<?php echo $employer_details->employer_id;?>">
                        <input type="hidden" name='employer_user_id' value="0">
						<div class='form-group'>
                             <label class='control-label col-sm-3  col-md-3 col-lg-3'>Title</label>
                              <div class='col-lg-4'>
                                                          <select class='form-control' name='title' id='title'>
                                                              <option >Mr.</option>
                                                              <option >Ms.</option>
                                                              <option >Mrs.</option>
                                                              <option >Dr.</option>
                                                          </select>
                              </div>
                         </div>
						<div class='form-group'>
                            <label class='control-label col-sm-3  col-md-3 col-lg-3'>First name</label>
                            <div class='col-sm-9  col-md-9 col-lg-9'>
                                <input type="text" class='form-control' name='first_name' id='first_name' placeholder="First Name"/>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='control-label col-sm-3  col-md-3 col-lg-3'>Last name</label>
                            <div class='col-sm-9  col-md-9 col-lg-9'>
                                <input type="text" class='form-control' name='last_name' id='last_name' placeholder="Last Name"/>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='control-label col-sm-3  col-md-3 col-lg-3'>Position</label>
                            <div class='col-sm-9  col-md-9 col-lg-9'>
                                <input type="text" class='form-control' name='position' id='position' placeholder="Position"/>
                            </div>
                        </div>
						
                         <div class='form-group'>
                            <label class='control-label col-sm-3  col-md-3 col-lg-3'>Email</label>
                            <div class='col-sm-9  col-md-9 col-lg-9'>
                                <input type="text" class='form-control' name='email' id='e_user_mail' placeholder="Email"/>
                            </div>
                        </div>
                       <div class='form-group'>
                            <label class='control-label col-sm-3  col-md-3 col-lg-3'>User Name</label>
                            <div class='col-sm-9  col-md-9 col-lg-9'>
                                <input type="text" class='form-control' name='user_name' id='e_user_name' placeholder="User name"/>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='control-label col-sm-3  col-md-3 col-lg-3'>Password</label>
                            <div class='col-sm-9  col-md-9 col-lg-9'>
                                <input type="password" class='form-control' name='password' id='password' placeholder="Password"/>
                            </div>
                        </div>
                        <div class='form-group'>
                            <label class='control-label col-sm-3  col-md-3 col-lg-3'> </label>
                            <div class='col-sm-9  col-md-9 col-lg-9'>
                                <button type="submit" class='btn btn-success'>Save</button>
                            </div>
                        </div>
                    </form>


            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<hr style="border:none!important;">
</div>