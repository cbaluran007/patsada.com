

<!-- Button trigger modal -->

<!-- Modal -->

<?php if($roll !='candidate' || $roll !='employer'){?>
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">REGISTER for FREE with PATSADA.com</h4>
            </div>
            <div class="modal-body" style="padding:2px!important;">
                <ul class="nav  nav-tabs tabber" id="myTab">
                    <li class="active"><a href="#job_seeker">Job Seeker <span class="icon-search" style="position:absolute; z-index:1; top:15px;right:10px;"></span></a> </li>
                    <li><a href="#employer">Employer <span class="icon-user" style="position:absolute; z-index:1; top:15px;right:10px;"></span></a></li>
                </ul>

                <div class="tab-content" >
                    <div class="tab-pane active" id="job_seeker" style="padding:4px!important;">
                        <div   id='alerts'> </div>
                        <form action="#" method="POST" id='registration_candidate' class='form-horizontal'>
                            <div class="form-group">
                                <div class='col-lg-12'>
                                    <div class='input-group'>
                                        <span class="input-group-addon"><i class='icon-user main-color'></i></span>
                                        <input type="text" name='user_name' id="user_name" placeholder="User Name" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class='col-lg-12'>
                                    <div class='input-group'>
                                        <span class="input-group-addon"><i class='icon-envelope main-color'></i></span>
                                        <input type="email" name='email' id="email" placeholder="Email" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class='col-lg-12'>
                                    <div class='input-group'>
                                        <span class="input-group-addon"><i class='icon-key main-color'></i></span>
                                        <input type="password" name='password' id="password" placeholder="Password" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class='col-lg-12'>
                                    <button class='btn btn-success form-control' type="submit"  >REGISTER AS JOB SEEKER</button>
                                </div>
                            </div>
							<div class="form-group">
								<div class='col-lg-12'>
									<small>
									Have an account already? <a data-toggle="modal" data-dismiss="modal" href="#login">LOGIN HERE</a><br>
									I agree to the terms stipulated in the service agreement.
									</small>
								</div>
                            </div>

							
                        </form>
                    </div>
                    <div class="tab-pane" id="employer">
						<div   id='alerts_emp'> </div>
                        <form class='form-horizontal' id='registration_employer'  action="#" method="POST">
							<div class="form-group">
                                <div class='col-lg-12'>
                                    <div class='input-group'>
                                        <span class="input-group-addon"><i class='icon-user main-color'></i></span>
                                        <input type="text" name='ecompany_name' id="ecompany_name" placeholder="Company Name" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                           
                           <div class="form-group">
                                <div class='col-lg-12'>
                                    <div class='input-group'>
                                        <span class="input-group-addon"><i class='icon-user main-color'></i></span>
                                        <input type="text" name='euser_name' id="euser_name" placeholder="User Name" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class='col-lg-12'>
                                    <div class='input-group'>
                                        <span class="input-group-addon"><i class='icon-envelope main-color'></i></span>
                                        <input type="email" name='eemail' id="eemail" placeholder="Email" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class='col-lg-12'>
                                    <div class='input-group'>
                                        <span class="input-group-addon"><i class='icon-key main-color'></i></span>
                                        <input type="password" name='epassword' id="epassword" placeholder="password" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="form-group">
                                <div class='col-lg-12'>
                                    <button class='btn btn-success form-control' type="submit">REGISTER AS EMPLOYER</button>
                                </div>
                            </div>
							<div class="form-group">
								<div class='col-lg-12'>
									<small>
									Have an account already? <a data-toggle="modal" data-dismiss="modal" href="#login">LOGIN HERE</a><br>
									I agree to the terms stipulated in the service agreement.
									</small>
								</div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">SIGN IN with PATSADA.com</h4>
            </div>
            <div class="modal-body">
                <div id='log_alerts'></div>
                <form class='form-horizontal' id="login_credintials" action="#" method="POST">
                    <div class='form-group'>

                        <div class='col-lg-12'>
                            <div class='input-group'>
                                <span class="input-group-addon"><i class='icon-user main-color'></i></span>
                                <input type="text" name='user_name' id="euser_name" placeholder="User Name" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class='form-group'>

                        <div class='col-lg-12'>
                            <div class='input-group'>
                                <span class="input-group-addon"><i class='icon-key main-color'></i></span>
                                <input type="password" name='password' id="epassword" placeholder="Password" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    
					  <div class='form-group'>
                        <div class='col-lg-12'>
                            <button class='btn btn-success form-control' type="submit" >SIGN IN</button>
                        </div>
                    </div>
					<div class='form-group'>
                        <div class='col-lg-12'>
						<small>
						<a href='#'  class="forgot pull-left">Forgot Password?</a>
						<span class="pull-right">Not yet a member? <a data-toggle="modal" data-dismiss="modal" href="#register">REGISTER HERE.</a></span>
						</small>
						</div>
					 </div>
                </form>
                <form class='form-horizontal hide' id="form_forgot" action="#" method="POST">
                    <div   id='alerts2'> </div>
                    <div class='form-group'>
                        <div class="col-lg-12"><p class="text-info text-center">Forgot password?. Please enter your email below</p></div>
                    </div>
                    <div class='form-group'>
                        <label class="col-lg-2 control-label">Type</label>
                        <div class='col-lg-10'>
                                <SELECT name="user_type" class="form-control">
                                        <option value="is_candidate">Candidate</option>
                                        <option value="is_employer">Employer</option>
                                </SELECT>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class="col-lg-2 control-label">Email</label>
                        <div class='col-lg-10'>
                            <div class='input-group'>
                                <span class="input-group-addon"><i class='icon-envelope main-color'></i></span>
                                <input type="text" name='email' id="f_email" placeholder="Enter your email Here" class="form-control"/>
                            </div>
                        </div>
                    </div>
					  <div class='form-group'>
                          <label class="col-lg-2 control-label"></label>
                        <div class='col-lg-10'>
                            <button class='btn btn-success' type="submit" >Send</button>
                        </div>
                    </div>
					<div class='form-group'>
                        <div class='col-lg-12'>
						<small>
						<a href='#'  class="get_back pull-left"><i class="icon-arrow-left"></i> Back</a>
						<span class="pull-right">Not yet a member? <a data-toggle="modal" data-dismiss="modal" href="#register">REGISTER HERE.</a></span>
						</small>
						</div>
					 </div>
                </form>


            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php }?>


<div class="container main_containt ">
    <div class="row row-offcanvas row-offcanvas-right min-height">
        <div class="visible-sm visible-md visible-lg">
                <div class="navbar-wrapper">
                    <div class="container container-nav">
                        <div class="navbar transparent navbar-inverse navbar-fixed-top">
                            <div class="container">
                                <div class="navbar-header">

                                    <a class="navbar-brand" href="#"> <img src="<?php echo base_url()?>/assets/img/patsada_logo.PNG" style='height:35px;width:83%;padding-top:3px;margin-left:20px;'></a>
                                    <span class='navbar-brand-span hidden-xs hidden-sm'  >Jobs, Right at Your Fingertips</span>
                                </div>
                                <div class="   navbar-right">
                                    <ul class="nav navbar-nav" style="padding-right: 20px!important;">
                                        <li <?=($this->uri->segment(1) == ''  ? ' class="active"' : '');?><?=($this->uri->segment(2) == 'post_jobs'  ? ' class="active"' : '');?>><a href="<?php echo base_url()?>">Jobs</a></li>
                                        <li <?=($this->uri->segment(1) == 'about' ? ' class="active"' : '');?>><a href="#about">About Us</a></li>
                                        <li <?=($this->uri->segment(1) == 'company' ? ' class="active"' : '');?>><a href="#company">Companies</a></li>
                                    </ul>
                                    <?php if($roll =='candidate'){?>

                                        <ul class="nav navbar-nav">
                                            <li <?=($this->uri->segment(1) == 'candidate' ? ' class="active"' : '');?>><a   href="<?php echo base_url()?>candidate/">Dashboard</a></li>
                                            <li style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;" class="hidden-xs "><a href="#" style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;cursor:default">|</a></li>
                                            <li> <a  href="<?php echo base_url()?>candidate/logout">Logout</a>  </li>
                                        </ul>
                                    <?php }elseif($roll=='employer'){?>

                                        <ul class="nav navbar-nav">
                                            <li <?=($this->uri->segment(1) == 'employer' ? ' class="active"' : '');?>><a   href="<?php echo base_url()?>employer/">Dashboard</a></li>
                                            <li style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;" class="hidden-xs"><a href="#" style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;cursor:default">|</a></li>
                                            <li> <a  href="<?php echo base_url()?>employer/logout">Logout</a>  </li>

                                        </ul>
                                    <?php } else{
                                        ?>
                                        <ul class="nav navbar-nav">
                                            <li><a data-toggle="modal" href="#login">Sign In</a></li>
                                            <li style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;" class="hidden-xs "><a href="#" style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;cursor:default">|</a></li>
                                            <li> <a data-toggle="modal" href="#register">Register</a>  </li>

                                        </ul>
                                    <?php
                                    }?>
                                </div>
                            </div>
                        </div>

                    </div>

                <div class='clearfix'></div>

                </div>
       </div>
        <div class=" visible-xs">
            <div class="col-xs-12" style="height: 30px!important;"></div>
            <div class="clearfix"></div>
            <div class="navbar navbar-default" role="navigation" >
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="offcanvas" > <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <a class="navbar-brand" href="#"> <img src="<?php echo base_url()?>/assets/img/patsada_logo.PNG" style='height:30px;width:70%;padding-top:3px;margin-left:20px;'></a>
                    </div>
            </div>

            </div>
            <div class="col-xs-12 col-sm-12 sidebar-offcanvas showhide" id="sidebar" role="navigation" >
                <div class="sidebar-nav" >
                    <ul class="nav nav-pills nav-stacked responsive_sidebar" >
                        <li <?=($this->uri->segment(1) == '' ? ' class="active"' : '');?>><a href="<?php echo base_url()?>">Jobs</a></li>
                        <li <?=($this->uri->segment(2) == 'about' ? ' class="active"' : '');?>><a href="#about">About Us</a></li>
                        <li <?=($this->uri->segment(2) == 'company' ? ' class="active"' : '');?>><a href="#company">Companies</a></li>
                    </ul>
                    <?php if($roll =='candidate'){?>
                        <div class="clearfix"></div>
                        <br/>
                        <?php if(!empty($get_application)):?>
                            <h4 class="heading" style="color: #fff!important;"> <span> My Application</span><span class="icon-eye-open pull-right hidden-xs" style="color: #ffffff;font-size:12px;">

                              </span>

                                <span class="left"></span></h4>
                                <ul class="nav nav-pills nav-stacked side_cat_b" >

                                    <?php foreach($get_application as $row):?>
                                        <li>
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $row->job_post_id; ?>"><i class="icon-circle-arrow-right"></i> <?php echo $row->position; ?></a>
                                            <ul id="collapse_<?php echo $row->job_post_id; ?>" class="panel-collapse collapse">

                                                    <li>
                                                        <a href="<?php echo base_url()?>patsada/jobs/<?php echo $row->job_post_id;?>"><?php echo $row->position?></a>
                                                        <p class="list-group-item-text">
                                                            <small><?php echo $row->company_name?></small>

                                                            <small><?php echo $row->company_address?></small>

                                                        </p>
                                                    </li>
                                            </ul>
                                        </li>
                                    <?php endforeach; ?>

                                </ul>
                        <?php endif;?>
                        <?php if($this->uri->segment(1) == 'patsada.com' || $this->uri->segment(1) == 'about' || $this->uri->segment(1) == 'company' || $this->uri->segment(1) == 'patsada'):?>
                            <h4 class="heading" style="color: #fff!important;"> <span>See Categories</span><span class="icon-eye-open pull-right hidden-xs" style="color: #ffffff;font-size:12px;">   </span>
                                <span class="left"></span></h4>
                            <ul class="nav nav-pills nav-stacked side_cat_b" >
                                <?php foreach($category_list[0] as $cat_list):?>
                                    <li>
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $cat_list[0]; ?>"><i class="icon-circle-arrow-right"></i> <?php echo $cat_list[1]; ?></a>
                                        <ul id="collapse_<?php echo $cat_list[0]; ?>" class="panel-collapse collapse">
                                            <?php foreach($category_list[1][$cat_list[0]] as $cat_list_sub):?>
                                                <li><a href="#"><?php echo $cat_list_sub[1]; ?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif;?>

                        <ul class="nav nav-pills nav-stacked responsive_sidebar">
                            <li <?=($this->uri->segment(1) == 'candidate' ? ' class="active"' : '');?>><a   href="<?php echo base_url()?>candidate/">Dashboard</a></li>
                            <li >    <a href="#change_pass" data-toggle="modal"  ><i class="icon-cog main-color"></i> Accounts Settings</a></li>

                            <li style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;" class="hidden-xs "><a href="#" style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;cursor:default">|</a></li>
                            <li> <a  href="<?php echo base_url()?>candidate/logout">Logout</a>  </li>
                        </ul>
                    <?php }elseif($roll=='employer'){?>
                        <ul class="nav nav-pills nav-stacked responsive_sidebar">

                             <li <?=($this->uri->segment(2) == 'my_profile' ? ' class="active"' : '');?>><a href="<?php echo base_url()?>employer/my_profile"   ><i class="icon-file "></i> My Profile</a></li>
                            <?php if($employer_user->is_manager == 'Y' && $employer_user->is_confirmed =='Y'): ?>
                                <li <?=($this->uri->segment(2) == 'add_users' ? ' class="active"' : '');?>><a href="<?php echo base_url()?>employer/add_users"  ><i class="icon-group "></i> Add Employers Users</a></li>
                            <?php endif; ?>
                            <li <?=($this->uri->segment(2) == 'post_job' ? ' class="active"' : '');?>><a href="<?php echo base_url()?>employer/post_job"   ><i class="icon-flag  "></i> Post A Job</a></li>
                            <li <?=($this->uri->segment(2) == 'job_posted' ? ' class="active"' : '');?> ><a href="<?php echo base_url()?>employer/job_posted"   ><i class="icon-bookmark  "></i> Job Posted</a></li>
                            <li <?=($this->uri->segment(2) == 'applicants' ? ' class="active"' : '');?>> <a href="<?php echo base_url()?>employer/applicants"  ><i class="icon-group  "></i> Applicants</a></li>


                        </ul>
                        <ul class="nav nav-pills nav-stacked responsive_sidebar">
                            <li  <?=($this->uri->segment(1) == 'employer' ? ' class="active"' : '');?>><a   href="<?php echo base_url()?>employer/">Dashboard</a></li>

                            <li style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;" class="hidden-xs"><a href="#" style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;cursor:default">_____________ </li>
                            <li> <a  href="<?php echo base_url()?>employer/logout">Logout</a>  </li>

                        </ul>



                    <?php } else{
                        ?>
                        <ul class="nav nav-pills nav-stacked responsive_sidebar">
                            <li><a data-toggle="modal" href="#login">Sign In</a></li>
                            <li style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;" class="hidden-xs "><a href="#" style="color:#f3c62b!important;border-bottom: none!important;outline:none!important;cursor:default">|</a></li>
                            <li> <a data-toggle="modal" href="#register">Register</a>  </li>

                        </ul>
                    <?php
                    }?>


                </div>
            </div>
        </div>
