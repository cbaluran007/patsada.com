<style>
    .side_padding{
        padding-top: 0!important;
    }
</style>
<div class="col-xs-12 col-sm-8 col-lg-9" style="margin-top:-40px!important;">
<div class="container">
<div class="row">
        <div class="col-12 col-sm-12 col-lg-12  ">
         <hr style="border: none!important;" class=" visible-xs">
            <div class="col-sm-12 col-xs-12 col-lg-12 visible-xs" >
                <div class='col-xs-12'>
                    <form action ="#" method="post" id="searchform">
                        <div class="input-group">
                            <input class="form-control" id="search_term" name="search_term" class='search_term' type="text"   placeholder="Search your job here... ">
                                                                          <span class="input-group-btn">
                                                                            <button class="btn btn-success" type="submit"> <i class='icon-search'></i></button>
                                                                          </span>
                        </div>
                    </form>
                    <br/>
                    <div class='clearfix'>
                        <?php if($roll=='employer'){?>
                            <a href="<?php echo base_url()?>employer/post_job" class='btn btn-success btn-lg ' style="width: 100%"> <i class='icon-circle-arrow-right '></i> POST A JOB HERE</a>

                        <?php }elseif($roll=='candidate'){?>
                            <a href="#" class='btn btn-success btn-lg  ' style="width: 100%"> <i class='icon-circle-arrow-right '></i> POST A JOB HERE</a>

                        <?php    }else{?>
                            <a href="<?php echo base_url()?>patsada/post_jobs" class='btn btn-success btn-lg ' style="width: 100%"> <i class='icon-circle-arrow-right '></i> POST A JOB HERE</a>
                        <?php }?>
                    </div>
                </div>


            </div>
        <hr style="border: none!important;" class=" visible-xs">

        <ul class="nav  nav-tabs tabber  " id="jobs"   >
                <li class="active"><a href="#home"><h4>All Jobs</h4></a></li>
                <li class='' style="background: none!important;">
                </li>
            </ul>
            <div class='col-lg-6 col-md-offset-6 hidden-xs hidden-sm' style="z-index: 12; position: absolute;top:0;right: 0">
                <form action ="#" method="post" id="searchform">
                    <div class="input-group">
                        <input class="form-control" id="search_term" name="search_term" class='search_term' type="text"   placeholder="Search your job here... ">
                         <span class="input-group-btn">
                           <button class="btn btn-success" type="submit"> SEARCH JOB</button>
                         </span>
                    </div>
                </form>
            </div>
            <div class="tab-content">
                    <div class="tab-pane active" id="home">
                            <table class='table   table-condensed table-hover' id='job-table' style="top:-10px!important;">
                                <thead  >
                                    <tr>
                                        <th style="text-align:left;width: 100px;"><i class="icon-calendar main-color"></i> Posted</th>
                                        <th style="text-align:left;"  ><i class="icon-file main-color"></i> Description</th>
                                        <th style="text-align:left;width: 200px;" class="hidden-xs"><i class="icon-map-marker   main-color"></i> Location</th>
                                        <th   class="hidden-xs" style="text-align:left;width: 100px;" ><i class="icon-bolt main-color"></i> Contract</th>
                                    </tr>
                                </thead>
                            <tbody class='tbody_jobs'>
                            <?php if(!empty($results)):?>
                                <?php foreach($results as $row): ?>
                                         <?php if($row->is_premium == 'Y'): ?>
                                        <tr class="tr_jobs premium" style="cursor: pointer;">
                                        <td> <p class="text-danger">URGENT</p></td>
                                        <?php else: ?>
                                        <tr class='tr_jobs' style="cursor: pointer;">
                                        <td><p>

                                                <?php     $date = $row->created_date_time;
                                                echo  $your_date = date("M d Y", strtotime($date));?>


                                            </p></td>
                                        <?php endif; ?>


                                            <td><a href="<?php echo base_url()?>patsada/jobs/<?php echo $row->job_post_id;?>" style="color:#282828;font-weight: bold!important;text-decoration: none!important;"><?php echo $row->position?></a>
                                                <p>
                                                    <small><?php echo $row->company_name?></small>

                                                    <small><?php echo $row->company_address?></small>

                                                </p>
                                            </td>
                                            <td class="hidden-xs"><?php echo $row->location?></td>
                                            <td  class="hidden-xs" ><?php echo $row->jobtype?></td>
                                        </tr>
                                    <?php

                                endforeach;?>
                            <?php endif;?>


                            </tbody>
                            </table>
                            <hr style="border: none!important;">
                                <table class="table table-condensed">
                                        <tr>
                                            <td style="border:none!important;">

                                                <?php if($search_term !=''): ?>

                                                    <p class="text-muted"><span style="font-size: 18px;">total results for <em><b><?php echo $search_term; ?></b></em> is:
                                                            <?php if(!empty($results)): ?>
                                                                <?php echo $total_rows; ?>
                                                            <?php else: ?>
                                                                0
                                                            <?php endif; ?>
                                                        </span></p>
                                                <?php else: ?>
                                                 <p class="text-muted"><span style="font-size: 18px;">Latest Jobs:<em style="font-size: 24px;"><?php echo $total_rows; ?></em></span></p>
                                                <?php endif; ?>
                                            </td>
                                            <td style="border:none!important;"> <span class='pull-right'><?php echo $links; ?></span></td>
                                        </tr>
                                </table>

                    </div>
            <div class="tab-pane" id="profile"> WALA PA</div>
            <div class="tab-pane" id="messages">WALA PA pud</div>
            <div class="tab-pane" id="settings">Wala pa jud </div>
            </div>


        </div>
        </div>
    </div><!-- /.row -->
</div><!-- /.container -->


