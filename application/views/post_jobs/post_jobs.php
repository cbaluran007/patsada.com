
<div class="container"  id="job_contain">
    <div class="row">
        <div class="col-sm-10 col-lg-10 col-md-10 col-xs-12   col-sm-offset-2  col-md-offset-2 col-lg-offset-2">
            <div class="col-sm-10 col-lg-10">
                <div class="panel panel-default ">
                    <div class="panel-heading">
                        <h3>Post A job For Free</h3>


                    </div>
                    <div class="panel-body">

                        <p class="text-center text-info " style="font-size: 18px;"> Have an account? sign in <a data-toggle="modal"  style="text-decoration: none!important;color:#f3c62b!important;"   href="#login">Here</a> or Don’t have yet? Fill out the form below and create an account later. </p>

                        <div class="clearfix"></div>
                        <div class="row" >
                          <form class='form-horizontal' id="jp_post_job_process" action="<?php echo base_url()?>patsada/post_jobs_process/" method="POST">
                              <div id="output">  <?php if ($this->session->flashdata('success') !== FALSE) { echo "<center><span class='alert alert-success'>".$this->session->flashdata('success')."</span></center><br/><br/><div class='clearfix'></div>"; } ?>    </div>

                              <div class="clearfix"></div>
                              <h4 class="heading" style="color: #545454!important;"><i class="icon-info-sign main-color"></i>
                                  Job Information
                                  <span class="left"></span></h4>
                                  <div class="form-group">
                                      <label class="col-lg-3 control-label">Job  Type</label>
                                      <div class="col-lg-9 controls">
                                          <fieldset>
                                              <div class="input-group">

                                                  <div class="btn-group" data-toggle="buttons"  >
                                                      <input type='hidden' name='jp_jobtype' value='Fulltime' id='jp_jobtype'>
                                                      <label class="btn btn-primary active"  id='ful'>
                                                          <input type="radio"  value="fulltime" checked> Full-time
                                                      </label>
                                                      <label class="btn btn-primary" id='partym'>
                                                          <input type="radio"  value="parttime"> Part-time
                                                      </label>
                                                      <label class="btn btn-primary" id='fre'>
                                                          <input type="radio"  value="contract"> Contract
                                                      </label>
                                                  </div>
                                              </div>
                                          </fieldset>
                                      </div>
                                  </div>



                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Position</label>
                                    <div class="col-lg-7">
                                        <input  type="text" name="jp_position" id="jp_position" tabindex="2" class="form-control" value="<?php echo set_value('position'); ?>" />
                                        <div class="e.g. 'Callcenter Agent', 'Sales'"></div>
                                    </div>
                                    <?php echo form_error('position'); ?>
                                </div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Location</label>
                                    <div class="col-lg-7">
                                        <select name="jp_location" id="jp_location" class="form-control" tabindex="3" >
                                            <option value="Anywhere"   >Anywhere</option>
                                            <option value="Butuan"     >Butuan</option>
                                            <option value="Cagayan"    SELECTED="selected">Cagayan de Oro</option>
                                            <option value="Dapitan"    >Dapitan</option>
                                            <option value="Davao"      >Davao</option>
                                            <option value="General"    >General Santos</option>
                                            <option value="Gingoog"    >Gingoog</option>
                                            <option value="Iligan"     >Iligan</option>
                                            <option value="Malaybalay" >Malaybalay</option>
                                            <option value="Marawi"     >Marawi</option>
                                            <option value="Ozamis"     >Ozamis</option>
                                            <option value="Pagadian"   >Pagadian</option>
                                        </select>
                                    </div>
                                    <?php echo form_error('compensation'); ?>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"> Job Description</label>
                                    <div class="col-lg-7">
                                        <input id="job_content_heddin" type="hidden" name="job_content">
                                        <textarea    class="ckeditor form-control" tabindex="6" name="job_content"  style="height:250px"  id='jp_job_content_id' placeholder="Enter Description..."  >   <?php echo set_value('jobdescription'); ?> </textarea>
                                    </div>
                                    <?php echo form_error('jobdescription'); ?>
                                </div>
                                  <div class="form-group">
                                      <label class="col-lg-3  control-label">Category</label>
                                      <div class="col-lg-5">

                                          <select name="category_id" id="jp_category_id" class="form-control"  tabindex="1">
                                              <?php foreach($category_list[0] as $cat_list):?>
                                                  <optgroup label=" <?php echo $cat_list[1]; ?>">
                                        <?php foreach($category_list[1][$cat_list[0]] as $cat_list_sub):?>
                                                      <option value="<?php echo $cat_list_sub[0]; ?>" ><?php echo $cat_list_sub[1]; ?></option>
                                                  <?php endforeach; ?>
                                            </optgroup>
                                      <?php endforeach; ?>
                                          </select>
                                      </div>
                                  </div>
                              <div class="form-group">
                                  <label class="col-lg-3 control-label">Years of experience</label>
                                  <div class="col-lg-5 ">
                                      <div class="input-group">
                                          <span class="input-group-addon" style='hieght:25px;'>From</span>
                                          <input type="text" class="form-control" id='jp_experience_from' name='jp_experience_from' placeholder="0" value="0">
                                          <span class="input-group-addon">TO</span>
                                          <input type="text" class="form-control" id='jp_experience_to' name='jp_experience_to' placeholder="3" value="0">
                                      </div>

                                  </div>

                              </div>


                              <div class="clearfix"></div>
                              <h4 class="heading" style="color: #545454!important;">
                                                 Company Details
                                  <span class="left"></span></h4>


                            <div class="form-group">
                                <label class="col-lg-3 control-label">Enter company name</label>
                                <div class="col-lg-7">
                                    <input  tabindex="6" type="text" name="ecompany_name" id="jp_company_name" class="form-control" value="" />
                                </div>
                                <?php echo form_error('company'); ?>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">User Name</label>
                                <div class="col-lg-7">
                                    <input  tabindex="6" type="text" name="euser_name" id="jp_user_name" class="form-control"  />

                                </div>

                            </div>


                            <div class="form-group">
                                <label class="col-lg-3 control-label">E-mail</label>
                                <div class="col-lg-7">
                                    <input  tabindex="6" type="text" name="eemail" id="jp_email" class="form-control"  />

                                </div>

                            </div>


                            <div class="form-group">
                                <label class="col-lg-3 control-label">Password</label>
                                <div class="col-lg-7">
                                    <input  tabindex="6" type="password" name="jp_password" id="jp_password" class="form-control"   />

                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Security Check</label>
                                <div class="col-lg-7">

                                    <?php echo $recaptcha_html; ?>


                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label"> </label>
                                <div class="col-lg-7">
                                   <input type="submit" class="submit btn btn-success btn-lg" id="submit_button" value="Post Job">
                                </div>
                            </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="title">Modal title</h4>
            </div>
            <div class="modal-body" id="validates">
                ...
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script src="<?php echo base_url()?>assets/js/jquery.form.js"></script> 
<script>
    $(document).ready(function(){
$('#jp_job_content_id').wysihtml5({
  "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
  "emphasis": true, //Italics, bold, etc. Default true
  "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
  "html": false, //Button which allows you to edit the generated HTML. Default false
  "link": true, //Button to insert a link. Default true
  "image": false, //Button to insert an image. Default true,
  "color": false //Button to change color of font  
});
      
        $('#ful').click(function(){

            $('#jbtype').val('Fulltime');

        });
        $('#partym').click(function(){

            $('#jbtype').val('Parttime');

        });
        $('#fre').click(function(){

            $('#jbtype').val('Contract');

        });

     $("#jp_post_job_process").validate({
       ignore: ":hidden:not(textarea)",
        rules:{
            euser_name:{
                required:true,
                remote: {
                    url: baseURL + "ajax/get_validate_user_name_duplication/N/Y",
                    type: "post",
                    data: {
                        euser_name: function(){ return $("#jp_user_name").val(); }
                    }
                }

            },
       
             job_content:{
                required:true},
 
            eemail:{
                required:true,
                email: true,
                remote: {
                    url: baseURL + "ajax/get_validate_email_duplication/employer",
                    type: "post",
                    data: {
                        eemail: function(){ return $("#jp_email").val(); }
                    }
                }
            },
            jp_password:{
                required:true,
                minlength: 6
            },
            jp_jobtype:{
                required:true
            },
            jp_position:{
                required:true
            },
            jp_location:{
                required:true
            },
            jp_category_id:{
                required:true
            },
            jp_experience_from:{
                required:true

            },
            jp_experience_to:{
                required:true
            },
            recaptcha_response_field:{
                required:true
            },
           ecompany_name:{
                required:true,
                remote: {
                    url: baseURL + "ajax/get_validate_company_duplication",
                    type: "post",
                    data: {
                        ecompany_name: function(){ return $("#jp_company_name").val(); }
                    }
                }
            }

        },
        messages: {
            eemail: {
                required: 'Email address is required',
                email: 'Please enter a valid email address',
                remote: 'Email already used.'
            },
            euser_name: {
                required: 'Username is requeired',
                remote: 'Username already used.'
            },
            ecompany_name: {
                required: 'Company Name is requeired',
                remote: 'This Company Name is already exist in the system. Please ask the company user account manager to add you as one of the user. THANK YOU!'
            },
            jp_password:{
              required: "Ooops this field can't be empty",
            },
            job_content:{
               required: "Please Insert Job Description",
            }
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        } 
    }); 
   
        $("#jp_post_job_process").ajaxForm({ // initialize the plugin
            // any other options,
            beforeSubmit: function () { 
                return $("#jp_post_job_process").valid(); // TRUE when form is valid, FALSE will cancel submit
            },
            success: function (data) {
                var obj = jQuery.parseJSON(data); 
                    if(obj.res){ 
                         bootbox.dialog({
                          title: "Notification",
                          message: obj.data_m, 
                          buttons: {
                              success: {
                                label: "Ok",
                                className: "btn-success",
                                    callback: function() {
                                         location.reload(true);
                                    }
                                  } 
                               }
                       }); 
                    }else{
                       bootbox.dialog({
                            title: "Ooops",
                            message: obj.data_m,
                            
                            buttons: {
                                success: {
                                  label: "Ok",
                                  className: "btn-success",
                                      callback: function() {
                                        Recaptcha.reload(); 
                                      }
                                    } 
                                 }
                         }); 
                    }
            }  

        });


    });
</script>