

        <div class="col-xs-12 col-sm-4 col-lg-3 hidden-xs side_padding "   role="navigation">
            <?php if($this->uri->segment(2) == 'jobs'):?>
                <div style="padding-top:140px;"></div>
            <?php endif;?>
            <?php if($this->uri->segment(2) == 'categories'):?>
                <div style="padding-top:140px;"></div>
            <?php endif;?>
            <div class="panel-group sidebar" id="accordion">
                <div class="panel-heading">
                    <h4 class="heading" style="color: #545454!important;"> <span>See Categories</span><span class="icon-eye-open pull-right hidden-xs" style="color: #737373;font-size:12px;">

                    </span>
                        <span class="pull-right" class='visible-xs' style="color: #737373;font-size:12px;">
                                      <a href="#side-menu" class="side-menu-link" title="Click for sub-menu">
                                          <i class="icon-arrow-left"></i>
                                      </a>
                    </span>
                        <span class="left"></span></h4>
                    <ul class="nav nav-pills nav-stacked side_cat" >
						<?php foreach($category_list[0] as $cat_list):?>
                        <li>
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $cat_list[0]; ?>"><i class="icon-circle-arrow-right"></i> <?php echo $cat_list[1]; ?></a>
                            <ul id="collapse<?php echo $cat_list[0]; ?>" class="panel-collapse collapse">
								<?php foreach($category_list[1][$cat_list[0]] as $cat_list_sub):?>
                                <li><a href="<?php echo base_url()?>patsada/categories/<?php echo $cat_list_sub[0]; ?>"><?php echo $cat_list_sub[1]; ?></a></li>
                               <?php endforeach; ?>
                            </ul>
                        </li>
                        <?php endforeach; ?>
                    </ul>

                </div>
            </div>
            <div class='panel-group   hide' style="background: #ffffff!important;">
                <hr style="border: none!important;">
                <div class='panel-heading' style="padding-bottom:3px!important;">
                    <h4 class="heading" style="color: #545454!important;"> <span>Our Sponsor</span><span class="icon-group  pull-right" style="color: #737373;font-size:12px;"></span>
                        <span class="left"></span></h4>
                </div>
                <div class="panel-body" style="margin-top: -31px !important;" >
					<center>
                    <a href="#"><img class='img-thumbnail' src="<?php echo base_url()?>assets/images/featured-emp/aegis.jpg"></a>
                    <a href="#"><img class='img-thumbnail' src="<?php echo base_url()?>assets/images/featured-emp/clickinglabs.jpg"></a>
                    <a href="#"><img class='img-thumbnail' src="<?php echo base_url()?>assets/images/featured-emp/convergys.jpg"></a>
                    <a href="#"><img class='img-thumbnail' src="<?php echo base_url()?>assets/images/featured-emp/dell.jpg"></a>
                    <a href="#"><img class='img-thumbnail' src="<?php echo base_url()?>assets/images/featured-emp/eperformax.jpg"></a>
                    <a href="#"><img class='img-thumbnail' src="<?php echo base_url()?>assets/images/featured-emp/sun.jpg"></a>
                    <a href="#"><img class='img-thumbnail' src="<?php echo base_url()?>assets/images/featured-emp/mosbeau.jpg"></a>
					</center>
                </div>
            </div>

            <div class='panel-group hidden-xs' >
                <div class="panel-heading">
                    <h4 class="media-heading">Media heading</h4>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas blandit mattis cursus. Maecenas ultrices et massa quis commodo. Nulla id venenatis tortor. Nulla accumsan ligula leo, vitae rutrum diam tempor et. Etiam in diam ante. Maecenas non vestibulum dolor. Nulla id nibh eget arcu rhoncus facilisis. Phasellus id aliquam diam. Donec aliquam commodo neque vel tempor. Quisque sit amet felis sed lectus rhoncus fringilla et nec augue. Pellentesque et metus orci. Pellentesque a suscipit erat. Nunc scelerisque dui nec eleifend iaculis.
                </div>
                <div class="panel-body">
                    <ul class="media-list">
                        <li class="media">
                            <a class="pull-left" href="#">
                                <img  class="media-object img-circle"  src="<?php echo base_url();?>assets/images/avafour.jpg"  style="width: 64px; height: 64px;">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">Media heading</h4>
                                <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>


                            </div>
                        </li>
                        <li class="media">
                            <a class="pull-right" href="#">
                                <img class="media-object img-circle"  src="<?php echo base_url();?>assets/images/avafour.jpg" style="width: 64px; height: 64px;">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">Media heading</h4>
                                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
          </div>
