<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/Untitled-1.png">

    <title>Patsada</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-wysihtml5.css"></link>
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Monda:400,700">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>assets/js/html5shiv.js"></script>
    <script src="<?php echo base_url();?>assets/js/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
	<script>
	var baseURL = '<?php echo base_url();?>';
	</script>
 
    <script src="<?php echo base_url();?>assets/js/wysihtml5-0.3.0.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script src="http://www.urimalo.com/assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap3-wysihtml5.js"></script>
    <script>


        window.open    = function(){};
        window.print   = function(){};
        // Support hover state for mobile.
        if (false) {
            window.ontouchstart = function(){};
        }


        $(document).ready(function() {
            $('[data-toggle=offcanvas]').click(function() {
                $('.row-offcanvas').toggleClass('active');
                $('.showhide').toggle();
            });
        });
    </script>

<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url()?>assets/js/bootbox.js"></script>
<script src="<?php echo base_url()?>assets/js/formvalidation.js"></script>

</head>
<!-- NAVBAR
================================================== -->
<body>
