

<div class="col-xs-12 col-sm-8 col-lg-9" id="job_contain" >
<div class="clearfix" id="job2contain"></div>
    <div class="row">

         <div class='panel panel-default'>
             <div class="panel-heading">
                Job Description
             </div>
             <div class="panel-body">
                 <?php if(!empty($results)):?>
                        <?php foreach ($results as $row):?>
                            <div class='col-xs-12 col-sm-12'>
                              <h4 class="heading" style="color: #545454!important;">
                                    <span><?php $position=$row->position;$job_post_id=$row->job_post_id; echo $row->position?></span>
                                    <p class='pull-right' style="font-size: 12px!important;"><i class='icon-calendar main-color'></i>  <?php     $date = $row->created_date_time;
                                        echo  $your_date = date("M d Y", strtotime($date));?></p>
                                  <span class="left"></span>
                              </h4>
                              </div>

                                <div class='col-xs-12 col-sm-12 col-md-8 col-lg-9'>
                                    <dl class="dl-horizontal">
                                        <dt>Company:</dt>
                                        <dd><strong><?php echo $row->company_name?></strong></dd>
                                        <dt>url:</dt>
                                        <dd><i class='icon-globe main-color'></i> <?php echo $row->url?> </dd>
                                        <dt>Address:</dt>
                                        <dd> <i class='icon-map-marker'></i><?php echo $row->company_address?>, <?php echo $row->company_city?>, <?php echo $row->company_country?> </dd>

                                        <dt>Description:</dt>
                                        <dd>  <?php echo $row->company_description?></dd>

                                    </dl>
                                    <br/>

                                </div>
                                <div class='col-sm-4 col-md-4 col-lg-3 hidden-xs hidden-sm'>
                                    <img src="<?php echo base_url()?>assets/images/employer_pic/<?php echo $row->company_logo?>" style="width:180px;height: 180px;">

                                </div>
                         <div class='clearfix'>      </div>
                             <div class='col-xs-12  col-sm-12 col-md-12 col-lg-12'>

                                 <hr style="border:none!important;">
                                 <h4 class="heading" style="color: #545454!important;">
                                     <span>Job Description</span>
                                     <span class="left"></span>
                                 </h4>


                             </div>


                         <div class='col-xs-12 col-sm-12  col-md-12'>
                             <div class='clearfix visible-xs visible-sm visible-md'>      </div>
                                 <?php echo $row->job_content?>


                             <div class='clearfix'>      </div>
                         </div>
                         <div class='col-xs-12 col-sm-12  col-md-12'>
                             <hr style="border:none!important;">
                             <br />
                             <div class='clearfix'>      </div>
                             <h4 class="heading" style="color: #545454!important;">
                                 Details
                                 <span class="left"></span>
                             </h4>
                             <dl class="dl-horizontal">
                                 <dt>Experience:</dt>
                                 <dd><i class='icon-time main-color'></i> <?php echo $row->experience?></dd>
                                 <dt>Location:</dt>
                                 <dd><i class='icon-map-marker main-color'></i> <?php echo $row->location?></dd>
                                 <dt>Category:</dt>
                                 <dd><?php echo $row->name?></dd>
                                 <dt>Job Type:</dt>
                                 <dd><?php echo $row->jobtype?></dd>
                             </dl>



                         </div>


                        <?php endforeach;?>
                 <?php endif;?>

                 <div class='col-xs-12 col-sm-12  col-md-12'>
                    <p class='text-center'>
                       <?php if($roll =='candidate')
                            {if($result_can){
                                ?>
                                <span   id='' class='btn btn-info btn-lg'>You already applied for this job</span>
                            <?php
                            }else{ ?>
                                <a href='#candidate_apply' data-toggle="modal" id='click_me' class='btn btn-success btn-lg'>Apply</a>
                            <?php }?>
                        <?php }
                            elseif($roll =='employer')
                            {?>
                        <?php }
                        else{
                            ?>
                            <a href='#modal_apply' data-toggle="modal" id='click_me' class='btn btn-success btn-lg'>Apply</a>
                        <?php }?>
                    </p>
                 </div>
         </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Related Jobs </div>
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th style="text-align:left;width: 100px;"><i class="icon-calendar main-color"></i> Posted</th>
                    <th style="text-align:left;"  ><i class="icon-file main-color"></i> Description</th>
                    <th style="text-align:left;width: 200px;" class="hidden-xs"><i class="icon-map-marker   main-color"></i> Location</th>
                    <th   class="hidden-xs" style="text-align:left;width: 100px;" ><i class="icon-bolt main-color"></i> Contract</th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($related_jobs)):?>
                    <?php foreach($related_jobs as $row):?>
                        <?php if($row->is_premium == 'Y'): ?>
                            <tr class="tr_jobs premium" style="cursor: pointer;">
                            <td> <p class="text-danger">URGENT</p></td>
                        <?php else: ?>
                            <tr class='tr_jobs' style="cursor: pointer;">
                            <td><p>

                                    <?php     $date = $row->created_date_time;
                                    echo  $your_date = date("M d Y", strtotime($date));?>


                                </p></td>
                        <?php endif; ?>


                        <td><a href="<?php echo base_url()?>patsada/jobs/<?php echo $row->job_post_id;?>" style="color:#282828;font-weight: bold!important;text-decoration: none!important;"><?php echo $row->position?></a>
                            <p>
                                <small><?php echo $row->company_name?></small>

                                <small><?php echo $row->company_address?></small>

                            </p>
                        </td>
                        <td class="hidden-xs"><?php echo $row->location?></td>
                        <td  class="hidden-xs" ><?php echo $row->jobtype?></td>
                        </tr>

                    <?php endforeach;?>
                <?php endif;?>
            </tbody>
        </table>
    </div>



<?php  if($this->session->userdata('logged_in')):?>

         <div class="modal fade" id="candidate_apply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             <div class="modal-dialog">
                 <div class="modal-content">
                     <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                         <h4 class="modal-title">Application</h4>
                     </div>
                     <div class="modal-body" id='candidate_application'>
                         <div id='log_alerts'></div>
                            <form id='form_candidate_application' action = '<?php echo base_url();?>ajax/application'  class='form-horizontal'  method="POST">

                                <input type="hidden" name='job_post_id' value="<?php echo $job_post_id;?>">
                                <input type="hidden" name='applied_datetime' value="<?php echo $date=date('Y-m-d h:i:s')?>">
                                <input type="hidden" name='candidate_id' value="<?php echo $id;?>">
                                <input type="hidden" name='job_post_event_id' value="0">

                                <div class="form-group">
                                    <label class='control-label'>Cover Letter</label>

                                        <textarea class="form-control" id="cover_letter" name='cover_letter' style='height:400px;' placeholder="Cover Letter">Dear Sir/Madam,

Hello and good day!

This letter is in response to your <?php echo $position;?> job advertisement in patsada.com on <?php echo $your_date?>.

[ Add description of your achievements and contact information here ]

Thank you very much.

Sincerely yours,

[ your name ]
                                        </textarea>

                                </div>
                                <div class='form-group'>
                                    <div class='col-lg-12'>
                                       <button   id="form_application" class='btn btn-lg btn-success'>Apply</button>
                                    </div>
                                </div>
                            </form>
                     </div>
                 </div><!-- /.modal-content -->
             </div><!-- /.modal-dialog -->
         </div><!-- /.modal -->
<?php else:?>

        <div class="modal fade" id="modal_apply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Notification</h4>
                    </div>
                    <div class="modal-body"  >
                            <p class="text-center text-info " style="font-size: 18px;"> Have an account? sign in <a data-toggle="modal"  style="text-decoration: none!important;color:#f3c62b!important;"   href="#login">Here</a> or Sign up <a data-toggle="modal"  style="text-decoration: none!important;color:#f3c62b!important;"   href="#register">Here</a> </p>

                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<?php endif;?>



    </div><!-- /.row -->
</div><!-- /.row -->



<script src="<?php echo base_url()?>assets/js/jquery.form.js"></script>
<script src="<?php echo base_url();?>assets/js/ckeditor/ckeditor.js"></script>

<script>
    $(document).ready(function(){
        var editor= CKEDITOR.replace( 'cover_letter',
            {
                uiColor: '#dddddd',
                toolbar:
                    [
                        ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList'],
                        ['UIColor']
                    ]

            });
      /*  var editor2= CKEDITOR.replace( 'resume_text',
            {
                uiColor: '#dddddd',
                toolbar:
                    [
                        ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList'],
                        ['UIColor']
                    ]

            });*/

      

/*

    $.validator.addMethod('filesize', function(value, element, param) {
        // param = size (en bytes)
        // element = element to validate (<input>)
        // value = value of the element (file name)
        return this.optional(element) || (element.files[0].size <= param)
    });

$( '#application' ).submit( function( e ) {
    e.preventDefault();
  }).validate({
            rules:{
                first_name:{
                    required:true
                },
                title:{
                    required:true
                },
                email:{
                    required:true
                },
                contact_no:{
                    required:true
                },
                cover_letter:{
                    required:true
                },
                resume_filename:{
                    required:{
                        depends: function (element) { //depends takes a function pointer that
                            //returns a value, informing the rule
                            //if it is required. In this case, always true.

                           if($('#resume_text').val() ==''){
                               return true;
                           }


                        }
                    },
                    accept: "doc|pdf|docx",
                    filesize: 1048576
                },
                resume_text:{
                    required:{
                        depends: function (element) { //depends takes a function pointer that
                            //returns a value, informing the rule
                            //if it is required. In this case, always true.
                           if($('#resume_filename').val() ==''){
                               return true;
                           }

                        }
                    }
                },
                recaptcha_response_field:{
                    required:true
                },
                ignore: []
            },
             messages: { resume_filename: "File must be doc, pdf, not exceed 500kb." },

            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            submitHandler: function(form){
              var dataString = $("#application").serialize();
                form = new FormData(document.getElementById('application'));
                        $.ajax( {
                          url: '<?php echo base_url()?>patsada/application',
                          type: 'POST',
                          data:form,
                          processData: false,
                          contentType: false,
                          dataType: 'html',              
                           success: function(data){
                             $('#captcaTxt').html(data.captca_text); 
                                    if(data['data_status'] == true){
                                          bootbox.dialog({
                                                message: data.data_result,
                                                title: "Application successfully submitted",
                                                buttons: {
                                                success: {
                                                  label: "Success!",
                                                  className: "btn-success",
                                                  callback: function() {
                                                     location.reload(true);
                                                  }
                                                } 
                                                 }
                                             });
                                        location.reload(true);
                                    }else{
                                        bootbox.dialog({
                                                message: data.data_result,
                                                title: "Application not successfully submitted",
                                                buttons: {
                                                success: {
                                                  label: "Success!",
                                                  className: "btn-success",
                                                  callback: function() {
                                                     location.reload(true);
                                                  }
                                                } 
                                                 }
                                             }); 
                                    }
                               alert(data);
                           }
                        } );
                 

            }
        });*/


    /*    $("#application").ajaxForm({ // initialize the plugin
            // any other options,
            url:'<?php echo base_url()?>patsada/application',
            dataType:'json',
            beforeSubmit: function () {
                return $("#application").valid(); // TRUE when form is valid, FALSE will cancel submit
            },
            success: function (data) {

                bootbox.dialog({
                    message: "Successfully",
                    title: "Application successfully submitted",
                    buttons: {
                    success: {
                      label: "Success!",
                      className: "btn-success",
                      callback: function() {
                         location.reload(true);
                      }
                    } 
                     }
                 });
                console.log(data);
                $('#captcaTxt').html(data.captca_text);
              
                if(data['data_status'] == true){
                    location.reload(true);
                }else{
                    $('#log_alerts').html(data.data_result);

                    $('#recaptcha_reload_btn').click();
                }
            }
        });
*/
/*
function showResponse(responseText, statusText, xhr, $form)  { 
    // for normal html responses, the first argument to the success callback 
    // is the XMLHttpRequest object's responseText property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'xml' then the first argument to the success callback 
    // is the XMLHttpRequest object's responseXML property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'json' then the first argument to the success callback 
    // is the json data object returned by the server 
 
    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
        '\n\nThe output div should have already been updated with the responseText.'); 
}*/

          $("#form_candidate_application").ajaxForm({ // initialize the plugin
            // any other options,
            beforeSubmit: function () {
                return $("#form_candidate_application").valid(); // TRUE when form is valid, FALSE will cancel submit
            },
            success: function (data) {
                var parsedJson = $.parseJSON(data);

                var stats=parsedJson.data_status;
                var datas=parsedJson.data_result;

                if(data.data_status == true){
                   
                bootbox.dialog({
                    message: data.data_result,
                    title: "Application successfully submitted",
                    buttons: {
                    success: {
                      label: "Success!",
                      className: "btn-success",
                      callback: function() {
                         location.reload(true);
                      }
                    } 
                     }
                 });
                }else{
                    bootbox.dialog({
                    message: data.data_result,
                    title: "Application not successfully submitted",
                    buttons: {
                    success: {
                      label: "Success!",
                      className: "btn-success",
                      callback: function() {
                         location.reload(true);
                      }
                    } 
                     }
                 });      
                }
            }
        });

    });

</script>