

<div class="col-xs-12 col-sm-8 col-lg-9" id="job_contain" >
<div class="clearfix" id="job2contain"></div>
    <div class="row">

         <div class='panel panel-default'>
             <div class="panel-heading">
                Job Description
             </div>
             <div class="panel-body">
                 <?php if(!empty($results)):?>
                        <?php foreach ($results as $row):?>
                            <div class='col-xs-12 col-sm-12'>
                              <h4 class="heading" style="color: #545454!important;">
                                    <span><?php $position=$row->position;$job_post_id=$row->job_post_id; echo $row->position?></span>
                                    <p class='pull-right' style="font-size: 12px!important;"><i class='icon-calendar main-color'></i>  <?php     $date = $row->created_date_time;
                                        echo  $your_date = date("M d Y", strtotime($date));?></p>
                                  <span class="left"></span>
                              </h4>
                              </div>

                                <div class='col-xs-12 col-sm-12 col-md-8 col-lg-9'>
                                    <dl class="dl-horizontal">
                                        <dt>Company:</dt>
                                        <dd><strong><?php echo $row->company_name?></strong></dd>
                                        <dt>url:</dt>
                                        <dd><i class='icon-globe main-color'></i> <?php echo $row->url?> </dd>
                                        <dt>Address:</dt>
                                        <dd> <i class='icon-map-marker'></i><?php echo $row->company_address?>, <?php echo $row->company_city?>, <?php echo $row->company_country?> </dd>

                                        <dt>Description:</dt>
                                        <dd>  <?php echo $row->company_description?></dd>

                                    </dl>
                                    <br/>

                                </div>
                                <div class='col-sm-4 col-md-4 col-lg-3 hidden-xs hidden-sm'>
                                <?php if($row->company_logo):?>
                                    <img src="<?php echo base_url()?>assets/images/employer_pic/<?php echo $row->company_logo;?>" style="width:180px;height: 180px;">
                                <?php else:?>
                                    <img src="<?php echo base_url()?>assets/images/round-logo.png" style="width:180px;height: 180px;">
                                
                                <?php endif;?>    
                                </div>
                         <div class='clearfix'>      </div>
                             <div class='col-xs-12  col-sm-12 col-md-12 col-lg-12'>

                                 <hr style="border:none!important;">
                                 <h4 class="heading" style="color: #545454!important;">
                                     <span>Job Description</span>
                                     <span class="left"></span>
                                 </h4>


                             </div>


                         <div class='col-xs-12 col-sm-12  col-md-12'>
                             <div class='clearfix visible-xs visible-sm visible-md'>      </div>
                                 <?php echo $row->job_content?>


                             <div class='clearfix'>      </div>
                         </div>
                         <div class='col-xs-12 col-sm-12  col-md-12'>
                             <hr style="border:none!important;">
                             <br />
                             <div class='clearfix'>      </div>
                             <h4 class="heading" style="color: #545454!important;">
                                 Details
                                 <span class="left"></span>
                             </h4>
                             <dl class="dl-horizontal">
                                 <dt>Experience:</dt>
                                 <dd><i class='icon-time main-color'></i> <?php echo $row->experience?></dd>
                                 <dt>Location:</dt>
                                 <dd><i class='icon-map-marker main-color'></i> <?php echo $row->location?></dd>
                                 <dt>Category:</dt>
                                 <dd><?php echo $row->name?></dd>
                                 <dt>Job Type:</dt>
                                 <dd><?php echo $row->jobtype?></dd>
                             </dl>



                         </div>


                        <?php endforeach;?>
                 <?php endif;?>

                 <div class='col-xs-12 col-sm-12  col-md-12'>
                    <p class='text-center'>
                       <?php if($roll =='candidate')
                            {if($result_can){
                                ?>
                                <span   id='' class='btn btn-info btn-lg'>You already applied for this job</span>
                            <?php
                            }else{ ?>
                                <a href='#candidate_apply' data-toggle="modal" id='click_me' class='btn btn-success btn-lg'>Apply</a>
                            <?php }?>
                        <?php }
                            elseif($roll =='employer')
                            {?>
                        <?php }
                        else{
                            ?>
                            <a href='#modal_apply' data-toggle="modal" id='click_me' class='btn btn-success btn-lg'>Apply</a>
                        <?php }?>
                    </p>
                 </div>
         </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Related Jobs </div>
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th style="text-align:left;width: 100px;"><i class="icon-calendar main-color"></i> Posted</th>
                    <th style="text-align:left;"  ><i class="icon-file main-color"></i> Description</th>
                    <th style="text-align:left;width: 200px;" class="hidden-xs"><i class="icon-map-marker   main-color"></i> Location</th>
                    <th   class="hidden-xs" style="text-align:left;width: 100px;" ><i class="icon-bolt main-color"></i> Contract</th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($related_jobs)):?>
                    <?php foreach($related_jobs as $row):?>
                        <?php if($row->is_premium == 'Y'): ?>
                            <tr class="tr_jobs premium" style="cursor: pointer;">
                            <td> <p class="text-danger">URGENT</p></td>
                        <?php else: ?>
                            <tr class='tr_jobs' style="cursor: pointer;">
                            <td><p>

                                    <?php     $date = $row->created_date_time;
                                    echo  $your_date = date("M d Y", strtotime($date));?>


                                </p></td>
                        <?php endif; ?>


                        <td><a href="<?php echo base_url()?>patsada/jobs/<?php echo $row->job_post_id;?>" style="color:#282828;font-weight: bold!important;text-decoration: none!important;"><?php echo $row->position?></a>
                            <p>
                                <small><?php echo $row->company_name?></small>

                                <small><?php echo $row->company_address?></small>

                            </p>
                        </td>
                        <td class="hidden-xs"><?php echo $row->location?></td>
                        <td  class="hidden-xs" ><?php echo $row->jobtype?></td>
                        </tr>

                    <?php endforeach;?>
                <?php endif;?>
            </tbody>
        </table>
    </div>



 

         <div class="modal fade" id="candidate_apply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             <div class="modal-dialog">
                 <div class="modal-content">
                     <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                         <h4 class="modal-title">Application</h4>
                     </div>
                     <div class="modal-body" id='candidate_application'>
                         <div id='log_alerts'></div>
                            <form id='form_candidate_application'  enctype="multipart/form-data"  action= '<?php echo base_url();?>ajax/application' class='form-horizontal'  method="POST">

                                <input type="hidden" name='job_post_id' value="<?php echo $job_post_id;?>">
                                <input type="hidden" name='applied_datetime' value="<?php echo $date=date('Y-m-d h:i:s')?>">
                                <input type="hidden" name='candidate_id' value="<?php echo $id;?>">
                                <input type="hidden" name='job_post_event_id' value="0">
                           
                                <div class="form-group">
                                    <label class='control-label'>Cover Letter</label>

                                        <textarea class="form-control" id="cover_letter1" name='cover_letter' style='height:400px;' placeholder="Cover Letter">Dear Sir/Madam,<br/><br/>

Hello and good day!

This letter is in response to your <?php echo $position;?> job advertisement in patsada.com on <?php echo $your_date?>.<br/><br/>

[ Add description of your achievements and contact information here ]<br/><br/>

Thank you very much.<br/><br/>

Sincerely yours,<br/><br/>

[ your name ]
                                        </textarea>

                                </div>
                                <div class='form-group'>
                                    <div class='col-lg-12'>
                                       <button   id="form_application" class='btn btn-lg btn-success'>Apply</button>
                                    </div>
                                </div>
                            </form>
                     </div>
                 </div><!-- /.modal-content -->
             </div><!-- /.modal-dialog -->
         </div><!-- /.modal -->
 

        <div class="modal fade" id="modal_apply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Notification</h4>
                    </div>
                    <div class="modal-body" id='jobs_id_body'>
                        <div id='log_alerts' ></div>
                        <form id="application"  enctype="multipart/form-data" class="form-horizontal" method="POST" role="form" action="<?php echo base_url()?>patsada/application">
                            <input type="hidden" name='job_post_id' value="<?php echo $job_post_id;?>">
                            <input type="hidden" name='applied_datetime' value="<?php echo $date=date('Y-m-d h:i:s')?>">
                                <div class="form-group">
                                       <label class='control-label col-lg-3'>Title</label>
                                       <div class='col-lg-3'>
                                         <select name='title' id='title' class="form-control">
                                             <option>Mr</option>
                                             <option>Ms</option>
                                             <option>Mrs</option>
                                             <option>Dr</option>
                                         </select>
                                       </div>
                                </div>
                                <div class="form-group">
                                       <label class='control-label col-lg-3'>First Name</label>
                                       <div class='col-lg-9'>
                                           <input type="text" class='form-control' name="first_name" id='first_name'>
                                       </div>
                                </div>
                                <div class="form-group">
                                       <label class='control-label col-lg-3'>Last Name</label>
                                       <div class='col-lg-9'>
                                           <input type="text" class='form-control' name="last_name" id='last_name'>
                                       </div>
                                </div>
                                <div class="form-group">
                                    <label class='control-label col-lg-3'>Email</label>
                                    <div class='col-lg-9'>
                                        <input type="email" class="form-control" id="email"  name='email' placeholder="Email" value="<?php set_value('email');?>">
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class='control-label col-lg-3'>Mobile/Telephone</label>
                                    <div class='col-lg-9'>
                                    <input type="text" class="form-control" id="contact_no" name='contact_no' placeholder="Contact" value="<?php set_value('location');?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class='control-label col-lg-3'>Cover Letter</label>
                                    <div class='col-lg-9'>
                                    <textarea class="form-control" id="cover_letter" name='cover_letter' style='height:400px;' placeholder="Cover Letter">Dear Sir/Madam,<br/><br/>

Hello and good day!

This letter is in response to your <?php echo $position;?> job advertisement in patsada.com on <?php echo $your_date?>.<br/><br/>

[ Add description of your achievements and contact information here ]<br/><br/>

Thank you very much.<br/><br/>

Sincerely yours,<br/><br/>

[ your name ]
                                    </textarea>
                                </div>
                                </div>

                            <div class="form-group">
                                <label class='control-label col-lg-3'>Resume</label>
                                <div class='col-lg-9'>
                                    <ul class="nav nav-tabs" id="resume_tabs">
                                        <li class="active"><a href="#attach" class='btn btn-info'>Attact</a></li>
                                        <li><a href="#paste" class='btn btn-info'>Paste</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane active" id="attach">
                                            <div class="form-group">
                                                <div class="col-lg-8">
                                                    <input type="file" name="resume_filename" id="resume_filename"  size="20"/>  </div>
                                              </div>


                                                 <span style="font-size: 11px;">
                                                  <br/>
                                                    <em> Your file must be in Word (.doc or .docx) or PDF (.pdf) format.
                                                        File size must not exceed 500kb.</em></span>
                                        </div>
                                        <div class="tab-pane" id="paste">
                                            <textarea class="form-control" id="resume_text" name='resume_text' style='height:300px;'><?php set_value('paste');?></textarea>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class='control-label col-lg-3'>Security</label>
                                <div class="col-lg-9">
                                    <label   >Please Enter text given.</label>
                                   <div class="col-xs-8">
                                    <input type="text" name = 'captcha'  class="form-control">
                                   </div>
                                   <label id="captcaTxt" class="col-xs-4" style="color:#515151;background: #ddd;padding: 6px;text-align: center;"><?php echo $captcha_text;?></label>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class='control-label col-lg-3'> </label>
                                <div class="col-lg-9">
                                    <button type="submit" id='submit_forms' class="btn btn-primary">Apply</button>
                                </div>

                            </div>
                        </form>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
 



    </div><!-- /.row -->
</div><!-- /.row -->
 
<script src="<?php echo base_url()?>assets/js/jquery.form.js"></script> 
<script>


 $(document).ready(function(){
      
 $('#cover_letter1').wysihtml5({
  "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
  "emphasis": true, //Italics, bold, etc. Default true
  "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
  "html": false, //Button which allows you to edit the generated HTML. Default false
  "link": true, //Button to insert a link. Default true
  "image": false, //Button to insert an image. Default true,
  "color": false //Button to change color of font  
});

  $('#cover_letter').wysihtml5({
  "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
  "emphasis": true, //Italics, bold, etc. Default true
  "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
  "html": false, //Button which allows you to edit the generated HTML. Default false
  "link": true, //Button to insert a link. Default true
  "image": false, //Button to insert an image. Default true,
  "color": false //Button to change color of font  
});
    $.validator.addMethod('filesize', function(value, element, param) {
        // param = size (en bytes)
        // element = element to validate (<input>)
        // value = value of the element (file name)
        return this.optional(element) || (element.files[0].size <= param)
    });
 
$( '#application' ).validate({
            ignore: ":hidden:not(textarea)",
            rules:{
                first_name:{
                    required:true
                },
                title:{
                    required:true
                },
                email:{
                    required:true
                },
                cover_letter:{
                    required:true
                },
                contact_no:{
                    required:true
                },
                cover_letter:{
                    required:true
                },
                resume_filename:{
                    required:{
                        depends: function (element) { //depends takes a function pointer that
                            //returns a value, informing the rule
                            //if it is required. In this case, always true.

                           if($('#resume_text').val() ==''){
                               return true;
                           }


                        }
                    },
                    accept: "doc|pdf|docx",
                    filesize: 1048576
                },
                resume_text:{
                    required:{
                        depends: function (element) { //depends takes a function pointer that
                            //returns a value, informing the rule
                            //if it is required. In this case, always true.
                           if($('#resume_filename').val() ==''){
                               return true;
                           }

                        }
                    }
                },
                recaptcha_response_field:{
                    required:true
                },
                ignore: []
            },
             messages: { resume_filename: "File must be doc, pdf, not exceed 500kb." },

            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            } 
        }); 
     
      $("#application").ajaxForm({ // initialize the plugin
            // any other options,
            beforeSubmit: function () {
                return $("#application").valid(); // TRUE when form is valid, FALSE will cancel submit
            },
            dataType:'json',
            success: function (data) { 
                if(data.data_status == true){
                    bootbox.dialog({
                    title: "Application successfully submitted",
                    message:data.data_result, 
                    buttons: {
                        success: {
                          label: "Ok",
                          className: "btn-success",
                              callback: function() {
                                   location.reload(true);
                              }
                            } 
                         }
                 });
                }else{
                 
                  bootbox.dialog({
                    title: "Notification",
                    message:data.data_result,
                    
                    buttons: {
                        success: {
                          label: "Ok",
                          className: "btn-success",
                              callback: function() { 
                              }
                            } 
                         }
                 });    
                    
                }
            }
        });
        
      $("#form_candidate_application").ajaxForm({ // initialize the plugin
            // any other options,
            beforeSubmit: function () {
                return $("#form_candidate_application").valid(); // TRUE when form is valid, FALSE will cancel submit
            },
            success: function (data) {
             
                if(data.data_status == false){
                  bootbox.dialog({
                    title: "Application successfully submitted",
                    message:data.data_result,
                    
                    buttons: {
                        success: {
                          label: "Ok",
                          className: "btn-success",
                              callback: function() { 
                              }
                            } 
                         }
                 });
                }else{
                     bootbox.dialog({
                    title: "Notification",
                    message: 'Your application has been send to Employer. we send you an approval if the employer approve your application or they will call you. <br/><center>THANK YOU FOR TRUSTING WITH US!</center>',
                    
                    buttons: {
                        success: {
                          label: "Ok",
                          className: "btn-success",
                              callback: function() {
                                   location.reload(true);
                              }
                            } 
                         }
                 });
                    
                }
            }
        });
    
    }); 



</script>