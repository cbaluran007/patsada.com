<div class="clearfix"></div>
<footer style="bottom: 0!important;">
    <div class='container'>
        <div class="footers">

            <div class="row">
                <img  src="<?php echo base_url();?>assets/img/rafting.png" style="position:absolute; !important; left: 5%;margin-top: -70px;  ">

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 hidden-xs">
                    <h4><span>Job Seekers</span></h4>
                    <small><a href="#">Create Account &amp; Resume</a> </small><br>
                    <small><a href="#">Search Jobs</a></small><br>
                    <small><a href="#">Job Seeker Login</a></small>

                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 hidden-xs">
                    <h4><span>Employers</span></h4>
                    <small><a href="#">Create Company Profile</a> </small><br>
                    <small><a href="#">Post a Job</a></small><br>
                    <small><a href="#">Employer Login</a></small>

                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 hidden-xs">
                    <h4><span>Resources</span></h4>
                    <small><a href="#">About PaTsada.com</a> </small><br>
                    <small><a href="#">Our Sponsors</a> </small><br>
                    <small><a href="#">Contact Us</a></small><br>
                    <small><a href="#">Help/Tips</a></small>

                </div>

                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <h6 >  <strong><img src="<?php echo base_url();?>assets/img/patsada_logo.PNG" style='height:30px;width:150px;padding-top:3px;'></strong><br></h6>
                    <small>
                        The word PaTsada is a local word from Cagayan de Oro City.<br>
                        Hence <b>PaTsada.com</b> is an online Job Search Site for kagay-anons. <br>
                        We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                        <b>PaTsada.com</b>... Jobs, Right at Your Fingertips<br/><br/><br/>
                    </small>
                </div>
            </div>
            <div class='row  hidden-xs'>
                <div class="social" id="social-media-links">
                    <legend style="border:none!important;color:#f5f5f5!important;">Socialize with us</legend>
                    <a href='#' class="icon-stack" rel="tooltip" data-placement="bottom" title="RSS"> <i class="icon-rss icon-light icon-large"></i> </a>
                    <a href='https://www.facebook.com/pages/Patsada/190587914447922' target="_blank" class="icon-stack" rel="tooltip" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-light icon-large"></i>  </a>
                    <a href='#' class="icon-stack" rel="tooltip" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-light icon-large"></i></a>
                    <a href='#' class="icon-stack" rel="tooltip" data-placement="bottom" title="Google plus"><i class="icon-google-plus icon-light icon-large"></i></a>
                </div><!-- END SOCIAL -->

            </div>
            <div class="row" style="border-top: 1px solid #717171;text-align: right;">
                <div class='col-lg-12 '><small>&copy; Copyright 2013. PaTsada.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359</small>
                </div>
            </div>

        </div><!-- CONTAINER FOOTER-->
    </div>
</footer>

<div id="fb-root"></div>
</div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="<?php echo base_url();?>assets/js/carousel.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>

<script src="<?php echo base_url();?>assets/js/collapse.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>assets/js/candidate_validation.js"></script>
<script src="<?php echo base_url();?>assets/js/ckeditor/ckeditor.js"></script>

<script src="<?php echo base_url();?>assets/js/tooltip.js"></script>
<script src="<?php echo base_url();?>assets/js/popover.js"></script>
<script src="<?php echo base_url();?>assets/js/uploadify_31/jquery.uploadify-3.1.min.js"></script>


<script>



    $(document).ready(function(){




        $("[rel='tooltip']").tooltip();

        $('#hover-img .thumbnail').hover(

            function () {
                $(this).find('.caption').fadeIn(250);
            },

            function () {
                $(this).find('.caption').fadeOut(250);
            });


        $('#userfile').uploadify({
             'auto':true,
             'swf':  baseURL+'assets/js/uploadify_31/uploadify.swf',
             'uploader': baseURL+'candidate/do_upload/'+$('#up_candidate_id').val(),
             'cancelImg':baseURL+'assets/js/uploadify_31/uploadify-cancel.png',
             'fileTypeExts':'*.jpg;*.jpeg;*.gif;*.png',
             'fileTypeDesc':'Image Files (.jpg,.bmp,.png,.tif)',
             'fileSizeLimit':'90kb',
             'fileDesc'  :'Please select Gif,JPG,PNG IMAGES',
             'fileObjName':'userfile',
             'buttonText':'Select Photo',
             'buttonClass' : 'btn btn-upload-resume',
             'multi':false,
             'width': 200,
             'simUploadLimit' : 1,
             'removeCompleted':true,
             'onUploadSuccess' : function(file, data, response) {
              var ret = jQuery.parseJSON(data);
              var uId = ret.file_name;
             //$('#mypic').src('<?php echo base_url();?>assets/images/job_keeper/"'+uId);
            document.getElementById('img_upload').src='<?php echo base_url();?>assets/images/job_seeker/'+uId;
             }
        });

        $('#resume').uploadify({
             'auto':true,
             'swf':  baseURL+'assets/js/uploadify_31/uploadify.swf',
             'uploader': baseURL+'candidate/resume_upload/'+$('#up_candidate_id').val(),
             'cancelImg':baseURL+'assets/js/uploadify_31/uploadify-cancel.png',
             'fileTypeExts':'*.doc;*.pdf;*.docx',
             'fileTypeDesc':'Resume Files type must (.doc,.pdf)',
             'fileSizeLimit':'500kb',
             'fileDesc'  :'Please select Gif,JPG,PNG IMAGES',
             'fileObjName':'userfile',
             'buttonText':'Upload Your RESUME Here',
             'buttonClass' : 'btn btn-upload-resume',
             'multi':false,
             'width': 200,
             'simUploadLimit' : 1,
             'removeCompleted':true,
             'onUploadSuccess' : function(file, data, response) {
              var ret = jQuery.parseJSON(data);
              var uId = ret.file_name;

             //$('#mypic').src('<?php echo base_url();?>assets/images/job_keeper/"'+uId);
              $('#resume_doc').toggleClass('show');
              $('#resume_doc').attr('href','<?php echo base_url();?>file/'+$('#up_candidate_id').val()+'/'+uId);
              $('#resume_doc').attr('download', uId);
           // document.getElementById('img_upload').src='<?php echo base_url();?>assets/images/job_seeker/'+uId;
             }
        });




        $('#form_fullname').click(function(){
            try{
                $("#form_address").removeClass('edit-arrow');
                $("#popup-address").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-address").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_email").removeClass('edit-arrow');
                $("#popup-email").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-email").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_bod").removeClass('edit-arrow');
                $("#popup-bod").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-bod").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_telephone").removeClass('edit-arrow');
                $("#popup-telephone").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-telephone").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_mobile").removeClass('edit-arrow');
                $("#popup-mobile").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-mobile").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_category").removeClass('edit-arrow');
                $("#popup-category").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-category").addClass('candidate-popover col-sm-7 col-lg-7 hide');
            }catch (e){}
            $("#popup-fullname").toggleClass('show');
            $(this).toggleClass('edit-arrow');
            $('#popup-fullname').css('left','49px');
        });
        $('#form_address').click(function(){
            try{
                $("#form_fullname").removeClass('edit-arrow');
                $("#popup-fullname").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-fullname").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_address").removeClass('edit-arrow');
                $("#popup-address").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-address").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_bod").removeClass('edit-arrow');
                $("#popup-bod").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-bod").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_telephone").removeClass('edit-arrow');
                $("#popup-telephone").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-telephone").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_mobile").removeClass('edit-arrow');
                $("#popup-mobile").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-mobile").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_category").removeClass('edit-arrow');
                $("#popup-category").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-category").addClass('candidate-popover col-sm-7 col-lg-7 hide');
            }catch (e){}
            $("#popup-address").toggleClass('show');

            $(this).toggleClass('edit-arrow');
            $('#popup-address').css('left','30px');

        });
        $('#form_email').click(function(){
            try{
                $("#form_fullname").removeClass('edit-arrow');
                $("#popup-fullname").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-fullname").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_address").removeClass('edit-arrow');
                $("#popup-address").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-address").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_bod").removeClass('edit-arrow');
                $("#popup-bod").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-bod").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_telephone").removeClass('edit-arrow');
                $("#popup-telephone").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-telephone").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_mobile").removeClass('edit-arrow');
                $("#popup-mobile").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-mobile").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_category").removeClass('edit-arrow');
                $("#popup-category").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-category").addClass('candidate-popover col-sm-7 col-lg-7 hide');
            }catch (e){}
            $("#popup-email").toggleClass('show');
            $(this).toggleClass('edit-arrow');
            $('#popup-email').css('left','30px');
        });
        $('#form_bod').click(function(){
            try{
                $("#form_fullname").removeClass('edit-arrow');
                $("#popup-fullname").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-fullname").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_address").removeClass('edit-arrow');
                $("#popup-address").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-address").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_email").removeClass('edit-arrow');
                $("#popup-email").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-email").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_telephone").removeClass('edit-arrow');
                $("#popup-telephone").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-telephone").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_mobile").removeClass('edit-arrow');
                $("#popup-mobile").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-mobile").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_category").removeClass('edit-arrow');
                $("#popup-category").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-category").addClass('candidate-popover col-sm-7 col-lg-7 hide');
            }catch (e){}
            $("#popup-bod").toggleClass('show');
            $(this).toggleClass('edit-arrow');
            $('#popup-bod').css('left','30px');

        });
        $('#form_telephone').click(function(){
            try{
                $("#form_fullname").removeClass('edit-arrow');
                $("#popup-fullname").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-fullname").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_address").removeClass('edit-arrow');
                $("#popup-address").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-address").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_email").removeClass('edit-arrow');
                $("#popup-email").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-email").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_bod").removeClass('edit-arrow');
                $("#popup-bod").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-bod").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_mobile").removeClass('edit-arrow');
                $("#popup-mobile").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-mobile").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_category").removeClass('edit-arrow');
                $("#popup-category").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-category").addClass('candidate-popover col-sm-7 col-lg-7 hide');
            }catch (e){}
            $("#popup-telephone").toggleClass('show');
            $(this).toggleClass('edit-arrow');
            $('#popup-telephone').css('left','30px');

        });
        $('#form_mobile').click(function(){
            try{
                $("#form_fullname").removeClass('edit-arrow');
                $("#popup-fullname").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-fullname").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_address").removeClass('edit-arrow');
                $("#popup-address").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-address").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_email").removeClass('edit-arrow');
                $("#popup-email").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-email").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_bod").removeClass('edit-arrow');
                $("#popup-bod").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-bod").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_telephone").removeClass('edit-arrow');
                $("#popup-telephone").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-telephone").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_category").removeClass('edit-arrow');
                $("#popup-category").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-category").addClass('candidate-popover col-sm-7 col-lg-7 hide');

            }catch (e){}
            $("#popup-mobile").toggleClass('show');
            $(this).toggleClass('edit-arrow');
            $('#popup-mobile').css('left','30px');

        });
        $('#form_category').click(function(){
            try{
                $("#form_fullname").removeClass('edit-arrow');
                $("#popup-fullname").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-fullname").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_address").removeClass('edit-arrow');
                $("#popup-address").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-address").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_email").removeClass('edit-arrow');
                $("#popup-email").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-email").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_bod").removeClass('edit-arrow');
                $("#popup-bod").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-bod").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_telephone").removeClass('edit-arrow');
                $("#popup-telephone").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-telephone").addClass('candidate-popover col-sm-7 col-lg-7 hide');
                $("#form_mobile").removeClass('edit-arrow');
                $("#popup-mobile").removeClass('candidate-popover col-sm-7 col-lg-7 show');
                $("#popup-mobile").addClass('candidate-popover col-sm-7 col-lg-7 hide');
            }catch (e){}
            $("#popup-category").toggleClass('show');
            $(this).toggleClass('edit-arrow');
            $('#popup-category').css('left','30px');

        });




        var editor = CKEDITOR.replace( 'objective',
            {
                uiColor: '#FFFFFF',
                toolbar:
                    [
                        ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList'],
                        ['UIColor']
                    ]

            });


        var editor2 = CKEDITOR.replace( 'role',
            {
                uiColor: '#FFFFFF',
                toolbar:
                    [
                        ['NumberedList', 'BulletedList'],
                        ['UIColor']
                    ]

            });



$('#toPresent0').change(function(){
	var date='<?php echo date('Y-m-d'); ?>';

    var c = this.checked ? '1' : '0';
    if(c == 1){
		$('#date_to0').val('0000-00-00');
	}else{
		$('#date_to0').val(date);
	}
});


var date='<?php echo date('Y-m-d'); ?>';
        var startDate = new Date(date);
        var FromEndDate = new Date();
        var ToEndDate = new Date();

		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

		var checkin = $('.from_date0').datepicker({
			format: 'yyyy-mm-dd',
			viewMode: "months",
			minViewMode: "months",
			endDate: ToEndDate,
		  onRender: function(date) {
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  if (ev.date.valueOf() > checkout.date.valueOf()) {
			var newDate = new Date(ev.date)
			newDate.setDate(newDate.getDate() + 1);
			checkout.setValue(newDate);
		  }
		  checkin.hide();
		  $('.to_date0')[0].focus();
		}).data('datepicker');
		var checkout = $('.to_date0').datepicker({
		format: 'yyyy-mm-dd',
		endDate: ToEndDate,
		viewMode: "months",
    minViewMode: "months",
		  onRender: function(date) {
			return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  checkout.hide();
		}).data('datepicker');

		var checkin = $('.from_datee0').datepicker({
			format: 'yyyy-mm-dd',
			viewMode: "months",
			minViewMode: "months",
			endDate: ToEndDate,
		  onRender: function(date) {
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  if (ev.date.valueOf() > checkout.date.valueOf()) {
			var newDate = new Date(ev.date)
			newDate.setDate(newDate.getDate() + 1);
			checkout.setValue(newDate);
		  }
		  checkin.hide();
		  $('.to_datee0')[0].focus();
		}).data('datepicker');
		var checkout = $('.to_datee0').datepicker({
		format: 'yyyy-mm-dd',
		endDate: ToEndDate,
		viewMode: "months",
    minViewMode: "months",
		  onRender: function(date) {
			return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  checkout.hide();
		}).data('datepicker');

        $('#account_settings').validate({
            rules:{
                password:{
                    required:true,
                    minlength: 6
                },
                password_2:{
                    required:true,
                    equalTo: "#password_1"
                }
            },
            messages: {
                password_2: {
                    equalTo: 'Password did not match'
                }
            },

            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            }
        });
    });



    function setDOB(){

        $('#birthdate').val($('#yy').val()+'-'+$('#mm').val()+'-'+$('#dd').val());
    }
	function close_popup(name){

		$("#form_"+name).removeClass('edit-arrow');
        $("#popup-"+name).removeClass('candidate-popover col-sm-7 col-lg-7 show');
        $("#popup-"+name).addClass('candidate-popover col-sm-7 col-lg-7 hide');
    }
</script>




</body>
</html>
