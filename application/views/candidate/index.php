

<?php 
//----------configuration---------------
$candidate_details = $candidate[0];
if(array_key_exists(0,$objective)){
    $candidate_objective_details = $objective[0];

}else{
    $candidate_objective_details = array();
}
$candidate_details = $candidate[0];
$fullname = $candidate_details->title.' '.$candidate_details->first_name.' '.$candidate_details->last_name;
$fulladdress = $candidate_details->address.', '.$candidate_details->city.', '.$candidate_details->province.', '.$candidate_details->country.', '.$candidate_details->zip_code;
$dateBOD = new DateTime($candidate_details->birthdate);
$DOB = $dateBOD->format('M d, Y');
?>




        <?php if($roll != 'public'): ?>
             <div class="col-xs-12 col-sm-9" id="job_contain">

               <?php else: ?>
                   <div class='col-xs-10 col-sm-10 col-md-10 col-lg-10 col-lg-offset-1' id="job_contain">
              <?php endif; ?>
        <div class='col-xs-12 col-sm-12 col-md-12  col-lg-12 visible-xs' >

        </div>
              <div class="container">
              <div class="row">
        <div class='panel panel-default'>
            <div class='panel-heading'>
                <b>Personal Information</b>
                <?php if($candidate_details->is_confirmed == 'N'): ?>
                    <span style="color:red;">(Un-verified)</span>
                <?php endif; ?>

                <?php if($roll != 'public'): ?>


                    <a data-toggle="modal" style="margin-top:-7px;" id="save-btn" href="<?php echo base_url()?>candidate/edit_profile" class="btn btn-primary pull-right hidden-xs"><i class='icon-pencil'></i> Edit Profile</a>
                    <a data-toggle="modal" style="margin-top:-2px;" id="save-btn" href="<?php echo base_url()?>candidate/edit_profile" class="pull-right vissble-xs hidden-lg hidden-sm hidden-md"><i class='icon-pencil'></i></a>
                <?php endif; ?>

                <?php if($roll != 'public'): ?>
                    <?php if(!empty($resume)){
                        $resume_details = $resume[0];
                        $resume_det=$resume_details->resume_id;
                        ?>
                        <a class=" pull-right hidden-xs hidden-sm hidden-md" style="color:#fff;margin-right: 10px;" href="<?php echo base_url();?>file/<?php  echo $candidate_details->candidate_id;?>/<?php echo $resume_details->resume_filename?>" download="<?php echo $resume_details->resume_filename ?>"><i class='icon-file'></i> Download Your UPLOADED RESUME Here</a>
                        <a class=" pull-right visible-xs visible-sm visible-md" style="color:#fff;margin-right: 10px;" href="<?php echo base_url();?>file/<?php  echo $candidate_details->candidate_id;?>/<?php echo $resume_details->resume_filename?>" download="<?php echo $resume_details->resume_filename ?>"><i class='icon-download'></i></a>

                    <?php
                    }else{$resume_det = '0';
                        ?>
                        <a  class=" pull-right hide" style="color:#fff;margin-right: 10px;" id='resume_doc' class='btn btn-primary' ><i class='icon-file'></i> Download Your UPLOADED RESUME Here</a>

                    <?php
                    }
                    ?>
                <?php endif; ?>
            </div>
            <div class='panel-body'>
                <div class="col-xs-12 col-sm-4 col-md-4  col-lg-3">
                    <center>
                        <?php if($candidate_details->avatar !=''){
                            ?>
                            <img src="<?php echo base_url()?>assets/images/job_seeker/<?php echo $candidate_details->avatar;?>"  class="img-rounded" style="height: 180px;width: 180px;"   id='img_upload' >
                        <?php
                        }else{
                            ?>
                            <img src="http://placehold.it/180x180" alt="ALT NAME" id='img_upload' >
                        <?php
                        }?>
                    </center>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8   col-lg-9">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12   col-lg-12">
                            <h2 class="noMarginPadding"><?php if($candidate_details->title !='' && $candidate_details->first_name !='' && $candidate_details->last_name !=''){echo $fullname; }else{echo "Mr Juan dela Cruz";}?></h2>
                            <small><?php if($fulladdress !=', , , , ') {echo $fulladdress;}else{echo "Address, City, Province, Country, Zip Code";};?>  <i class="icon-map-marker"></i></small>

                        </div>
                        <div class="col-sm-12" style="height: 20px;">
                        </div>
                        <div class="col-sm-12">
                            <i class="icon-bookmark"></i> <?php if($candidate_details->category_name !=''){echo  $candidate_details->category_name; }else{echo "Select Employment Category";}?> <br>
                            <i class="icon-envelope"></i> <?php echo $candidate_details->email; ?> <br>
                            <i class="icon-gift"></i> <?php if($candidate_details->birthdate != NULL){echo $DOB;}else{echo "Date Of Birth";} ?><br>
                            <i class="icon-phone"></i> <?php if($candidate_details->telephone !=''){echo $candidate_details->telephone;}else{echo "888-8888";}?><br>
                            <i class="icon-mobile-phone"></i> <?php if($candidate_details->mobile !=''){echo $candidate_details->mobile;}else{echo "+63 9xx xxx xxxx";}?>

                        </div>
                        <div class="span12" style="height: 20px;">
                        </div>

                    </div>
                </div>


            </div>
            <?php if($roll != 'public'): ?>
                <div class='panel-footer'>
                    <small><i class="icon-globe"></i> <a href='<?php echo base_url()?>candidate/public_profile/<?php echo $candidate_details->first_name." ".$candidate_details->last_name;?>/<?php echo $candidate_details->candidate_id;?>' class="act-primary" target="_blank"><?php echo base_url()?>candidate/public_profile/<?php echo $candidate_details->first_name." ".$candidate_details->last_name;?>/<?php echo $candidate_details->candidate_id;?></a></small>
                </div>
            <?php endif; ?>
        </div>

        <div class='panel panel-default'>
            <div class='panel-heading'>
                <b>Summary</b>
            </div>
            <div class='panel-body'>

                <?php if(!empty($candidate_objective_details)): ?>
                    <?php echo  $candidate_objective_details->objective; ?>
                <?php endif; ?>

            </div>
        </div>
        <div class='panel panel-default'>
            <div class='panel-heading'>
                <b>Experience</b>
            </div>
            <div class='panel-body'>

                <?php if(!empty($experience)){
                    foreach($experience as $row){
                        ?>
                        <div>
                            <div class='col-lg-12'>
                                <h2><?=$row->company_name?></h2>
                            </div>

                            <div class='col-lg-12' style="margin-top:-10px!important;">
                                <small> <?=$row->company_address?>
                                    <i class="icon-map-marker"></i></small>
                            </div>
                            <hr style="border:none!important;">
                            <div class='col-lg-12'>
                                <h4> <?=$row->position?><small>(<?php
                                        $from = date("M d,Y", strtotime($row->date_from));
                                        if($row->date_to == '0000-00-00'){
                                            $to = "Present";
                                        }else{
                                            $to = date("M d,Y", strtotime($row->date_to));
                                        }
                                        $datesattend = $from ." - ".$to;
                                        if(!empty($from) && !empty($to)){
                                            echo $datesattend;
                                        }
                                        ?>)</small>
                                </h4>
                                <?=$row->role?>
                            </div>
                            <hr>
                        </div>


                    <?php
                    }}?>

            </div>
        </div>


        <div class='panel panel-default'>
            <div class='panel-heading'>
                <b>Skills</b>
            </div>
            <div class='panel-body'>
                <table class='table table-condensed table-hover'>
                    <thead>
                    <th style="text-align: center; ">Skills</th>
                    <th style="text-align: center; " >Years</th>
                    <th style="text-align: center; ">Proficiency</th>

                    </thead>
                    <tbody>
                    <?php if($skills !=''){ ?>
                        <?php foreach($skills as $row) { ?>

                            <tr id='trbody'>
                                <td style="font-size: 12px;text-align: center"><?=$row->name?></td>
                                <td style="font-size: 12px;text-align: center"><?=$row->year?> </td>
                                <td style="font-size: 12px;text-align: center"><?=$row->proficiency?> </td>

                            </tr>

                        <?php

                        } ?>
                    <?php } ?>
                    </tbody>
                </table>


            </div>
        </div>


        <div class='panel panel-default'>
            <div class='panel-heading'>
                <b>Education</b>
            </div>
            <div class='panel-body'>
                <table class='table table-condensed table-hover'>
                    <thead>
                    <th style="text-align: center; ">School</th>
                    <th style="text-align: center; " class="hidden-xs hidden-sm" >Address</th>
                    <th style="text-align: center; ">Degree</th>
                    <th style="text-align: center; "  class="hidden-xs hidden-sm">Education Attainment</th>
                    <th style="text-align: center"  class="hidden-xs hidden-sm">Year Attainment</th>
                    </thead>
                    <tbody>
                    <?php if($education !=''){ ?>
                        <?php foreach($education as $row) { ?>

                            <tr id='trbody'>
                                <td style="font-size: 12px;text-align: center"><?=$row->school?></td>
                                <td style="font-size: 12px;text-align: center" class="hidden-xs hidden-sm"><?=$row->address?> </td>
                                <td style="font-size: 12px;text-align: center"><?=$row->degree?> </td>
                                <td style="font-size: 12px;text-align: center" class="hidden-xs hidden-sm"><?=$row->education_attainment?> </td>
                                <td style="font-size: 12px;text-align: center" class="hidden-xs hidden-sm">
                                    <?php
                                    $from = date("M d,Y", strtotime($row->date_from));
                                    $to = date("M d,Y", strtotime($row->date_to));
                                    $datesattend = $from ." - ".$to;
                                    if(!empty($from) && !empty($to)){
                                        echo $datesattend;
                                    }
                                    ?></td>

                            </tr>

                        <?php

                        } ?>
                    <?php } ?>
                    </tbody>
                </table>


            </div>

        </div>
        </div>
        </div>

               <?php if($roll != 'public'): ?>
                   </div><!--/span-->
                  <?php else: ?>
                      </div><!--/span-->
                      <?php endif; ?>

