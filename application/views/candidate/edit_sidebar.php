<?php $candidate_details = $candidate[0];?>
<?php if(!empty($resume)){
    $resume_details = $resume[0];
    $resume_det=$resume_details->resume_id;
}else{$resume_det = '0';} ?>
<div class="col-sm-3 col-lg-3">
    <div class="col-sm-12 col-lg-12 noMarginPadding">
        <form action="#" method="post" class='form-horizontal'>
            <div class='form-group'>
                <div class='col-lg-12'>
                    <input type="hidden" name='candidate_id' id='up_candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                    <input type="hidden" name='resume_id' value="<?php echo $resume_det;?>">
                    <input id="resume" name="resume" type="file" class='form-control'/>
                </div>
            </div>
        </form>
    </div>
    <div class='clearfix'></div>
    <div class="span12" style="height: 20px;"></div>
    <div class="panel panel-default">
        <div class="panel-heading">
            My Application
        </div>
        <div class="panel-body">
        <?php if(!empty($get_application)):?>
            <div class="list-group" style="overflow-y: scroll; height: 200px;  ">
                <?php foreach($get_application as $row):?>
                    <a class="list-group-item " href="<?php echo base_url()?>patsada/jobs/<?php echo $row->job_post_id;?>" style="color:#282828;font-weight: bold!important;text-decoration: none!important;">
                        <?php echo $row->position?>
                        <p class="list-group-item-text">
                            <small><?php echo $row->company_name?></small>

                            <small><?php echo $row->company_address?></small>

                        </p>
                    </a>
                <?php endforeach;?>
            </div>
        <?php endif;?>
      </div>
    </div>

        <div class="list-group">
            <a href="#change_pass" data-toggle="modal" class="list-group-item"><i class="icon-cog main-color"></i> Accounts Settings</a>

        </div>

    <address>
        <strong>Here some tips for getting a job here in patsada.com </strong><br>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam euismod eu neque id semper. Curabitur luctus felis eget turpis gravida, a pulvinar arcu bibendum. Aenean porta scelerisque molestie. Vestibulum vel pretium turpis. In leo diam, facilisis et nunc ac, congue pharetra augue. Nam quis gravida arcu. Cras ut congue lacus. Phasellus aliquet non tortor at lacinia. Sed euismod arcu ipsum, id semper diam placerat id. Nunc adipiscing turpis at odio varius malesuada.<br>


    </address>
</div>
</div>
</div>


<hr style="border:none!important;">



<div class="modal fade" id="change_pass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content "   >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">

                <form action="<?php echo base_url()?>ajax/accounts_settings" method="post" id="account_settings" class='form-horizontal'>
                    <div id="alerts_message"></div>
                    <input type="hidden" name='tbl_name'  value="login_credentials">
                    <input type="hidden" name='login_credentials_id'  value="1">
                    <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                    <div class='form-group'>
                        <label class="control-label col-lg-3">New Password</label>
                        <div class='col-lg-9'>
                            <input id="password_1" class='form-control' name="password" type="password" />
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class="control-label col-lg-3">Confirm Password</label>
                        <div class='col-lg-9'>
                            <input id="password_2" class='form-control' name="password_2" type="password" />
                        </div>
                    </div>
                    <div class='form-group'>
                        <label class="control-label col-lg-3"> </label>
                        <div class='col-lg-9'>
                            <button type="submit" class='btn btn-success btn-lg'>Save</button>

                        </div>
                    </div>
                </form>

            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script src="<?php echo base_url()?>assets/js/jquery.form.js"></script>
<script>
    $(document).ready(function(){
        $("#account_settings").ajaxForm({ // initialize the plugin
            // any other options,
            beforeSubmit: function () {
                return $("#account_settings").valid(); // TRUE when form is valid, FALSE will cancel submit
            },
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                $('#alerts_message').html('<p class="text-success text-center">'+obj.data_results +'</p>').fadeOut(5000);
                $('#account_settings').children('input[type=password]').val();
            },
            target:"#alerts_message",
            clearForm: true,
            resetForm: true

        });
    });
</script>