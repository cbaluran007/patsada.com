
<link href="<?php echo base_url();?>assets/css/datepicker.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/js/uploadify_31/uploadify.css" rel="stylesheet">
<?php
//----------configuration---------------
$candidate_details = $candidate[0];
if(array_key_exists(0,$objective)){
    $candidate_objective_details = $objective[0];

}else{
    $candidate_objective_details = array();
}

$fullname = $candidate_details->title.' '.$candidate_details->first_name.' '.$candidate_details->last_name;
$fulladdress = $candidate_details->address.', '.$candidate_details->city.', '.$candidate_details->province.', '.$candidate_details->country.', '.$candidate_details->zip_code;
$dateBOD = new DateTime($candidate_details->birthdate);
$DOB = $dateBOD->format('M d, Y');
if($candidate_details->birthdate != NULL){
    $bdate = explode("-", $candidate_details->birthdate);
    $yyyy = $bdate[0];
    $mm   = $bdate[1];
    $dd   = $bdate[2];
}else{
    $yyyy = "0000";
    $mm   = "00";
    $dd   = "00";

}
?>

<div class="col-sm-12 col-lg-12 " id="job_contain">
    <div class="row ">
       <div class='col-xs-12 col-sm-9 col-lg-9'>
                <div class='panel panel-default'>
					<div class='panel-heading'>
                        <b>Personal Information</b>
						<?php if($candidate_details->is_confirmed == 'N'): ?>
						<span style="color:red;">(Un-verified)</span>
						<?php endif; ?>
                        <a data-toggle="modal" style="margin-top:-7px;" id="save-btn" href="<?php echo base_url()?>candidate/" class="btn btn-success pull-right hidden-xs">Done Edit Profile</a>
                        <a data-toggle="modal" style="margin-top:-7px;" id="save-btn" href="<?php echo base_url()?>candidate/" class="btn btn-success pull-right visible-xs"><i class="icon-check"></i></a>

                        <?php if(!empty($resume)){
                            $resume_details = $resume[0];
                            $resume_det=$resume_details->resume_id;
                            ?>
                            <a class="hidden-xs pull-right" style="color:#fff;margin-right: 10px;" href="<?php echo base_url();?>file/<?php  echo $candidate_details->candidate_id;?>/<?php echo $resume_details->resume_filename?>" download="<?php echo $resume_details->resume_filename ?>"><i class='icon-file'></i> Download Your UPLOADED RESUME Here</a>
                            <a class="visible-xs pull-right" style="color:#fff;margin-right: 10px;" href="<?php echo base_url();?>file/<?php  echo $candidate_details->candidate_id;?>/<?php echo $resume_details->resume_filename?>" download="<?php echo $resume_details->resume_filename ?>"><i class='icon-download'></i></a>

                        <?php
                        }else{$resume_det = '0';
                            ?>
                            <a  class=" pull-right hide" style="color:#fff;margin-right: 10px;" id='resume_doc' class='btn btn-primary' ><i class='icon-file'></i> Download Your UPLOADED RESUME Here</a>

                        <?php
                        }
                        ?>

						
                    </div>
					 <div class='panel-body'>
						<div class="col-sm-3 col-lg-3" style="padding:0!important;list-style: none!important;">
                                <ul class="unstyled" id="hover-img" style='list-style: none!important;'>
                                    <li class="col-sm-12" style='text-decoration:none!important; padding:0!important;'>
                                        <a href="#uploads" data-toggle="modal" id="hover-img">
                                        <div class="thumbnail">
                                            <div class="caption">
                                                <br/>
                                                <br/>
                                             <p class='text-center btn-lg'> <i class='icon-camera' ></i></p>
                                            </div>
                                            <?php if($candidate_details->avatar !=''){
                                                ?>
                                                <img src="<?php echo base_url()?>assets/images/job_seeker/<?php echo $candidate_details->avatar;?>" alt="ALT NAME" id='img_upload' style="height: 180px;width: 180px;" >
                                            <?php
                                            }else{
                                                ?>
                                                <img src="http://placehold.it/180x180" alt="ALT NAME" id='img_upload' >
                                            <?php
                                            }?>

                                        </div>
                                        </a>
                                    </li>
                                 </ul>
                        </div>
						<div class="col-sm-8 col-lg-9">
							<div class="row" style="margin-left:-10px!important;">
							  <div class="col-sm-12">
                                  <div class="col-sm-12">
                                      <h3 class="noMarginPadding">
                                          <a class='popups '  id='form_fullname' href='#'  > <i class='icon-edit blue'></i> </a>
                                          <div id="popup-fullname" class='candidate-popover-big col-sm-7 col-lg-7 hide' style="z-index: 12;background: #fff;position: absolute;left:62px!important;">
                                              <div class='edit-arrow2' style="height:60px;"></div>
                                              <div style="padding: 5px;">
                                              <form class='form-horizontal' action="<?php echo base_url()?>candidate/process_candidate_profile" METHOD="POST" id='form-full-name'>

                                                  <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                                                  <div class='form-group'>
                                                      <label class='col-lg-3 control-group' style="font-size: 12px!important;">Title</label>
                                                      <div class='col-lg-4'>
                                                          <select class='form-control' name='title' id='title'>
                                                              <option <?php if($candidate_details->title == 'Mr.'){ echo 'selected';}?>>Mr.</option>
                                                              <option <?php if($candidate_details->title == 'Ms.'){ echo 'selected';}?>>Ms.</option>
                                                              <option <?php if($candidate_details->title == 'Mrs.'){ echo 'selected';}?>>Mrs.</option>
                                                              <option <?php if($candidate_details->title == 'Dr.'){ echo 'selected';}?>>Dr.</option>
                                                          </select>
                                                      </div>
                                                  </div>
                                                  <div class='form-group'>
                                                      <label class='col-lg-3 control-group' style="font-size: 12px!important;">First Name</label>
                                                      <div class='col-lg-9'>
                                                          <input name='first_name' id='first_name' class='form-control' value="<?php echo $candidate_details->first_name?>">
                                                      </div>
                                                  </div>
                                                  <div class='form-group'>
                                                      <label class='col-lg-3 control-group' style="font-size: 12px!important;">Last Name</label>
                                                      <div class='col-lg-9'>
                                                          <input name='last_name' id='last_name' class='form-control' value="<?php echo $candidate_details->last_name?>">
                                                      </div>
                                                  </div>
                                                  <div class='form-group'>
                                                      <label class='col-lg-3 control-group' style="font-size: 12px!important;"></label>
                                                      <div class='col-lg-9'>
                                                          <button type="submit" class='btn btn-success'>Save</button>
                                                           <a   class='btn btn-default' id='cancel' onClick="close_popup('fullname');">cancel</a>
                                                      </div>
                                                  </div>
                                              </form>
                                             </div>
                                          </div>

                                          <?php if($candidate_details->title !='' && $candidate_details->first_name !='' && $candidate_details->last_name !=''){echo $fullname; }else{echo "Mr Juan dela Cruz";}?>
                                      </h3>
                                  </div>
                                  <div class='col-sm-12'>
                                  <a class='popups '  id='form_address' href='#'  > <i class='icon-edit blue'></i> </a>
                                  <div id="popup-address" class='candidate-popover col-sm-7 col-lg-7 hide' style="z-index: 12;background: #fff;position: absolute;top:2px!important">
                                      <div class='edit-arrow2' style=" height: 16px!important;"></div>
                                      <div style="padding: 5px;">
                                      <form class='form-horizontal' action="<?php echo base_url()?>candidate/process_candidate_profile"  METHOD="POST" id='form-full-name'>

                                          <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                                          <div class='form-group'>
                                              <label class='col-lg-3 control-group' style="font-size: 12px!important;">Address</label>
                                              <div class='col-lg-9'>
                                                  <input type="text" name='address' id='address' class='form-control' value="<?php echo $candidate_details->address?>">
                                              </div>
                                          </div>
                                          <div class='form-group'>
                                              <label class='col-lg-3 control-group' style="font-size: 12px!important;">City</label>
                                              <div class='col-lg-9'>
                                                  <input name='city' id='city' class='form-control' value="<?php if($candidate_details->city!=''){echo $candidate_details->city;}else{echo "Cagayan de Oro City";}?>">
                                              </div>
                                          </div>
                                          <div class='form-group'>
                                              <label class='col-lg-3 control-group' style="font-size: 12px!important;">Province</label>
                                              <div class='col-lg-9'>
                                                  <input name='province' id='province' class='form-control' value="<?php if($candidate_details->province !=''){echo $candidate_details->province;}else{echo "Misamis Oriental";}?>">
                                              </div>
                                          </div>
                                          <div class='form-group'>
                                              <label class='col-lg-3 control-group' style="font-size: 12px!important;">Country</label>
                                              <div class='col-lg-9'>
                                                  <select name='country' id='country' class="form-control">
                                                      <?php if($candidate_details->country !=''){$selected='selected';}else{$selected='';}
                                                            foreach($countries as $rows){
                                                          ?>

                                                          <option><?=$rows?></option>
                                                      <?php
                                                      }?>
                                                  </select>
                                              </div>
                                          </div>
                                          <div class='form-group'>
                                              <label class='col-lg-3 control-group' style="font-size: 12px!important;">Zip Code</label>
                                              <div class='col-lg-9'>
                                                  <input name='zip_code' id='zip_code' class='form-control' value="<?php if($candidate_details->zip_code !=''){echo $candidate_details->zip_code;}else{echo "9000";}?>">
                                              </div>
                                          </div>
                                          <div class='form-group'>
                                              <label class='col-lg-3 control-group' style="font-size: 12px!important;"></label>
                                              <div class='col-lg-9'>
                                                  <button type="submit" class='btn btn-primary'>Save</button>
                                                  <a   class='btn btn-default' id='cancel' onClick="close_popup('address');">cancel</a>
                                              </div>
                                          </div>
                                      </form>
                                  </div>
                                  </div>
                                  <small> <?php if($fulladdress !=', , , , ') {echo $fulladdress;}else{echo "Address, City, Province, Country, Zip Code";};?>
                                      <i class="icon-map-marker"></i></small>
							   </div>
							   </div>
							  <hr style="border:none!important;">
							  <div class="col-sm-12">
                                  <div class='col-sm-12'>
                                      <a class='popups '  id='form_category' href='#'  > <i class='icon-edit blue'></i> </a>
                                      <div id="popup-category" class='candidate-popover col-sm-7 col-lg-7 hide ' style="z-index: 12;background: #fff;position: absolute;top:2px!important">
                                          <div class='edit-arrow2' style=" height: 16px!important;"></div>
                                          <div style="padding: 5px;">
                                              <form class='form-horizontal' action="<?php echo base_url()?>candidate/process_candidate_profile" METHOD="POST" id='form-full-name'>

                                                  <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                                                  <div class='form-group'>
                                                      <label class='col-lg-3 control-group' style="font-size: 12px!important;">Employment Category</label>
                                                      <div class='col-lg-9'>
                                                                <select name='category_name' id="category_name" class='form-control'>
                                                                       <?php if($catResults!=''){

                                                                            foreach($catResults as $row){

                                                                                    ?>
                                                                                    <option <?php if($candidate_details->category_name == $row[1]){echo "selected";};?>><?=$row[1]?></option>
                                                                                   <?php

                                                                            }
                                                                    }?>
                                                                </select>
                                                      </div>
                                                  </div>

                                                  <div class='form-group'>
                                                      <label class='col-lg-3 control-group' style="font-size: 12px!important;"></label>
                                                      <div class='col-lg-9'>
                                                          <button type="submit" class='btn btn-success'>Save</button>
                                                          <a   class='btn btn-default' id='cancel' onClick="close_popup('category');">cancel</a>
                                                      </div>
                                                  </div>
                                              </form>

                                          </div>
                                      </div>
                                      <i class="icon-bookmark"></i> <?php if($candidate_details->category_name !=''){echo  $candidate_details->category_name; }else{echo "Select Employment Category";}?>
                                  </div>

                                  <div class='col-sm-12'>
                                      <a class='popups '  id='form_email' href='#'  > <i class='icon-edit blue'></i> </a>
                                      <div id="popup-email" class='candidate-popover col-sm-7 col-lg-7 hide ' style="z-index: 12;background: #fff;position: absolute;top:2px!important">
                                          <div class='edit-arrow2' style=" height: 16px!important;"></div>
                                          <div style="padding: 5px;">
                                          <form class='form-horizontal' action="<?php echo base_url()?>candidate/process_candidate_profile" METHOD="POST" id='form-full-name'>

                                              <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                                              <div class='form-group'>
                                                  <label class='col-lg-3 control-group' style="font-size: 12px!important;">Email</label>
                                                  <div class='col-lg-9'>
                                                      <input type="text" name='email' id='email' class='form-control' value="<?php echo $candidate_details->email?>">
                                                  </div>
                                              </div>

                                              <div class='form-group'>
                                                  <label class='col-lg-3 control-group' style="font-size: 12px!important;"></label>
                                                  <div class='col-lg-9'>
                                                      <button type="submit" class='btn btn-success'>Save</button>
                                                       <a   class='btn btn-default' id='cancel' onClick="close_popup('email');">cancel</a>
                                                  </div>
                                              </div>
                                          </form>

                                  </div>
                                 </div>
                                      <i class="icon-envelope"></i> <?php echo $candidate_details->email; ?>
                                </div>


                                      <div class='col-sm-12'>
                                          <a class='popups '  id='form_bod' href='#'  > <i class='icon-edit blue'></i> </a>
                                          <div id="popup-bod" class='candidate-popover col-sm-10 col-lg-10 hide ' style="z-index: 12;background: #fff;position: absolute;top:2px!important">
                                              <div class='edit-arrow2' style=" height: 16px!important;"></div>
                                              <div style="padding: 5px;">
                                                  <form class='form-horizontal' action="<?php echo base_url()?>candidate/process_candidate_profile" METHOD="POST" id='form-full-name'>

                                                      <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                                                      <div class='form-group'>
                                                          <label class='col-lg-2 control-group' style="font-size: 12px!important;">Birthdate</label>
                                                          <div class='col-lg-10'>

                                                              <div class='input-group'>
                                                                  <span class="input-group-addon">MM</span>
                                                                  <select name='mm' id='mm' class="form-control" onchange="setDOB()" >
                                                                      <option></option>
                                                                      <?php
                                                                      $Mmonth_array = array('Pleas Select Month',
                                                                      'January',
                                                                      'February',
                                                                      'March',
                                                                      'April',
                                                                      'May',
                                                                      'June',
                                                                      'July',
                                                                      'August',
                                                                      'September',
                                                                      'October',
                                                                      'November',
                                                                      'December'
                                                                      );                                                                      for($i=1; $i<13; $i++) {
                                                                          $selected = '';
                                                                          if ($mm == $i) $selected = ' selected="selected"';
                                                                          print('<option value="'.$i.'"'.$selected.'>'.$Mmonth_array[$i].'</option>'."\n");
                                                                      }
                                                                      ?>
                                                                  </select>
                                                                  <span class="input-group-addon">DD</span>
                                                                  <select name='dd' id='dd' class="form-control" onchange="setDOB()">
                                                                      <option></option>
                                                                      <?php
                                                                      for($i=1; $i<32; $i++) {
                                                                          $selected = '';
                                                                          if ($dd == $i) $selected = ' selected="selected"';
                                                                          print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                                                      }
                                                                      ?>
                                                                  </select>
                                                                  <span class="input-group-addon">Year</span>
                                                                  <select name="yy" id='yy' class='form-control ' onchange="setDOB()">
                                                                      <option></option>
                                                                      <?php
                                                                      for($i=date('Y'); $i>1965; $i--) {
                                                                          $selected = '';
                                                                          if ($yyyy == $i) $selected = ' selected="selected"';
                                                                          print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                                                      }
                                                                      ?>
                                                                  </select>
                                                                  <input type='hidden' name='birthdate' id='birthdate' value='<?php echo $candidate_details->birthdate;?>'>
                                                              </div>
                                                          </div>
                                                      </div>

                                                      <div class='form-group'>
                                                          <label class='col-lg-2 control-group' style="font-size: 12px!important;"></label>
                                                          <div class='col-lg-10'>
                                                              <button type="submit" class='btn btn-success'>Save</button>
                                                                <a   class='btn btn-default' id='cancel' onClick="close_popup('bod');">cancel</a>
                                                          </div>
                                                      </div>
                                                  </form>
                                              </div>
                                          </div>
                                          <i class="icon-gift"></i> <?php if($candidate_details->birthdate != NULL){echo $DOB;}else{echo "Date Of Birth";} ?>
                                      </div>

                                  <div class='col-sm-12'>
                                      <a class='popups '  id='form_telephone' href='#'  > <i class='icon-edit blue'></i> </a>
                                      <div id="popup-telephone" class='candidate-popover col-sm-7 col-lg-7 hide ' style="z-index: 12;background: #fff;position: absolute;top:2px!important">
                                          <div class='edit-arrow2' style=" height: 16px!important;"></div>
                                          <div style="padding: 5px;">
                                               <form class='form-horizontal' action="<?php echo base_url()?>candidate/process_candidate_profile" METHOD="POST" id='form-full-name'>

                                                      <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                                                  <div class='form-group'>
                                                      <label class='col-lg-3 control-group' style="font-size: 12px!important;">Telephone</label>
                                                      <div class='col-lg-9'>
                                                          <input type="text" name='telephone' id='telephone' class='form-control' value="<?php echo $candidate_details->telephone?>">
                                                      </div>
                                                  </div>

                                                  <div class='form-group'>
                                                      <label class='col-lg-3 control-group' style="font-size: 12px!important;"></label>
                                                      <div class='col-lg-9'>
                                                          <button type="submit" class='btn btn-success'>Save</button>
                                                          <a   class='btn btn-default' id='cancel' onClick="close_popup('telephone');">cancel</a>
                                                      </div>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                       <i class="icon-phone "></i> <?php if($candidate_details->telephone !=''){echo $candidate_details->telephone;}else{echo "888-8888";}?>
                                  </div>

                                  <div class='col-sm-12'>
                                      <a class='popups '  id='form_mobile' href='#'  > <i class='icon-edit blue'></i> </a>
                                      <div id="popup-mobile" class='candidate-popover col-sm-7 col-lg-7 hide ' style="z-index: 12;background: #fff;position: absolute;top:2px!important">
                                          <div class='edit-arrow2' style=" height: 16px!important;"></div>
                                          <div style="padding: 5px;">
                                               <form class='form-horizontal' action="<?php echo base_url()?>candidate/process_candidate_profile" METHOD="POST" id='form-full-name'>

                                                      <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                                                  <div class='form-group'>
                                                      <label class='col-lg-3 control-group' style="font-size: 12px!important;">Mobile</label>
                                                      <div class='col-lg-9'>
                                                          <input type="text" name='mobile' id='mobile' class='form-control' value="<?php echo $candidate_details->mobile?>">
                                                      </div>
                                                  </div>

                                                  <div class='form-group'>
                                                      <label class='col-lg-3 control-group' style="font-size: 12px!important;"></label>
                                                      <div class='col-lg-9'>
                                                          <button type="submit" class='btn btn-success'>Save</button>
                                                          <a   class='btn btn-default' id='cancel' onClick="close_popup('mobile');">cancel</a>
                                                      </div>
                                                  </div>
                                              </form>
                                          </div>
                                      </div>
                                      <i class="icon-mobile-phone"></i> <?php if($candidate_details->mobile !=''){echo $candidate_details->mobile;}else{echo "+63 9xx xxx xxxx";}?>
                                  </div>


								</div>    
								<div class="col-sm-12" style="height: 20px;">
							   </div>
								
							</div>
						</div>
                        
					</div>
                </div>

                <div class='panel panel-default'>
                    <div class='panel-heading'>
                        <b>Summary</b>
                    </div>
                    <div class='panel-body'>

                        <form class='form-horizontal' action="<?php echo base_url()?>candidate/process_candidate_obj_profile"  method="post" action='#'>

                            <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                            <?php if(empty($candidate_objective_details)): ?>
                                 <input type="hidden" name='candidate_objective_id' value="0">
                            <?php else: ?>
                                <input type="hidden" name='candidate_objective_id' value="<?php echo $candidate_objective_details->candidate_objective_id;?>">
                            <?php endif; ?>
                            <div class='form-group'>
                              <div class='col-lg-12'>
                              <textarea class='form-control'  tabindex="6" name='objective' id='objective' placeholder="Enter Description..."  >
                                  <?php if(!empty($candidate_objective_details)): ?>
                                  <?php echo  $candidate_objective_details->objective; ?>
                                  <?php endif; ?>

                              </textarea>
                            </div>
                            </div>
                            <div class='form-group'>
                                <div class='col-lg-12'>
                                    <button type="submit" class='btn btn-success'>Save</button>
                                    <button   class='btn btn-danger' id='cancel'>cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class='panel panel-default'>
                    <div class='panel-heading'>
                        <b>Experience</b>
                        <a href="#exp"   class='pull-right' data-toggle="modal" style="color:#fff;"><i class="icon-plus "></i> Add</a>
                    </div>
                    <div class='panel-body'>

                                    <?php if(!empty($experience)){
                                                foreach($experience as $row){
                                        ?>
                                                <div>
                                                    <div class='col-lg-12'>
                                                        <div class="btn-group" style="position: absolute;right: 0">

                                                            <form class='form-horizontal' action="<?php echo base_url()?>candidate/process_candidate_exp_profile" method="POST">
                                                                <button href='#exp<?=$row->candidate_experience_id?>' data-toggle="modal" class="btn btn-primary"><i class='icon-edit'></i></button>

                                                                <input type="hidden" name='candidate_experience_id' value="<?php echo $row->candidate_experience_id;?>">
                                                                <button type="submit" class="btn btn-danger" id='<?=$row->candidate_experience_id?>'><i class='icon-trash'></i></button>
                                                            </form>
                                                        </div>
                                                        <h2><?=$row->company_name?></h2>

                                                    </div>

                                                    <div class='col-lg-12' style="margin-top:-10px!important;">
                                                           <small> <?=$row->company_address?>
                                                            <i class="icon-map-marker"></i></small>
                                                    </div>
                                                        <hr style="border:none!important;">
                                                    <div class='col-lg-12'>
                                                        <h4> <?=$row->position?><small>(<?php
                                                        $from = date("M d,Y", strtotime($row->date_from));
														if($row->date_to == '0000-00-00'){
															$to = "Present";
                                                        }else{
														$to = date("M d,Y", strtotime($row->date_to));
														}
                                                        $datesattend = $from ." - ".$to;
                                                        if(!empty($from) && !empty($to)){
                                                            echo $datesattend;
                                                        }
                                                        ?>)</small>
                                                        </h4>
                                                        <?=$row->role?>
                                                    </div>
                                                     <hr>
                                                </div>


                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exp<?=$row->candidate_experience_id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title">Job Experience</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form class='form-horizontal' action="<?php echo base_url()?>candidate/process_candidate_exp_profile" method="POST">

                                                                        <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                                                                        <input type="hidden" name='candidate_experience_id' value="<?php echo $row->candidate_experience_id;?>">
                                                                        <div class='form-group'>
                                                                            <label class="control-label col-lg-3">Company Name</label>
                                                                            <div class='col-lg-9'>
                                                                                <input type="text" class="form-control" name="company_name" id='company_name'  value="<?php echo $row->company_name;?>" >
                                                                            </div>
                                                                        </div>

                                                                        <div class='form-group'>
                                                                            <label class="control-label col-lg-3">Company Address</label>
                                                                            <div class='col-lg-9'>
                                                                                <input type="text" class="form-control" name="company_address"  value="<?php echo $row->company_address;?>" id='company_address'>
                                                                            </div>
                                                                        </div>

                                                                        <div class='form-group'>
                                                                            <label class="control-label col-lg-3">Position</label>
                                                                            <div class='col-lg-9'>
                                                                                <input type="text" class="form-control" name="position" id='position' value="<?php echo $row->position;?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-lg-3 control-label login-class">Work Period</label>
                                                                            <div class="col-lg-9">
                                                                                <div class='input-group'>
                                                                                    <span class="input-group-addon">FROM</span>
                                                                                    <input type="text" class="from_date<?=$row->candidate_experience_id?> form-control" name='date_from' id='date_from<?=$row->candidate_experience_id?>' placeholder="From"  value="<?php echo $row->date_from;?>"   contenteditable="false">
                                                                                    <span class="input-group-addon">TO</span>
                                                                                    <input type="text" class="to_date<?=$row->candidate_experience_id?> form-control" name='date_to' id='date_to<?=$row->candidate_experience_id?>'  placeholder="To" value="<?php echo $row->date_to;?>" contenteditable="false">
																				
                                                                                </div>
																				 <div class="checkbox">
																					<label>
																					  <input type="checkbox" name="dd" id="toPresent<?=$row->candidate_experience_id?>" <?php if($row->date_to == '0000-00-00'){ echo "checked"; }?>> To Present
																					</label>
																				  </div>
                                                                            </div>
                                                                        </div>
																		
                                                                        <div class='form-group'>
                                                                            <label class="control-label col-lg-3">Job Role</label>
                                                                            <div class='col-lg-9'>
                                                                                <textarea class='form-control' name='role' id="role<?php echo $row->candidate_experience_id;?>"><?php echo $row->role;?></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <script>
                                                                            $(document).ready(function(){
                                                                            var editor<?php echo $row->candidate_experience_id;?> = CKEDITOR.replace( 'role<?php echo $row->candidate_experience_id;?>',
                                                                                {
                                                                                    uiColor: '#FFFFFF',
                                                                                    toolbar:
                                                                                        [
                                                                                            ['NumberedList', 'BulletedList'],
                                                                                            ['UIColor']
                                                                                        ]

                                                                                });
																				
																				 $('#toPresent<?php echo $row->candidate_experience_id;?>').change(function(){
																					var date='<?php echo date('Y-m-d'); ?>';
																					   
																					var c = this.checked ? '1' : '0';
																					if(c == 1){
																						$('#date_to<?php echo $row->candidate_experience_id;?>').val('0000-00-00');
																					}else{
																						$('#date_to<?php echo $row->candidate_experience_id;?>').val(date);
																					}
																				});
																				
																				
																				var ToEndDate='<?php echo date('Y-m-d'); ?>';
																				

																				var nowTemp = new Date();
																				var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
																				 
																				var checkin = $('.from_date<?php echo $row->candidate_experience_id;?>').datepicker({
																					format: 'yyyy-mm-dd',
																					viewMode: "months", 
																					minViewMode: "months",
																					endDate: ToEndDate,
																				  onRender: function(date) {
																					return date.valueOf() < now.valueOf() ? 'disabled' : '';
																				  }
																				}).on('changeDate', function(ev) {
																				  if (ev.date.valueOf() > checkout.date.valueOf()) {
																					var newDate = new Date(ev.date)
																					newDate.setDate(newDate.getDate() + 1);
																					checkout.setValue(newDate);
																				  }
																				  checkin.hide();
																				  $('.to_date<?php echo $row->candidate_experience_id;?>')[0].focus();
																				}).data('datepicker');
																				var checkout = $('.to_date<?php echo $row->candidate_experience_id;?>').datepicker({
																				format: 'yyyy-mm-dd',
																				endDate: ToEndDate,
																				viewMode: "months", 
																			minViewMode: "months",
																				  onRender: function(date) {
																					return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
																				  }
																				}).on('changeDate', function(ev) {
																				  checkout.hide();
																				}).data('datepicker');
                                                                             });
																			 
																			

																				


																			
																				
                                                                        </script>
                                                                        <div class='form-group'>
                                                                            <label class="control-label col-lg-3"> </label>
                                                                            <div class='col-lg-9'>
                                                                                <button type="submit" class="btn btn-primary">Save</button>
                                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>

                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div><!-- /.modal -->



                                    <?php
                                                }}?>


                    </div>
                </div>

                <div class='panel panel-default'>
                    <div class='panel-heading'>
                        <b>Skills</b>
                        <a href="#skills"   class='pull-right' data-toggle="modal" style="color:#fff;"><i class="icon-plus"></i> Add</a>
                    </div>
                    <div class='panel-body'>
                                <table class='table table-condensed table-hover'>
                                    <thead>
                                    <th style="text-align: center; ">Skills</th>
                                    <th style="text-align: center; " >Years</th>
                                    <th style="text-align: center; ">Proficiency</th>
                                    <th style="text-align: center"></th>
                                    </thead>
                                    <tbody>
                                    <?php if($skills !=''){ ?>
                                        <?php foreach($skills as $row) { ?>

                                            <tr id='trbody'>
                                                <td style="font-size: 12px;text-align: center"><?=$row->name?></td>
                                                <td style="font-size: 12px;text-align: center"><?=$row->year?> </td>
                                                <td style="font-size: 12px;text-align: center"><?=$row->proficiency?> </td>
                                                <td>

                                                    <form class='form-horizontal' action="<?php echo base_url()?>candidate/delete_candidate_skills_profile" method="POST">
                                                       <div class=' pull-right'>
                                                        <button  href='#skills<?php echo $row->skills_id;?>' data-toggle="modal" class="btn btn-primary"><i class='icon-edit'></i></button>

                                                        <input type="hidden" name='skills_id' value="<?php echo $row->skills_id;?>">
                                                        <button type="submit" class="btn btn-danger" id='<?=$row->skill_id?>'><i class='icon-trash'></i></button>
                                                       </div>
                                                    </form>

                                                </td>
                                            </tr>

                                        <?php

                                        } ?>
                                    <?php } ?>
                                    </tbody>
                                </table>


                        <?php if($skills !=''){ ?>
                        <?php foreach($skills as $row) { ?>


                        <div class="modal fade" id="skills<?php echo $row->skills_id;?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Skills</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form id="modal-form3" accept-charset="UTF-8" action="<?php echo base_url();?>candidate/process_candidate_skills_profile" data-remote="true" method="post" class="form-horizontal">

                                            <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                                            <input type="hidden" name='skills_id' value="<?php echo $row->skills_id;?>">
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label">Skills</label>
                                                <div class="col-lg-8">
                                                    <input type="text" name="name" id="name" class="form-control" value="<?=$row->name?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label">Year</label>
                                                <div class="col-lg-8">
                                                    <input type="text" name="year" id="year" class="form-control" placeholder="e.g 1 year" value="<?=$row->year?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label">Proficiency</label>
                                                <div class="col-lg-8">
                                                    <select name='proficiency' id='proficiency' class="form-control">
                                                        <option <?php if($row->proficiency == 'Beginner. '){ echo 'selected';}?>>Beginner</option>
                                                        <option <?php if($row->proficiency == 'Intermediate. '){ echo 'selected';}?>>Intermediate</option>
                                                        <option <?php if($row->proficiency == 'Advance.'){ echo 'selected';}?>>Advance</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label"></label>
                                                <div class="col-lg-8">
                                                    <button type="submit" class='btn btn-success'>Submit</button>
                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->


                            <?php

                            } ?>
                        <?php } ?>

                    </div>
                </div>

                <div class='panel panel-default'>
                    <div class='panel-heading'>
                        <b>Education</b>
                        <a href="#educ"   class='pull-right' data-toggle="modal" style="color:#fff;"><i class="icon-plus"></i> Add</a>
                    </div>
                    <div class='panel-body'>
                                <table class='table table-condensed table-hover'>
                                    <thead>
                                    <th style="text-align: center; ">School</th>
                                    <th style="text-align: center; " class="hidden-xs hidden-sm" >Address</th>
                                    <th style="text-align: center; ">Degree</th>
                                    <th style="text-align: center; "  class="hidden-xs hidden-sm">Education Attainment</th>
                                    <th style="text-align: center"  class="hidden-xs hidden-sm">Year Attainment</th>
                                    <th> </th>
                                    </thead>
                                    <tbody>
                                    <?php if($education !=''){ ?>
                                        <?php foreach($education as $row) { ?>

                                            <tr id='trbody'>
                                                <td style="font-size: 12px;text-align: center"><?=$row->school?></td>
                                                <td style="font-size: 12px;text-align: center" class="hidden-xs hidden-sm"><?=$row->address?> </td>
                                                <td style="font-size: 12px;text-align: center"><?=$row->degree?> </td>
                                                <td style="font-size: 12px;text-align: center" class="hidden-xs hidden-sm"><?=$row->education_attainment?> </td>
                                                <td style="font-size: 12px;text-align: center" class="hidden-xs hidden-sm">
                                                    <?php
                                                    $from = date("M d,Y", strtotime($row->date_from));
                                                    $to = date("M d,Y", strtotime($row->date_to));
                                                    $datesattend = $from ." - ".$to;
                                                    if(!empty($from) && !empty($to)){
                                                        echo $datesattend;
                                                    }
                                                    ?></td>
                                                <td>

                                                    <form class='form-horizontal' action="<?php echo base_url()?>candidate/delete_candidate_profile" method="POST">
                                                       <div class=' pull-right'>
                                                        <button  href='#educ<?php echo $row->candidate_education_id;?>' data-toggle="modal" class="btn btn-primary"><i class='icon-edit'></i></button>

                                                        <input type="hidden" name='candidate_education_id' value="<?php echo $row->candidate_education_id;?>">
                                                        <button type="submit" class="btn btn-danger" id='<?=$row->candidate_education_id?>'><i class='icon-trash'></i></button>
                                                       </div>
                                                    </form>

                                                </td>
                                            </tr>

                                        <?php

                                        } ?>
                                    <?php } ?>
                                    </tbody>
                                </table>


                        <?php if($education !=''){ ?>
                        <?php foreach($education as $row) { ?>


                        <div class="modal fade" id="educ<?php echo $row->candidate_education_id;?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Education</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form id="modal-form3" accept-charset="UTF-8" action="<?php echo base_url();?>candidate/process_candidate_education_profile" data-remote="true" method="post" class="form-horizontal">

                                            <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                                            <input type="hidden" name='candidate_education_id' value="<?php echo $row->candidate_education_id;?>">
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">School</label>
                                                <div class="col-lg-9">
                                                    <input type="text" name="school" id="school" class="form-control" value="<?=$row->school?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Address</label>
                                                <div class="col-lg-9">
                                                    <input type="text" name="address" id="address" class="form-control"   value="<?=$row->address?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Degree</label>
                                                <div class="col-lg-9">
                                                    <input type="text" name="degree" id="degree" class="form-control"  value="<?=$row->degree?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Education Attainment</label>
                                                <div class="col-lg-9">

                                                    <SELECT class='form-control' name='education_attainment' id='education_attainment'>
                                                        <option <?php if($row->education_attainment == 'Graduate'){ echo 'selected';}?>>Graduate</option>
                                                        <option <?php if($row->education_attainment == 'Under Graduate'){ echo 'selected';}?>>Under Graduate</option>
                                                        <option <?php if($row->education_attainment == 'High School Graduate'){ echo 'selected';}?>>High School Graduate</option>
                                                        <option <?php if($row->education_attainment == 'High School Level'){ echo 'selected';}?>>High School Level</option>
                                                        <option <?php if($row->education_attainment == 'Post Graduate'){ echo 'selected';}?>>Post Graduate</option>
                                                        <option <?php if($row->education_attainment == 'Masteral Units'){ echo 'selected';}?>>Masteral Units</option>
                                                      </SELECT>
                                                  </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label login-class">Work Period</label>
                                                <div class="col-lg-9">
                                                    <div class='input-group'>
                                                        <span class="input-group-addon">FROM</span>
                                                        <input type="text" class="from_datee<?php echo $row->candidate_education_id;?> form-control" name='date_from' id='date_from' placeholder="From" contenteditable="false" value="<?=$row->date_from?>">
                                                        <span class="input-group-addon">TO</span>
                                                        <input type="text" class="to_datee<?php echo $row->candidate_education_id;?> form-control" name='date_to' id='date_to'  placeholder="To" contenteditable="false" value="<?=$row->date_to?>">

                                                    </div>
													
                                                </div>
											
											
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-4 control-label"></label>
                                                <div class="col-lg-8">
                                                    <button type="submit" class='btn btn-success'>Submit</button>
                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->


																		<script>
                                                                            $(document).ready(function(){
                                                                            
																				
																				
																				var ToEndDate='<?php echo date('Y-m-d'); ?>';
																				

																				var nowTemp = new Date();
																				var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
																				 
																				var checkin = $('.from_datee<?php echo $row->candidate_education_id;?>').datepicker({
																					format: 'yyyy-mm-dd',
																					viewMode: "months", 
																					minViewMode: "months",
																					endDate: ToEndDate,
																				  onRender: function(date) {
																					return date.valueOf() < now.valueOf() ? 'disabled' : '';
																				  }
																				}).on('changeDate', function(ev) {
																				  if (ev.date.valueOf() > checkout.date.valueOf()) {
																					var newDate = new Date(ev.date)
																					newDate.setDate(newDate.getDate() + 1);
																					checkout.setValue(newDate);
																				  }
																				  checkin.hide();
																				  $('.to_datee<?php echo $row->candidate_education_id;?>')[0].focus();
																				}).data('datepicker');
																				var checkout = $('.to_datee<?php echo $row->candidate_education_id;?>').datepicker({
																				format: 'yyyy-mm-dd',
																				endDate: ToEndDate,
																				viewMode: "months", 
																			minViewMode: "months",
																				  onRender: function(date) {
																					return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
																				  }
																				}).on('changeDate', function(ev) {
																				  checkout.hide();
																				}).data('datepicker');
                                                                             });
																			 
																			

																				


																			
																				
                                                                        </script>
						
						
						
                            <?php

                            } ?>
                        <?php } ?>

                    </div>
                </div>
        </div>




       <!-- Modal -->
       <div class="modal fade" id="exp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
           <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                       <h4 class="modal-title">Job Experience</h4>
                   </div>
                   <div class="modal-body">
                    <form class='form-horizontal' action="<?php echo base_url()?>candidate/process_candidate_exp_profile" method="POST">

                        <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                        <input type="hidden" name='candidate_experience_id' value="0">

                            <div class='form-group'>
                                <label class="control-label col-lg-3">Company Name</label>
                                <div class='col-lg-9'>
                                    <input type="text" class="form-control" name="company_name" id='company_name'>
                                </div>
                            </div>
                            <div class='form-group'>
                                <label class="control-label col-lg-3">Company Address</label>
                                <div class='col-lg-9'>
                                    <input type="text" class="form-control" name="company_address" id='company_address'>
                                </div>
                            </div>
                        <div class='form-group'>
                            <label class="control-label col-lg-3">Position</label>
                            <div class='col-lg-9'>
                                <input type="text" class="form-control" name="position" id='position'>
                            </div>
                        </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label login-class">Work Period</label>
                                <div class="col-lg-9">
                                    <div class='input-group'>
                                        <span class="input-group-addon">FROM</span>
                                        <input type="text" class="from_date0 form-control" name='date_from' id='date_from0' placeholder="From" contenteditable="false">
                                        <span class="input-group-addon">TO</span>
                                        <input type="text" class="to_date0 form-control" name='date_to' id='date_to0'  placeholder="To" contenteditable="false">

                                    </div>
									<div class="checkbox">
																					<label>
																					  <input type="checkbox" name="dd" id="toPresent0" onClick="setPresent();"> To Present
																					</label>
																				  </div>
                                </div>
                            </div>
                            <div class='form-group'>
                                <label class="control-label col-lg-3">Job Role</label>
                                <div class='col-lg-9'>
                                  <textarea class='form-control' name='role' id="role"></textarea>
                                </div>
                            </div>
                                <div class='form-group'>
                                <label class="control-label col-lg-3"> </label>
                                <div class='col-lg-9'>
                                    <button type="submit" class="btn btn-success">Save</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                    </form>
                   </div>

               </div><!-- /.modal-content -->
           </div><!-- /.modal-dialog -->
       </div><!-- /.modal -->


       <div class="modal fade" id="skills">
           <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                       <h4 class="modal-title">Skills</h4>
                   </div>
                   <div class="modal-body">
                       <form id="modal-form3" accept-charset="UTF-8" action="<?php echo base_url();?>candidate/process_candidate_skills_profile" data-remote="true" method="post" class="form-horizontal">

                           <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                           <input type="hidden" name='skills_id' value="0">
                           <div class="form-group">
                               <label class="col-lg-4 control-label">Skills</label>
                               <div class="col-lg-8">
                                   <input type="text" name="name" id="skills" class="form-control">
                               </div>
                           </div>
                           <div class="form-group">
                               <label class="col-lg-4 control-label">Year</label>
                               <div class="col-lg-8">
                                   <input type="text" name="year" id="year" class="form-control" placeholder="e.g 1 year">
                               </div>
                           </div>
                           <div class="form-group">
                               <label class="col-lg-4 control-label">Proficiency</label>
                               <div class="col-lg-8">
                                   <select name='proficiency' id='proficiency' class="form-control">
                                       <option></option>
                                       <option value="Beginner">Beginner</option>
                                       <option value="Intermediate">Intermediate</option>
                                       <option value="Advance">Advance</option>
                                   </select>
                               </div>
                           </div>
                           <div class="form-group">
                               <label class="col-lg-4 control-label"></label>
                               <div class="col-lg-8">
                                   <button type="submit" class='btn btn-success'>Submit</button>
                                   <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                               </div>
                           </div>
                       </form>
                   </div>

               </div><!-- /.modal-content -->
           </div><!-- /.modal-dialog -->
       </div><!-- /.modal -->


       <div class="modal fade" id="educ">
           <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                       <h4 class="modal-title">Education</h4>
                   </div>
                   <div class="modal-body">
                       <form id="modal-form3" accept-charset="UTF-8" action="<?php echo base_url();?>candidate/process_candidate_education_profile" data-remote="true" method="post" class="form-horizontal">

                           <input type="hidden" name='candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                           <input type="hidden" name='candidate_education_id' value="0">
                           <div class="form-group">
                               <label class="col-lg-3 control-label">School</label>
                               <div class="col-lg-9">
                                   <input type="text" name="school" id="school" class="form-control" >
                               </div>
                           </div>
                           <div class="form-group">
                               <label class="col-lg-3 control-label">Address</label>
                               <div class="col-lg-9">
                                   <input type="text" name="address" id="address" class="form-control" placeholder="Addreess" >
                               </div>
                           </div>
                           <div class="form-group">
                               <label class="col-lg-3 control-label">Degree</label>
                               <div class="col-lg-9">
                                   <input type="text" name="degree" id="degree" class="form-control" placeholder="ex. BS Information Technology Or Masters In Management">
                               </div>
                           </div>
                           <div class="form-group">
                               <label class="col-lg-3 control-label">Education Attainment</label>
                               <div class="col-lg-9">
                                   <SELECT class='form-control' name='education_attainment' id='education_attainment'>
                                       <option>Graduate</option>
                                       <option>Under Graduate</option>
                                       <option>High School Graduate</option>
                                       <option>High School Level</option>
                                       <option>Post Graduate</option>
                                       <option>Masteral Units</option>
                                   </SELECT>
                               </div>
                           </div>
                           <div class="form-group">
                               <label class="col-lg-3 control-label login-class">Year Attainment</label>
                               <div class="col-lg-9">
                                   <div class='input-group'>
                                       <span class="input-group-addon">FROM</span>
                                       <input type="text" class="from_datee0 form-control" name='date_from' id='date_frome0' placeholder="From" contenteditable="false"  >
                                       <span class="input-group-addon">TO</span>
                                       <input type="text" class="to_datee0 form-control" name='date_to' id='date_toe0'  placeholder="To" contenteditable="false" >

                                   </div>
                               </div>
                           </div>
                           <div class="form-group">
                               <label class="col-lg-4 control-label"></label>
                               <div class="col-lg-8">
                                   <button type="submit" class='btn btn-success'>Submit</button>
                                   <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                               </div>
                           </div>
                       </form>
                   </div>

               </div><!-- /.modal-content -->
           </div><!-- /.modal-dialog -->
       </div><!-- /.modal -->

       <div class="modal fade" id="uploads">
           <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                       <h4 class="modal-title">Upload Image</h4>
                   </div>
                   <div class="modal-body">
                       <form action="#" method="post" class='form-horizontal'>
                            <div class='form-group'>
                                   <div class='col-lg-4'>
                                       <input type="hidden" name='candidate_id' id='up_candidate_id' value="<?php echo $candidate_details->candidate_id;?>">
                                       <input id="userfile" name="userfile" type="file" />
                                   </div>
                                  <label class='col-lg-8 control-label'>
                                      <span style="font-size: 11px; padding-left:15px;"><em> Please limit file size to 90kb. only JPG, GIF and PNG files are accepted.  Recommended size 200 x 200 px</em></span>
                                  </label>
                            </div>
                       </form>

                   </div>

               </div><!-- /.modal-content -->
           </div><!-- /.modal-dialog -->
       </div><!-- /.modal -->