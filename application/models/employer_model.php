<?php 

class Employer_model extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
		$this->load->library(array('session'));
		$this->load->database();
        parent::__construct();

    }
    
    function get_employer($id){

        $sql="SELECT * FROM employer where employer_id = $id;";

        $query = $this->db->query($sql);
		
		return $query->result();
        
    }
	function get_employer_user($id){

        $sql="SELECT * FROM employer_user where employer_user_id = $id;";

		$query = $this->db->query($sql);
		return $query->result();
        
    }
	function get_employer_sub_user($id){

        $sql="SELECT * FROM employer_user where employer_id = $id and is_manager ='N';";

		$query = $this->db->query($sql);
		return $query->result();
        
    }
	
	function get_validate_email_duplication($email){
        $sql="SELECT count(*) as count FROM employer_user where email = '$email' Limit 1;";
		$query = $this->db->query($sql);
		$res = $query->result();
		$status = false;
		if($res[0]->count > 0){
			$status = true;
		}
		return $status;
        
    }
	
	function get_validate_user_name_duplication($user_name){
        $sql="SELECT count(*) as count FROM login_credentials where user_name = '$user_name' and is_candidate = 'Y' Limit 1;";
		$query = $this->db->query($sql);
		$res = $query->result();
		$status = false;
		if($res[0]->count > 0){
			$status = true;
		}
		return $status;
        
    }
	
	function employer_get_validated($param){
		$code_param = json_decode(base64_decode($param));
		//get validate email
		$sql="SELECT count(*) as count FROM login_credentials where user_name = '".$code_param->user_name."' and `password` = '".$code_param->password."' and is_employer = 'Y'  Limit 1;";
		$query = $this->db->query($sql);
		$res = $query->result();
		$status = false;
		if($res[0]->count > 0){
			$sqlUpdateCandidate = "UPDATE employer_user set is_confirmed = 'Y' where email = '".$code_param->email."';";
			$this->db->query($sqlUpdateCandidate);
			$sqlUpdateCandidate = "UPDATE login_credentials set is_confirmed = 'Y' where user_name = '".$code_param->user_name."' and `password` = '".$code_param->password."' and is_employer = 'Y';";
			$this->db->query($sqlUpdateCandidate);
			
			$sql_login_credentials ="SELECT count(*) as count ,  is_candidate, is_employer,employer_user_id,candidate_id from login_credentials where user_name = '".$code_param->user_name."' and `password` = '".$code_param->password."' and is_active = 'Y' and is_confirmed='Y' and is_admin ='N';";
			$query = $this->db->query($sql_login_credentials);
			$res = $query->result();
			

			if($res[0]->count > 0){
				$resLogs = array($res[0]->is_candidate, $res[0]->is_employer ,$res[0]->employer_user_id,$res[0]->candidate_id);
				
				
					$status['data']=array('type'=>'employer','id'=>$resLogs[2]);
                     $secret_code = random_string('alnum',10);   
					$sess_array = array(
						'id' => $resLogs[2],
						'roll' => 'employer',
                        'code'=>$secret_code,
                        'to'=>$code_param->email,
                        'user'=>$code_param->user_name,
                        'company_code'=>$code_param->company_code,
					);


                    // Update a table wherein Authentication code is given
                   // $employerid = $resLogs[2]; 

				    /*$sqlUpdateEmployer = "UPDATE employer set authentication_code = '".$secret_code."' where employer_id = $employerid";
                    $this->db->query($sqlUpdateEmployer);*/
				   $empdata = array('tbl_name'=>'employer','employer_id'=>$code_param->employer_id,'authentication_code'=>$secret_code);
                   $this->tools_model->saveDatas($empdata,false);

			}
			
			
           
			$status = true;
		}
		
			
		
        
		return array($status, $sess_array);
        
    }


	function employer_user_activate_deactivate($user_employer_id,$type){
		
		
		$sqlUpdateEmployer = "UPDATE employer_user set is_active = '".$type."' where employer_user_id = ".$user_employer_id.";";
		$this->db->query($sqlUpdateEmployer);
		$sqlUpdateEmployer = "UPDATE login_credentials set is_active = '".$type."' where employer_user_id = ".$user_employer_id.";";
		$this->db->query($sqlUpdateEmployer);
	}


    function get_all_applicants($id){
        $sql="SELECT jp.*,
                jp_event.*,
                job_seeker.first_name,
                job_seeker.last_name,
                job_seeker.email,
                job_seeker.mobile,
                job_seeker.avatar,
                resume_fi.resume_filename,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN concat(job_seeker.first_name,' ',job_seeker.last_name)
                ELSE concat(jp_event.first_name,' ',jp_event.last_name)
                END  AS full_name,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN   job_seeker.email
                ELSE jp_event.email
                END  AS applicants_email,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN   job_seeker.mobile
                ELSE jp_event.contact_no
                END  AS applicants_contact_no,
               CASE
                WHEN jp_event.candidate_id <> 0 THEN    concat(jp_event.candidate_id,'/',resume_fi.resume_filename)
                ELSE  concat('applicants/',jp_event.resume_filename)
                END  AS applicants_resume_filename,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN   ''
                ELSE jp_event.resume_text
                END  AS applicants_resume_text,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN   job_seeker.avatar
                ELSE null
                END  AS applicants_avatar
              FROM job_post as jp   INNER JOIN job_post_event as jp_event on jp_event.job_post_id = jp.job_post_id
                                    LEFT JOIN candidate as job_seeker on job_seeker.candidate_id = jp_event.candidate_id
                                    LEFT JOIN resume as resume_fi on resume_fi.candidate_id = job_seeker.candidate_id

                    WHERE jp.employer_id  = $id  ORDER BY applied_datetime DESC;";

        $query = $this->db->query($sql);

        return $query->result();
    }


    function search_applicants($id,$searchterm){
        $filter ="";
        if($searchterm !=''){
            $filter =" and jp.position LIKE '%".$searchterm."%' or CASE
                WHEN jp_event.candidate_id <> 0 THEN concat(job_seeker.first_name,' ',job_seeker.last_name)
                ELSE concat(jp_event.first_name,' ',jp_event.last_name)
                END like '%$searchterm%'";
        }

        $sql="SELECT jp.position,
                jp.job_post_id,
                jp_event.job_post_event_id,
                jp_event.job_post_id,
                jp_event.first_name,
                jp_event.last_name,
                jp_event.email,
                jp_event.is_viewed,
                jp_event.is_accepted,
                jp_event.is_declined,
                jp_event.applied_datetime,
                job_seeker.first_name,
                job_seeker.last_name,
                job_seeker.email,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN concat(job_seeker.first_name,' ',job_seeker.last_name)
                ELSE concat(jp_event.first_name,' ',jp_event.last_name)
                END  AS full_name,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN   job_seeker.email
                ELSE jp_event.email
                END  AS applicants_email
              FROM job_post as jp   INNER JOIN job_post_event as jp_event on jp_event.job_post_id = jp.job_post_id
                                    LEFT JOIN candidate as job_seeker on job_seeker.candidate_id = jp_event.candidate_id

					WHERE jp.employer_id  = $id  $filter ORDER BY applied_datetime DESC;";



        $query=$this->db->query($sql);

        if($query -> num_rows() > 0)
        {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        else
        {
            return array();
        }
    }



    function accepted_applicants($id){

        $sql = "UPDATE  job_post_event SET is_accepted='Y',is_declined ='N'  WHERE job_post_event_id = ".$id."";
        $this->db->query($sql);

        $status = true;

        return $status;
    }

    function declined_applicants($id){

        $sql = "UPDATE  job_post_event SET is_accepted='N',is_declined ='Y'  WHERE job_post_event_id = ".$id."";
        $this->db->query($sql);

        $status = true;

        return $status;
    }
    function delete_applicants($post_data){
        $sql = "DELETE  FROM ".$post_data['tbl_name']."  WHERE ".$post_data['tbl_name'].'_id = '.$post_data[$post_data['tbl_name'].'_id'];

        $this->db->query($sql);
        $status = true;

        return $status;
    }
    function delete_selected_applicants($row){
        $sql = "DELETE  FROM job_post_event  WHERE job_post_event_id = ".$row."";

        $this->db->query($sql);
        $status = true;

        return $status;
    }

    function search_employer_posted_jobs($id,$searchterm){
        $filter ="";
        if($searchterm !=''){
            $filter =" and jp.position LIKE '%".$searchterm."%'";
        }

        $sql="SELECT
            jp.position,
            jp.job_post_id,
            jp.created_date_time,
              jp.expiration_datetime,
            jp.is_confirm,
            count(jp_event.job_post_id) as total
            FROM job_post as jp
            LEFT  OUTER JOIN job_post_event as jp_event on jp_event.job_post_id = jp.job_post_id
            WHERE jp.employer_id  ='$id'  $filter group by jp.job_post_id DESC;";

         

        $query=$this->db->query($sql);

        if($query -> num_rows() > 0)
        {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        else
        {
            return array();
        }
    }

    function get_total_applicants($id){


        $sql="SELECT  count( job_post_id) as total  FROM job_post_event   WHERE job_post_id  ='$id';";
        $query=$this->db->query($sql);

        if($query -> num_rows() > 0)
        {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        else
        {
            return array();
        }
    }

    function get_this_job_applicants($id){
        $sql="SELECT  jp_event.*,
                job_seeker.*,
                resume_fi.*,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN concat(job_seeker.first_name,' ',job_seeker.last_name)
                ELSE concat(jp_event.first_name,' ',jp_event.last_name)
                END  AS full_name,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN   job_seeker.email
                ELSE jp_event.email
                END  AS applicants_email,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN   job_seeker.mobile
                ELSE jp_event.contact_no
                END  AS applicants_contact_no,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN    concat(jp_event.candidate_id,'/',resume_fi.resume_filename)
                ELSE  concat('applicants/',jp_event.resume_filename)
                END  AS applicants_resume_filename,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN   ''
                ELSE jp_event.resume_text
                END  AS applicants_resume_text,
              CASE
                WHEN jp_event.candidate_id <> 0 THEN   job_seeker.avatar
                ELSE 'no'
                END  AS applicants_avatar
              FROM job_post_event as jp_event
                                    LEFT JOIN candidate as job_seeker on job_seeker.candidate_id = jp_event.candidate_id
                                    LEFT JOIN resume as resume_fi on resume_fi.candidate_id = job_seeker.candidate_id

					WHERE jp_event.job_post_id  = $id  ORDER BY applied_datetime DESC;";

        $query = $this->db->query($sql);

        return $query->result();
    }


}
