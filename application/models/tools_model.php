<?php

class Tools_model extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        $this->load->database();
        $this->load->library(array('PHP_Mailer'));
        parent::__construct();
    }

    function create_candidate_and_login_credentials($post_data){

        $this->db->trans_begin();

        $sql_candidate ="INSERT INTO candidate(
        email,
        is_active,
        is_confirmed
        )
         VALUES (
         '".$post_data['email']."',
         'Y',
         'N'
         );";



        $this->db->query($sql_candidate);
        $insert_id = $this->db->insert_id();

        $sql_login_credentials ="INSERT INTO login_credentials(
        is_candidate,
        candidate_id,
        user_name,
        password,
        created_datetime,
        is_admin
        )
         VALUES (
         'Y',
         ".$insert_id.",
         '".$post_data['user_name']."',
         '".md5($post_data['password'])."',
         NOW(),
         'N'
         );";
        $this->db->query($sql_login_credentials);




        $status = false;
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();

            $for_verification = array(
                'email' => $post_data['email'],
                'user_name' => $post_data['user_name'],
                'password' => md5($post_data['password'])
            );

            $verification_params = base64_encode(json_encode($for_verification));
            //$params,$type,$to,$user
            $this->send_verification_links($verification_params,'candidate', $post_data['email'], $post_data['user_name'] );

            $status = true;

        }
        if($status){
            return array(true,'Y', 'N',0,$insert_id);
        }
        return array(false,'', '','','' );

    }

    function create_employer_and_login_credentials($post_data){

        $this->db->trans_begin();
        $code = $this->gencompanycode($post_data['ecompany_name'],6);
        $sql_employer ="INSERT INTO employer(
        company_code,    
        company_name,
        is_active,
        is_confirmed
        )
         VALUES (
         '".$code."',   
         '".$post_data['ecompany_name']."',
         'Y',
         'N'
         );";



        $this->db->query($sql_employer);
        $insert_id = $this->db->insert_id();

        $sql_employer_user ="INSERT INTO employer_user(
        employer_id,
        created_datetime,
        email,
        is_manager
        )
         VALUES (
         ".$insert_id.",
         Now(),
         '".$post_data['eemail']."',
         'Y'
         );";



        $this->db->query($sql_employer_user);
        $insert_employer_user_id = $this->db->insert_id();


        $sql_login_credentials ="INSERT INTO login_credentials(
        is_employer,
        employer_user_id,
        user_name,
        password,
        password_r,
        created_datetime,
        is_admin
        )
         VALUES (
         'Y',
         ".$insert_employer_user_id.",
         '".$post_data['euser_name']."',
         '".md5($post_data['epassword'])."',
          '".base64_encode($post_data['epassword'])."',
         NOW(),
         'N'
         );";
        $this->db->query($sql_login_credentials);




        $status = false;
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            $for_verification = array(
                'email' => $post_data['eemail'],
                'user_name' => $post_data['euser_name'],
                'password' => md5($post_data['epassword']),
                'company_code' => $code,
                'employer_id' =>$insert_id

            );

            $verification_params = base64_encode(json_encode($for_verification));
            //$params,$type,$to,$user
            $this->send_verification_links($verification_params,'employer', $post_data['eemail'], $post_data['euser_name']);
            $status = true;

        }
        if($status){
            return array(true,'N', 'Y',$insert_employer_user_id,0);
        }
        return array(false,'', '','','' );
    }

private function gencompanycode($agencyName, $length = 6) { // to generate a company code
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    $result = preg_replace("/[^a-zA-Z0-9]+/", "", $agencyName); 
     $characters = $result;
    $randomString2 = '';
    for ($i = 0; $i < 3; $i++) {
        $randomString2 .= $characters[rand(0, strlen($characters) - 1)];
    }
    return  $randomString2.$randomString;




  }

    // function send_job_post_number($user,$job_id){

    //         $msqHeader = '<table align="center" cellspacing="0" border="0" cellpadding="0" width="700" bgcolor="#FFFFFF" style="width:700px;background-color:#fff;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
    //         <tbody>
    //         <tr>
    //         <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;">
    //         <a href="'.base_url().'" target="_blank" style="text-decoration:none;">
    //         <img src="'.base_url().'assets/img/patsada_logo.PNG" style="height:30px;width:150px;padding-top:3px;margin-left:20px;" alt="PATSADA.com"><br>
    //         <span style="color:#fff;margin-left: 21px;font-size:12px;">Jobs, Right at Your Finger Tips</span>
    //         </a>

    //         </td>
    //         </tr>';

    //                 $msqBody = '
    //         <tr>
    //             <td style="padding-top:34px;padding-left:39px;padding-right:39px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd">
    //                 <h2 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:30px;color:#262626;font-weight:normal;margin-top:0;margin-bottom:13px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;letter-spacing:0">
    //                 Hi '.$user.'!
    //                 </h2>

    //                 <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
    //                 Congratulations! Your job post has successfully been posted.<br>
    //                 This is your job number for future purposes: '.$job_id.';

    //                 </h3>
    //             </td>
    //         </tr>';

    //                 $msqFooter = '
    //         <tr>
    //             <td style="color:#797c80;font-size:12px;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;padding-top:23px;padding-left:39px;padding-right:13px;padding-bottom:23px;text-align:left">
    //                 <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
    //                 About PATSADA.com
    //                 </h3>
    //                 <p style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:13px;padding-right:0;padding-left:0;line-height:20px">
    //                 The word PaTsada is a local word from Cagayan de Oro City.<br>
    //                         Hence <b>PaTsada.com</b> is an online Job Search Site for kagay-anons. <br>
    //                         We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
    //                         <b>PATSADA.com</b>... Jobs, Right at Your Fingertips

    //                 </p>

    //             </td>
    //         </tr>

    //         <tr>
    //     </tr>
    //     <tr>
    //         <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;text-align: right;font-size: 10px;">
    //             &copy; Copyright 2013. PATSADA.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359
    //         </td>
    //         </tr>
    //     </tbody>
    //     </table>';



    //             $msgWhole = $msqHeader.$msqBody.$msqFooter;

    //             $subject = '[PaTSADA] Confirm Your Employer Account';
    //             //echo "../_documents/".$results[0]['filename'];
    //             $mail = new PHP_Mailer();

    //             $mail->AddReplyTo("noreply@patsada.com","PaTSADA");

    //             $mail->SetFrom("noreply@patsada.com","PaTSADA");
    //             $mail->AddAddress($to, $user);
    //             $mail->Subject   = $subject;
    //             $mail->MsgHTML($msgWhole);
    //             //$mail->AddAttachment("../_documents/".$results[0]['filename']);

    //             if($_SERVER['SERVER_NAME'] =='localhost'){
    //                 $file = 'mailHtml/mailHtml.html';

    //                 // Write the contents back to the file
    //                 file_put_contents($file, $msgWhole);

    //             }else{
    //                 if($mail->Send()){
    //                     $response = array ( "success" => true);
    //                 }else{
    //                     echo "Mailer Error: " . $mail->ErrorInfo;
    //                     $response = array ( "failure" => true);
    //                 }
    //             }



    //             break;
    //         }
    // }


    function send_verification_links($params,$type,$to,$user){ //VErification Email
 
        switch ($type) {
            case "candidate":{


                $msqHeader = '<table align="center" cellspacing="0" border="0" cellpadding="0" width="700" bgcolor="#FFFFFF" style="width:700px;background-color:#fff;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
        <tbody>
        <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;">
        <a href="'.base_url().'" target="_blank" style="text-decoration:none;">
        <img src="'.base_url().'assets/img/patsada_logo.PNG" style="height:30px;width:150px;padding-top:3px;margin-left:20px;" alt="PATSADA.com"><br>
        <span style="color:#fff;margin-left: 21px;font-size:12px;">Jobs, Right at Your Finger Tips</span>
        </a>

        </td>
        </tr>';

                $msqBody = '
        <tr>
            <td style="padding-top:34px;padding-left:39px;padding-right:39px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd">
                <h2 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:30px;color:#262626;font-weight:normal;margin-top:0;margin-bottom:13px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;letter-spacing:0">
                Hi '.$user.'!
                </h2>

                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                Thanks for signing up to PATSADA.com. Please verify your account by clicking the link below.
                </h3>
            </td>
        </tr>
        <tr>
            <td align="left" style="background-color:#dea803;font-size:14px;color:#1f1f1f;border-top-width:1px;border-top-style:solid;border-top-color:#dae3ea;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#dae3ea;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:20px;padding-bottom:20px;padding-right:39px;padding-left:39px;text-align:left">
                <a style="color:#fff;font-family:Arial;font-size:20px" href="'.base_url().'candidate/verify/'.$params.'" target="_blank">Verify my account and login</a></strong>
            </td>
        </tr>';

                $msqFooter = '
        <tr>
            <td style="color:#797c80;font-size:12px;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;padding-top:23px;padding-left:39px;padding-right:13px;padding-bottom:23px;text-align:left">
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                About PATSADA.com
                </h3>
                <p style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:13px;padding-right:0;padding-left:0;line-height:20px">
                The word PaTsada is a local word from Cagayan de Oro City.<br>
                        Hence <b>PATSADA.com</b> is an online Job Search Site for kagay-anons. <br>
                        We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                        <b>PATSADA.com</b>... Jobs, Right at Your Fingertips

                </p>

            </td>
        </tr>

        <tr>
    </tr>
    <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;text-align: right;font-size: 10px;">
            &copy; Copyright 2013. PATSADA.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359
        </td>
        </tr>
    </tbody>
    </table>';



                $msgWhole = $msqHeader.$msqBody.$msqFooter;

                $subject = '[PATSADA] Confirm Your Job Seeker Account';
                //echo "../_documents/".$results[0]['filename'];
                $mail = new PHP_Mailer();

                $mail->AddReplyTo("noreply@patsada.com","PATSADA");

                $mail->SetFrom("noreply@patsada.com","PATSADA");
                $mail->AddAddress($to, $user);
                $mail->Subject   = $subject;
                $mail->MsgHTML($msgWhole);
                //$mail->AddAttachment("../_documents/".$results[0]['filename']);

                if($_SERVER['SERVER_NAME'] =='localhost'){
                    $file = 'mailHtml/mailHtml.html';

                    // Write the contents back to the file
                    file_put_contents($file, $msgWhole);

                }else{
                    if($mail->Send()){
                        $response = array ( "success" => true);
                    }else{
                        echo "Mailer Error: " . $mail->ErrorInfo;
                        $response = array ( "failure" => true);
                    }
                }



                break;
            }

            case "employer":{

                $msqHeader = '<table align="center" cellspacing="0" border="0" cellpadding="0" width="700" bgcolor="#FFFFFF" style="width:700px;background-color:#fff;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
        <tbody>
        <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;">
        <a href="'.base_url().'" target="_blank" style="text-decoration:none;">
        <img src="'.base_url().'assets/img/patsada_logo.PNG" style="height:30px;width:150px;padding-top:3px;margin-left:20px;" alt="PATSADA.com"><br>
        <span style="color:#fff;margin-left: 21px;font-size:12px;">Jobs, Right at Your Finger Tips</span>
        </a>

        </td>
        </tr>';

                $msqBody = '
        <tr>
            <td style="padding-top:34px;padding-left:39px;padding-right:39px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd">
                <h2 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:30px;color:#262626;font-weight:normal;margin-top:0;margin-bottom:13px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;letter-spacing:0">
                Hi '.$user.'!
                </h2>

                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                Thanks for signing up to PATSADA.com. Please verify your account by clicking the link below.
                </h3>
            </td>
        </tr>
        <tr>
            <td align="left" style="background-color:#dea803;font-size:14px;color:#1f1f1f;border-top-width:1px;border-top-style:solid;border-top-color:#dae3ea;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#dae3ea;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:20px;padding-bottom:20px;padding-right:39px;padding-left:39px;text-align:left">
                <a style="color:#fff;font-family:Arial;font-size:20px" href="'.base_url().'employer/verify/'.$params.'" target="_blank">Verify my account and login</a></strong>
            </td>
        </tr>';

                $msqFooter = '
        <tr>
            <td style="color:#797c80;font-size:12px;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;padding-top:23px;padding-left:39px;padding-right:13px;padding-bottom:23px;text-align:left">
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                About PATSADA.com
                </h3>
                <p style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:13px;padding-right:0;padding-left:0;line-height:20px">
                The word PaTsada is a local word from Cagayan de Oro City.<br>
                        Hence <b>PaTsada.com</b> is an online Job Search Site for kagay-anons. <br>
                        We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                        <b>PATSADA.com</b>... Jobs, Right at Your Fingertips

                </p>

            </td>
        </tr>

        <tr>
    </tr>
    <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;text-align: right;font-size: 10px;">
            &copy; Copyright 2013. PATSADA.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359
        </td>
        </tr>
    </tbody>
    </table>';



                $msgWhole = $msqHeader.$msqBody.$msqFooter;

                $subject = '[PaTSADA] Confirm Your Employer Account';
                //echo "../_documents/".$results[0]['filename'];
                $mail = new PHP_Mailer();

                $mail->AddReplyTo("noreply@patsada.com","PaTSADA");

                $mail->SetFrom("noreply@patsada.com","PaTSADA");
                $mail->AddAddress($to, $user);
                $mail->Subject   = $subject;
                $mail->MsgHTML($msgWhole);
                //$mail->AddAttachment("../_documents/".$results[0]['filename']);

                if($_SERVER['SERVER_NAME'] =='localhost'){
                    $file = 'mailHtml/mailHtml.html';

                    // Write the contents back to the file
                    file_put_contents($file, $msgWhole);

                }else{
                    if($mail->Send()){
                        $response = array ( "success" => true);
                    }else{
                        echo "Mailer Error: " . $mail->ErrorInfo;
                        $response = array ( "failure" => true);
                    }
                }



                break;
            }
        }

    }


    function autoMail($message,$to,$user){
                $data['msg'] = $message;
                $msqHeader = $this->load->view('mail/mailContent',$data,true);
                $msgWhole = $msqHeader; 
                $subject = '[PaTSADA] Authentication Code';
                //echo "../_documents/".$results[0]['filename'];
                $mail = new PHP_Mailer();

                $mail->AddReplyTo("noreply@patsada.com","PaTSADA");

                $mail->SetFrom("noreply@patsada.com","PaTSADA");
                $mail->AddAddress($to, $user);
                $mail->Subject   = $subject;
                $mail->MsgHTML($msgWhole);
                $mail->AddAttachment(".../mailHtml/JPTEMPLATE_2.pdf");

                if($_SERVER['SERVER_NAME'] =='localhost'){
                    $file = 'mailHtml/mailHtml.html';

                    // Write the contents back to the file
                    file_put_contents($file, $msgWhole);

                }else{
                    if($mail->Send()){
                        $response = array ( "success" => true);
                    }else{
                        echo "Mailer Error: " . $mail->ErrorInfo;
                        $response = array ( "failure" => true);
                    }
                }
    }


    function employersercretcode($code){
               
                 $msqHeader = '<table align="center" cellspacing="0" border="0" cellpadding="0" width="700" bgcolor="#FFFFFF" style="width:700px;background-color:#fff;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
        <tbody>
        <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;">
        <a href="'.base_url().'" target="_blank" style="text-decoration:none;">
        <img src="'.base_url().'assets/img/patsada_logo.PNG" style="height:30px;width:150px;padding-top:3px;margin-left:20px;" alt="PATSADA.com"><br>
        <span style="color:#fff;margin-left: 21px;font-size:12px;">Jobs, Right at Your Finger Tips</span>
        </a>

        </td>
        </tr>';

                $msqBody = '
        <tr>
            <td style="padding-top:34px;padding-left:39px;padding-right:39px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd">
                <h2 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:30px;color:#262626;font-weight:normal;margin-top:0;margin-bottom:13px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;letter-spacing:0">
                Hi '.$code['user'].'!
                </h2>

                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                Thanks for signing up to PATSADA.com. Please remember your own authentication code and company code. This will serve as your code as subject to your email for job posting:
                <br>
                <b> Authentication Code: </b>' .$code['code']. '<br/>
                <b> Company Code: </b>'.$code['company_code'].'<br/>
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                    You can also use our e-mailing feature wherein you can post, edit, and delete your job postings via e-mail. 
                    <br><br>
                     <b>Please make sure that you follow the correct subject and body template for your emails so that it can be read properly thru our system.<br>
                     Should there be any cases that you have not received any notification, please check your spam mail. Thank you.</b>
                     <br><br>
                     INSTRUCTIONS:
                 

                    <br><br>
                    <b>Job Posting:</b><br>
                    Email to: postjob@patsada.com<br>
                    Subject: (authentication code) (company code)<br>
                    Body:<br>
                    Post Type: (Choose between Urgent or Non-Urgent)<br>
                    Job Type: (Either Full-Time or Part-Time)<br>
                    Position: (Name of Position currently hiring)<br>
                    Description: (Brief description of the job hiring)<br>
                    Years of Experience: (Must be numeric)<br>
                    Category: (Categories are shown below)<br>
                    Location: (Location of job hiring)<br>
                    Salary Type: R, N, T, or A<br>
                    Salary From: (Minimum salary offer. Must be numeric with no special characters)<br>
                    Salary To: (Maximum salary offer. Must be numeric with no special characters)<br>
                    <br>
                    <b>Example:</b><br>
                    Post Type: Urgent<br>
                    Job Type: Part-Time<br>
                    Position: Back-end Developer<br>
                    Description: Must have knowledge in PHP and Python programming language.<br>
                    Years of Experience: 2<br>
                    Category: Software<br>
                    Location: Manila<br>
                    Salary Type: N<br>
                    Salary From: 20000<br>
                    Salary To: 25000<br>
                    <br><br>

                    <b>Job Deletion</b><br>
                    Email to: deletejob@patsada.com<br>
                    Subject: (authentication code) (company code)<br>
                    Body:<br>
                    Delete Job Number: (Job Number of Job Post)<br>
                    <br><br>

                    <b>Job Edit:</b><br>
                    Email to: editjob@patsada.com<br>
                    Subject: (authentication code) (company code)<br>
                    Body:<br>
                    Edit Job Number: (Job Number of Job Post)<br>
                    Post Type: (Urgent or Non-Urgent)<br>
                    Job Type: (Part-Time or Full Time)<br>
                    Category: (Categories are given below)<br>
                    Position: (Name of Position currently hiring)<br>
                    Location: (Location of job hiring)<br>
                    Years of Experience: (Must be numeric)<br>
                    Description: (Brief description of the job hiring)<br>
                    Salary Type: (R, N, T, A)<br>
                    Salary From: (Minimum salary offer. Must be numeric with no special characters)<br>
                    Salary To: (Maximum salary offer. Must be numeric with no special characters)<br><br>

                    <b>Note: It is not necessary that you must fill-out all the fields. Only the one that needs to be edited as long as there is the job post number stated</b><br>
                    <br>
                    <b>Example:</b><br>
                    Job Post Number: 12<br>
                    Position: IT Developer<br>

                    <br><br>

                    <b>Get Applicants Information:</b><br>
                    Email to: getapplicants@patsada.com<br>
                    Subject: (authentication code) (company code)<br>
                    Body:<br>
                    Get Applicants Job Number: (Job Number of Job Post)<br>
                    <br><br>


                    <b>CATEGORIES:</b>
                    <br>
                    <b>1.Accounting/Finance</b><br>
                        2. General/Cost Acct<br>
                        3. Banking/Financial<br>
                        4. Audit/Taxation<br>
                        5. Finance/Investment<br>
                    <b>6. Admin/HR</b><br>
                        7. Clerical/Administrative<br>
                        8. Human Resources<br>
                        9. Secretarial<br>
                        10. Top Management<br>
                    <b>11. Computer/IT</b><br>
                        12. Software<br>
                        13. Hardware<br>
                        14. Secretarial<br>
                    <b>15. Engineering</b><br>
                        16. Mechanical/Automotive<br>
                        17. Other Eng<br>
                        18. Electrical Eng<br>
                        19. Environmental<br>
                        20. Oil/Gas Eng<br>
                        21. Industrial Eng<br>
                        22. Chemical Eng<br>
                    <b>23. Sales/Marketing</b><br>
                        24. Sales - Financial Services<br>
                        25. Marketing<br>
                        26. Retail Sales<br>
                        27. Telesales/Telemarketing<br>
                        28. Sales - Corporate<br>
                        29. Sales - Eng/Tech/IT<br>
                        30. Merchandising<br>
                    <b>31. Manufacturing</b><br>
                        32. Manufacturing<br>
                        33. Maintenance<br>
                        34. Quality Assurance<br>
                        35. Purchasing<br>
                        36. Process Design & Control<br>
                    <b>37. Arts/Media/Comm</b><br>
                        38. Arts/Creative Design<br>
                        39. Advertising<br>
                        40. Public Relations<br>
                        41. Entertainment<br>
                    <b>42. Building/Construction</b><br>
                        43. Civil Eng/Construction<br>
                        44. Architect/Interior<br>
                        45. Property/Real Estate<br>
                        46. Quantity Survey<br>
                    <b>47. Service</b><br>
                        48. Customer Service<br>
                        49. Tech & Helpdesk Support<br>
                        50. Personal Care<br>
                        51. Logistics/Supply Chain<br>
                        52. Security/Armed Forces<br>
                        53. Law/Legal Services<br>
                        54. Social Services<br>
                    <b>55. Hotel/Restaurant</b><br>
                        56. F & B<br>
                        57. Hotel/Tourism<br>
                    <b>58. Sciences</b><br>
                        59. Food Tech/Nutritionist<br>
                        60. Agriculture & Dev.<br>
                        61. Chemistry<br>
                        62. Aviation<br>
                        63. Actuarial/Statistics<br>
                        64. Science & Tech<br>
                        65. Geology<br>
                        66. Biotechnology<br>
                    <b>67. Healthcare</b><br>
                        68. Nurse/Medical Support<br>
                        69. Doctor/Diagnosis<br>
                        70. Pharmacy<br>
                    <b>71. Others</b><br>
                        72. General Work<br>
                        73 Others<br>
                        74. Journalist/Editors<br>
                        75. Publishing<br> 
                    </h3>
            </td>
        </tr> ';

                $msqFooter = '
        <tr>
            <td style="color:#797c80;font-size:12px;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;padding-top:23px;padding-left:39px;padding-right:13px;padding-bottom:23px;text-align:left">
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                About PATSADA.com
                </h3>
                <p style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:13px;padding-right:0;padding-left:0;line-height:20px">
                The word PaTsada is a local word from Cagayan de Oro City.<br>
                        Hence <b>PaTsada.com</b> is an online Job Search Site for kagay-anons. <br>
                        We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                        <b>PATSADA.com</b>... Jobs, Right at Your Fingertips

                </p>

            </td>
        </tr>


        <tr>
    </tr>
    <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;text-align: right;font-size: 10px;">
            &copy; Copyright 2013. PATSADA.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359
        </td>
        </tr>
    </tbody>
    </table>';



                $msgWhole = $msqHeader.$msqBody.$msqFooter; 
                $subject = '[PaTSADA] Authentication Code, Company Code, and E-Mail Template [DO NOT DELETE]';
                //echo "../_documents/".$results[0]['filename'];
                $mail = new PHP_Mailer();

                $mail->AddReplyTo("noreply@patsada.com","PaTSADA");

                $mail->SetFrom("noreply@patsada.com","PaTSADA");
                $mail->AddAddress($code['to'], $code['user']);
                $mail->AddAttachment("mailHtml/JobPostTemplate.pdf");
                $mail->Subject   = $subject;
                $mail->MsgHTML($msgWhole);
                //$mail->AddAttachment("../_documents/".$results[0]['filename']);

                if($_SERVER['SERVER_NAME'] =='localhost'){
                    $file = 'mailHtml/mailHtml.html';

                    // Write the contents back to the file
                    file_put_contents($file, $msgWhole);

                }else{
                    if($mail->Send()){
                        $response = array ( "success" => true);
                    }else{
                        echo "Mailer Error: " . $mail->ErrorInfo;
                        $response = array ( "failure" => true);
                    }
                }
    }

    function get_validate_company_duplication($comp){
        $sql="SELECT count(*) as count FROM employer where company_name = '$comp' Limit 1;";
        $query = $this->db->query($sql);
        $res = $query->result();
        $status = false;
        if($res[0]->count > 0){
            $status = true;
        }
        return $status;
    }
    function get_validate_user_name_duplication($user_name,$isCandidate, $isEmployer){
        $sql="SELECT count(*) as count FROM login_credentials where user_name = '$user_name' and is_candidate = '$isCandidate' and is_employer = '$isEmployer' Limit 1;";
        $query = $this->db->query($sql);
        $res = $query->result();
        $status = false;
        if($res[0]->count > 0){
            $status = true;
        }
        return $status;

    }
    function login($post_data){

        if (isset($post_data['password'])) {
            $sql_login_credentials ="SELECT count(*) as count , is_candidate, is_employer,employer_user_id,candidate_id from login_credentials where user_name = '".$post_data['user_name']."' and `password` = '".md5($post_data['password'])."' and is_active = 'Y' and is_confirmed='Y' and is_admin ='N';";

        }else{
            $sql_login_credentials ="SELECT count(*) as count , is_candidate, is_employer,employer_user_id,candidate_id from login_credentials where user_name = '".$post_data['user_name']."'  and is_active = 'Y' and is_confirmed='Y' and is_admin ='N';";

        }

        

        $query = $this->db->query($sql_login_credentials);
        $res = $query->result();
        if($res[0]->count > 0){
            $is_candidate = 'Y';
            if($res[0]->is_candidate == '' || $res[0]->is_candidate == null|| $res[0]->is_candidate == 'N'){
                $is_candidate = 'N';
            }
            $is_employer = 'Y';
            if($res[0]->is_employer == ''  || $res[0]->is_employer == null || $res[0]->is_employer == 'N'){
                $is_employer = 'N';
            }
            return array(true,$is_candidate, $is_employer ,$res[0]->employer_user_id,$res[0]->candidate_id);
        }
        return array(false,'', '','','' );
    }


    function update_viewedapplicant($post_data, $isId = false){
        $this->db->where('job_post_id',$post_data['job_post_id']);
        $this->db->update('job_post_event',array('is_viewed'=>'Y'));
    }


    function saveDatas($post_data,$isId = false){
        $this->db->trans_begin(); 
        if($post_data[$post_data['tbl_name'].'_id'] == 0){

            $sql = 'INSERT INTO '.$post_data['tbl_name'].' ( ';
            $setSQLCol = "";
            $setSQLVal = "";
            foreach($post_data as $key => $value){
                if($key != 'tbl_name' && $key != $post_data['tbl_name'].'_id'){
                    $setSQLCol .= "`".$key."`,";
                    $setSQLVal .= "'".$value."',";
                }
            }
            $setCol = substr($setSQLCol, 0, -1);
            $setVal = substr($setSQLVal, 0, -1);

            $sql .=$setCol.') values ( ';
            $sql .=$setVal.' );'; 

        }else{
            $sql = 'UPDATE '.$post_data['tbl_name'].' set ';
            $setSQL = "";
            foreach($post_data as $key => $value){
                if($key != 'tbl_name' && $key != $post_data['tbl_name'].'_id' && $key != 'dd' && $key != 'mm' && $key != 'yy'){
                    $setSQL .=' '.$key.' = \''.$value.'\' ,';
                }

            }
            $set = substr($setSQL, 0, -1);
            $sql .=$set.' where '.$post_data['tbl_name'].'_id ='.$post_data[$post_data['tbl_name'].'_id'] ;



        }


        $this->db->query($sql);
        $status = false;
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $insert_id2 = $this->db->insert_id();
            $this->db->trans_commit();

            if($isId){
                $status = $insert_id2;
            }else{
                $status = true;
            }

        }
        return $status;
    }

    function deleteDatas($post_data){
        $this->db->trans_begin();
        $sql = "DELETE FROM ".$post_data['tbl_name']." WHERE ".$post_data['tbl_name'].'_id = '.$post_data[$post_data['tbl_name'].'_id'];



        $this->db->query($sql);
        $status = false;
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();

            $status = true;

        }
        return $status;
    }

    function search_record_premium($searchterm){
        $filter ="";
        if($searchterm !=''){
            $filter =" and jp.position LIKE '%".$searchterm."%' or emp.company_name like '%$searchterm%'";
        }

        $sql = "SELECT jp.*,emp.* FROM job_post as jp LEFT JOIN employer as emp on emp.employer_id = jp.employer_id
                    WHERE jp.is_confirm ='Y' and jp.is_premium = 'Y' and datediff(CURDATE(),jp.created_date_time ) <= 30  $filter  ORDER BY  jp.created_date_time DESC" ;

        $query=$this->db->query($sql);

        if($query -> num_rows() > 0)
        {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        else
        {
            return array();
        }
    }
    function search_record_by_category_premium($searchterm,$id){
        $filter ="";
        if($searchterm !=''){
            $filter =" and jp.position LIKE '%".$searchterm."%' or emp.company_name like '%$searchterm%'";
        }

        $sql = "SELECT jp.*,emp.*,cat.* FROM job_post as jp LEFT JOIN employer as emp on emp.employer_id = jp.employer_id LEFT JOIN category as cat on cat.category_id = jp.category_id
                    WHERE jp.is_confirm ='Y' and jp.category_id = '$id' and jp.is_premium = 'Y' and datediff(CURDATE(),jp.created_date_time ) <= 30  $filter  ORDER BY  jp.created_date_time DESC" ;

        $query=$this->db->query($sql);

        if($query -> num_rows() > 0)
        {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        else
        {
            return array();
        }
    }
    function search_record_by_category($searchterm,$id){
        $filter ="";
        if($searchterm !=''){
            $filter =" and jp.position LIKE '%".$searchterm."%' or emp.company_name like '%$searchterm%'";
        }

        $sql = "SELECT jp.*,emp.*,cat.* FROM job_post as jp LEFT JOIN employer as emp on emp.employer_id = jp.employer_id LEFT JOIN category as cat on cat.category_id = jp.category_id
                    WHERE jp.is_confirm ='Y' and jp.category_id = '$id' and jp.is_premium = 'N' and datediff(CURDATE(),jp.created_date_time ) <= 30  $filter  ORDER BY  jp.created_date_time DESC" ;

        $query=$this->db->query($sql);

        if($query -> num_rows() > 0)
        {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        else
        {
            return array();
        }
    }

    function search_record($searchterm){
        $filter ="";
        if($searchterm !=''){
            $filter =" and jp.position LIKE '%".$searchterm."%' or emp.company_name like '%$searchterm%'";
        }

        $sql = "SELECT jp.*,emp.* FROM job_post as jp LEFT JOIN employer as emp on emp.employer_id = jp.employer_id
                    WHERE jp.is_confirm ='Y' and jp.is_premium = 'N' and datediff(CURDATE(),jp.created_date_time ) <= 30  $filter  ORDER BY  jp.created_date_time DESC" ;

        $query=$this->db->query($sql);

        if($query -> num_rows() > 0)
        {

            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        else
        {
            return array();
        }
    }

    function searchterm_handler($searchterm)
    {
        $this->session->unset_userdata('searchterm');
        if($searchterm)
        {
            $this->session->set_userdata('searchterm', $searchterm);
            return $searchterm;
        }
        elseif($this->session->userdata('searchterm'))
        {
            $searchterm = $this->session->userdata('searchterm');
            return $searchterm;
        }
        else
        {
            $searchterm ="";
            return $searchterm;
        }
    }


    function verify_apply($id,$email){
        $sql="SELECT count(*) as count FROM job_post_event where job_post_id = '$id' and email = '$email' Limit 1;";
        $query = $this->db->query($sql);
        $res = $query->result();
        $status = false;
        if($res[0]->count > 0){
            $status = true;
        }
        return $status;
    }

    function get_job_applicants($id){
        echo $sql = "SELECT jpe.title, jpe.first_name, jpe.last_name, jpe.email, jpe.contact_no, is_viewed, is_accepted, is_declined, emp.company_name, emp_user.email AS employer_email
        FROM job_post_event as jpe 
        INNER JOIN job_post as jp ON jpe.job_post_id = jp.job_post_id 
        INNER JOIN employer as emp ON emp.employer_id = jp.employer_id
        INNER JOIN employer_user as emp_user ON emp.employer_id = emp_user.employer_id 
        WHERE jp.job_post_id= '$id';";

        $query = $this->db->query($sql);

        return $query->result();
    }


    function get_job_posted($id){
         $sql="SELECT jp.*,emp.*,cat.category_id, cat.name, eu.email FROM job_post as jp  
            LEFT JOIN employer as emp on emp.employer_id = jp.employer_id 
            LEFT JOIN employer_user as eu on emp.employer_id = eu.employer_id
            LEFT JOIN category as cat on cat.category_id = jp.category_id
            WHERE jp.job_post_id = $id";

        $query = $this->db->query($sql);

        return $query->result();
    }


    function get_employers(){
        $sql='SELECT e.employer_id, e.company_name, eu.email from employer as e
        LEFT JOIN employer_user as eu
        ON e.employer_id = eu.employer_id';

        $query = $this->db->query($sql);

        return $query->result();

    }

    function get_employers_info($email){
        $sql="SELECT lc.user_name from employer as e
        LEFT JOIN employer_user as eu ON e.employer_id = eu.employer_id
        LEFT JOIN login_credentials as lc ON eu.employer_user_id = lc.employer_user_id
        WHERE eu.email = '".$email."' ";

        $query = $this->db->query($sql);

        return $query->result();

    }


    function count_applicants($id){
        $sql='SELECT job_post_id, COUNT(job_post_event_id) as applicants from job_post_event where job_post_id = '.$id.'';

        $query = $this->db->query($sql);

        return $query->result();
    }

    function get_jobpost($id){
        $sql='SELECT job_post_id, created_date_time from job_post where employer_id = '.$id.'';

        $query = $this->db->query($sql);

        return $query->result();

    }



    

    function get_related_jobs($id,$job_id){

        $filter="and jp.job_post_id != '".$job_id."'";
        $sql="SELECT jp.job_post_id,jp.position,jp.category_id,jp.is_premium,jp.created_date_time,jp.location,jp.jobtype,emp.employer_id,emp.company_name,emp.company_address FROM job_post as jp  LEFT JOIN employer as emp on emp.employer_id = jp.employer_id
                    WHERE jp.category_id = $id $filter and datediff(CURDATE(),jp.created_date_time ) <= 30   ORDER BY  jp.created_date_time, RAND() DESC LIMIT 10" ;


        $query = $this->db->query($sql);

        return $query->result();
    }
    function verify_candidate_application($id,$candidate_id){
        $sql="SELECT count(*) as count FROM job_post_event where job_post_id = '$id' and candidate_id = '$candidate_id' Limit 1;";
        $query = $this->db->query($sql);
        $res = $query->result();
        $status = false;
        if($res[0]->count > 0){
            $status = true;
        }
        return $status;
    }

    function get_candidate_apply($id){
        $sql="SELECT jp.*,emp.*,jp_event.* FROM job_post as jp  LEFT JOIN employer as emp on emp.employer_id = jp.employer_id LEFT JOIN job_post_event as jp_event on jp_event.job_post_id = jp.job_post_id
                    WHERE jp_event.candidate_id = $id;";

        $query = $this->db->query($sql);

        return $query->result();
    }

    function forgot_pass_word($post_data){

        if($post_data['user_type']=='is_candidate'){

            $sql="SELECT * FROM candidate where email = '".$post_data['email']."';";
            $query = $this->db->query($sql);

            $res= $query->result();
            if(!empty($res)){
                foreach ($query->result() as $row) {
                    $data[] = $row;
                }

                $sql2="SELECT * FROM login_credentials where candidate_id = '".$data[0]->candidate_id."' LIMIT 1;";
                $query2 = $this->db->query($sql2);

                foreach ($query2->result() as $row2) {
                    $data2[] = $row2;
                }

                $user =$data2[0]->user_name;
                $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
                $pass_new= $s;
                $pass_change=md5($pass_new);
                $sql3="UPDATE  login_credentials SET password='$pass_change' where candidate_id = '".$data[0]->candidate_id."';";
                $this->db->query($sql3);
                $newPassword=$pass_new;
                $to='candidate';
                $status=true;
            }else{
                $status = false;
            }
        }else{
            $sql="SELECT * FROM employer_user where email = '".$post_data['email']."';";
            $query = $this->db->query($sql);

            $res= $query->result();
            if(!empty($res)){
                foreach ($query->result() as $row) {
                    $data[] = $row;
                }
                $sql2="SELECT * FROM login_credentials where employer_user_id = '".$data[0]->employer_user_id."' LIMIT 1;";
                $query2 = $this->db->query($sql2);
                $res2= $query2->result();
                foreach ($query2->result() as $row2) {
                    $data2[] = $row2;
                }
                $user =$data2[0]->user_name;

                $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
                $pass_new= $s;
                $pass_change=md5($pass_new);
                $sql3="UPDATE  login_credentials SET password='$pass_change' where employer_user_id = '".$data[0]->employer_user_id."';";
                $this->db->query($sql3);
                $newPassword=$pass_new;
                $status=true;
                $to='employer';
            }else{
                $status = false;
            }

        }

        $msqHeader = '<table align="center" cellspacing="0" border="0" cellpadding="0" width="700" bgcolor="#FFFFFF" style="width:700px;background-color:#fff;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
                <tbody>
                <tr>
                <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;">
                <a href="'.base_url().'" target="_blank" style="text-decoration:none;">
                <img src="'.base_url().'assets/img/patsada_logo.PNG" style="height:30px;width:150px;padding-top:3px;margin-left:20px;" alt="PATSADA.com"><br>
                <span style="color:#fff;margin-left: 21px;font-size:12px;">Jobs, Right at Your Finger Tips</span>
                </a>

                </td>
                </tr>';

        $msqBody = '
                <tr>
                    <td style="padding-top:34px;padding-left:39px;padding-right:39px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd">
                        <h2 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:30px;color:#262626;font-weight:normal;margin-top:0;margin-bottom:13px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;letter-spacing:0">
                        Hi '.$user.'!
                        </h2>

                        <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                       Your Account has been changed. Thanks for using PATSADA.com.
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="background-color:#dea803;font-size:14px;color:#1f1f1f;border-top-width:1px;border-top-style:solid;border-top-color:#dae3ea;border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#dae3ea;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:20px;padding-bottom:20px;padding-right:39px;padding-left:39px;text-align:left">
                        Username: '.$user.' <br>
                        New Password: '.$newPassword.'
                    </td>
                </tr>';

        $msqFooter = '
                <tr>
                    <td style="color:#797c80;font-size:12px;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;padding-top:23px;padding-left:39px;padding-right:13px;padding-bottom:23px;text-align:left">
                        <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                        About PATSADA.com
                        </h3>
                        <p style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:13px;padding-right:0;padding-left:0;line-height:20px">
                        The word PaTsada is a local word from Cagayan de Oro City.<br>
                                Hence <b>PATSADA.com</b> is an online Job Search Site for kagay-anons. <br>
                                We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                                <b>PATSADA.com</b>... Jobs, Right at Your Fingertips

                        </p>

                    </td>
                </tr>

                <tr>
            </tr>
            <tr>
                <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;text-align: right;font-size: 10px;">
                    &copy; Copyright 2013. PATSADA.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359
                </td>
                </tr>
            </tbody>
            </table>';



        $msgWhole = $msqHeader.$msqBody.$msqFooter;

        $subject = '[PATSADA] Retriving User Account';
        //echo "../_documents/".$results[0]['filename'];
        $mail = new PHP_Mailer();

        $mail->AddReplyTo("noreply@patsada.com","PATSADA");

        $mail->SetFrom("noreply@patsada.com","PATSADA");
        $mail->AddAddress($to, $user);
        $mail->Subject   = $subject;
        $mail->MsgHTML($msgWhole);
        //$mail->AddAttachment("../_documents/".$results[0]['filename']);

        if($_SERVER['SERVER_NAME'] =='localhost'){
            $file = 'mailHtml/mailHtml.html';

            // Write the contents back to the file
            file_put_contents($file, $msgWhole);

        }else{
            if($mail->Send()){
                $response = array ( "success" => true);
            }else{
                echo "Mailer Error: " . $mail->ErrorInfo;
                $response = array ( "failure" => true);
            }
        }

        return $status;
    }

    function mail_getjob_applicants($msg, $to, $user, $jobpost_id){

                $basemail= base64_encode($to);
                
               
                 $msqHeader = '<table align="center" cellspacing="0" border="0" cellpadding="0" width="700" bgcolor="#FFFFFF" style="width:700px;background-color:#fff;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
        <tbody>
        <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;">
        <a href="'.base_url().'" target="_blank" style="text-decoration:none;">
        <img src="'.base_url().'assets/img/patsada_logo.PNG" style="height:30px;width:150px;padding-top:3px;margin-left:20px;" alt="PATSADA.com"><br>
        <span style="color:#fff;margin-left: 21px;font-size:12px;">Jobs, Right at Your Finger Tips</span>
        </a>

        </td>
        </tr>';

                $msqBody = '
        <tr>
            <td style="padding-top:34px;padding-left:39px;padding-right:39px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd">
                <h2 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:30px;color:#262626;font-weight:normal;margin-top:0;margin-bottom:13px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;letter-spacing:0">
                </h2>

            
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">                 
                    Dear '.$user.',
                    <br><br>
                    Below are the applicants for job number '.$jobpost_id.'
                    <br><br>
                    <center>
                    '.$msg.'
                    <br><br>
                    If you would like to see more info, you can access it <a href="http://www.patsada.com/employer/flags/'.$basemail.'/'.$jobpost_id.'">here.</a>
                    <br>Thank you!
                </h3>

                    <br>'
                    ;

                $msqFooter = '
        <tr>
            <td style="color:#797c80;font-size:12px;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;padding-top:23px;padding-left:39px;padding-right:13px;padding-bottom:23px;text-align:left">
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                About PATSADA.com
                </h3>
                <p style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:13px;padding-right:0;padding-left:0;line-height:20px">
                The word PaTsada is a local word from Cagayan de Oro City.<br>
                        Hence <b>PaTsada.com</b> is an online Job Search Site for kagay-anons. <br>
                        We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                        <b>PATSADA.com</b>... Jobs, Right at Your Fingertips

                </p>

            </td>
        </tr>


        <tr>
    </tr>
    <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;text-align: right;font-size: 10px;">
            &copy; Copyright 2013. PATSADA.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359
        </td>
        </tr>
    </tbody>
    </table>';



                $msgWhole = $msqHeader.$msqBody.$msqFooter; 
                $subject = '[PaTSADA] Applicants for Job Number '.$jobpost_id.'';
                //echo "../_documents/".$results[0]['filename'];
                $mail = new PHP_Mailer();

                $mail->AddReplyTo("noreply@patsada.com","PaTSADA");

                $mail->SetFrom("noreply@patsada.com","PaTSADA");
                $mail->AddAddress($to, $user);
                $mail->Subject   = $subject;
                $mail->MsgHTML($msgWhole);
                //$mail->AddAttachment("../_documents/".$results[0]['filename']);

                if($_SERVER['SERVER_NAME'] =='localhost'){
                    $file = 'mailHtml/mailHtml.html';

                    // Write the contents back to the file
                    file_put_contents($file, $msgWhole);

                }else{
                    if($mail->Send()){
                        $response = array ( "success" => true);
                    }else{
                        echo "Mailer Error: " . $mail->ErrorInfo;
                        $response = array ( "failure" => true);
                    }
                }
    }


     function mail_edit_jobpost($to, $user, $jobpost_id){


               $basemail= base64_encode($to);

                 $msqHeader = '<table align="center" cellspacing="0" border="0" cellpadding="0" width="700" bgcolor="#FFFFFF" style="width:700px;background-color:#fff;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
        <tbody>
        <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;">
        <a href="'.base_url().'" target="_blank" style="text-decoration:none;">
        <img src="'.base_url().'assets/img/patsada_logo.PNG" style="height:30px;width:150px;padding-top:3px;margin-left:20px;" alt="PATSADA.com"><br>
        <span style="color:#fff;margin-left: 21px;font-size:12px;">Jobs, Right at Your Finger Tips</span>
        </a>

        </td>
        </tr>';

                $msqBody = '
        <tr>
            <td style="padding-top:34px;padding-left:39px;padding-right:39px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd">
                <h2 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:30px;color:#262626;font-weight:normal;margin-top:0;margin-bottom:13px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;letter-spacing:0">
                </h2>

            
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">                 
                    Dear '.$user.',
                    <br><br>
                    Congratulations! Job Number '.$jobpost_id.' has been successfully edited<br>
                    If you wish to view this, you can access it <a href="http://www.patsada.com/employer/flags/'.$basemail.'/'.$jobpost_id.'">here.</a>
                    <br>Thank you!
                    
                    <br><br>
                    
                </h3>

                    <br>'
                    ;

                $msqFooter = '
        <tr>
            <td style="color:#797c80;font-size:12px;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;padding-top:23px;padding-left:39px;padding-right:13px;padding-bottom:23px;text-align:left">
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                About PATSADA.com
                </h3>
                <p style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:13px;padding-right:0;padding-left:0;line-height:20px">
                The word PaTsada is a local word from Cagayan de Oro City.<br>
                        Hence <b>PaTsada.com</b> is an online Job Search Site for kagay-anons. <br>
                        We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                        <b>PATSADA.com</b>... Jobs, Right at Your Fingertips

                </p>

            </td>
        </tr>


        <tr>
    </tr>
    <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;text-align: right;font-size: 10px;">
            &copy; Copyright 2013. PATSADA.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359
        </td>
        </tr>
    </tbody>
    </table>';



                $msgWhole = $msqHeader.$msqBody.$msqFooter; 
                $subject = '[PaTSADA] Job Number '.$jobpost_id.' has successfully been edited';
                //echo "../_documents/".$results[0]['filename'];
                $mail = new PHP_Mailer();

                $mail->AddReplyTo("noreply@patsada.com","PaTSADA");

                $mail->SetFrom("noreply@patsada.com","PaTSADA");
                $mail->AddAddress($to, $user);
                $mail->Subject   = $subject;
                $mail->MsgHTML($msgWhole);
                //$mail->AddAttachment("../_documents/".$results[0]['filename']);

                if($_SERVER['SERVER_NAME'] =='localhost'){
                    $file = 'mailHtml/mailHtml.html';

                    // Write the contents back to the file
                    file_put_contents($file, $msgWhole);

                }else{
                    if($mail->Send()){
                        $response = array ( "success" => true);
                    }else{
                        echo "Mailer Error: " . $mail->ErrorInfo;
                        $response = array ( "failure" => true);
                    }
                }
    }


     function notify_newapplicants($to, $user, $jobpost_id, $applicants){


               $basemail= base64_encode($to);

                 $msqHeader = '<table align="center" cellspacing="0" border="0" cellpadding="0" width="700" bgcolor="#FFFFFF" style="width:700px;background-color:#fff;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
        <tbody>
        <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;">
        <a href="'.base_url().'" target="_blank" style="text-decoration:none;">
        <img src="'.base_url().'assets/img/patsada_logo.PNG" style="height:30px;width:150px;padding-top:3px;margin-left:20px;" alt="PATSADA.com"><br>
        <span style="color:#fff;margin-left: 21px;font-size:12px;">Jobs, Right at Your Finger Tips</span>
        </a>

        </td>
        </tr>';



                $msqBody = '
        <tr>
            <td style="padding-top:34px;padding-left:39px;padding-right:39px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd">
                <h2 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:30px;color:#262626;font-weight:normal;margin-top:0;margin-bottom:13px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;letter-spacing:0">
                </h2>

            
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">                 
                    Dear '.$user.',
                    <br><br>
                    There are '.$applicants.' new applicant/s for Job Number '.$jobpost_id.'.<br>
                    If you wish to view them, you can access it thru the site or email at getapplicants@patsada.com with the proper format to be followed.
                    <br>
                    <a href="http://www.patsada.com/employer/flags/'.$basemail.'/'.$jobpost_id.'">Job Post Link</a>
                    <br>
                    <br>
                    <br>
                    Thank you!
                    <br><br>
                    
                </h3>

                    <br>'
                    ;

                $msqFooter = '
        <tr>
            <td style="color:#797c80;font-size:12px;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;padding-top:23px;padding-left:39px;padding-right:13px;padding-bottom:23px;text-align:left">
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                About PATSADA.com
                </h3>
                <p style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:13px;padding-right:0;padding-left:0;line-height:20px">
                The word PaTsada is a local word from Cagayan de Oro City.<br>
                        Hence <b>PaTsada.com</b> is an online Job Search Site for kagay-anons. <br>
                        We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                        <b>PATSADA.com</b>... Jobs, Right at Your Fingertips

                </p>

            </td>
        </tr>


        <tr>
    </tr>
    <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;text-align: right;font-size: 10px;">
            &copy; Copyright 2013. PATSADA.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359
        </td>
        </tr>
    </tbody>
    </table>';



                $msgWhole = $msqHeader.$msqBody.$msqFooter; 
                $subject = '[PaTSADA] You have '.$applicants.' new applicants for Job Number '.$jobpost_id.'';
                //echo "../_documents/".$results[0]['filename'];
                $mail = new PHP_Mailer();

                $mail->AddReplyTo("noreply@patsada.com","PaTSADA");

                $mail->SetFrom("noreply@patsada.com","PaTSADA");
                $mail->AddAddress($to, $user);
                $mail->Subject   = $subject;
                $mail->MsgHTML($msgWhole);
                //$mail->AddAttachment("../_documents/".$results[0]['filename']);

                if($_SERVER['SERVER_NAME'] =='localhost'){
                    $file = 'mailHtml/mailHtml.html';

                    // Write the contents back to the file
                    file_put_contents($file, $msgWhole);

                }else{
                    if($mail->Send()){
                        $response = array ( "success" => true);
                    }else{
                        echo "Mailer Error: " . $mail->ErrorInfo;
                        $response = array ( "failure" => true);
                    }
                }
    }

     function reply_error($to, $user){


               $basemail= base64_encode($to);

                 $msqHeader = '<table align="center" cellspacing="0" border="0" cellpadding="0" width="700" bgcolor="#FFFFFF" style="width:700px;background-color:#fff;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
        <tbody>
        <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;">
        <a href="'.base_url().'" target="_blank" style="text-decoration:none;">
        <img src="'.base_url().'assets/img/patsada_logo.PNG" style="height:30px;width:150px;padding-top:3px;margin-left:20px;" alt="PATSADA.com"><br>
        <span style="color:#fff;margin-left: 21px;font-size:12px;">Jobs, Right at Your Finger Tips</span>
        </a>

        </td>
        </tr>';



                $msqBody = '
        <tr>
            <td style="padding-top:34px;padding-left:39px;padding-right:39px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd">
                <h2 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:30px;color:#262626;font-weight:normal;margin-top:0;margin-bottom:13px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;letter-spacing:0">
                </h2>

            
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">                 
                    Hi there,
                    <br><br>
                    It seems like the format you sent is invalid. Please try again.
                    Thank you!
                    
                </h3>

                    <br>'
                    ;

                $msqFooter = '
        <tr>
            <td style="color:#797c80;font-size:12px;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;padding-top:23px;padding-left:39px;padding-right:13px;padding-bottom:23px;text-align:left">
                <h3 style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;font-size:16px;color:#3e434a;font-weight:normal;margin-top:0;margin-bottom:19px;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;line-height:25px">
                About PATSADA.com
                </h3>
                <p style="font-family:Helvetica Neue,Arial,Helvetica,sans-serif;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:13px;padding-right:0;padding-left:0;line-height:20px">
                The word PaTsada is a local word from Cagayan de Oro City.<br>
                        Hence <b>PaTsada.com</b> is an online Job Search Site for kagay-anons. <br>
                        We keep it simple and easy to used and provide a best expirience in Job Hunting and Recruiting.<br>
                        <b>PATSADA.com</b>... Jobs, Right at Your Fingertips

                </p>

            </td>
        </tr>


        <tr>
    </tr>
    <tr>
        <td style="padding:3px;text-align:left;border-left-width:1px;border-left-style:solid;border-left-color:#ddd;border-right-width:1px;border-right-style:solid;border-right-color:#ddd;background-color:#000;color:#fff;text-align: right;font-size: 10px;">
            &copy; Copyright 2013. PATSADA.com. All rights reserved. NHA, Kauswagan, Cagayan de Oro City, Philippines, 9000 Tel. (088) 323-4359
        </td>
        </tr>
    </tbody>
    </table>';



                $msgWhole = $msqHeader.$msqBody.$msqFooter; 
                $subject = '[PaTSADA] Invalid Format';
                $mail = new PHP_Mailer();

                $mail->AddReplyTo("noreply@patsada.com","PaTSADA");

                $mail->SetFrom("noreply@patsada.com","PaTSADA");
                $mail->AddAddress($to, $user);
                $mail->Subject   = $subject;
                $mail->MsgHTML($msgWhole);
                //$mail->AddAttachment("../_documents/".$results[0]['filename']);

                if($_SERVER['SERVER_NAME'] =='localhost'){
                    $file = 'mailHtml/mailHtml.html';

                    // Write the contents back to the file
                    file_put_contents($file, $msgWhole);

                }else{
                    if($mail->Send()){
                        $response = array ( "success" => true);
                    }else{
                        echo "Mailer Error: " . $mail->ErrorInfo;
                        $response = array ( "failure" => true);
                    }
                }
    }


}