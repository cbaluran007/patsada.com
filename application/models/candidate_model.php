<?php 

class Candidate_model extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
		$this->load->library(array('session'));
		$this->load->database();
        parent::__construct();
    }
    
    function get_candidate($id){

        $sql="SELECT * FROM candidate where candidate_id = $id;";

       $query = $this->db->query($sql);
		
		return $query->result();
        
    }
	 function save_candidate($post_data){

        $sql="UPDATE candidate set
		first_name = '".$post_data['first_name']."',
		last_name = '".$post_data['last_name']."',
		address = '".$post_data['address']."',
		telephone = '".$post_data['telephone']."',
		mobile = '".$post_data['mobile']."',
		birthdate = '".$post_data['birthdate']."',
		title = '".$post_data['title']."',
		skype_name = '".$post_data['skype_name']."',
		city = '".$post_data['city']."',
		province = '".$post_data['province']."',
		country = '".$post_data['country']."',
		facebook_url = '".$post_data['facebook_url']."',
		twitter_url = '".$post_data['twitter_url']."',
		g_plus_url = '".$post_data['g_plus_url']."',
		zip_code = '".$post_data['zip_code']."'
		where candidate_id = '".$post_data['candidate_id']."'
		
		";

       $res = $this->db->query($sql);
		if($res){
			return true;
		}else{
			return false;
		}
		
        
    }
	 function get_candidate_objective($id){

        $sql="SELECT * FROM candidate_objective where candidate_id = $id;";

      $query = $this->db->query($sql);
		
		return $query->result();
        
    }
	function save_candidate_objective($id_objective,$post_data){
		$status = false;
		if($id_objective == 0 or $id_objective == '0'){
			 $sql="INSERT INTO candidate_objective (objective, candidate_id) values (
			 '".$post_data['objective']."',
			 '".$post_data['candidate_id']."'
			 );";

			   $res =  $this->db->query($sql);
			 
		}else{
			$sql="UPDATE candidate_objective set
			objective = '".$post_data['first_name']."'
			where candidate_objective_id = '".$post_data['id_objective']."'
			";

			 $res = $this->db->query($sql);
			
		
        }
		if($res){
			return true;
		}else{
			return false;
		}
    }
	
	function get_candidate_education($id){

        $sql="SELECT * FROM candidate_education where candidate_id = $id;";

		$query = $this->db->query($sql);
		return $query->result();
        
    }
	function get_candidate_experience($id){

        $sql="SELECT * FROM candidate_experience where candidate_id = $id;";
		$query = $this->db->query($sql);
		
		return $query->result();
        
    }
	function get_candidate_skills($id){

        $sql="SELECT * FROM skills where candidate_id = $id;";
		$query = $this->db->query($sql);
		
		return $query->result();
        
    }
	function get_resume($id){

        $sql="SELECT * FROM resume where candidate_id = $id;";
		$query = $this->db->query($sql);

		return $query->result();

    }
	function get_candidate_invitations_all($id){

        $sql="SELECT * FROM candidate_invitations where candidate_id = $id;";
		$query = $this->db->query($sql);
		
		return $query->result();
        
    }
	function get_candidate_invitations_all_approved($id){
        $sql="SELECT * FROM candidate_invitations where candidate_id = $id and is_viewed = 'Y';";
		$query = $this->db->query($sql);
		return $query->result();
        
    }
	function get_validate_email_duplication($email){
        $sql="SELECT count(*) as count FROM candidate where email = '$email' Limit 1;";
		$query = $this->db->query($sql);
		$res = $query->result();
		$status = false;
		if($res[0]->count > 0){
			$status = true;
		}
		return $status;
        
    }
	
	
	
	function candidate_get_validated($param){
		$code_param = json_decode(base64_decode($param));
		
		$sql="SELECT count(*) as count FROM login_credentials where user_name = '".$code_param->user_name."' and `password` = '".$code_param->password."' and is_candidate = 'Y'  Limit 1;";
		$query = $this->db->query($sql);
		$res = $query->result();
		$status = false;
		if($res[0]->count > 0){
			$sqlUpdateCandidate = "UPDATE candidate set is_confirmed = 'Y' where email = '".$code_param->email."';";
			$this->db->query($sqlUpdateCandidate);
			$sqlUpdateCandidate = "UPDATE login_credentials set is_confirmed = 'Y' where user_name = '".$code_param->user_name."' and `password` = '".$code_param->password."' and is_candidate = 'Y';";
			$this->db->query($sqlUpdateCandidate);
			
			$sql_login_credentials ="SELECT count(*) as count ,  is_candidate, is_employer,employer_user_id,candidate_id from login_credentials where user_name = '".$code_param->user_name."' and `password` = '".$code_param->password."' and is_active = 'Y' and is_confirmed='Y' and is_admin ='N';";
			$query = $this->db->query($sql_login_credentials);
			$res = $query->result();
			
			if($res[0]->count > 0){
				$resLogs = array($res[0]->is_candidate, $res[0]->is_employer ,$res[0]->employer_user_id,$res[0]->candidate_id);
				
				if($resLogs[0]=='Y' && $resLogs[1] == 'N'){
					$status['data']=array('type'=>'candidate','id'=>$resLogs[3]);

					$sess_array = array(
						'id' => $resLogs[3],
						'roll' => 'candidate',
					);
					
				}
				
				

			}
			
			
           
			$status = true;
		}
		
			
		
        
		return array($status, $sess_array);
        
    }
}
