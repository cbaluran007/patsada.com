<?php 

class Category extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
		$this->load->database();
        parent::__construct();
    }
    
    function get_all_category(){

        $sqlParent="SELECT * FROM category c;";

        $query=$this->db->query($sqlParent);
		$dataParent = array();
		$dataChild = array();
		foreach($query->result() as $res){
			if($res->is_parent == 'Y'){
				array_push($dataParent, array($res->category_id,$res->name));
			}else{
				if(!array_key_exists($res->category_parent_id,$dataChild)){
					array_push($dataChild,array($res->category_parent_id => array($res->category_id,$res->name)));
				}else{
					array_push($dataChild[$res->category_parent_id], array($res->category_id,$res->name));
				}
			}
			
		
		}
		$results = array($dataParent,$dataChild);
		
		return $results;
        
    }
    function get_category_name($id){
        $sql="SELECT name,category_id FROM category WHERE category_id ='$id';";
        $query = $this->db->query($sql);

        return $query->result();
    }

}
