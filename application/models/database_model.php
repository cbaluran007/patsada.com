<?php 

class Database_model extends CI_Model {

   
    function __construct()
    {
        // Call the Model constructor
		$this->load->library(array('session'));
		$this->load->database();
        parent::__construct();
    }
    

    function checkComp($sender, $auth, $comp){
      $sql = "SELECT emp.authentication_code, emp.company_code, emp_user.email FROM employer AS emp 
			LEFT JOIN employer_user AS emp_user ON emp_user.employer_id = emp.employer_id WHERE emp_user.email 
			= '".$sender."' and  emp.authentication_code = '".$auth."' and emp.company_code = '".$comp."'";


	   $query=$this->db->query($sql);
        $status = false;
        if($query -> num_rows() > 0)
        {
            	$status = true;
        }	
       return $status;
    }
 
    function getCategory($category){ 
        $sql = "SELECT category_id FROM category where name = '".$category."'"; 
        $query=$this->db->query($sql);
          if($query -> num_rows() > 0) { 
             $results = $query->result_array(); 
             return $results[0]['category_id'];
        } else{ 
            return false;
        }
        
    } 

    function getEmployerId($comp_code){
 
        $sql = "SELECT employer_id FROM employer where company_code = '".$comp_code."'";
        $query=$this->db->query($sql);
         if($query -> num_rows() > 0) { 
            $results = $query->result_array();
             return $results[0]['employer_id'];
        } else{ 
            return false;
        }
    }
    
}
